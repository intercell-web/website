<?php
date_default_timezone_set('Asia/Calcutta');
$validateData =false;
  if((isset($_GET['uid'])) && (isset($_GET['sid'])))
  {
    if((!empty($_GET['uid'])) && (!empty($_GET['sid'])))
    {
        $validateData = true;
    }
  }
  if(!$validateData)
  {
      echo '<h1>Required information missing or invalid</h1>';
      exit;
  }


?><!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- start: HEAD -->

<head>
    <title>Intercell Video Session</title>
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Video Session Screen" name="description" />
    <meta content="InterCell" name="author" />
    <!-- end: META -->

    <!-- start: MAIN CSS -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../plugins/iCheck/skins/all.css">
    <link rel="stylesheet" href="../plugins/perfect-scrollbar/src/perfect-scrollbar.css">
    <link rel="stylesheet" href="../plugins/animate.css/animate.min.css">

    <link rel="stylesheet" href="../css/styles.css">
    <link rel="stylesheet" href="../css/styles-responsive.css">
    <link rel="stylesheet" href="../css/plugins.css">
    <link rel="stylesheet" href="../css/themes/theme-style8.css" type="text/css" id="skin_color">
    <link rel="stylesheet" href="../css/print.css" type="text/css" media="print" />
    <link rel="stylesheet" href="../css/mo_custom.css" type="text/css" />
    <link rel="stylesheet" href="../css/myStyle.css" type="text/css" />
    <link rel="stylesheet" href="../css/ps_Style.css" type="text/css" />

  <link rel="stylesheet" href="../css/animate.css">

    <style>
        .video-img .video-icon-list{ top:20px; right:12px;}

.video-bx-bg { position:relative; z-index:9; background:none !important;}
.video-icon-list a{ background:#EE3E34;}
.video-icon-list i{background:#EE3E34;}
.video-icon-list a::after{border-color:transparent transparent transparent #EE3E34;}

.video-session-blk{overflow: visible; margin-top:0px;height: 100vh !important; text-align:center; background-image: url('../images/video-start.jpg'); background-size:100%; background-repeat:no-repeat;background-size:cover;}
.video-bx-bg {  padding:0px;}
.video-sections img{ width:100%}
.video-img { margin: 8px; padding: 7px;  border: 1px solid #ddd; background:#ddd;}
.video_part2 { width: 220px; height: auto; position: absolute; z-index: 99; top: 26px;
   left: 26px; box-shadow:1px 0px 9px 1px #ddd;}
.Counsellor_dashboard { background: #fff; overflow: hidden;}
.cht-msg-bx.clearfix { margin-top: 50px;}
.video-sections img{ width:100%;}
.video_part2 img { width: 100%; height: auto;}

.video-list h4 span {margin-left: 13px;}
li.video-list { margin-top: 11px;}

.chat-bx-inner{ background:none; overflow:visible;height: 100%;}

#textchat {position: relative; width: 100%; float: left; right: 0; height: 100%; background: none; margin-top:40px;}
#history {width: 100%; height: calc(100% - 40px); overflow: auto; padding-top:15px;}
input#msgTxt { height: 40px;position: absolute; bottom: 0; width: 100%;}
#history .mine { color: #fff; text-align: right; margin-right: 10px;  padding: 8px 10px; text-align:left;
 background:green;
   border: 1px solid green;  border-radius: 32px;border-bottom-right-radius:4px;word-break: break-all;

}
#history .theirs {
    color: #fff;
    border: 1px solid #d9534f;
    padding: 8px 10px;
    display: inline-block;
    border-radius: 32px;word-break: break-all;

	background:#d9534f;
	border-bottom-left-radius:4px;
	float:left;      
}
.video-bx-bg{width:100%;}

#history div{width:100%; margin-bottom:5px; position:relative; overflow:hidden;}
#history .mine{float:right;}
.chat-historys {
    width: 100%;
    height: 500px !important;
}	
.video-screens{background-image: url('../images/video-img.png'); width:100%; min-height:100%;overflow: auto;	}
.video-session-blk h1 { width:100%; 
    font-size: 60px;
    text-align: center;
    font-weight: 700;
	position: absolute; color:#fff;
    top: 50%; transform:translateY(-50%);line-hight:40px;
	
font-family: unset;

}
.video-session-blk h1 small{font-size:55px; width:100%;text-align:center; float:left; color:seagreen;
font-family: unset;}     
  </style>
    <!-- end: CORE CSS -->
    <link rel="shortcut icon" href="favicon.png" />
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
    <div class="main-wrapper Counsellor_dashboard" >
        <!-- start: TOPBAR -->
        <header class="topbar navbar navbar-inverse navbar-fixed-top inner">
            <!-- start: TOPBAR CONTAINER -->
            <div class="container">
                <div class="navbar-header">
                    <!-- start: LOGO -->
                    <a class="navbar-brand" href="http://www.intercellworld.com/" target="_blank">
                        <img src="../images/logo.png" alt="InterCell Logo" />
                    </a>
                    <!-- end: LOGO -->
                </div>

                <div class="topbar-tools" id="session_timer" style="display:none;padding-top:0px;">
                    <!-- start: TOP NAVIGATION MENU -->
                     <ul class="nav navbar-right">
                        <!-- start: USER DROPDOWN -->


                        <li class="video-list">
                             <h4>Session Time left <span class="pull-right"> <strong id="timer"></strong></span></h4></span>


                        </li>

                    </ul>
                    <!-- end: TOP NAVIGATION MENU -->
                </div>
            </div>
            <!-- end: TOPBAR CONTAINER -->
        </header>
        <!-- end: TOPBAR -->


        <div id="messageBox" class="Container-fluid video-session-blk">
            <h1>Please wait we are connecting to Video Server...</h1>
        </div>


        <!-- start: MAIN CONTAINER -->
        <script type="text/javascript">
            var userData = '<?php echo json_encode($_GET)?>';
            var feedbackURL ='';
        </script>


        <?php
          if($_GET['type']=='Counsellor'){
            echo '<script type="text/javascript">feedbackURL="feedback_counsoller.php?uid='.
                            $_GET['uid'].'&sid='.$_GET['sid'].'";</script>';
          }else{
            echo '<script type="text/javascript">feedbackURL="feedback_student.php?uid='.
                            $_GET['uid'].'&sid='.$_GET['sid'].'";</script>';
          }
         ?>


        <script type="text/javascript" src="https://static.opentok.com/v2/js/opentok.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link href="http://fonts.googleapis.com/css?family=Ubuntu:500" rel="stylesheet" type="text/css">

        <!-- start: MAIN CONTAINER -->
        <div class="Container-fluid video-session-blk" id="videoScreen" style="display:none;">
		<div class="video-screens">
            <div class="col-md-9 col-sm-8 video-bx-bg">
                <div class="video-img" id="videos">
                    <div class="video-sections" id="subscriber" style="height: 580px; background-image: url('../images/subscriber.jpg');">
                       </div>
                    <div class="video_part2" id="publisher" style="height: 150px;background-image: url('../images/publisher.jpg');">


                </div>



                <div class="video-icon-list">
                    <ul class="list-unstyled text-right">
                        <!--<li onclick="endSession()"><a href="javascript:void(0);" class="">End Session</a> <i class="fa fa-stop" data-placement="top" data-original-title="End Session"></i> </li>
                       ---> <li class="chat-video-hide" onclick="showHideVideo()"><a href="javascript:void(0);" class="">Hide Video</a> <i class="fa fa-eye-slash"></i> </li>
	<li class="chat-icn"><a href="javascript:void(0);" class="">Chat</a> <i id="chat-ico" class="fa fa-comment-o" data-wow-delay="0.3s"></i> </li>
                        <!--<li><a href="javascript:void(0);" class="">Chat</a> <i class="fa fa-wechat"></i></li>-->
                    </ul>
                </div>

			</div>
            </div>
            <div class="col-md-3 col-sm-4 chat-bx">
				<div class="chat-bx-inner">
                   
                <!--<div class="col-xs-4 text-center">
                    <img src="assets/images/avatar-1.jpg" alt="" class="img-responsive">
                    <h5>Jhon Doe</h5>
                </div>
                <div class="col-xs-8">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                </div>-->
                <!-- Chat box start-->

                <div class="chat-field" id="textchat">
                  <p class="chat-historys" id="history"></p>
                    <form class="cht-frm">

                        <div class="clearfix"></div>
                        <div class="cht-msg-bx clearfix">
                            <div class="input-group">
                                <input type="text" class="form-control" id="msgTxt" placeholder="Type a message here"></input>
                                <div class="input-group-addon chat-btn send_message"><a href="javascript:void(0);" class="fa fa-send"></a> </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
                <!-- Chat box end-->
				</div>
            </div>
        </div>
        <!-- end: PAGE -->
        </div>
		</div>
        <!-- end: MAIN CONTAINER -->
    <div class="clearfix"></div>
        <!-- start: FOOTER -->
    <!--   <footer class="inner">
            <div class="footer-inner">
                <div class="pull-left">
                    <?php echo date('Y');?> &copy; InterCell by Digitigi.
                </div>
            </div>
        </footer>-->
        <!-- end: FOOTER -->
    </div>






<script type="text/javascript">
var VIDEO_SERVER_BASE_URL = 'https://www.intercellworld.com/icvideo/server/web';
var API_SERVER_BASE_URL = 'https://www.intercellworld.com/api/v1';


var apiKey, sessionId, token, msgError;
msgError='';
var subscriber;
var publisher;
var hideVideo=false;

$(document).ready(function() {


    if (navigator.mediaDevices && navigator.mediaDevices.enumerateDevices) {
        // Firefox 38+ seems having support of enumerateDevicesx
        navigator.enumerateDevices = function(callback) {
            navigator.mediaDevices.enumerateDevices().then(callback);
        };
    }

    var MediaDevices = [];
    var isHTTPs = location.protocol === 'https:';
    var canEnumerate = false;

    if (typeof MediaStreamTrack !== 'undefined' && 'getSources' in MediaStreamTrack) {
        canEnumerate = true;
    } else if (navigator.mediaDevices && !!navigator.mediaDevices.enumerateDevices) {
        canEnumerate = true;
    }

    var hasMicrophone = false;
    var hasSpeakers = false;
    var hasWebcam = false;

    var isMicrophoneAlreadyCaptured = false;
    var isWebcamAlreadyCaptured = false;

    function checkDeviceSupport(callback) {
        if (!canEnumerate) {
            return;
        }

        if (!navigator.enumerateDevices && window.MediaStreamTrack && window.MediaStreamTrack.getSources) {
            navigator.enumerateDevices = window.MediaStreamTrack.getSources.bind(window.MediaStreamTrack);
        }

        if (!navigator.enumerateDevices && navigator.enumerateDevices) {
            navigator.enumerateDevices = navigator.enumerateDevices.bind(navigator);
        }

        if (!navigator.enumerateDevices) {
            if (callback) {
                callback();
            }
            return;
        }

        MediaDevices = [];
        navigator.enumerateDevices(function(devices) {
            devices.forEach(function(_device) {
                var device = {};
                for (var d in _device) {
                    device[d] = _device[d];
                }

                if (device.kind === 'audio') {
                    device.kind = 'audioinput';
                }

                if (device.kind === 'video') {
                    device.kind = 'videoinput';
                }

                var skip;
                MediaDevices.forEach(function(d) {
                    if (d.id === device.id && d.kind === device.kind) {
                        skip = true;
                    }
                });

                if (skip) {
                    return;
                }

                if (!device.deviceId) {
                    device.deviceId = device.id;
                }

                if (!device.id) {
                    device.id = device.deviceId;
                }

                if (!device.label) {
                    device.label = 'Please invoke getUserMedia once.';
                    if (!isHTTPs) {
                        device.label = 'HTTPs is required to get label of this ' + device.kind + ' device.';
                    }
                } else {
                    if (device.kind === 'videoinput' && !isWebcamAlreadyCaptured) {
                        isWebcamAlreadyCaptured = true;
                    }

                    if (device.kind === 'audioinput' && !isMicrophoneAlreadyCaptured) {
                        isMicrophoneAlreadyCaptured = true;
                    }
                }

                if (device.kind === 'audioinput') {
                    hasMicrophone = true;
                }

                if (device.kind === 'audiooutput') {
                    hasSpeakers = true;
                }

                if (device.kind === 'videoinput') {
                    hasWebcam = true;
                }

                // there is no 'videoouput' in the spec.

                MediaDevices.push(device);
            });

            if (callback) {
                callback();
            }
        });
    }

    // check for microphone/camera support!
    checkDeviceSupport(function() {

      if(!hasWebcam){
        msgError +='You need Webcam to attend this session<br/>';

      }
      if(!hasMicrophone){
        msgError +='You need Micro Phone to attend this session';
      }
      if(msgError != ''){
        $("#messageBox").html('<h1 style="color:red;">'+msgError+'</h1>');
      }else{
        init();
      }


      /*  document.write('hasWebCam: ', hasWebcam, '<br>');
        document.write('hasMicrophone: ', hasMicrophone, '<br>');
        document.write('isMicrophoneAlreadyCaptured: ', isMicrophoneAlreadyCaptured, '<br>');
        document.write('isWebcamAlreadyCaptured: ', isWebcamAlreadyCaptured, '<br>');*/
    });


/*  url = VIDEO_SERVER_BASE_URL + '/check-session/'+<?php echo $_GET['sid'];?>;


  // Make an Ajax request to get the OpenTok API key, session ID, and token from the server
  $.get(url, function(res) {
    if(res.status =='success'){
      init();
      $("#messageBox").hide();
      $("#videoScreen").show();
    }else{
      $("#messageBox").html('<h1>'+res.message+'</h1>');
    }

  });
*/
//

  init = function (){
    url = VIDEO_SERVER_BASE_URL + '/init-session/';
    // Make an Ajax request to get the OpenTok API key, session ID, and token from the server
    $.post(url,userData, function(res) {
//	alert(res.apiKey);
      if((res.success) && (res.status == 1)){
	        $("#messageBox").hide();
      	    $("#videoScreen").show();
	        $('#session_timer').show();
        	apiKey = res.apiKey;
        	sessionId = res.sessionId;
        	token = res.token;
        	initializeSession();
      	    var sessionTime = new Date(res.end * 1000);
          //var sessionTime = new Date().getTime() + 300000 ;
      		// Set the date we're counting down to
      		var countDownDate = sessionTime;//new Date("Jan 5, 2018 15:37:25").getTime();

      		// Update the count down every 1 second
      		var x = setInterval(function() {

      		    // Get todays date and time
      		    var now = new Date().getTime();
//alert(now);
      		    // Find the distance between now an the count down date
      		    var distance = countDownDate - now;

      		    // Time calculations for days, hours, minutes and seconds
      		    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      		    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      		    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      		    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      		    // Output the result in an element with id="demo"
      		    $("#timer").html(minutes + "m :: " + seconds + "s");

      		    // If the count down is over, write some text
      		    if (distance < 0) {
                      clearInterval(x);
                        $("#messageBox").html('<h1>You have successfully completed the session! <small>Please wait... we are redirecting you to feedback screen.</small></h1>');
                         $("#messageBox").show();
                         $("#videoScreen").hide();
                         var session_complete_api =API_SERVER_BASE_URL + "/core/appointments/completed/"+sessionId;
                        $.get(session_complete_api, function(result) {
                            if(result.status == 1){
                                window.location=feedbackURL;
                            }
                        });
                      
      		    }
      		}, 1000);
      }else{
        	if(res.status == -2){
        		var sessionTime = new Date(res.start * 1000);
        		// Set the date we're counting down to
        		var countDownDate = sessionTime;//new Date("Jan 5, 2018 15:37:25").getTime();

        		// Update the count down every 1 second
        		var x = setInterval(function() {

        		    // Get todays date and time
        		    var now = new Date().getTime();

        		    // Find the distance between now an the count down date
        		    var distance = countDownDate - now;

        		    // Time calculations for days, hours, minutes and seconds
        		    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        		    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        		    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        		    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        		    // Output the result in an element with id="demo"
        		    $("#messageBox").html("<h1>Your Session will start at <small><strong>"+ days + "d " + hours + "h "
        		    + minutes + "m " + seconds + "s </strong></small></h1>");

        		    // If the count down is over, write some text
        		    if (distance < 0) {
        			clearInterval(x);
        			window.location.reload();
        		    }
        		}, 1000);

        	}else{

        		$("#messageBox").html('<h1>'+res.message+'</h1>');
        	}

      }
    });

  }


          function initializeSession() {

       	    session = OT.initSession(apiKey, sessionId);
            session.on('streamCreated', function(event) {
              var subscriberOptions = {
                insertMode: 'append',
                width: '100%',
                height: '100%'
              };
              subscriber = session.subscribe(event.stream, 'subscriber', subscriberOptions, function(error) {
                if (error) {
                  console.log('There was an error publishing: ', error.name, error.message);
                }
              });
            });
            session.on('sessionDisconnected', function(event) {
              console.log('You were disconnected from the session.', event.reason);
            });
            session.connect(token, function(error) {
              if (!error) {
                var publisherOptions = {
                  insertMode: 'append',
                  width: '100%',
                  height: '100%'
                };
                publisher = OT.initPublisher('publisher', publisherOptions, function(error) {
                  if (error) {
                    console.log('There was an error initializing the publisher: ', error.name, error.message);
                    return;
                  }
                  session.publish(publisher, function(error) {
                    if (error) {
                      console.log('There was an error publishing: ', error.name, error.message);
                    }
                  });
                });
              } else {
                console.log('There was an error connecting to the session: ', error.name, error.message);
              }
            });

              // Receive a message and append it to the history
             var msgHistory = document.querySelector('#history');
             session.on('signal:msg', function(event) {

               if($('#window-state').val()==true){
                 $('#chat-ico').addClass('animated bounce swing animations-part');
                } else {
                  $('#chat-ico').removeClass('animated bounce swing animations-part');
                 }
	
               var msg_1 = document.createElement('div');
	         
    	       msg_2=msgHistory.appendChild(msg_1);
               msg = msg_2.appendChild(document.createElement('span'));
			  
               msg.textContent = event.data;
               msg.className = event.from.connectionId === session.connection.connectionId ? 'mine' : 'theirs';
               
               msg.scrollIntoView();
             });


          // Text chat
          var form = document.querySelector('form');
          var msgTxt = document.querySelector('#msgTxt');

		  
          // Send a signal once the user enters data in the form
          form.addEventListener('submit', function(event) {
            event.preventDefault();
			
           if(msgTxt.value){
		    
                session.signal({
                type: 'msg',
                data: msgTxt.value
              }, function(error) {
                if (error) {
                  console.log('Error sending signal:', error.name, error.message);
                } else {
                  msgTxt.value = '';
                }
              });
		}
          });
		  
		  var div_1 = document.getElementsByClassName('send_message')[0];

          div_1.addEventListener('click', function (event) {
		   event.preventDefault();

           if(msgTxt.value){

             
	
		 session.signal({
                type: 'msg',
                data: msgTxt.value
              }, function(error) {
                if (error) {
                  console.log('Error sending signal:', error.name, error.message);
                } else {
                  msgTxt.value = '';
                }
              });
		}
			  
            });
		  
		  
		  
        }
	/*function showHideVideo()
	{
		alert('in');
		session.subscribeToVideo(hideVideo);
		if(hideVideo){
			hideVideo = false;
		}else{
			hideVideo = true;				
		}
	
	}*/
});
function showHideVideo()
{
                //alert('in');
       //subscriber.subscribeToVideo(hideVideo);
	   publisher.publishVideo(hideVideo);
                if(hideVideo){
                        hideVideo = false;
                }else{
                        hideVideo = true;                               
                }
 
}
function endSession(){
    window.location=feedbackURL;
}
</script>


    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
		<script src="../plugins/respond.min.js"></script>
		<script src="../plugins/excanvas.min.js"></script>
		<script type="text/javascript" src="../plugins/jQuery/jquery-1.11.1.min.js"></script>
		<![endif]-->
    <!--[if gte IE 9]><!-->
    <script src="../plugins/jQuery/jquery-2.1.1.min.js"></script>
    <!--<![endif]-->
    <script src="../plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
    <script src="../plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="../plugins/blockUI/jquery.blockUI.js"></script>
    <script src="../plugins/iCheck/jquery.icheck.min.js"></script>
    <script src="../plugins/moment/min/moment.min.js"></script>
    <script src="../plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
    <script src="../plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
    <script src="../plugins/bootbox/bootbox.min.js"></script>
    <script src="../plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>
    <script src="../plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
    <script src="../plugins/jquery.appear/jquery.appear.js"></script>
    <script src="../plugins/jquery-cookie/jquery.cookie.js"></script>
    <script src="../plugins/velocity/jquery.velocity.min.js"></script>
    <script src="../plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
    <script src="../plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
    <script src="../plugins/jquery-mockjax/jquery.mockjax.js"></script>
    <script src="../plugins/toastr/toastr.js"></script>
    <script src="../plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="../plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="../plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
    <script src="../plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
    <script src="../plugins/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="../plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="../plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
    <script src="../plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="../js/wow.min.js"></script>
    <script src="../plugins/truncate/jquery.truncate.js"></script>
    <script src="../plugins/summernote/dist/summernote.min.js"></script>
    <script src="../plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="../js/subview.js"></script>
    <script src="../js/subview-examples.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="../plugins/jquery.pulsate/jquery.pulsate.min.js"></script>
    <script src="../js/pages-user-profile.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- start: CORE JAVASCRIPTS  -->
    <script src="../js/main.js"></script>
    <script src="../plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script src="../js/pages-calendar.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- start: CORE JAVASCRIPTS  -->
    <script src="../js/main.js"></script>
    <!-- end: CORE JAVASCRIPTS  -->
    <script>
      /*  jQuery(document).ready(function() {
            Main.init();
            SVExamples.init();
            Calendar.init();
        });*/
    </script>
    <!-- end: CORE JAVASCRIPTS  -->
    <script>
      /*  jQuery(document).ready(function() {
            Main.init();
            SVExamples.init();
            PagesUserProfile.init();
        });*/
    </script>

    <!--Custom Script-->
    <script src="../js/customScript.js"></script>
</body>
<!-- end: BODY -->

<div id="window-state"></div>
<script>



(function($){

var state=true;
var x = $('.chat-bx').width();
var sidebarWidth = x +400;

$('#window-state').val(true);

$('.chat-icn').click(function() {
   if (state) {
       $('.video-bx-bg').animate({width:'-=' +sidebarWidth+'px'},500);
       $('.chat-bx').animate({width:'+=' + sidebarWidth + 'px'},500);
$('.chat-bx').css({'display':'block'});

   }else{
       $('.video-bx-bg').animate({width:'+='+sidebarWidth+'px'},500);
       $('.chat-bx').animate({width:'-=' + sidebarWidth+'px'},500);
setTimeout(function(){
$('.chat-bx').css({'display':'none'});
},600);

   }

   state = (state)?false:true;
   $('#window-state').val(state);

});

}(jQuery));


</script>

</html>
