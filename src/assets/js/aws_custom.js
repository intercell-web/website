AWS.config.region = 'ap-northeast-1'; // 1. Enter your region
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
  IdentityPoolId: 'ap-northeast-1:2a5d7090-e5a5-447e-b3b3-daf12e36e87e' // 2. Enter your identity pool
});

var bucketName = 'intercell-profileimages'; // Enter your bucket name
var bucket = new AWS.S3({
  params: {
    Bucket: bucketName
  }
});

var fileChooser =document.getElementById('file-chooser');
var button = document.getElementById('upload-button');
//var results = document.getElementById('results');

function uploadS3(){

  var file = document.getElementById('file-chooser-aws').files[0];
  var size = document.getElementById('file-chooser-aws').files[0].size;

  if (file) {
    if(size < 5097152){
      document.getElementById('image_user').setAttribute('src', window.URL.createObjectURL(file));
      document.getElementById('maxSize').style.display='none';

      var fileArray = file.name.split(".");

      if(document.getElementById('awsfileName').value){
        var objKey = document.getElementById('awsfileName').value;
      } else {
        var objKey = new Date().getTime() + "_" + Math.floor(Math.random() * 20) + "." + fileArray[1];
        document.getElementById('awsfileName').value=objKey;
      }

      var params = {
        Key: objKey,
        ContentType: file.type,
        Body: file,
        ACL: 'public-read'
      };

       // console.log(params);

      bucket.putObject(params, function(err, data) {
        //document.getElementById('results').innerHTML ='Uploading file';
        if (err) {
          document.getElementById('results').innerHTML = 'ERROR While Uploading: ' + err;
        } else {
          //document.getElementById('results').innerHTML = 'SUCCESS';
          //listObjs();
        }
      });
      document.getElementById('maxSize').style.display='none';

    } else {
      document.getElementById('maxSize').style.display='block';
	    document.getElementById('profileimageerror').style.display='none';
    }

  } else {

    //document.getElementById('results').innerHTML = 'Nothing to upload.';
  }
}


function uploadS3Counsellor(){
  var file = document.getElementById('file-counsellor-aws-profile').files[0];

  if (file) {
    var fileArray = file.name.split(".");

    if(document.getElementById('awsfileNameCounsellor').value){

      document.getElementById('counsellor_dpImage').setAttribute('src', window.URL.createObjectURL(file));
      //document.getElementById('header-left-pic').setAttribute('src', window.URL.createObjectURL(file));
      document.getElementById('header-right-pic').setAttribute('src', window.URL.createObjectURL(file));

      document.getElementById('couns_dp_image').setAttribute("style","background-image: url("+ window.URL.createObjectURL(file)+");background-repeat: no-repeat;background-size: 100px 100px");

      //var objKey = document.getElementById('awsfileNameCounsellor').value;
      var objKey = new Date().getTime() + "_" + Math.floor(Math.random() * 20) + "." + fileArray[1];
      document.getElementById('awsfileNameCounsellor').value=objKey;
    } else {
      //document.getElementById('header-left-pic').setAttribute('src', window.URL.createObjectURL(file));
      document.getElementById('header-right-pic').setAttribute('src', window.URL.createObjectURL(file));

      document.getElementById('couns_dp_image_null').setAttribute("style","background-image: url("+ window.URL.createObjectURL(file)+");background-repeat: no-repeat;background-size: 100px 100px");

      document.getElementById('counsellor_dpImage_null').setAttribute('src', window.URL.createObjectURL(file));
      var objKey = new Date().getTime() + "_" + Math.floor(Math.random() * 20) + "." + fileArray[1];
      document.getElementById('awsfileNameCounsellor').value=objKey;
    }

    var params = {
      Key: objKey,
      ContentType: file.type,
      Body: file,
      ACL: 'public-read'
    };

    bucket.putObject(params, function(err, data) {
      //document.getElementById('results').innerHTML ='Uploading file';
      if (err) {
        //document.getElementById('results').innerHTML = 'ERROR While Uploading: ' + err;
      } else {
        //document.getElementById('results').innerHTML = 'SUCCESS';
        //listObjs();
      }
    });
  } else {
    //document.getElementById('results').innerHTML = 'Nothing to upload.';
  }
}


function uploadS3Student(){
  var file = document.getElementById('file-student-aws-profile').files[0];

  if (file) {
    var fileArray = file.name.split(".");
console.log(fileArray);
    if(document.getElementById('awsfileNameStudent').value){
      document.getElementById('student_dpImage').setAttribute('src', window.URL.createObjectURL(file));
      //document.getElementById('header-left-pic').setAttribute('src', window.URL.createObjectURL(file));
      document.getElementById('header-right-pic').setAttribute('src', window.URL.createObjectURL(file));

      document.getElementById('stud_dp_image').setAttribute("style","background-image: url("+ window.URL.createObjectURL(file)+");background-repeat: no-repeat;background-size: 100px 100px");

      var objKey = new Date().getTime() + "_" + Math.floor(Math.random() * 20) + "." + fileArray[1];
      document.getElementById('awsfileNameStudent').value=objKey;
    } else {
     // document.getElementById('header-left-pic').setAttribute('src', window.URL.createObjectURL(file));
      document.getElementById('header-right-pic').setAttribute('src', window.URL.createObjectURL(file));

      document.getElementById('stud_dp_image_null').setAttribute("style","background-image: url("+ window.URL.createObjectURL(file)+");background-repeat: no-repeat;background-size: 100px 100px");

      document.getElementById('student_dpImage_null').setAttribute('src', window.URL.createObjectURL(file));
      var objKey = new Date().getTime() + "_" + Math.floor(Math.random() * 20) + "." + fileArray[1];
      document.getElementById('awsfileNameStudent').value=objKey;
    }

    var params = {
      Key: objKey,
      ContentType: file.type,
      Body: file,
      ACL: 'public-read'
    };

    bucket.putObject(params, function(err, data) {
      console.log(data);
      //document.getElementById('results').innerHTML ='Uploading file';
      if (err) {
      } else {

      }
    });
  } else {
  }
}


/*if(hostname =='www.intercellworld.com'){
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-105100790-1', 'auto');
  ga('send', 'pageview');
} */

$( document ).ready(function() {
  function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
  }

  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
  }
});


$(window).scroll(function() {
  if ($(this).scrollTop() > 100){
    $('.header-section').addClass("sticky");
  }
  else{
    $('.header-section').removeClass("sticky");
  }
});