
  var url = window.location.href
  var hostname, ajaxURLRoot;

  if (url.indexOf("://") > -1) {
    hostname = url.split('/')[2];
  }
  else {
    hostname = url.split('/')[0];
  }

  //find & remove port number
  hostname = hostname.split(':')[0];
  //find & remove "?"
  hostname = hostname.split('?')[0];
  if(hostname =='localhost'){
    ajaxURLRoot = 'http://localhost/intercell/api/v1/';
  }else if(hostname =='demo.intercellworld.com'){
    ajaxURLRoot = 'http://demo.intercellworld.com/api/v1/';
  }else{
    ajaxURLRoot = 'http://www.intercellworld.com/api/v1/';
  }

  function closeProfile(){
    document.getElementById("myNav").style.width = "0%";
  }

  function openProfile(){

    userId = document.getElementById('openProfileUser').value;
    token = document.getElementById('openProfileToken').value;

    userType = document.getElementById('showUserType').value;

    if(userType == 2){


      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          var jsonObj = JSON.parse(this.responseText);

          if((jsonObj.status == 1) && (jsonObj.type == 'success')){
            document.getElementById("myNav").style.width = "100%";
            document.getElementById("profile_full_name").innerHTML = jsonObj.response.first_name +" "+ jsonObj.response.last_name;
            //document.getElementById("profile_summary").innerHTML = jsonObj.response.summary;

            if( jsonObj.response.dp_image){
              document.getElementById("myImg").src = "http://d3213zi13us7f1.cloudfront.net/" + jsonObj.response.dp_image;
            } else {
              document.getElementById("myImg").src  = "assets/images/anonymous.jpg";
            }

            if(jsonObj.response.ln){
              document.getElementById("profile_linked").innerHTML = "LinkedIn: " + jsonObj.response.ln;
            }

            document.getElementById("profile_location").innerHTML = jsonObj.response.city+", "+jsonObj.response.state+", "+jsonObj.response.country;


            var eduxhttp = new XMLHttpRequest();
            eduxhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {

                var jsonObj = JSON.parse(this.responseText);

                if((jsonObj.status == 1) && (jsonObj.type == 'success')){
                  var expJson = jsonObj.response;

                  if(expJson[0].field && expJson[0].school_name){
                    document.getElementById("profile_title").innerHTML = expJson[0].field +' in '+expJson[0].school_name;
                  }

                  document.getElementById("profile_education").style.display ='block';
                  var strEdu = '';
                  $.each(expJson, function(index, value) {
                    strEdu +='<div class="col-sm-4 col-xs-12">'+
                      '<div class="col-sm-2 col-xs-3">'+
                      '<img src="assets/images/school2.png">'+
                      '</div>'+
                      '<div class="col-sm-9 col-xs-8">'+
                      '<ul>'+
                      '<li>'+
                      '<h3>'+value.qualification+' in '+value.field+'</h3>'+
                      '</li>'+
                      '<li>'+value.school_name+'</li>'+
                      '<li>'+value.enrollment_date+' to '+value.complation_date+
                      '</li>'+
                      /*'<li>'+value.description+'</li>'+*/
                      '</ul>'+
                      '</div>'+
                      '</div>';
                  });

                  document.getElementById("list_educations").innerHTML = strEdu;
                }
              }
            };
            eduxhttp.open("GET", ajaxURLRoot+"student/details/qualification_details/"+userId+"/"+token, true);
            eduxhttp.send();

            var aoixhttp = new XMLHttpRequest();
            aoixhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {

                var jsonObj = JSON.parse(this.responseText);

                if((jsonObj.status == 1) && (jsonObj.type == 'success')){
                  var expJson = jsonObj.response;

                  document.getElementById("profile_interest_area").style.display ='block';
                  var strAoi = '';
                  $.each(expJson, function(index, value) {
                    strAoi +='<div class="col-sm-4 col-xs-12" style="margin-bottom: 20px;">'+
                      '<div class="col-sm-2 col-xs-3">'+
                      '<img src="assets/images/school.png">'+
                      '</div>'+
                      '<div class="col-sm-9 col-xs-8">'+
                      '<ul>'+
                      '<li>'+
                      '<h3>'+value.field_name+'</h3>'+
                      '</li>'+

                      '</ul>'+
                      '</div>'+
                      '</div>';
                  });
                  document.getElementById("list_interest_areas").innerHTML = strAoi;
                }
              }
            };
            aoixhttp.open("GET", ajaxURLRoot+"student/details/interest_details/"+userId+"/"+token, true);
            aoixhttp.send();

          }

        }
      };
      xhttp.open("GET", ajaxURLRoot+"core/users/profile_stud/"+userId+"/"+token, true);
      xhttp.send();

    }
    else {

      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          var jsonObj = JSON.parse(this.responseText);

          if((jsonObj.status == 1) && (jsonObj.type == 'success')){
            document.getElementById("myNav").style.width = "100%";
            document.getElementById("profile_full_name").innerHTML = jsonObj.response.first_name +" "+ jsonObj.response.last_name;
            document.getElementById("profile_summary").innerHTML = jsonObj.response.summary;

            if( jsonObj.response.dp_image){
              document.getElementById("myImg").src = "http://d3213zi13us7f1.cloudfront.net/" + jsonObj.response.dp_image;
            } else {
              document.getElementById("myImg").src  = "assets/images/anonymous.jpg";
            }

            if(jsonObj.response.ln){
              document.getElementById("profile_linked").innerHTML = "LinkedIn: " + jsonObj.response.ln;
            }

            document.getElementById("profile_location").innerHTML = jsonObj.response.city+", "+jsonObj.response.state+", "+jsonObj.response.country;
            var user_type_id = jsonObj.response.user_type_id;
            //alert(user_type_id);

            var expxhttp = new XMLHttpRequest();
            expxhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {

                var jsonObj = JSON.parse(this.responseText);

                if((jsonObj.status == 1) && (jsonObj.type == 'success')){
                  var expJson = jsonObj.response;
                  document.getElementById("profile_title").innerHTML = expJson[0].title +' at '+expJson[0].company;
                  document.getElementById("profile_experience").style.display ='block';

                  var strExp = '';
                  $.each(expJson, function(index, value) {
                    strExp +='<div class="col-sm-4 col-xs-12">'+
                      '<div class="col-sm-2 col-xs-3">'+
                      '<img src="assets/images/school.png">'+
                      '</div>'+
                      '<div class="col-sm-9 col-xs-8">'+
                      '<ul>'+
                      '<li>'+
                      '<h3>'+value.title+'</h3>'+
                      '</li>'+
                      '<li>'+value.company+'</li>'+
                      '<li>'+value.start_time_month+' '+value.start_time_year+' to '+value.end_time_month+' '+value.end_time_year+
                      '</li>'+
                      '<li>'+value.location+'</li>'+
                      '</ul>'+
                      '</div>'+
                      '</div>';
                  });
                  document.getElementById("list_experiences").innerHTML = strExp;
                }
              }
            };
            expxhttp.open("GET", ajaxURLRoot+"counsellor/details/experience_details/"+userId+"/"+token, true);
            expxhttp.send();

            var eduxhttp = new XMLHttpRequest();
            eduxhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {

                var jsonObj = JSON.parse(this.responseText);

                if((jsonObj.status == 1) && (jsonObj.type == 'success')){
                  var expJson = jsonObj.response;
                  document.getElementById("profile_education").style.display ='block';
                  var strEdu = '';
                  $.each(expJson, function(index, value) {
                    strEdu +='<div class="col-sm-4 col-xs-12">'+
                      '<div class="col-sm-2 col-xs-3">'+
                      '<img src="assets/images/school2.png">'+
                      '</div>'+
                      '<div class="col-sm-9 col-xs-8">'+
                      '<ul>'+
                      '<li>'+
                      '<h3>'+value.degree+' in '+value.field+'</h3>'+
                      '</li>'+
                      '<li>'+value.school+'</li>'+
                      '<li>'+value.start_time+' to '+value.end_time+
                      '</li>'+
                      /*'<li>'+value.description+'</li>'+*/
                      '</ul>'+
                      '</div>'+
                      '</div>';
                  });
                  document.getElementById("list_educations").innerHTML = strEdu;
                }
              }
            };
            eduxhttp.open("GET", ajaxURLRoot+"counsellor/details/education_details/"+userId+"/"+token, true);
            eduxhttp.send();

            var certixhttp = new XMLHttpRequest();
            certixhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {

                var jsonObj = JSON.parse(this.responseText);

                if((jsonObj.status == 1) && (jsonObj.type == 'success')){
                  var expJson = jsonObj.response;
                  document.getElementById("profile_certificate").style.display ='block';
                  var strCerti = '';
                  $.each(expJson, function(index, value) {
                    strCerti +='<div class="col-sm-4 col-xs-12">'+
                      '<div class="col-sm-2 col-xs-3">'+
                      '<img src="assets/images/trophies.png">'+
                      '</div>'+
                      '<div class="col-sm-9 col-xs-8">'+
                      '<ul>'+
                      '<li>'+
                      '<h3>'+value.certificate+'</h3>'+
                      '</li>'+
                      '<li>'+value.institute+'</li>'+
                      '<li>'+value.time+
                      '</li>'+
                      /*'<li>'+value.description+'</li>'+*/
                      '</ul>'+
                      '</div>'+
                      '</div>';
                  });

                  document.getElementById("list_certificates").innerHTML = strCerti;
                }
              }
            };
            certixhttp.open("GET", ajaxURLRoot+"counsellor/details/certificate_details/"+userId+"/"+token, true);
            certixhttp.send();
          }

        }
      };
      xhttp.open("GET", ajaxURLRoot+"core/users/profile_couns/"+userId+"/"+token, true);
      xhttp.send();

    }

  }