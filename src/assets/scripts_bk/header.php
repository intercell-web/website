<!DOCTYPE html>
<html lang="en">

<!--<![endif]-->

<!-- start: HEAD -->

<head>

<title>Intercell Session Feedback</title>

<!-- start: META -->

<meta charset="utf-8" />

<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->

<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">

<meta name="apple-mobile-web-app-capable" content="yes">

<meta name="apple-mobile-web-app-status-bar-style" content="black">

<meta content="" name="description" />

<meta content="" name="author" />

<!-- end: META -->

<!-- start: MAIN CSS -->

<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>	

<link rel="stylesheet" href="../plugins/bootstrap/css/bootstrap.min.css">

<link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">

<link rel="stylesheet" href="../plugins/iCheck/skins/all.css">

<link rel="stylesheet" href="../plugins/perfect-scrollbar/src/perfect-scrollbar.css">

<link rel="stylesheet" href="../plugins/animate.css/animate.min.css">

<!-- end: MAIN CSS -->

<!---Start Include css-->

<link rel="stylesheet" href="../css/ihover.css" type="text/css" />

<!---End Include css-->

<!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->

<link rel="stylesheet" href="../plugins/owl-carousel/owl-carousel/owl.carousel.css">

<link rel="stylesheet" href="../plugins/owl-carousel/owl-carousel/owl.theme.css">

<link rel="stylesheet" href="../plugins/owl-carousel/owl-carousel/owl.transitions.css">

<link rel="stylesheet" href="../plugins/summernote/dist/summernote.css">

<link rel="stylesheet" href="../plugins/fullcalendar/fullcalendar/fullcalendar.css">

<link rel="stylesheet" href="../plugins/toastr/toastr.min.css">

<link rel="stylesheet" href="../plugins/bootstrap-select/bootstrap-select.min.css">

<link rel="stylesheet" href="../plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">

<link rel="stylesheet" href="../plugins/DataTables/media/css/DT_bootstrap.css">

<link rel="stylesheet" href="../plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">

<link rel="stylesheet" href="../plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">

<link rel="stylesheet" href="../css/styles.css">

<link rel="stylesheet" href="../css/styles-responsive.css">

<link rel="stylesheet" href="../css/plugins.css">

<link rel="stylesheet" href="../css/themes/theme-style8.css" type="text/css" id="skin_color">

<link rel="stylesheet" href="../css/print.css" type="text/css" media="print" />

<link href="../css/editor.css" type="text/css" rel="stylesheet"/>

<!--<link rel="stylesheet" href="../css/mo_custom.css" type="text/css" />-->

<link rel="stylesheet" href="../css/myStyle.css" type="text/css" />

<link rel="stylesheet" href="../css/ps_Style.css" type="text/css" />

<!-- end: CORE CSS -->

<link rel="shortcut icon" href="favicon.png" />







</head>

<!-- end: HEAD -->

<!-- start: BODY -->

<body>

<div class="main-wrapper Counsellor_dashboard">

<!-- Top Navigation Start-->

<!-- start: TOPBAR -->

<header class="topbar navbar navbar-inverse navbar-fixed-top inner">

  <!-- start: TOPBAR CONTAINER -->

  <nav class="navbar navbar-default">

    <div class="container-fluid">

      <!-- Brand and toggle get grouped for better mobile display -->

      <div class="navbar-header">

        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>

<!--          <ul class="list-unstyled list-inline hidden-sm hidden-lg hidden-md mobile-menu">

              <li><a href="#"><i class="fa fa-mobile"></i></a></li>

              <li><a href="#"><img src="assets/images/counsellor-icn.png" alt=""></a></li>

              <li><a href="#"><i class="fa fa-gears"></i></a></li>

              <li><a href="#" class="mbl-sign-in"><i class="fa fa-sign-in"></i></a></li>

          </ul>-->

        <a class="navbar-brand" href="http://www.intercellworld.com"><img src="../images/logo.png" alt="logo" class="img-responsive"></a> </div>

	  

	

</div>

	  

      <!-- /.navbar-collapse -->

    </div>

    <!-- /.container-fluid -->

  </nav>

  <!-- end: TOPBAR CONTAINER -->

</header>

<!-- end: TOPBAR -->



  







<!-- Modal -->

<div class="students_select">

<div id="ordine" class="tabs_part modal fade" role="dialog">

  <div class="modal-dialog">



    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header tabs_part">

        <button type="button" class="close" data-dismiss="modal">×</button>

            </div>

      <div class="modal-body">

            <div class="row">

		<div class="col-md-12">

						<div class="tabbable-panel">

				<div class="tabbable-line">

					<ul class="nav nav-tabs ">

						<li class="active">

							<a href="#tab_default_1" data-toggle="tab">

							Counsellor </a>

						</li>

						<li>

							<a href="#tab_default_2" data-toggle="tab">

							Students</a>

						</li>

											</ul>

					<div class="tab-content">

						<div class="tab-pane active" id="tab_default_1">

							<div id="signup">   

          <h1>Welcome to Intercell</h1>

		  <p class="prices">Register Yourself</p>

		  <span class="prices">Counsellor Rs. 500 /- </span><br><br>

          

<a href="#" class="skips"> Skip </a><button type="submit" class="btn btn-danger button-block"/>Proceed to pay</button>

        </div>

							

						</div>

						<div class="tab-pane" id="tab_default_2">

							   <div id="login">   

          <h1>Welcome to Intercell</h1>

		  <p class="prices">Register Yourself</p>

           <span class="prices">Students Rs. 500 /- </span><br><br>

       

 <a href="#" class="skips"> Skip </a><button class="btn btn-danger button-block"/>Proceed to pay</button>

        </div>

							

						

						</div>

					

					</div>

				</div>

			</div>



			

		

	</div>

</div>

     

     

    </div>



  </div>

</div>





  </div>

  </div>


