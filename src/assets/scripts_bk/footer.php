
<footer class="inner">


    <div class="footer-inner">

        <div class="pull-left">

            2017 &copy; Intercell Technologies Pvt. Ltd.

        </div>

        <div class="pull-right">

            <span class="go-top"><i class="fa fa-chevron-up"></i></span>

        </div>

    </div>

</footer>

<!-- FOOTER -->

</div>



	



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<script src="../plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

<script src="../plugins/bootstrap/js/bootstrap.min.js"></script>

<script src="../plugins/blockUI/jquery.blockUI.js"></script>

<script src="../plugins/iCheck/jquery.icheck.min.js"></script>

<script src="../plugins/moment/min/moment.min.js"></script>

<script src="../plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>

<script src="../plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>

<script src="../plugins/bootbox/bootbox.min.js"></script>

<script src="../plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>

<script src="../plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>

<script src="../plugins/jquery.appear/jquery.appear.js"></script>

<script src="../plugins/jquery-cookie/jquery.cookie.js"></script>

<script src="../plugins/velocity/jquery.velocity.min.js"></script>

<script src="../plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>

<!-- end: MAIN JAVASCRIPTS -->

<!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->

<script src="../plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>

<script src="../plugins/jquery-mockjax/jquery.mockjax.js"></script>

<script src="../plugins/toastr/toastr.js"></script>

<script src="../plugins/bootstrap-modal/js/bootstrap-modal.js"></script>

<script src="../plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>

<script src="../plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>

<script src="../plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>

<script src="../plugins/bootstrap-select/bootstrap-select.min.js"></script>

<script src="../plugins/jquery-validation/dist/jquery.validate.min.js"></script>

<script src="../plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>

<script src="../plugins/DataTables/media/js/jquery.dataTables.min.js"></script>

<script src="../plugins/truncate/jquery.truncate.js"></script>

<script src="../plugins/summernote/dist/summernote.min.js"></script>

<script src="../plugins/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="../js/subview.js"></script>

<script src="../js/subview-examples.js"></script>

<!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->

<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

<script src="../plugins/jquery.pulsate/jquery.pulsate.min.js"></script>

<script src="../js/pages-user-profile.js"></script>

<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

<!-- start: CORE JAVASCRIPTS  -->

<script src="../js/main.js"></script>

<script src="../plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script src="../js/pages-calendar.js"></script>

<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

<!-- start: CORE JAVASCRIPTS  -->



<script src="../js/editor.js"></script>

<script src="../js/main.js"></script>

<!-- end: CORE JAVASCRIPTS  -->

<script>

  jQuery(document).ready(function () {

	Main.init();

	SVExamples.init();

	Calendar.init();

	 PagesUserProfile.init();

  });

</script>



<script>

	$(window).load(function() {

		$("#txtEditor").Editor();

	});

	$.fn.expose = function(options) {

	

  var $modal = $(this),

      $trigger = $("a[href=" + this.selector + "]");

  

  $modal.on("expose:open", function() {

    

    $modal.addClass("is-visible");

    $modal.trigger("expose:opened");

  });

  

  $modal.on("expose:close", function() {

    

    $modal.removeClass("is-visible");

    $modal.trigger("expose:closed");

  });

  

  $trigger.on("click", function(e) {

    

    e.preventDefault();

    $modal.trigger("expose:open");

  });

  

  $modal.add( $modal.find(".close") ).on("click", function(e) {

    

    e.preventDefault();

    

    // if it isn't the background or close button, bail

    if( e.target !== this )

      return;

    

  	$modal.trigger("expose:close");

  });

  

  return;

}



$("#Popup").expose();



// Example Cancel Button



$(".cancel").on("click", function(e) {

  

  e.preventDefault();

  $(this).trigger("expose:close");

});

$('.form').find('input, textarea').on('keyup blur focus', function (e) {

  

  var $this = $(this),

      label = $this.prev('label');



	  if (e.type === 'keyup') {

			if ($this.val() === '') {

          label.removeClass('active highlight');

        } else {

          label.addClass('active highlight');

        }

    } else if (e.type === 'blur') {

    	if( $this.val() === '' ) {

    		label.removeClass('active highlight'); 

			} else {

		    label.removeClass('highlight');   

			}   

    } else if (e.type === 'focus') {

      

      if( $this.val() === '' ) {

    		label.removeClass('highlight'); 

			} 

      else if( $this.val() !== '' ) {

		    label.addClass('highlight');

			}

    }



});



$('.tab a').on('click', function (e) {

  

  e.preventDefault();

  

  $(this).parent().addClass('active');

  $(this).parent().siblings().removeClass('active');

  

  target = $(this).attr('href');



  $('.tab-content > div').not(target).hide();

  

  $(target).fadeIn(600);

  

});

</script>



<!--Custom Script-->

<script src="../js/customScript.js"></script>

</body>

<!-- end: BODY -->



</html>

