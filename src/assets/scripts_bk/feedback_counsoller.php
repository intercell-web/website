<?php
$isQueSubmitted = false;
$isRatingSubmitted = false;
if((isset($_POST['uid'])) && (isset($_POST['uid'])) && (isset($_POST['rating'])))
{
	$isRatingSubmitted = true;
}

if(isset($_POST['que_1'])){
	$isQueSubmitted = true;
}

include("header.php");
?>
<script>
function validateForm() {
    var x = document.forms["myForm"]["rating"].value;
    if (x == "") {
        alert("Rating must be filled out");
        return false;
    }
}
</script>
<!--Header End-->
<!--Page Content Start-->
<div class="container-fluid p-0">
    <div class="feedback-bx" style="max-width:100%;width:80%;">
	<?php
		if($isQueSubmitted){
			echo '<h3 class="row title">Thank you for your feedback</h3>';
                        echo '<p>Thank you for providing us your feedback. We really appreciate it!</p>';
		}else if($isRatingSubmitted){
			echo '<h3 class="row title">Thank You for Your Feedback</h3>';
			echo '<p>Thank you for providing us your feedback. We really appreciate it!</p>';
			echo ' <div class="question-section">
      <h3>Please add questions asked by student during session.</h3>
      <form class="question-form">
        <div class="question-input">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group ">
                <label> 1. Question </label>
                <textarea rows="2" name="que_1" class="col-xs-12 "></textarea>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group ">
                <label> 2. Question </label>
                <textarea name="que_2" rows="2" class="col-xs-12"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="question-input">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group ">
                <label> 3. Question </label>
                <textarea rows="2" name="que_3" class="col-xs-12"></textarea>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group ">
                <label> 4. Question </label>
                <textarea rows="2" name="que_4" class="col-xs-12"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="question-input">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group ">
                <label> 5. Question </label>
                <textarea rows="2" name="que_5" class="col-xs-12"></textarea>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group ">
                <label> 6. Question </label>
                <textarea rows="2" name="que_6" class="col-xs-12"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="question-input">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group ">
                <label> 7. Question </label>
                <textarea rows="2" name="que_7" class="col-xs-12"></textarea>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group ">
                <label> 8. Question </label>
                <textarea rows="2" name="que_8" class="col-xs-12"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="question-input">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group ">
                <label> 9. Question </label>
                <textarea rows="2" name="que_9" class="col-xs-12"></textarea>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group ">
                <label> 10. Question </label>
                <textarea rows="2" name="que_10" class="col-xs-12"></textarea>
              </div>
              <button class="pull-right btn btn-primary"> Submit</button>
            </div>
          </div>
        </div>
      </form>
    </div>';
		}else{
	?>
        <h3 class="row title">Give Your Feedback</h3>
        <form class="col-xs-12" method="POST" name="myForm" onsubmit="return validateForm()">
	    <input type="hidden" name="uid" value="<?php echo $_GET['uid'];?>">
	    <input type="hidden" name="sid" value="<?php echo $_GET['sid'];?>">	
            <div class="form-group">
              <h4>How would you rate this session?</h4>
              <fieldset class="rating">
                <input type="radio" id="star5" name="rating" value="5" />
		<label class = "full" for="star5" title="Awesome - 5 stars"></label>
                <input type="radio" id="star4half" name="rating" value="4.5" />
		<label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>

                <input type="radio" id="star4" name="rating" value="4" />
		<label class = "full" for="star4" title="Pretty good - 4 stars"></label>

                <input type="radio" id="star3half" name="rating" value="3.5" />
		<label class="half" for="star3half" title="Meh - 3.5 stars"></label>

                <input type="radio" id="star3" name="rating" value="3" />
		<label class = "full" for="star3" title="Meh - 3 stars"></label>

                <input type="radio" id="star2half" name="rating" value="2.5" />
		<label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>

                <input type="radio" id="star2" name="rating" value="2" />
		<label class = "full" for="star2" title="Kinda bad - 2 stars"></label>

                <input type="radio" id="star1half" name="rating" value="1.5" />
		<label class="half" for="star1half" title="Meh - 1.5 stars"></label>

                <input type="radio" id="star1" name="rating" value="1" />
		<label class = "full" for="star1" title="Sucks big time - 1 star"></label>

                <input type="radio" id="starhalf" name="rating" value="0.5" />
		<label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>

              </fieldset>

            </div>

            <div class="clearfix"></div>

            <div class="form-group clearfix">
		<h4>Your Detailed Feedback</h4>

           <textarea id="txtEditor" placeholder="Write note here..."></textarea> 

				

			

            </div>

            <input type="submit" value="Send" class="btn btn-default btn-blue pull-right">

            <div class="clearfix"></div>

        </form>
<?php }?>
        <div class="clearfix"></div>

    </div>

      

</div>

<!--Page Content End-->



<!--Footer Start-->

<?php  include("footer.php");?>

<!--Footer End-->



