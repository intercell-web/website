<?php
date_default_timezone_set('Asia/Calcutta');
$validateData =false;
  if((isset($_GET['uid'])) && (isset($_GET['sid'])))
  {
    if((!empty($_GET['uid'])) && (!empty($_GET['sid'])))
    {
        $validateData = true;
    }
  }
  if(!$validateData)
  {
      echo '<h1>Required information missing or invalid</h1>';
      exit;
  }


?><!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- start: HEAD -->

<head>
    <title>Intercell Video Session</title>
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Video Session Screen" name="description" />
    <meta content="Deepak Patil" name="author" />
    <!-- end: META -->

    <!-- start: MAIN CSS -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../plugins/iCheck/skins/all.css">
    <link rel="stylesheet" href="../plugins/perfect-scrollbar/src/perfect-scrollbar.css">
    <link rel="stylesheet" href="../plugins/animate.css/animate.min.css">
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->
    <!--<link rel="stylesheet" href="../plugins/owl-carousel/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="../plugins/owl-carousel/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="../plugins/owl-carousel/owl-carousel/owl.transitions.css">
    <link rel="stylesheet" href="../plugins/summernote/dist/summernote.css">
    <link rel="stylesheet" href="../plugins/fullcalendar/fullcalendar/fullcalendar.css">
    <link rel="stylesheet" href="../plugins/toastr/toastr.min.css">
    <link rel="stylesheet" href="../plugins/bootstrap-select/bootstrap-select.min.css">
    <link rel="stylesheet" href="../plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
    <link rel="stylesheet" href="../plugins/DataTables/media/css/DT_bootstrap.css">
    <link rel="stylesheet" href="../plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
    <link rel="stylesheet" href="../plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">-->
    <!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->


    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- start: CORE CSS -->
    <link rel="stylesheet" href="../css/styles.css">
    <link rel="stylesheet" href="../css/styles-responsive.css">
    <link rel="stylesheet" href="../css/plugins.css">
    <link rel="stylesheet" href="../css/themes/theme-style8.css" type="text/css" id="skin_color">
    <link rel="stylesheet" href="../css/print.css" type="text/css" media="print" />
    <link rel="stylesheet" href="../css/mo_custom.css" type="text/css" />
    <link rel="stylesheet" href="../css/myStyle.css" type="text/css" />
    <link rel="stylesheet" href="../css/ps_Style.css" type="text/css" />	
    <style>
        .video-img .video-icon-list{ top:20px; right:12px;}

.video-bx-bg {
   background: #fff !important;
 
}
.video-icon-list a{ background:#EE3E34;}
.video-icon-list i{background:#EE3E34;}
.video-icon-list a::after{border-color:transparent transparent transparent #EE3E34;}

.video-session-blk{ padding-top:60px;height: 100vh !important;
   overflow: hidden;}
.video-bx-bg {
   background: #fff !important; padding:0px;
}
.video-img {
   margin: 8px;
   padding: 7px;
   border: 1px solid #EDECEC;
}
.video_part2 {
   width: 220px;
   height: auto;
   position: absolute;
   z-index: 99;
   top: 26px;
   left: 26px;
   /* padding: 6px; */
   /* border: 2px solid #eee; */
   box-shadow:1px 0px 9px 1px #ddd;
}
.Counsellor_dashboard {
   background: #fff;
   overflow: hidden;
}
.cht-msg-bx.clearfix {
   margin-top: 50px;
}
.video-sections img{ width:100%;}
.video_part2 img {
   width: 100%;
   height: auto;
}

.video-list h4 span {
margin-left: 13px;}
li.video-list {
   margin-top: 11px;
}



.video-bx-bg{width:70%;}
.chat-bx{width:30%;}
    </style>
    <!-- end: CORE CSS -->
    <link rel="shortcut icon" href="favicon.png" />
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
    <div class="main-wrapper Counsellor_dashboard" >
        <!-- start: TOPBAR -->
        <header class="topbar navbar navbar-inverse navbar-fixed-top inner">
            <!-- start: TOPBAR CONTAINER -->
            <div class="container">
                <div class="navbar-header">
                    <!-- start: LOGO -->
                    <a class="navbar-brand" href="https://www.intercellworld.com/" target="_blank">
                        <img src="../images/logo.png" alt="InterCell Logo" />
                    </a>
                    <!-- end: LOGO -->
                </div>

                <div class="topbar-tools" id="session_timer" style="display:none;padding-top:0px;">
                    <!-- start: TOP NAVIGATION MENU -->
                     <ul class="nav navbar-right">
                        <!-- start: USER DROPDOWN -->
                       
                        
                        <li class="video-list">
                             <h4>Session Time left <span class="pull-right"> <strong id="timer"> 25m :: 53s</strong></span></h4></span>
                           

                        </li>
                      
                    </ul>
                    <!-- end: TOP NAVIGATION MENU -->
                </div>
            </div>
            <!-- end: TOPBAR CONTAINER -->
        </header>
        <!-- end: TOPBAR -->


        <div id="messageBox" class="Container-fluid video-session-blk" style="margin-left:60px;">
            <h1>Please wait we are connecting to Video Server...</h1>
        </div>


        <!-- start: MAIN CONTAINER -->
        <script type="text/javascript">
            var userData = '<?php echo json_encode($_GET)?>';
        </script>
        <script type="text/javascript" src="https://static.opentok.com/v2/js/opentok.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link href="http://fonts.googleapis.com/css?family=Ubuntu:500" rel="stylesheet" type="text/css">

        <!-- start: MAIN CONTAINER -->
        <div class="Container-fluid video-session-blk" id="videoScreen" style="display:none;">
            <div class="col-md-9 col-sm-8 video-bx-bg">
                <div class="video-img" id="videos">
                    <div class="video-sections" id="subscriber" style="height: 100vh;">
                       </div>
                    <div class="video_part2" id="publisher" style="height: 150px;">
                       
                    
                </div>
                     
                     
                    
                <div class="video-icon-list">
                    <ul class="list-unstyled text-right">
                        <li><a href="javascript:void(0);" class="">End Session</a> <i class="fa fa-stop" data-placement="top" data-original-title="End Session"></i> </li>
                        <li class="chat-video-hide"><a href="javascript:void(0);" class="">Hide Video</a> <i class="fa fa-eye-slash"></i> </li>
						<li class="chat-icn"><a href="javascript:void(0);" class="">Chat</a> <i class="fa fa-comment-o"></i> </li>
                        <!--<li><a href="javascript:void(0);" class="">Chat</a> <i class="fa fa-wechat"></i></li>-->
                    </ul>
                </div>
           
			</div>
            </div>
            <div class="col-md-3 col-sm-4 chat-bx">
				<div class="chat-bx-inner">
               
                <hr>
                <div class="col-xs-4 text-center">
                    <img src="assets/images/avatar-1.jpg" alt="" class="img-responsive">
                    <h5>Jhon Doe</h5>
                </div>
                <div class="col-xs-8">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                </div>
                <!-- Chat box start-->
                <div class="chat-field">
                    <form class="cht-frm">
                        <div class="chat-msg chat-lt">
                            <span>Lorem Ipsum is simply dummy text</span>
                        </div>
                        <div class="chat-msg chat-rt">
                            <span>Lorem Ipsum is simply dummy text</span>
                        </div>
                        <div class="chat-msg chat-lt">
                            <span>dummy text</span>
                        </div>
                        <div class="chat-msg chat-lt">
                            <span>Lorem Ipsum is text</span>
                        </div>
                        <div class="chat-msg chat-rt">
                            <span>dummy text</span>
                        </div>
                        <div class="chat-msg chat-rt">
                            <span>Lorem Ipsum is simply dummy text</span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="cht-msg-bx clearfix">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Type a message here">
                                <div class="input-group-addon chat-btn"><a href="javascript:void(0);" class="fa fa-send"></a> </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
                <!-- Chat box end-->
				</div>
            </div>
        </div>
        <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
    <div class="clearfix"></div>
        <!-- start: FOOTER -->
       <footer class="inner">
            <div class="footer-inner">
                <div class="pull-left">
                    <?php echo date('Y');?> &copy; InterCell by Digitigi.
                </div>
            </div>
        </footer>
        <!-- end: FOOTER -->
    </div>


    <!--- Give your feedback box start---->
    <div class="modal fade" id="giveYourFeedbackBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Give Your Feedback</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <h5>How would you rate the services?</h5>
                            <fieldset class="rating">
                                <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                                <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                                <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                                <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                                <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                                <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                            </fieldset>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group clearfix">
                            <h5>Write your Review</h5>
                            <textarea class="form-control" placeholder="Message"></textarea>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">Send</button>
                </div>
            </div>
        </div>
    </div>
    <!--- Give your feedback box end---->
    
    
    
<script type="text/javascript">
var SAMPLE_SERVER_BASE_URL = 'https://www.intercellworld.com/icvideo/server/web';
var apiKey,sessionId,token;



$(document).ready(function() {
var apiKey, sessionId,token;
/*  url = SAMPLE_SERVER_BASE_URL + '/check-session/'+<?php echo $_GET['sid'];?>;


  // Make an Ajax request to get the OpenTok API key, session ID, and token from the server
  $.get(url, function(res) {
    if(res.status =='success'){
      init();
      $("#messageBox").hide();
      $("#videoScreen").show();
    }else{
      $("#messageBox").html('<h1>'+res.message+'</h1>');
    }

  });
*/
//init();

  //init = function (){
    url = SAMPLE_SERVER_BASE_URL + '/init-session/';
    // Make an Ajax request to get the OpenTok API key, session ID, and token from the server
    $.post(url,userData, function(res) {
//	alert(res.apiKey);
      if((res.success) && (res.status == 1)){
	$("#messageBox").hide();
      	$("#videoScreen").show();
	$('#session_timer').show();
      	apiKey = res.apiKey;
      	sessionId = res.sessionId;
      	token = res.token;
      	initializeSession();
      	var sessionTime = new Date(res.end * 1000);
		// Set the date we're counting down to
		var countDownDate = sessionTime;//new Date("Jan 5, 2018 15:37:25").getTime();

		// Update the count down every 1 second
		var x = setInterval(function() {

		    // Get todays date and time
		    var now = new Date().getTime();
		    
		    // Find the distance between now an the count down date
		    var distance = countDownDate - now;
		    
		    // Time calculations for days, hours, minutes and seconds
		    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		    
		    // Output the result in an element with id="demo"
		    $("#timer").html(minutes + "m :: " + seconds + "s");
		    
		    // If the count down is over, write some text 
		    if (distance < 0) {
                clearInterval(x);
                window.location.reload();
		    }
		}, 1000);
      }else{
	if(res.status == -2){
		var sessionTime = new Date(res.start * 1000);
		// Set the date we're counting down to
		var countDownDate = sessionTime;//new Date("Jan 5, 2018 15:37:25").getTime();

		// Update the count down every 1 second
		var x = setInterval(function() {

		    // Get todays date and time
		    var now = new Date().getTime();
		    
		    // Find the distance between now an the count down date
		    var distance = countDownDate - now;
		    
		    // Time calculations for days, hours, minutes and seconds
		    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		    
		    // Output the result in an element with id="demo"
		    $("#messageBox").html("<h1>Your Session will start at <small><strong>"+ days + "d " + hours + "h "
		    + minutes + "m " + seconds + "s </strong></small></h1>");
		    
		    // If the count down is over, write some text 
		    if (distance < 0) {
			clearInterval(x);
			window.location.reload();
		    }
		}, 1000);

	}else{
		
		$("#messageBox").html('<h1>'+res.message+'</h1>');
	}
	
      }
    });

  //}





          function initializeSession() {
//alert(apiKey +'---'+ sessionId)
        var session = OT.initSession(apiKey, sessionId);
            session.on('streamCreated', function(event) {
              var subscriberOptions = {
                insertMode: 'append',
                width: '100%',
                height: '100%'
              };
              session.subscribe(event.stream, 'subscriber', subscriberOptions, function(error) {
                if (error) {
                  console.log('There was an error publishing: ', error.name, error.message);
                }
              });
            });
            session.on('sessionDisconnected', function(event) {
              console.log('You were disconnected from the session.', event.reason);
            });
            session.connect(token, function(error) {
              if (!error) {
                var publisherOptions = {
                  insertMode: 'append',
                  width: '100%',
                  height: '100%'
                };
                var publisher = OT.initPublisher('publisher', publisherOptions, function(error) {
                  if (error) {
                    console.log('There was an error initializing the publisher: ', error.name, error.message);
                    return;
                  }
                  session.publish(publisher, function(error) {
                    if (error) {
                      console.log('There was an error publishing: ', error.name, error.message);
                    }
                  });
                });
              } else {
                console.log('There was an error connecting to the session: ', error.name, error.message);
              }
            });
          }
});
                 </script>
                 

    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
		<script src="../plugins/respond.min.js"></script>
		<script src="../plugins/excanvas.min.js"></script>
		<script type="text/javascript" src="../plugins/jQuery/jquery-1.11.1.min.js"></script>
		<![endif]-->
    <!--[if gte IE 9]><!-->
    <script src="../plugins/jQuery/jquery-2.1.1.min.js"></script>
    <!--<![endif]-->
    <script src="../plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
    <script src="../plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="../plugins/blockUI/jquery.blockUI.js"></script>
    <script src="../plugins/iCheck/jquery.icheck.min.js"></script>
    <script src="../plugins/moment/min/moment.min.js"></script>
    <script src="../plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
    <script src="../plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
    <script src="../plugins/bootbox/bootbox.min.js"></script>
    <script src="../plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>
    <script src="../plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
    <script src="../plugins/jquery.appear/jquery.appear.js"></script>
    <script src="../plugins/jquery-cookie/jquery.cookie.js"></script>
    <script src="../plugins/velocity/jquery.velocity.min.js"></script>
    <script src="../plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
    <script src="../plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
    <script src="../plugins/jquery-mockjax/jquery.mockjax.js"></script>
    <script src="../plugins/toastr/toastr.js"></script>
    <script src="../plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="../plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="../plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
    <script src="../plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
    <script src="../plugins/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="../plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="../plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
    <script src="../plugins/DataTables/media/js/jquery.dataTables.min.js"></script>

    <script src="../plugins/truncate/jquery.truncate.js"></script>
    <script src="../plugins/summernote/dist/summernote.min.js"></script>
    <script src="../plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="../js/subview.js"></script>
    <script src="../js/subview-examples.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="../plugins/jquery.pulsate/jquery.pulsate.min.js"></script>
    <script src="../js/pages-user-profile.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- start: CORE JAVASCRIPTS  -->
    <script src="../js/main.js"></script>
    <script src="../plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script src="../js/pages-calendar.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- start: CORE JAVASCRIPTS  -->
    <script src="../js/main.js"></script>
    <!-- end: CORE JAVASCRIPTS  -->
    <script>
        jQuery(document).ready(function() {
            Main.init();
            SVExamples.init();
            Calendar.init();
        });
    </script>
    <!-- end: CORE JAVASCRIPTS  -->
    <script>
        jQuery(document).ready(function() {
            Main.init();
            SVExamples.init();
            PagesUserProfile.init();
        });
    </script>

    <!--Custom Script-->
    <script src="../js/customScript.js"></script>
</body>
<!-- end: BODY -->

<script>



(function($){

var state=true;

var sidebarWidth = $('.chat-bx').width();

$('.chat-icn').click(function() {
    if (state) {
        $('.video-bx-bg').animate({width:'+=' +sidebarWidth+'px'},2000);
        $('.chat-bx').animate({width:'-=' + sidebarWidth + 'px'},2000);
		setTimeout(function(){
			$('.chat-bx').css({'display':'none'});
		},2000);
		
    }else{ 
        $('.video-bx-bg').animate({width:'-='+sidebarWidth+'px'},2000);
        $('.chat-bx').animate({width:'+=' + sidebarWidth+'px'},2000);
		$('.chat-bx').css({'display':'block'});
		
    }

    state = (state)?false:true;
});

}(jQuery));


</script>

</html>
	


    
