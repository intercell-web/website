// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  version: '(dev)',
   serverUrl: 'https://www.intercellworld.com/oldapi/t',
  defaultLanguage: 'en-US',
  supportedLanguages: [
    'en-US',
    'fr-FR'
  ],
  paymentUrl: 'https://www.intercellworld.com/oldapi/t',
  termUrl: 'https://www.intercellworld.com/term',
  counsellorProfileUrl : 'https://www.intercellworld.com/counsellor-profile',
  fbAppId : '1553551184724203',
  cdnProfileUrl:  "https://d3213zi13us7f1.cloudfront.net/"
};
