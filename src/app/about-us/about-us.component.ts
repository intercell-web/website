import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../services/global.service';
import { Router } from '@angular/router';
import { Title,Meta } from '@angular/platform-browser';
import { LandingService } from '../services/landing.service';
import { environment } from '../../environments/environment';



@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
    userType: any;
    isLogin: any = localStorage.getItem('userId');

    constructor(private globalService: GlobalService,private title: Title, private meta: Meta , private router: Router, private landingService: LandingService) {

      window.scrollTo(0, 0);
      if(!(this.globalService.userType)){
        this.globalService.userType = +localStorage.getItem('userType');
      }

      this.userType = this.globalService.userType;
// Start to Set Component  Title, Description and Keywords
this.title.setTitle('About Us - Intercellworld.com');
this.meta.updateTag({ name: 'description', content: 'About Us' });
this.meta.updateTag({ name: 'keyword', content: 'Intercell' });
// End to Set Component Title, Description and Keywords
    }


    ngOnInit() {
      
          
          $(window).scroll(function(){
      
            if ($(this).scrollTop() > 280) {
              $('#mentee-part').addClass('animated fadeInUp');
            }
      
            if ($(this).scrollTop() > 700) {
              $('#middle-part').addClass('animated fadeInDown');
            }
      
            if ($(this).scrollTop() > 1000) {
              $('#mentor-part').addClass('animated fadeInUp');
            }
      
            if ($(this).scrollTop() > 1700) {
              $('.bottom-part').addClass('animated fadeInUp');
            }
      
            if ($(this).scrollTop() > 280) {
              $('#stream-part').addClass('animated fadeInUp');
            }
      
          });
          $('.toggle_nav').click(function() {	 
            $('#mainnav').show()
          
            });
              $('.toggle_nav1').click(function() {	 
            $('#mainnav').hide()
            });
      

        }


 

  onFaq(){
    this.router.navigate(['/faq']);
  }

  onMain(){
    this.globalService.showMentor = 1;
    this.router.navigate(['']);
  }

}
