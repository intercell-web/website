import 'rxjs/add/operator/finally';
import { Component, OnInit , Inject } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import * as AppConst from '../app.const';
import { DOCUMENT, Title,Meta } from '@angular/platform-browser';
import { LandingService } from '../services/landing.service';
import { JwtHelper } from '../services/jwt.service';
import { GlobalService } from '../services/global.service';
import { environment } from '../../environments/environment';
declare var gapi: any;


@Component({
  selector: 'app-network',
  templateUrl: './network.component.html',
  styleUrls: ['./network.component.scss']
})
export class NetworkComponent implements OnInit {

  
  isLoading: boolean;
  serviceDetails: any = [];
  meetCounsellorDetails: any = [];
  studentSpeakDetails:any = [];
  myConst: any;
  formSearch: FormGroup;
  formCounsellor: FormGroup;
  consellorId: number;
  popFullName: string;
  popUserPic: string;
  patchDetails: any = [];
  incorrectDetails: any = false;
  cdnPath: any = environment.cdnProfileUrl;
  userId: any = +localStorage.getItem('userId');
  userType: any = +localStorage.getItem('userType');

  constructor(@Inject(DOCUMENT) private document,private title: Title, private meta: Meta, private globalService: GlobalService, private loginservice : LoginService, private landingService: LandingService , private router: Router , private jwtHelper : JwtHelper,private fb: FormBuilder) {
    localStorage.setItem('serviceDetails', '');
    localStorage.setItem('showTrackId', '');
    localStorage.setItem('showServiceId', '');
  }

  ngOnInit() {

  
    $(window).scroll(function(){

      if ($(this).scrollTop() > 280) {
        $('#mentee-part').addClass('animated fadeInUp');
      }

      if ($(this).scrollTop() > 700) {
        $('#middle-part').addClass('animated fadeInDown');
      }

      if ($(this).scrollTop() > 1000) {
        $('#mentor-part').addClass('animated fadeInUp');
      }

      if ($(this).scrollTop() > 1700) {
        $('.bottom-part').addClass('animated fadeInUp');
      }

      if ($(this).scrollTop() > 280) {
        $('#stream-part').addClass('animated fadeInUp');
      }

    });


    this.myConst = AppConst;



    //fetching Services Data
    this.landingService.getMeetCounsellor()
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status ==1))
        {
          this.meetCounsellorDetails = response.response;
          //this.router.navigate(['/home']);
        } else {
          console.log("There is Some Error");
        }
      });

      if(this.globalService.showMentor) {

        $('html, body').animate({ scrollTop: $('#mentor-part').offset().top+640 }, 'slow');


       /* $('html,body').animate({
          scrollTop: $('#mentor-scroll').offset().top
        }, 1000);*/

        //document.getElementById('mentor-scroll').scrollIntoView(true);

        /*$('html, body').animate({
          scrollTop: $('#mentor-scroll').offset().top
          //scrollTop: $("#mentor-scroll").position().top
        }, 1000);*/

      }

    this.globalService.showMentor = 0;
  }
  
//ngOnInit end 


}

