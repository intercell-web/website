import { Component, OnInit } from '@angular/core';
import * as AppConst from '../app.const';
import {FormGroup, FormControl, FormBuilder, Validators, Form} from '@angular/forms';
import { LoginService } from '../services/login.service';
import { CounsellorService } from '../services/counsellor.service';
import { GlobalService } from '../services/global.service';
import { Router } from '@angular/router';
import { QuoteService } from '../services/quote.service';
declare var jQuery: any;
import { environment } from '../../environments/environment';
import { JwtHelper } from '../services/jwt.service';


@Component({
  selector: 'app-basic-details',
  templateUrl: './basic-details.component.html',
  styleUrls: ['./basic-details.component.scss']
})

export class BasicDetailsComponent implements OnInit {
  myConst: any;
  form: FormGroup;
  formEducation: FormGroup;
  emailError: boolean = false;
  phoneError: boolean = false;
  services: any;
  subservices: any;
  confirmPasswordError: boolean = false;
  barLabel: string = 'Strength';
  termUrl: any = environment.termUrl;
  formCounsellingStudent: FormGroup;
  formCounsellingCounsellor: FormGroup;
  formExperience: FormGroup;
  timer: any = null;
  selectedSubServices: any = [];
  subServiceList = [];
  subRequireCounselling = [];
  selectedSubRequire: any =[];
  emailInvalid: boolean = false;
  emailVerified: boolean = false;
  phoneVerified: boolean = false;
  phoneInvalid: boolean = false;
  phoneCode:any;
  mailCode:any;
  condtionsAgree: boolean =false;
  formArray: any = {};
  formExperienceArray : any = {};
  formEducationArray : any = {};
  formCounsellingStudentArray : any = {};
  formCounsellingCounsellorArray : any = {};
  subLoop : any;
  counsellorSubfield : any;
  resMail: boolean = false;
  resPhone: boolean = false;
  noneNeedSubField: boolean =false;
  emailTimer:any = null;
  sizeLimit = 2000000;
  imageUrl : any;
  mypassword : any;
  showDOB: boolean = false;
  hasBaseDropZoneOver: boolean = false;
  showCounsellingSub : boolean = false;
  showStudReq: boolean = false;
  currentScreen: any ="#step-1";
  eduYearValidation: boolean = false;
  expYearValidation: boolean = false;
  previousPhone: any;
  previousMail: any;
  cdnPath: any = environment.cdnProfileUrl;
  emailMessage: any = false;

  options: Object = {
    url: 'http://localhost/intercell/api/v1/core/Basic/upload',
  };

  dropdownSettings= {
    singleSelection: false,
    text:"Select Specialisation",
    selectAllText:'Select All',
    unSelectAllText:'UnSelect All',
    enableSearchFilter: true,
    classes:"myclass custom-class"
  };

  formErrors = {
    firstname : '',
    lastname : '',
    emailid : '',
    password : '',
    cpassword : '',
    mobile : ''
  };


  formExperienceErrors = {
  };

  formCounsellingErrors = {
    linkedInLink : ''
  };

  validationMessages = {
    emailid : {
      required : 'Email address is mandatory',
      minlength : 'Please enter a valid email address',
      maxlength : 'Max length cant be more then 30 characters'
    },
    password : {
      required : 'Password is mandatory',
      minlength : 'Your password must be minimum 6 characters',
      maxlength : 'Your password must not exceed 20 characters',
      pattern : 'Your password must have a minimum of 1 digit and 1 special character'
    },
    firstname : {
      required : 'You must enter your First Name'
    },
    lastname : {
      required : 'You must enter your Last Name'
    },
    cpassword : {
      required : 'Confirm Password is mandatory'
    },
    mobile : {
      required : 'Contact number is mandatory',
      minlength : 'Your contact number should be of 10 digits',
      maxlength : 'Your contact number should be of 10 digits'
    },
    gender : {
      required : 'You must select one option from the menu',
      minlength : 'Minimum length must be 3 characters',
    },
    month : {
      required : 'You must enter your month of birth',
    },
    year : {
      required : 'You must enter your year of birth',
    },
    day : {
      required : 'Your must enter your date of birth',
    },
    usertype: {
      required : 'Please select one option from the menu',
    },
    linkedInLink : {
    pattern: 'Invalid web page entered'}
  }

  constructor(private globalService: GlobalService , private counsellorservice: CounsellorService, private router: Router, private quoteService: QuoteService, private loginservice: LoginService , private fb: FormBuilder , private jwtHelper: JwtHelper) {
  }


  ngOnInit() {
    $("html, body").animate({ scrollTop: 0 }, 'slow');
    jQuery('#step-7').fadeOut(0);
    jQuery('#step-6').fadeOut(0);
    jQuery('#step-5').fadeOut(0);
    jQuery('#step-4').fadeOut(0);
    jQuery('#step-3').fadeOut(0);
    jQuery('#step-2').fadeOut(0);
    jQuery('#step-8').fadeOut(0);
    this.myConst = AppConst;
    this.buildForm();

    jQuery('#datePicker').datepicker({format: 'dd/mm/yyyy',endDate: new Date(),autoclose: true});

    jQuery('#dateValue').change(() => {
      this.form.patchValue({dob: jQuery('#dateValue').datepicker({dateFormat: 'dd/mm/yyyy',autoclose: true,endDate: new Date()}).val()});
    });

    jQuery('.checkmarkPhone').hide();
    jQuery('.checkmarkMail').hide();

    //fetching All Services
    this.counsellorservice.getAllServices()
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          this.services = response.response;
        } else {
        }
      });


    //fetching All Sub Services
    this.counsellorservice.getAllSubServices()
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          this.subservices = response.response;
        } else {
        }
      });

    $('form :input').attr("autocomplete", "off");
  }

  buildForm() {
    this.form = this.fb.group({
      firstname : ['', Validators.compose([Validators.required])],
      lastname : ['', Validators.compose([Validators.required])],
      emailid : ['', Validators.compose([Validators.required , Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$')])],
      password : ['', Validators.compose([Validators.required,  Validators.minLength(6) , Validators.maxLength(20) , Validators.pattern('^(?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*?[.~#?!@$%^&*-]).{3,}$')])],
      cpassword : ['', Validators.compose([Validators.required])],
      usertype : ['' , Validators.compose([Validators.required])],
      mobile : ['' , Validators.compose([Validators.required , Validators.pattern('^[0-9]{10}$')])],
      dob : [{value: '' , disabled: true} , Validators.compose([Validators.required])],
    });


    this.formEducation = new FormGroup({
      school : new FormControl('' , Validators.compose([Validators.required])),
      degree : new FormControl('' , Validators.compose([Validators.required])),
      startTime : new FormControl('' , Validators.compose([Validators.required])),
      endTime : new FormControl('' , Validators.compose([Validators.required])),
      field : new FormControl('' , Validators.compose([Validators.required])),
      description : new FormControl('' , Validators.compose([])),
      educationId : new FormControl(''),
    });

    this.formCounsellingStudent = new FormGroup({
      requireField : new FormControl('' , Validators.compose([Validators.required])),
      requireSubField : new FormControl('' , Validators.compose([]))
    });

    this.formCounsellingCounsellor = new FormGroup({
      currentField : new FormControl('' , Validators.compose([Validators.required])),
      currentSubField : new FormControl({value: '' , disabled:true} , Validators.compose([Validators.required])),
      linkedInLink : new FormControl('' , Validators.compose([Validators.pattern('^http(s)?:\/\/([w]{3}\.)?linkedin\.com\/in\/([a-zA-Z0-9-]{5,30})\/?')]))
    });

    this.formExperience = new FormGroup({
      title : new FormControl('' , Validators.compose([Validators.required])),
      company : new FormControl('' , Validators.compose([Validators.required])),
      location : new FormControl('' , Validators.compose([Validators.required])),
      description : new FormControl(''),
      startTimeYear : new FormControl('' , Validators.compose([Validators.required])),
      endTimeYear : new FormControl({value: '' , disabled: false} , Validators.compose([Validators.required])),
      startTimeMonth : new FormControl('' , Validators.compose([Validators.required])),
      endTimeMonth : new FormControl({value: '' , disabled: false} , Validators.compose([Validators.required])),
      //current : new FormControl(''),
      experienceId : new FormControl('')
    });

    this.form.valueChanges.subscribe(data => this.validateForm());
    this.formExperience.valueChanges.subscribe(data => this.validateExperienceForm());
    this.formCounsellingCounsellor.valueChanges.subscribe(data => this.validateCounsellingForm());
  }

  validateCounsellingForm(){
    for(let field in this.formCounsellingErrors){
      this.formCounsellingErrors[field]= '';

      let input = this.formCounsellingCounsellor.get(field);

      if(input.invalid && input.dirty){
        for(let error in input.errors){
          this.formCounsellingErrors[field] = this.validationMessages[field][error];
        }
      }
    }
  }



  showEmailMessage () {
    this.emailMessage = true;
  }

  removeEmailMessage() {
    this.emailMessage = false;
  }


  validateForm(){
     for(let field in this.formErrors) {

       if(field == 'mobile' || field == 'emailid'){

       } else {
         this.formErrors[field] = '';

         this.confirmPasswordError = false;
         this.phoneError = false;
         //this.userNameError =false;
         this.emailError = false;

         let input = this.form.get(field);

         if (input.invalid && input.dirty) {
           for (let error in input.errors) {
             this.formErrors[field] = this.validationMessages[field][error];
           }
         }
       }

    }
  }

  onlyNumberKey(event) {
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  }

  validateExperienceForm(){
    for(let field in this.formExperienceErrors){
      this.formExperienceErrors[field]= '';

      let input = this.formExperience.get(field);

      if(input.invalid && input.dirty){
        for(let error in input.errors){
          this.formExperienceErrors[field] = this.validationMessages[field][error];
        }
      }
    }
  }


  fileOverBase(e: any) : void {
    this.hasBaseDropZoneOver = e;
  }


  onInput= function() {

    if(jQuery('#education_start_time').val() && jQuery('#education_end_time').val()){

    if(jQuery('#education_start_time').val() > jQuery('#education_end_time').val()){
      this.eduYearValidation = true;
    } else {
      this.eduYearValidation = false;
    }
    } else {
      this.eduYearValidation = false;
    }
  }


  onExpInput = function() {
    $("html, body").animate({ scrollTop: 0 }, 'slow');

    if(jQuery("#expStartYear").val() && jQuery("#expEndYear").val() && jQuery("#expStartMonth").val() && jQuery("#expEndMonth").val()){
      if(jQuery("#expStartYear").val() > jQuery("#expEndYear").val()){
        this.expYearValidation=!this.expYearValidation;
      } else if(jQuery('#expStartYear').val() == jQuery('#expEndYear').val()) {

        if($('#expStartMonth option:selected').attr('id') >  $('#expEndMonth option:selected').attr('id')) {
          this.expYearValidation = true;
        } else {
          this.expYearValidation = false;
        }
      }else {
        this.expYearValidation = false;
      }
    } else {
      this.expYearValidation = false;
    }
  }




  onEducationPrevious = function() {
    $("html, body").animate({ scrollTop: 0 }, 'slow');

    jQuery("#step-2").hide();
      jQuery("#step-1").removeClass('hide');
      jQuery("#step-1").fadeIn(700);
      this.currentScreen = "#step-1";
  }


  onCounsPrevious = function() {
    $("html, body").animate({ scrollTop: 0 }, 'slow');

    jQuery("#step-3").hide();
    jQuery("#step-2").removeClass('hide');
    jQuery("#step-2").fadeIn(700);
    this.currentScreen = "#step-2";

  }

  onCancel = function(off){
    $("html, body").animate({ scrollTop: 0 }, 'slow');

    jQuery(this.currentScreen).hide();
    jQuery("#step-8").removeClass('hide');
    jQuery("#step-8").fadeIn(700);
  }

  onReturn = function(off){
    $("html, body").animate({ scrollTop: 0 }, 'slow');

    jQuery("#step-8").hide();
    jQuery(this.currentScreen).removeClass('hide');
    jQuery(this.currentScreen).fadeIn(700);
  }

  onStudCounsPrevious = function() {
    $("html, body").animate({ scrollTop: 0 }, 'slow');

    jQuery("#step-4").hide();
    jQuery("#step-2").removeClass('hide');
    jQuery("#step-2").fadeIn(700);
    this.currentScreen = "#step-2";
  }

  onExpPrevious = function() {
    $("html, body").animate({ scrollTop: 0 }, 'slow');

    jQuery("#step-5").hide();
    jQuery("#step-3").removeClass('hide');
    jQuery("#step-3").fadeIn(700);
    this.currentScreen = "#step-3";
  }

  onPicPrevious = function() {
    $("html, body").animate({ scrollTop: 0 }, 'slow');

    jQuery("#step-6").hide();

    if(this.globalService.userType == 3) {
      jQuery("#step-5").removeClass('hide');
      jQuery("#step-5").fadeIn(700);
      this.currentScreen = "#step-5";
    } else {
      jQuery("#step-4").removeClass('hide');
      jQuery("#step-4").fadeIn(700);
      this.currentScreen = "#step-4";
    }
  }

  onValidPrevious = function() {
    $("html, body").animate({ scrollTop: 0 }, 'slow');
    jQuery("#step-7").hide();

    if(this.globalService.userType == 3) {
      jQuery("#step-6").removeClass('hide');
      jQuery("#step-6").fadeIn(700);
      this.currentScreen = "#step-6";
    } else {
      jQuery("#step-6").removeClass('hide');
      jQuery("#step-6").fadeIn(700);
      this.currentScreen = "#step-6";
    }
  }


  onEducationSubmit = function(education) {
    $("html, body").animate({ scrollTop: 0 }, 'slow');
    jQuery("#step-2").hide();

    if(this.globalService.userType == 3)
    {
      jQuery("#step-3").removeClass('hide');
      jQuery("#step-3").fadeIn(700);
      this.currentScreen = "#step-3";
    } else if(this.globalService.userType == 2)
    {
      jQuery("#step-4").removeClass('hide');
      jQuery("#step-4").fadeIn(700);
      this.currentScreen = "#step-4";
    }
 }

  showBirth = function(dob){
    if(dob){
      this.form.controls.dob.enable();
    } else{
      this.form.controls.dob.disable();
    }
  }

showOffic= function(showOfficial){

if(showOfficial){
  this.formExperience.controls.endTimeMonth.disable();
  this.formExperience.controls.endTimeYear.disable();
  }else{
    this.formExperience.controls.endTimeMonth.enable();
    this.formExperience.controls.endTimeYear.enable();

}

}

  verifyMail = function (){
    //call services for sending Code
    this.loginservice.confirmMailCode(this.form.controls.emailid.value,this.mailCode)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          this.emailInvalid = false ;
          this.emailVerified = true;
          jQuery('.checkmarkMail').show();

        } else {
          this.mailCode = '';
          this.emailInvalid = true ;

          setTimeout(() => {
            this.emailInvalid = false;
          }, 3000);
        }
      });

  };

  resendMail = function (){

 if(this.mailCode){
    //call services for sending Code
    this.loginservice.resendMailCode(this.form.controls.emailid.value)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          this.emailVerified=false;
          this.emailInvalid=false;
          this.resMail=true;
          this.mailCode = "";

          setTimeout(()=>{
            this.resMail=false;
          }, 3000);

        } else {
          this.emailError=true;
        }
      });

      }
  };

  verifyPhone = function (){

 if(this.phoneCode){
    //call services for sending Code
    this.loginservice.confirmPhoneCode(this.form.controls.mobile.value, this.phoneCode)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          this.phoneVerified = true;
          jQuery('.checkmarkPhone').show();
          this.phoneInvalid = false ;
        } else {
          this.phoneInvalid = true ;
          this.phoneCode ="";

          setTimeout(()=>{
            this.phoneInvalid = false;
          }, 3000);

        }
      });

      }
  };



  resendPhone = function (){

    //call services for sending Code
    this.loginservice.resendPhoneCode(this.form.controls.mobile.value)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          this.phoneVerified=false;
          this.phoneInvalid=false;
          this.resPhone=true;
          this.phoneCode ="";

          setTimeout(()=>{
            this.resPhone=false;
          }, 3000);

        } else {
          this.emailError=true;
        }
      });
  };


  onServiceChange = function (event){
    this.subServiceList = this.subservices[event];

    if (this.subServiceList == undefined){
      this.showCounsellingSub = false;
      this.formCounsellingCounsellor.controls.currentSubField.disable();
    }else{
      this.showCounsellingSub= true;
      this.formCounsellingCounsellor.controls.currentSubField.enable();
    }

    this.selectedSubServices = [];
  };


  onRequireServiceChange = function (event){
    if(event=="none"){
      this.noneNeedSubField = true;
    }else {
      this.noneNeedSubField = false;
      this.subRequireCounselling = this.subservices[event];

      if (this.subRequireCounselling == undefined){
        this.showStudReq = false;
        this.formCounsellingStudent.controls.requireSubField.disable();
      }else{
        this.showStudReq= true;
        this.formCounsellingStudent.controls.requireSubField.enable();
      }

      this.selectedSubRequire = [];
    }
  };

  onSubServiceChange = function (event){
  };

  onCounsellingStudent = function(counselling) {
    jQuery("#step-4").hide();
    jQuery("#step-6").removeClass('hide');
    jQuery("#step-6").fadeIn(700);
    $("html, body").animate({ scrollTop: 0 }, 'slow');
  }

  onFinish = function() {

    this.subLoop = this.formCounsellingCounsellor.controls['currentSubField'].value;

    for (var i = 0; i < this.subLoop.length; i++) {

      if(i==0){
        this.counsellorSubfield = this.subLoop [i].id ;
      } else {
        this.counsellorSubfield += ',' + this.subLoop [i].id ;
      }
    }


    for (var key in this.form.controls) {
      this.formArray[key] = this.form.controls[key].value;
    }

    for (var key in this.formExperience.controls) {
      this.formExperienceArray[key] = this.formExperience.controls[key].value;
    }

    for (var key in this.formEducation.controls) {
      this.formEducationArray[key] = this.formEducation.controls[key].value;
    }

    for (var key in this.formCounsellingCounsellor.controls) {
      this.formCounsellingCounsellorArray[key] = this.formCounsellingCounsellor.controls[key].value;
    }

    this.formCounsellingCounsellorArray['subfield'] =  this.counsellorSubfield;

    for (var key in this.formCounsellingStudent.controls) {
      this.formCounsellingStudentArray[key] = this.formCounsellingStudent.controls[key].value;
    }

    //call services for sending Code
    this.loginservice.createUser(this.formArray, this.formExperienceArray, this.formEducationArray, this.formCounsellingCounsellorArray, this.formCounsellingStudentArray,jQuery("#awsfileName").val())
      .finally(() => { })
      .subscribe((response: any) => {
        if((response.status == 1) && (response.type === 'success'))
        {

          this.data = this.jwtHelper.decodeToken(response.response.response);
          localStorage.setItem('id_token', response.response.response);
          this.globalService.id_token = response.response.response;

          this.globalService.userId = this.data.id;
          localStorage.setItem('userId', this.data.id);

          this.globalService.newUser = 0;
          localStorage.setItem('newUser', '0');
          localStorage.setItem('userType', this.form.controls.usertype.value);

          this.globalService.userType = this.form.controls.usertype.value;

          if(jQuery("#awsfileName").val()){
            this.globalService.userPic = this.cdnPath + jQuery("#awsfileName").val();
          } else {
            this.globalService.userPic ="https://d3213zi13us7f1.cloudfront.net/website/assets/images/intercell-user-pic.png";
          }

          localStorage.setItem('userPic', this.globalService.userPic);

          if(this.globalService.userType == 3)
          {
            this.globalService.userTypeText = 'Mentor';
            localStorage.setItem('userTypeText','Mentor');
            this.router.navigate(['/counsellor']);
          } else if(this.globalService.userType == 2)
          {
            this.globalService.userTypeText = 'Mentee';
            localStorage.setItem('userTypeText','Mentee');
            this.router.navigate(['/student']);
          }

          //this.router.navigate(['/details']);
        } else {

        }
      });
  }

  onCounsellingCounsellor = function(counselling) {
    jQuery("#step-3").hide();
    jQuery("#step-5").removeClass('hide');
    jQuery("#step-5").fadeIn(700);

    $("html, body").animate({ scrollTop: 0 }, 'slow');
  }

  onPicture = function() {
    jQuery("#step-6").hide();

    $("html, body").animate({ scrollTop: 0 }, 'slow');

    if(this.previousPhone!=this.form.controls.mobile.value ) {

      //call services for sending Code
      this.loginservice.sendPhoneCode(this.form.controls.mobile.value)
        .finally(() => {
        })
        .subscribe((response: any) => {

          if ((response.status == 1) && (response.type === 'noRecord')) {
            jQuery('.checkmarkPhone').hide();
            this.phoneVerified = false;
            this.phoneCode = '';
            this.previousPhone = this.form.controls.mobile.value;
          } else if ((response.status == 1) && (response.type === 'success')) {
            this.emailError = false;
          } else {
            this.emailError = true;
          }

        });
    }

    if(this.previousMail!=this.form.controls.emailid.value ) {
      //call services for sending Code
      this.loginservice.sendMailCode(this.form.controls.emailid.value)
        .finally(() => {
        })
        .subscribe((response: any) => {

          if ((response.status == 1) && (response.type === 'noRecord')) {
            jQuery('.checkmarkMail').hide();
            this.emailVerified = false;
            this.mailCode = '';
            this.previousMail = this.form.controls.emailid.value;
          } else if ((response.status == 1) && (response.type === 'success')) {
            this.emailError = false;
          } else {
            this.emailError = true;
          }
        });
    }

    jQuery('#step-7').removeClass('hide');
    jQuery('#step-7').fadeIn(700);

    $('html, body').animate({ scrollTop: 0 }, 'slow');
  }

  selectAllContent($event) {
    $event.target.select();
  }

  onExperience = function() {
    $("html, body").animate({ scrollTop: 0 }, 'slow');

    jQuery('#step-5').hide();
    jQuery('#step-6').removeClass('hide');
    jQuery('#step-6').fadeIn(700);
  }

  onDone = function(user) {
    this.isLoading = true;
    if (this.form.controls.password.value === this.form.controls.cpassword.value) {

      $('html, body').animate({ scrollTop: 0 }, 'slow');

      jQuery('#step-1').hide();
      //jQuery("#step-2").removeClass('hide');
      jQuery('#step-2').fadeIn(700);

      this.userType = user.usertype;
      this.globalService.newUser = 0;
      this.globalService.userType = this.userType;

    } else {
      this.confirmPasswordError = true;
    }
  }


  onPassword = function(){
    jQuery('#strength').show();
  }

  onPasswordOut = function(){
    jQuery('#strength').hide();
  }


  onEmailOut = function(){
    clearTimeout(this.emailTimer);
    if(this.form.controls.emailid.valid){
      clearTimeout(this.timer);
      this.formErrors.emailid ="";

      this.timer = setTimeout(()=>{
        //check unique EmailId
        this.loginservice.checkLoginEmail(this.form.controls.emailid.value)
          .finally(() => { })
          .subscribe((response: any) => {

            if((response.status == 1) && (response.type === 'success'))
            {
              this.emailError=false;
            } else {
              this.emailError=true;
            }
          });

      }, 700);
    } else {
      this.emailTimer = setTimeout(()=>{
        this.formErrors.emailid ="Invalid email address";
      }, 1500);
    }
  }


  fileChange(event) {
    let fileList: FileList = event.target.files;

    if (fileList.length > 0) {
      let file: File = fileList[0];
      document.getElementById('image_user').setAttribute('src', window.URL.createObjectURL(file));
    }
  }


  onPhoneOut = function(){
    clearTimeout(this.phoneTimer);

    if(this.form.controls.mobile.valid){
      clearTimeout(this.timer);
      this.formErrors.mobile ="";
      this.timer = setTimeout(()=>{
        //check unique EmailId
        this.loginservice.checkPhone(this.form.controls.mobile.value)
          .finally(() => { })
          .subscribe((response: any) => {

            if((response.status == 1) && (response.type === 'success'))
            {
              this.phoneError=false;
            } else {
              this.phoneError=true;
            }
          });
      }, 700);
    } else {
      this.phoneTimer = setTimeout(() => {
        this.formErrors.mobile ="Your contact number should be of 10 digits";
      }, 1500);
    }
  }

  onTerm = function () {
    localStorage.setItem('termType',  this.form.controls.usertype.value);
    window.open(this.termUrl, '_blank');
    //this.router.navigate(['/term']);
  }

}
