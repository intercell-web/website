import { Component, OnInit , Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { ResetPasswordService } from '../services/reset-password.service';
import {FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { JwtHelper } from '../services/jwt.service';

declare var jQuery: any;



@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})

export class ChangePasswordComponent implements OnInit {

  emailId: any;
  userId: any;
  validKey: any;
  insertId: any;
  keyError: boolean = false;
  passError: boolean = false;
  password: any;
  cpassword: any;
  formPassword : any;
  token:any;
  data:any;

  formPasswordErrors = {
    password : '',
    cpassword : ''
  };

  validationMessages = {
    password : {
      required : 'Password is mandatory',
      minlength : 'Your password must be minimum 6 characters',
      maxlength : 'Your password must not exceed 20 characters',
      pattern : 'Password must have a minimum of 1 digit and 1 special character'
    },
    cpassword : {
      required: 'Confirm Password is mandatory'
    }
  }

  constructor(@Inject(DOCUMENT) private document, private route: ActivatedRoute ,private jwtHelper : JwtHelper,private router: Router, private fb: FormBuilder,private resetPasswordService: ResetPasswordService) {
  }

  ngOnInit() {
    this.buildForm();

    this.route.params.subscribe(params => {
      this.token = params['token'];
      this.data = this.jwtHelper.decodeToken(this.token);
      this.userId = this.data.user_id ;
      this.insertId = this.data.insertId ;
    });
    

  }

  buildForm() {
    this.formPassword = this.fb.group({
      password : ['', Validators.compose([Validators.required,  Validators.minLength(6) , Validators.maxLength(20) , Validators.pattern('^(?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*?[.~#?!@$%^&*-]).{3,}$')])],
      cpassword : ['', Validators.compose([Validators.required])]
    });

    this.formPassword.valueChanges.subscribe(data => this.validatePasswordForm());
  }


  validatePasswordForm() {
    for(let field in this.formPasswordErrors){
      this.formPasswordErrors[field]= '';

      let input = this.formPassword.get(field);

      if(input.invalid && input.dirty){
        for(let error in input.errors){
          this.formPasswordErrors[field] = this.validationMessages[field][error];
        }
      }
    }
  }

  //jQuery('.forgot-blk1').slideDown();

  onCheck = function() {
    this.resetPasswordService.onCheck(this.userId, this.validKey)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          jQuery('.forgot-blk1').hide();
          jQuery('.forgot-blk2').slideDown();

        } else {
          this.keyError = true;
        }
      });
  }


  onReset = function() {
    

    if(this.formPassword.controls.password.value ==undefined || this.formPassword.controls.password.value =='' ){
      this.formPasswordErrors.password = 'Please Enter New Password Password';
      return false;
  
      }else if(this.formPassword.controls.cpassword.value ==undefined || this.formPassword.controls.cpassword.value =='' ){
        this.formPasswordErrors.password = 'Please Enter Confirm Password';
        return false;
    
        }else if(this.formPassword.controls.password.value != this.formPassword.controls.cpassword.value  ){
          this.formPasswordErrors.password = 'Password Mismatch';
          return false;
      
          }else{
            this.resetPasswordService.onReset(this.password, this.userId, this.insertId)
            .finally(() => {
            })
            .subscribe((response: any) => {
    
              if ((response.status == 1) && (response.type === 'success')) {
                jQuery('.forgot-blk2').hide();
                jQuery('.forgot-blk3').slideDown();
              } else {
              }
            });
          
          }
       
      }
}
