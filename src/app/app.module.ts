import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { PasswordStrengthBarModule } from 'ng2-password-strength-bar';
import { ImageUploadModule } from 'angular2-image-upload';
import { Ng2UploaderModule } from 'ng2-uploader';
import { ModalModule } from 'ng2-modal';
import * as $ from "jquery";

import { AppComponent } from './app.component';

//Module lists
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { HomeModule } from './home/home.module';
import { AboutModule } from './about/about.module';
import { LoginModule } from './login/login.module';
import { AppointmentsModule } from './appointments/appointments.module';
import { MakeAppointmentModule } from './make-appointment/make-appointment.module';
import { DirectAppointmentModule } from './direct-appointment/direct-appointment.module';
import { ResheduleAppointmentModule } from './reshedule-appointment/reshedule-appointment.module';
import { SetAvailabilityModule } from './set-availability/set-availability.module';

//component lists

import { AboutUsComponent } from './about-us/about-us.component';
import { DownloadAppComponent } from './download-app/download-app.component';
import { ReferFriendComponent } from './refer-friend/refer-friend.component';
import { CareersComponent } from './careers/careers.component';
import { FooterComponent } from './footer/footer.component';
import { FaqComponent } from './faq/faq.component';
import { TermComponent } from './term/term.component';
import { NewsComponent } from './news/news.component';
import { StudentComponent } from './student/student.component';
import { CounsellorComponent } from './counsellor/counsellor.component';
import { LoginComponent } from './login/login.component';
import { AccountComponent } from './account/account.component';
import { OverviewComponent } from './overview/overview.component';
import { SettingComponent } from './setting/setting.component';
import { MessageComponent } from './message/message.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { FavoriteComponent } from './favorite/favorite.component';
import { LoginHeaderComponent } from './login-header/login-header.component';
import { PaymentComponent } from './payment/payment.component';
import { PaymentCancelComponent } from './payment-cancel/payment-cancel.component';
import { GetPaymentComponent } from './get-payment/get-payment.component';
import { StreamComponent } from './stream/stream.component';
import { TrackComponent } from './track/track.component';
import { PaymentSuccessComponent } from './payment-success/payment-success.component';
import { CounsellorProfileComponent } from './counsellor-profile/counsellor-profile.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { DetailComponent } from './detail/detail.component';
import { BasicDetailsComponent } from './basic-details/basic-details.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { AboutStudentComponent } from './about-student/about-student.component';
import { AboutCounsellorComponent } from './about-counsellor/about-counsellor.component';
//import { GoogleTestComponent } from './google-test/google-test.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { LearnMentorComponent } from './learn-mentor/learn-mentor.component';
import { LearnMenteeComponent } from './learn-mentee/learn-mentee.component';
import { SearchMentorComponent } from './search-mentor/search-mentor.component';
import { SignuphomeComponent } from './signuphome/signuphome.component';
import { ContactusComponent } from './contactus/contactus.component';

//service lists
import { LoginService } from './services/login.service';
import { StudentService } from './services/student.service';
import { CounsellorService } from './services/counsellor.service';
import { AvailabilityService } from './services/availability.service';
import {GlobalService} from './services/global.service';
import {QuoteService} from './services/quote.service';
import { LandingService } from './services/landing.service';
import { JwtHelper } from './services/jwt.service';
import { PaymentService } from './services/payment.service';
import { BasicService } from './services/basic.service';
import { AuthGuard } from './services/auth-guard.service';
import { MessageService } from './services/message.service';
import { DetailService } from './services/detail.service';
import { ResetPasswordService } from './services/reset-password.service';

//Pipe  lists
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { NetworkComponent } from './network/network.component';
import { ServicesliderComponent } from './slider/serviceslider/serviceslider.component';
import { MentopanelsliderComponent } from './slider/mentopanelslider/mentopanelslider.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RelatedmentorComponent } from './slider/relatedmentor/relatedmentor.component';
import { ImageCarouselComponent } from './slider/image-carousel/image-carousel.component';
import { ServicesCarouselComponent } from './slider/services-carousel/services-carousel.component';
import { RelatedMentorCarouselComponent } from './slider/related-mentor-carousel/related-mentor-carousel.component';
import { AppointmentsDetailsComponent } from './appointments-details/appointments-details.component';
import { InnerFooterComponent } from './inner-footer/inner-footer.component';
import { NeedHelpComponent } from './need-help/need-help.component';
import { IntercellStoryComponent } from './intercell-story/intercell-story.component';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    TranslateModule.forRoot(),
    NgbModule.forRoot(),
    CoreModule,
    SharedModule,
    HomeModule,
    AboutModule,
    AppointmentsModule,
    MakeAppointmentModule,
    DirectAppointmentModule,
    ResheduleAppointmentModule,
    SetAvailabilityModule,
    LoginModule,
    AppRoutingModule,
    ModalModule,
    ReactiveFormsModule,
    AngularMultiSelectModule,
    InfiniteScrollModule,
    PasswordStrengthBarModule,
    ImageUploadModule.forRoot(),
    Ng2UploaderModule,    
  ],
  declarations: [
    AppComponent,
    AboutUsComponent,
    DownloadAppComponent,
    ReferFriendComponent,
    CareersComponent,
    FooterComponent,
    FaqComponent,
    TermComponent,
    NewsComponent,
    StudentComponent,
    LoginComponent,
    CounsellorComponent,
    AccountComponent,
    OverviewComponent,
    //SetAvailabilityComponent,
    SettingComponent,
    MessageComponent,
    ProfileComponent,
    FavoriteComponent,
    LoginHeaderComponent,
    HomeComponent,
    PaymentComponent,
    PaymentCancelComponent,
    GetPaymentComponent,
    StreamComponent,
    TrackComponent,
    PaymentSuccessComponent,
    CounsellorProfileComponent,
    ResetPasswordComponent,
    ChangePasswordComponent,
    DetailComponent,
    BasicDetailsComponent,
    PrivacyComponent,
    AboutStudentComponent,
    AboutCounsellorComponent,
    //GoogleTestComponent,
    FeedbackComponent,
    LearnMentorComponent,
    LearnMenteeComponent,
    SearchMentorComponent,
    SignuphomeComponent,
    SafeHtmlPipe,
    NetworkComponent,
    ContactusComponent,
    ServicesliderComponent,
    MentopanelsliderComponent,
    DashboardComponent,
    RelatedmentorComponent,
    ImageCarouselComponent,
    ServicesCarouselComponent,
    RelatedMentorCarouselComponent,
    AppointmentsDetailsComponent,
    InnerFooterComponent,
    NeedHelpComponent,
    IntercellStoryComponent    
],

  providers: [
    AvailabilityService,
    LoginService,
    StudentService,
    CounsellorService,
    QuoteService,
    GlobalService,
    BasicService,
    LandingService,
    JwtHelper,
    PaymentService,
    AuthGuard,
    MessageService,
    ResetPasswordService,
    DetailService
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
