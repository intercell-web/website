import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LearnMenteeComponent } from './learn-mentee.component';

describe('LearnMenteeComponent', () => {
  let component: LearnMenteeComponent;
  let fixture: ComponentFixture<LearnMenteeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LearnMenteeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LearnMenteeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
