import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from './../services/global.service';

@Component({
  selector: 'app-learn-mentee',
  templateUrl: './learn-mentee.component.html',
  styleUrls: ['./learn-mentee.component.scss']
})
export class LearnMenteeComponent implements OnInit {

  isLogin: any = localStorage.getItem('userId');
  constructor(private globalService: GlobalService , private router: Router) { }

  ngOnInit() {
    $('html, body').animate({ scrollTop: 0 }, 'slow');
  }

  onMain(){
    this.globalService.showMentor = 1;
    this.router.navigate(['']);
  }

}
