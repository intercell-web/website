import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutCounsellorComponent } from './about-counsellor.component';

describe('AboutCounsellorComponent', () => {
  let component: AboutCounsellorComponent;
  let fixture: ComponentFixture<AboutCounsellorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutCounsellorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutCounsellorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
