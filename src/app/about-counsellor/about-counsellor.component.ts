import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title,Meta } from '@angular/platform-browser';
import { GlobalService } from './../services/global.service'
import { LandingService } from '../services/landing.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-about-counsellor',
  templateUrl: './about-counsellor.component.html',
  styleUrls: ['./about-counsellor.component.scss']
})
export class AboutCounsellorComponent implements OnInit {

userType: any;
isLogin: any = localStorage.getItem('userId');

  constructor(private router: Router, private title: Title, private meta: Meta,private globalService: GlobalService, private landingService: LandingService) { 

    // Start to Set Component  Title, Description and Keywords
	  window.scrollTo(0, 0);
this.title.setTitle('About Mentor - Intercellworld.com');
this.meta.updateTag({ name: 'description', content: 'About Mentor' });
this.meta.updateTag({ name: 'keyword', content: 'Intercell' });
// End to Set Component Title, Description and Keywords
  }

  ngOnInit() {
      
          
          $(window).scroll(function(){
      
            if ($(this).scrollTop() > 280) {
              $('#mentee-part').addClass('animated fadeInUp');
            }
      
            if ($(this).scrollTop() > 700) {
              $('#middle-part').addClass('animated fadeInDown');
            }
      
            if ($(this).scrollTop() > 1000) {
              $('#mentor-part').addClass('animated fadeInUp');
            }
      
            if ($(this).scrollTop() > 1700) {
              $('.bottom-part').addClass('animated fadeInUp');
            }
      
            if ($(this).scrollTop() > 280) {
              $('#stream-part').addClass('animated fadeInUp');
            }
            $('.toggle_nav').click(function() {	 
              $('#mainnav').show()
            
              });
                $('.toggle_nav1').click(function() {	 
              $('#mainnav').hide()
              });
      
          });
      
          $("#testimonial").css("background-attachment", "fixed");
   
      
        }

  //add menu/links functionality routering functionality
  aboutFaq(){
    this.router.navigate(['/faq']);
  }
}
