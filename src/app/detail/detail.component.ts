import { Component, OnInit } from '@angular/core';
declare var jQuery: any;
import { GlobalService } from '../services/global.service';
import { DetailService } from '../services/detail.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  userId: any;
  validKey: any;
  keyError: any;

  constructor(private router: Router , private globalService: GlobalService ,private detailService: DetailService) { }

  ngOnInit() {
    this.userId = this.globalService.userId;
  }


  onCheck = function() {
    this.detailService.onCheck(this.userId, this.validKey)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {

          if(this.globalService.userType == 3)
          {
            this.router.navigate(['/counsellor']);
          } else if(this.globalService.userType == 2)
          {
            this.router.navigate(['/student']);
          }

        } else {
          this.keyError = true;
        }
      });
  }
}
