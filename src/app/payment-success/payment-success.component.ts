import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment-success.component.html',
  styleUrls: ['./payment-success.component.scss']
})

export class PaymentSuccessComponent implements OnInit {
  userType :any;

  constructor(private globalService: GlobalService , private router: Router) { }

  ngOnInit() {
    this.globalService.userId = +localStorage.getItem("userId");
    this.userType = +localStorage.getItem("userType");
    this.globalService.newUser = +localStorage.getItem("newUser");

    this.globalService.userType = this.userType;
    this.globalService.userFullName = localStorage.getItem("userFullName");
    this.globalService.userTypeText = localStorage.getItem("userTypeText");
    this.globalService.userPic = localStorage.getItem("userPic");
    this.router.navigate(['/appointments']);

  }

}
