import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { AppointmentsService } from './../services/appointments.service';
import { environment } from '../../environments/environment';

declare var swal: any;

@Component({
  selector: 'app-make-appointment',
  templateUrl: './make-appointment.component.html',
  styleUrls: ['./make-appointment.component.scss']
})
export class MakeAppointmentComponent implements OnInit {
    request: any = {};
    params: any = {};
    services: any;
    levelsPeriods: any;
    counsellors: any;
    calendaring: any;
    withSub: boolean =true;
    isLoading: boolean;
    isServicesTab = true;
    isLevelTab= false;
    isExpectationTab = false;
    isCounsellorTab = false;
    isCalendarTab = false;
    isCheckoutTab = false;
    dropdownList= [];
    selectedSubServices: any = [];
    ErrorsuggestionMessages:string='';
    
    dropdownSettings= {};
    expectationMessages: string;
    suggestionMessages: string;
    myConst: any;
    userId: any;
    userType: any;
    userTypeText: string;
    showSubServices: boolean = true;
    prevousArraow: boolean = false;
    nextArraow: boolean = false;
    appointmentTable: boolean = false;
    paymentUrl: any = environment.paymentUrl + '/core/payment/pay_session';
    profileUrl: any = environment.cdnProfileUrl;
    cdnPath: any = environment.cdnProfileUrl;

    item: any= [];
    discount: any= 0.00;
    promoCode: string = '';
    constructor(@Inject(DOCUMENT) private document,private globalService : GlobalService , private router: Router, private appointObj: AppointmentsService) {

      
      if(!(this.globalService.userId)){
        this.globalService.userId = +localStorage.getItem('userId');
      }

      if(!(this.globalService.userType)){
        this.globalService.userType = +localStorage.getItem('userType');

      }
      if(!(this.globalService.userTypeText)){
        this.globalService.userTypeText = localStorage.getItem('userTypeText');
      }
      if(!(this.globalService.userFullName)){
        this.globalService.userFullName = localStorage.getItem('userFullName');
      }

      if(!(this.globalService.userPic)){
        this.globalService.userPic = localStorage.getItem('userPic');
      }

      if(!(this.globalService.id_token)){
        this.globalService.id_token = localStorage.getItem('id_token');
      }

      if(!(this.globalService.newUser)){
        this.globalService.newUser = +localStorage.getItem('newUser');
      }

      if(!(this.globalService.userCountryId)){
        this.globalService.userCountryId = localStorage.getItem('userCountryId');
      }

     
      this.userId = this.globalService.userId;
      this.userType = this.globalService.userType;
      this.userTypeText = this.globalService.userTypeText;
      this.request.studentId = this.userId;
      this.request.priceId = 3;
      this.request.paymentId = 3;
      this.request.duration = 30;
      this.request.genricSubServices = false;
      this.request.expectationMessages = '';
      if(this.userType==3){
        this.router.navigate(['/overview']);
      }
    }

    ngOnInit() {

      this.document.getElementById('menuBook').className = 'active';
      
      $('html, body').animate({ scrollTop: 0 }, 'slow');

      

      $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
          $('.scrollToTop').fadeIn();
        } else {
          $('.scrollToTop').fadeOut();
        }
      });

      //Click event to scroll to top
      $('.scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0}, 800);
        return false;
      });
      
      
      
      this.isLoading = true;
      
      
      this.appointObj.getServices()
        .finally(() => { this.isLoading = false; })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type == 'success'))
          {
            this.services = response.response;
          }
      });

      if (this.globalService.userId) {
        this.userId = this.globalService.userId;
      } else {
        this.userId = localStorage.getItem('userId');
      }

      if (this.globalService.userType) {
        this.userType = this.globalService.userType;
      } else {
        this.userType = localStorage.getItem('userType');
      }

      this.appointObj.getPricing()
        .finally(() => { this.isLoading = false; })
        .subscribe((response: any) => {
          //console.log(response);
          if((response.status == 1) && (response.type == 'success'))
          {
            this.item.unitPrice = response.response[0].amount;
            this.item.qty = 1;
            this.item.gst = response.response[0].gst;
            this.item.amount_with_gst = response.response[0].amount_with_gst;
            this.item.our_commision = response.response[0].our_commision;
            this.item.ccavenue_commision = response.response[0].ccavenue_commision;
            this.request.priceId  = response.response[0].id;
          }
      });
    }

    setLevelTab(service: any) {
        if(service.id > 0){
          this.isLoading = true;
          this.request.serviceName = service.name;
          this.request.serviceId = service.id;
          this.appointObj.getSubServices(service.id)
          .finally(() => { this.isLoading = false; })
          .subscribe((response: any) => {
            if((response.status == 1) && (response.type == 'success'))
            {
              this.levelsPeriods = response.response;
              this.dropdownList = this.levelsPeriods.services;
              this.selectedSubServices = [];
              this.dropdownSettings = {
                                          singleSelection: false,
                                          text:"Choose specialisation"+' in '+service.name,
                                          selectAllText:'Select All',
                                          unSelectAllText:'UnSelect All',
                                          enableSearchFilter: true,
                                          classes:"myclass custom-class"
                                        };
              this.params.selectedLevel =[];
              this.withSub = true;
              this.showTab('isLevelTab');
              //this.showTab('isServicesTab');
            }else{
              this.setCounsellorTab();
            }
          });
          return true;
        }
      return false;
    }


  counsellorDetail = function(id){
    localStorage.setItem('profileCounsId', id);
    localStorage.setItem('alreadyLogin', '1');
    window.open(this.profileUrl ,'_blank');
  }

    setCounsellorTab(withSubCat= '') {
      var isValid: boolean = true;
     
      if(withSubCat=='yes'){
        this.withSub = true;
        if((Object.keys(this.selectedSubServices).length === 0)){
          isValid = false;// set false if mandatory 
        }
      } else {
        this.selectedSubServices = [];
        this.withSub = false;
        
      }


      if(!isValid){
        swal('You must select one or more specialisations');
      }else{
        this.request.subServices = this.selectedSubServices;
        this.isLoading = true;

        this.appointObj.getCounsellors(this.request.serviceId, this.request.genricSubServices, this.selectedSubServices, this.globalService.id_token)
        .finally(() => { this.isLoading = false; })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type == 'success'))
          {
            this.counsellors = response.response;
          }else{
            this.counsellors = false;
          }
          this.showTab('isCounsellorTab');
        });
      }

      return true;
    }

    setExpectationTab() {
        this.isLoading = true;

        this.showTab('isExpectationTab');
    }

    setCalendarTab(user: any) {

      var metype = this.globalService.userCountryId;
      var mrtype = user.country_id;
      var msg = this.showMessageOfMentee(metype,mrtype);
      if(user.id> 0 && msg>0){
          this.request.token =  this.globalService.id_token;
          this.request.counsellorId = user.id;
          this.request.gender = user.gender;
          this.request.gendertype = (user.gender=='Male')?'Mr.':'Ms.';
          this.request.counsellorName = user.first_name+' '+user.last_name;
          this.showCounsellorCalendar('new');
          
      }
      return true;
    }


    showMessageOfMentee = function(metype,mrtype){

     if(metype!=101 && mrtype!=101){
      swal('Please be advised that users outside of India can book a session shortly. You will hear from us soon.');
      return 0;
     }else if(metype==101 && mrtype!=101){
      swal('Please be advised that sessions with International Mentors will be available soon. You will hear from us shortly.');
      return 0;
    }else if(metype!=101 && mrtype==101){
      swal('Please be advised that users outside of India can book a session shortly. You will hear from us soon.');
      return 0; 
     }else{
      return 1;
     }
    }
    
    showCounsellorCalendar(action: string){

      this.appointmentTable = false;
      this.appointObj.getCalendar(this.request.counsellorId, action ,  this.globalService.id_token)
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
        if((response.status == 1) && (response.type == 'success'))
        {
          this.calendaring = response.response;
         
          var today:any ;
          var endday:any ;
          
          today = this.globalService.getdatefun(0);
          endday = this.globalService.getdatefun(30);      

          if(response.response[0].date == today){
            this.prevousArraow = false;
          }else{
            this.prevousArraow = true;
          }
          if(response.response[6].date >= endday){
            this.nextArraow = false;
          }else{
            this.nextArraow = true;
          }
          this.showTab('isCalendarTab');
          this.appointmentTable=true;
          //this.showTab('isServicesTab');
        }
      });
    }
    setCheckoutTab(){
      this.request.expectationMessages = this.expectationMessages;
      this.showTab('isCheckoutTab');
    }

    getSuggestions(){

      
      if(this.suggestionMessages=='' || this.suggestionMessages==undefined){
      
         this.ErrorsuggestionMessages = 'You must enter your suggestion Messages';
        return false;
      }else{
        this.request.expectationMessages = this.suggestionMessages;
        this.request.studentId = localStorage.getItem('userId');
        this.isLoading = true;
        this.ErrorsuggestionMessages ='';
        this.appointObj.suggestAppointment(this.request)
        .finally(() => { this.isLoading = false; })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type == 'success'))
          {
            swal(response.message);
            this.router.navigate(['/appointments']);
          }
        });
        return true;
      }
      
    }

    getDiscount(){
        if(this.promoCode==''){
          swal('Please enter coupon/promo code');
          return false;
        }
        this.isLoading = true;
        this.appointObj.checkDiscount(this.promoCode, this.globalService.id_token)
        .finally(() => { this.isLoading = false; })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type == 'success'))
          {
            this.discount = response.response.discount;
          }
        });
    }


    checkOut() {
        if (this.request.counsellorId && this.request.time) {
          $('#booking_confirm').attr("disabled", "disabled").html('Processing...');
      
          this.isLoading = true;
         
          this.appointObj.createAppointment(this.request)
            .finally(() => { this.isLoading = false; })
            .subscribe((response: any) => {
            
              if((response.status == 1) && (response.type == 'success'))
              {
                //defining Global Variable
                localStorage.setItem('userId', this.userId);
                localStorage.setItem('userType', this.userType);
                localStorage.setItem('newUser', '"' + this.globalService.newUser + '"');
                localStorage.setItem('userTypeText', this.userTypeText);
                localStorage.setItem('userFullName', this.globalService.userFullName);
                localStorage.setItem('userPic', this.globalService.userPic);
                swal('Your request has been sent to the Mentor');
                this.router.navigate(['/appointments']);
              }
            });
          return true;

        }
    }

    checkGenric(e){
      if(e.target.checked){
        this.showSubServices = false;
        this.request.genricSubServices = true;
      }else{
        this.showSubServices = true;
        this.request.genricSubServices = true; // set false if there is genric checkbox
      }

    }

    showTab(tab: string) {
      this.isServicesTab = false;
      this.isLevelTab= false;
      this.isCounsellorTab = false;
      this.isExpectationTab = false;
      this.isCalendarTab = false;
      this.isCheckoutTab = false;

      if('isServicesTab' == tab)
      {
        this.isServicesTab = true;
      } else if('isLevelTab' == tab)
      {
        if(!this.withSub){
          this.isServicesTab = true;
        } else {
          this.isLevelTab= true;
        }
      }else if('isExpectationTab' == tab)
      {
        this.isExpectationTab = true;
      }else if('isCounsellorTab' == tab)
      {
        this.isCounsellorTab= true;
      }else if('isCalendarTab' == tab)
      {
        this.isCalendarTab= true;
        //this.setCounsellorTab();
      }else if('isCheckoutTab' == tab)
      {
        this.isCheckoutTab= true;
      }
      //eval("this."+ tab +'=true');
    }

    setService(serviceId:number){
      this.request.serviceId = serviceId;
    }

    setLevel(levelId:number,periodId:number){
      //this.request.duration = '45 minutes';
      this.request.levelId = levelId;
      this.request.periodId = periodId;
    }
    setCounsellor(counsellorId:number){
      this.request.counsellorId = counsellorId;
    }
    setStartDateTime(dateTime:any){
      this.request.dateTime = dateTime;
    }

    updateLevels(value,event){
      //alert(value+'=='+event.target.checked);
      if((this.params.selectedLevel == undefined) || (this.params.selectedLevel == 'undefined'))
      {
        this.params.selectedLevel = [];
      }
      if(event.target.checked){
          this.params.selectedLevel.push(value);
      }
        else if (!event.target.checked){
          let indexx = this.params.selectedLevel.indexOf(value);
          this.params.selectedLevel.splice(indexx, 1);
        }
      }

      setDateTime(date, time){

        swal({
          title: 'Are you sure?',
          text: 'You want to book this time slot',
          showCancelButton: true,
          confirmButtonColor: '#DD6B55',
          confirmButtonText: 'Yes',
          cancelButtonText: 'No',
          closeOnConfirm: true,
          closeOnCancel: true
        }).then(() => { 

          this.request.date = date;
          this.request.time = time;
          this.showTab('isExpectationTab');

        });

     
      }

      setOtherDateTime(date, time){

        this.request.date = date;
        this.request.time = time;
        this.appointObj.checkSessionOfMentee(this.request.studentId,this.request.counsellorId,this.request.date,this.request.time, this.globalService.id_token)
        .finally(() => { this.isLoading = false; })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type == 'success'))
          {
            swal(response.message);
            this.showTab('isExpectationTab');

          }else{
            if(response.status == 2){
              swal(response.message);
            }else{
              swal({
                title: 'Are you sure?',
                text: response.message,
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                closeOnConfirm: true,
                closeOnCancel: true
              }).then(() => { 
      
                this.request.date = date;
                this.request.time = time;
                this.showTab('isExpectationTab');
      
              });
            }
         
            
          }
        });
      
      }

      onPeriodChange(duration)
      {
        if(duration.id == 1)
        {
          this.request.duration = 30;
        }else if(duration.id == 2)
        {
          this.request.duration = 45;
        }else if(duration.id == 3)
        {
          this.request.duration = 60;
        }else if(duration.id == 4)
        {
          this.request.duration = 90;
        }else if(duration.id == 5)
        {
          this.request.duration = 120;
        }else if(duration.id == 6)
        {
          this.request.duration = 150;
        }else if(duration.id == 7)
        {
          this.request.duration = 180;
        }

        this.request.durationId = duration.id;
      }
      generateArray(obj){
        return Object.keys(obj).map((key)=>{ return obj[key]});
      }
      getClassName(idx){
        if(idx==0){
          return 'one';
        }else if(idx==1){
          return 'two';
        }else if(idx==2){
          return 'three';
        }else if(idx==3){
          return 'four';
        }else if(idx==4){
          return 'five';
        }else if(idx==5){
          return 'six';
        }else if(idx==6){
          return 'seven';
        }
        return 'one';
      }

    onItemSelect(item:any){
      this.request.genricSubServices = false;
    }

    OnItemDeSelect(item:any){
      this.request.genricSubServices = false;
    }

    onSelectAll(items: any){
      this.request.genricSubServices = true;
    }

    onDeSelectAll(items: any){
      this.request.genricSubServices = false;
    }

    getPreviousWeek(){
      this.showCounsellorCalendar('prev');
    }

    getNextWeek(){
      this.showCounsellorCalendar('next');
    }

    setCounsellor_popup = function(user,indCounsellor, imageUrl){
      //alert('ljlhl');
      indCounsellor++;
      this.consellorId = user.id;
      this.summary = user.profile_summary;
      this.popFullName = user.first_name +" "+user.last_name;
    
      localStorage.setItem('directCounsellorName', user.first_name +' '+user.last_name);
    
      if(imageUrl){
        this.popUserPic = this.cdnPath+imageUrl;
      }else{
        this.popUserPic = "https://d3213zi13us7f1.cloudfront.net/website/assets/images/intercell-user-pic.png";
      }
      //this.document.getElementById('username').value;
    }




}
