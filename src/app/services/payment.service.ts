import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

@Injectable()
export class PaymentService {

  constructor(private http: Http) {
  }

  doPayment(user : any , userId: any) {
    return this.http.post('/core/Payment/pay', JSON.stringify({ userType : user.usertype, userId : user.userId
    })).map((response: Response) => response.json());
  }

  sessionPayments(userId : any , token: any) {
    return this.http.post('/core/Payment/session_payment', JSON.stringify({ userId : userId , token: token
    })).map((response: Response) => response.json());
  }

  studentPayments(userId: any , token: any) {
    return this.http.post('/core/Payment/student_payment', JSON.stringify({ userId : userId , token: token
    })).map((response: Response) => response.json());
  }

  returnedPayment(userId : any , token: any) {
    return this.http.post('/core/Payment/returnedPayments', JSON.stringify({  userId : userId, token: token
    })).map((response: Response) => response.json());
  }


  refundedPayment(userId: any , token: any) {
    return this.http.post('/core/Payment/refunded_payment', JSON.stringify({  userId : userId, token: token
    })).map((response: Response) => response.json());
  }


  returnPayment(userId : any , amount : any, token: any) {
    return this.http.post('/core/Payment/returnPayments', JSON.stringify({  userId : userId , amount : amount, token: token
    })).map((response: Response) => response.json());
  }

  registerUser(userId : any, token: any) {
    return this.http.post('/core/Payment/register_user', JSON.stringify({  userId : userId , token:token
    })).map((response: Response) => response.json());
  }
}
