import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class AppointmentsService {
  calendarStartDay: string ='';
  calendarNextStartDay: string ='';
  calendarPrevStartDay: string ='';
  constructor(private http: Http){}

  getUpcoming( userType: string , token: any ) {
    return this.http.post('/core/appointments/upcomming', JSON.stringify({  userType: userType , token: token })).map((response: Response) => response.json());
  }

  cancelAppointment( userId: number, sessionId: number , token: any) {
    return this.http.post('/core/appointments/cancel', JSON.stringify({ userId: userId, sessionId: sessionId , token :token})).map((response: Response) => response.json());
  }

  approveAppointment(userId: number, sessionId: number, token: any  ) {
    return this.http.post('/core/appointments/approve', JSON.stringify({ userId: userId, sessionId: sessionId, token :token })).map((response: Response) => response.json());
  }

  rejectAppointment(userId: number, sessionId: number , token: any ) {
    return this.http.post('/core/appointments/reject', JSON.stringify({ userId: userId, sessionId: sessionId , token : token })).map((response: Response) => response.json());
  }

  startAppointment(userId: number,userType: number, sessionId: number , token: any ) {
    return this.http.post('/core/appointments/start', JSON.stringify({ userId: userId,userType:userType, sessionId: sessionId , token : token })).map((response: Response) => response.json());
  }

  getServices(){
    return this.http.get('/core/services/lists').map((response: Response) => response.json());
  }

  getSubServices(serviceId: number) {
      return this.http.get('/core/services/subservices-durations/'+serviceId).map((response:Response) => response.json());
  }

  getLevels(){
      return this.http.get('/core/services/levels-durations').map((response:Response) => response.json());
  }

  getCounsellors(serviceId: any, isGenric: boolean, subServices: any , token : any){
      return this.http.post('/core/services/counsellors-by-subservices', JSON.stringify({ serviceId:serviceId,isGenric: isGenric, subServices: subServices, token: token })).map((response: Response) => response.json());
  }

  getCounsellorslist(first_name: any, last_name: any,category: any, token : any){
    return this.http.post('/core/services/counsellors-by-name', JSON.stringify({ first_name:first_name,last_name: last_name,category:category, token: token })).map((response: Response) => response.json());
}
  getPricing(){
      return this.http.get('/core/services/core-price').map((response: Response) => response.json());
  }


  getCalendar(userId: number, action, token: any){
    var dd:any;
    var mm:any;
    var yyyy:number;

    if(action == 'new')
    {
      this.calendarStartDay = '';
      action = 'next';
    }

   
    if(this.calendarStartDay == ''){
      var today:any = new Date();
      today.setDate(today.getDate());
      dd = today.getDate();
      mm = today.getMonth() + 1; // 0 is January, so we must add 1
      yyyy = today.getFullYear();

      if(dd<10) {
        dd='0'+dd
      }

      if(mm<10) {
        mm='0'+mm
      }
      today = yyyy+'-'+mm+'-'+dd;

      var targetDate: any = new Date();
      targetDate.setDate(targetDate.getDate() + 6);
      dd = targetDate.getDate();
      mm = targetDate.getMonth() + 1; // 0 is January, so we must add 1
      yyyy = targetDate.getFullYear();

      if(dd<10) {
        dd='0'+dd
      }

      if(mm<10) {
        mm='0'+mm
      }

      targetDate = yyyy+'-'+mm+'-'+dd;
    }else{
      if(action=='prev'){
        this.calendarStartDay = this.calendarPrevStartDay;

      }else{
        this.calendarStartDay = this.calendarNextStartDay;
      }
      var toDate:any = new Date(this.calendarStartDay);
    //  toDate.setDate(toDate.getDate() + 1);
      dd = toDate.getDate();
      mm = toDate.getMonth() + 1; // 0 is January, so we must add 1
      yyyy = toDate.getFullYear();

      if(dd<10) {
        dd='0'+dd
      }

      if(mm<10) {
        mm='0'+mm
      }

      today = yyyy+'-'+mm+'-'+dd;

      var targetDate:any = new Date(today);
      targetDate.setDate(targetDate.getDate() + 6);
      dd = targetDate.getDate();
      mm = targetDate.getMonth() + 1; // 0 is January, so we must add 1
      yyyy = targetDate.getFullYear();

      if(dd<10) {
        dd='0'+dd
      }

      if(mm<10) {
        mm='0'+mm
      }
      targetDate = yyyy+'-'+mm+'-'+dd;
    }

    var prevTargetDate:any = new Date(today);
    prevTargetDate.setDate(prevTargetDate.getDate() - 6);
    dd = prevTargetDate.getDate();
    mm = prevTargetDate.getMonth() + 1; // 0 is January, so we must add 1
    yyyy = prevTargetDate.getFullYear();

    if(dd<10) {
      dd='0'+dd
    }

    if(mm<10) {
      mm='0'+mm
    }

    prevTargetDate = yyyy+'-'+mm+'-'+dd;
    this.calendarNextStartDay = targetDate;
    this.calendarPrevStartDay = prevTargetDate;

    if(action=='prev'){
      this.calendarStartDay = this.calendarPrevStartDay;

    }else{
      this.calendarStartDay = this.calendarNextStartDay;
    }

    var urllist =  "/counsellor/calendars/lists/"+userId+'/'+today+'/'+targetDate+'/'+ token;
    //console.log(urllist);
    return this.http.get(decodeURI(urllist)).map((response:Response) => response.json());
  }

  checkDiscount(pCode: any, token: any){
    return this.http.get(decodeURI("/core/services/check-promo-code/"+pCode+"/"+token)).map((response:Response) => response.json());

  }
  suggestAppointment(request: any) {
    return this.http.post("/core/appointments/suggest", JSON.stringify(request)).map((response:Response) => response.json());
  }
  createAppointment(request: any) {
    return this.http.post("/core/appointments/book", JSON.stringify(request)).map((response: Response) => response.json());
  }

  rescheduleAppointment(request: any) {
    return this.http.post("/core/appointments/reschedule", JSON.stringify(request)).map((response: Response) => response.json());
  }

  getAppointmentStatus(request: any) {
    return this.http.post('/core/appointments/getCurrentStatus', JSON.stringify(request)).map((response: Response) => response.json());
  }


  getUpcomingCount( userType: string , token: any ) {
    return this.http.post('/core/appointments/upcomming_count', JSON.stringify({  userType: userType , token: token })).map((response: Response) => response.json());
  }

  getSessionDetails( sessionId: string ) {
    return this.http.post('/core/appointments/session_details', JSON.stringify({  sessionId: sessionId })).map((response: Response) => response.json());
  }

  getSessionDetailsview( sessionId: string,userId: number,userType: number ) {
    return this.http.post('/core/appointments/session_detailsview', JSON.stringify({  sessionId: sessionId,userId: userId,userType: userType })).map((response: Response) => response.json());
  }

  checkSessionOfMentee(studentId: any,counsellorId: any,date: any,time: any, token: any){
    return this.http.post("/core/common/Session_CheckOf_Other_Mentee", JSON.stringify({  studentId: studentId,counsellorId: counsellorId,date: date ,time: time})).map((response:Response) => response.json());

  }

}
