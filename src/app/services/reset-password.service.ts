import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';


@Injectable()
export class ResetPasswordService {
  constructor(private http: Http) {
  }

  doSubmit(emailId: any) {
    return this.http.post('/core/Users/reset_password', JSON.stringify({email : emailId})).map((response: Response) => response.json());
  }

  onCheck(userId: any , validKey: any) {
    return this.http.post('/core/Users/validate_key', JSON.stringify({userId : userId , key : validKey })).map((response: Response) => response.json());
  }

  onReset(password: any , userId: any , insertId: any) {
    return this.http.post('/core/Users/reset', JSON.stringify({userId : userId , password : password , insertId : insertId })).map((response: Response) => response.json());
  }
}
