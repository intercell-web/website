import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

@Injectable()
export class DetailService {

  constructor(private http: Http) { }

  onCheck(userId: any , validKey: any) {
    return this.http.post('/core/Users/validate_login_pass', JSON.stringify({userId : userId , key : validKey })).map((response: Response) => response.json());
  }

}
