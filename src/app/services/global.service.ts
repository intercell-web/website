import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class GlobalService {

  /** Local Settings */
  /*  public API_SERVER:string ='http://localhost/intercell/icapi/v1/';
  public SESSION_SERVER:string ='http://localhost/intercell/icvideo/';
  public BASE_PATH:string ='http://localhost:4200/';
  public SESSION_SCREEN :string = 'http://localhost/intercell/web/src/video_session.php';
  */

  public BASE_PATH:string = 'https://www.intercellworld.com/';
  public API_SERVER:string = this.BASE_PATH + 'api/v1/';
  public SESSION_SERVER:string = this.BASE_PATH +'icvideo/';
  public SESSION_SCREEN :string = this.BASE_PATH +'assets/scripts/video_session.php';
  public userId: number;
  public newUser: number = 1;
  public userType: number = 0;
  public userTypeText: string;
  public userFullName: string;
  public userPic: string;
  public userProfilePath: string = this.API_SERVER + 'uploads/';
  public serviceDetails: number = 0;
  public showServiceId: number = 0;
  public showTrackId: number = 0;
  public id_token: any;
  public appointDirectDetail: any;
  public form: any;
  public formEducation: any;
  public formCounsellingStudent: any;
  public formCounsellingCounsellor: any;
  public formExperience: any;
  public username: any;
  public showMentor: any = 0;
  public unreadmessagecount: number = 0;
  private subject = new Subject<any>();
  public userCountryId: string;
  private displayname = new Subject<any>();

  private pic = new Subject<any>();

  sendMessage(message: string) {
    this.subject.next({ text: message });
  }

  clearMessage() {
    this.subject.next();
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

  sendDisplayName(message: string) {
    this.displayname.next({ text: message });
  }

  sendPic(message: string) {
    this.pic.next({ text: message });
  }

  clearDisplayName() {
    this.displayname.next();
  }

  clearPic() {
    this.pic.next();
  }

  ngClassForColorsection(value){
    let data = '';
    if(value == 'Rejected' || value == 'Cancelled'){
    data = 'text-red';
    }else{
    data = 'text-green';
    }
    return data;
    }


    
  getDisplayName(): Observable<any> {
    return this.displayname.asObservable();
  }

  getPic(): Observable<any> {
    return this.pic.asObservable();
  }


  getdatefun = function (number) {
    
   var date = new Date();
   var newdate = new Date(date);  
   newdate.setDate(newdate.getDate() + number);
   var dd = newdate.getDate();
   var mm = newdate.getMonth() + 1;
   var y = newdate.getFullYear();
   var dd1:any;
   var mm1:any;
   if(dd<10) {
      dd1='0'+dd;
   }else{
      dd1=dd;
   }
   if(mm<10) {
     mm1='0'+mm;
   }else{
     mm1=mm;
   }        
   var someFormattedDate = y + '-' + mm1 + '-' + dd1;
   return someFormattedDate
}

  constructor() {}
}
