import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';


@Injectable()
export class LoginService {
  public baseUrl_verify : string = '/core/Users/VerifyUrlEmail';
  constructor(private http: Http) {
  }

  doLogin(user: any) {
    return this.http.post('/core/Users/insert', JSON.stringify({password : user.password ,
      userType : user.usertype , firstName : user.firstname ,
      lastName : user.lastname , email : user.emailid })).map((response: Response) => response.json());
  }

  doSign(user: any) {
    return this.http.post('/core/Users/user_login', JSON.stringify({password : user.password ,
      userType : user.usertype , email : user.emailid })).map((response: Response) => response.json());
  }

  doSocialSign(emailId: any) {
    return this.http.post('/core/Users/google_login', JSON.stringify({ gmail : emailId })).map((response: Response) => response.json());
  }

  checkEmail(emailId: any) {
    return this.http.post('/core/Users/unique_emailId', JSON.stringify({email : `${emailId}` })).map((response: Response) => response.json());
  }

  checkLoginEmail(emailId: any , token : any) {
    return this.http.post('/core/Users/login_unique_emailId', JSON.stringify({email : `${emailId}` , token : `${token}` })).map((response: Response) => response.json());
  }


  checkUserName(userName: any) {
    return this.http.post('/core/Users/unique_userName', JSON.stringify({userName : `${userName}` })).map((response: Response) => response.json());
  }

  checkPhone(phone: any, token : any) {
    return this.http.post('/core/Users/unique_phone', JSON.stringify({phone : `${phone}` , token : `${token}` })).map((response: Response) => response.json());
  }

  sendPhoneCode(phone: any,countrycode:any, token : any) {
    return this.http.post('/core/Users/emailid_check_sendcode', JSON.stringify({type : 'phone',value : `${phone}`,countrycode : `${countrycode}`, token : `${token}`  })).map((response: Response) => response.json());
  }

  sendMailCode(mail: any, token : any) {
    return this.http.post('/core/Users/emailid_check_sendcode', JSON.stringify({type : 'mail',value : `${mail}`,  token : `${token}` })).map((response: Response) => response.json());
  }

  resendPhoneCode(phone: any,countrycode:any, token : any) {
    return this.http.post('/core/Users/resend_phone_code', JSON.stringify({phone : `${phone}`,countrycode : `${countrycode}`, token : `${token}` })).map((response: Response) => response.json());
  }

  resendMailCode(mail: any, token : any) {
    return this.http.post('/core/Users/resend_mail_code', JSON.stringify({mail : `${mail}`, token : `${token}` })).map((response: Response) => response.json());
  }


  confirmPhoneCode(phone: any , code: any, token : any) {
    return this.http.post('/core/Users/confirm_phone_code', JSON.stringify({phone : `${phone}` , code : `${code}`, token : `${token}` })).map((response: Response) => response.json());
  }

  confirmMailCode(mail: any, code: any, token : any) {
    return this.http.post('/core/Users/confirm_mail_code', JSON.stringify({mail : `${mail}` , code : `${code}`, token : `${token}` })).map((response: Response) => response.json());
  }


  createUser(formArray: any, formExperienceArray: any , formEducationArray: any , formCounsellingCounsellorArray: any, formCounsellingStudentArray: any , imageUrl: any ) {
    return this.http.post('/core/Users/create_user', JSON.stringify({form : formArray , formExperience : formExperienceArray , formEducation : formEducationArray  , formCounsellingCounsellor : formCounsellingCounsellorArray, formCounsellingStudent : formCounsellingStudentArray , url : imageUrl   })).map((response: Response) => response.json());
  }

  getPrivacyValues(userId: any , token : any) {
    return this.http.post('/core/Users/get_privacy_values', JSON.stringify({userId : `${userId}` , token : `${token}` })).map((response: Response) => response.json());
  }

  updateNewMail(emailAddress: any, userId: any , token : any) {
    return this.http.post('/core/Users/update_mail_address', JSON.stringify({userId : `${userId}`, emailAddress: `${emailAddress}` , token : `${token}`  })).map((response: Response) => response.json());
  }

 updateNewPhone(phone: any , userId: any , token: any,countrycodes:any) {
    return this.http.post('/core/Users/update_phone', JSON.stringify({userId : `${userId}` ,  phone: `${phone}` , token : `${token}` ,countrycodes : `${countrycodes}`})).map((response: Response) => response.json());
  }

  sendMailPhoneCode(phone: any,mail: any, countrycodes : any) {
    return this.http.post('/core/Users/send_mailphone_code', JSON.stringify({phone : `${phone}`,mail : `${mail}`, countrycodes : `${countrycodes}`  })).map((response: Response) => response.json());
  }

  verifyEmailUrl(urlId: any) {
    const url = `${this.baseUrl_verify}/${urlId}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  
  getCmsPage(short_code: any) {
    return this.http.post('/core/Users/cms_page', JSON.stringify({short_code :short_code})).map((response: Response) => response.json());
  }

}
