import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

@Injectable()
export class CounsellorService {

  public baseUrl = '/core/Users/profile';
  public baseUrl_counsellor = '/counsellor/Details/details';
  public baseUrl_counsellor_education = '/counsellor/Details/education_details';
  public baseUrl_counsellor_experience = '/counsellor/Details/experience_details';
  public baseUrl_counsellor_delete_education = '/counsellor/Details/delete_counsellor_education';
  public baseUrl_counsellor_delete_experience = '/counsellor/Details/delete_counsellor_experience';
  public baseUrl_subservice_all = '/core/Services/all_sub_services';
  public baseUrl_service_all = '/core/Services/lists';
  public baseUrl_service_alllist = '/core/Services/Service_lists';
  public baseUrl_counsellor_delete_certificate = '/counsellor/Details/delete_counsellor_certificate';
  public baseUrl_counsellor_certificate = '/counsellor/Details/certificate_details';
  public baseUrl_counsellor_payment = '/counsellor/Details/payment_details';
  public baseUrl_ProfileImage = '/core/Users/delete_ProfileImage';

  constructor(private http: Http){
  }


  getUserDetails(userId: any , token: any ) {
    const url = `${this.baseUrl}/${userId}/${token}`;
    return this.http.get(url).map((response: Response) => response.json() );
  }

  getCounsellorDetails( userId: any , token: any ) {
    const url = `${this.baseUrl_counsellor}/${userId}/${token}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  getCounsellorPaymentDetails(userId: any , token: any ) {
    const url = `${this.baseUrl_counsellor_payment}/${userId}/${token}`;
    return this.http.get(url).map((response: Response) => response.json());
  }


  getCounsellorEducation(userId: any , token: any ) {
    const url = `${this.baseUrl_counsellor_education}/${userId}/${token}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  getCounsellorExperience( userId: any , token: any) {
    const url = `${this.baseUrl_counsellor_experience}/${userId}/${token}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  getCounsellorCertificate( userId: any , token: any) {
    const url = `${this.baseUrl_counsellor_certificate}/${userId}/${token}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  updateCounsellorDetails(user: any, token: any){
    return this.http.post('/counsellor/Details/update_details', JSON.stringify({ subcategory : user.subcategory , summary : user.summary,brand_worked_for : user.brand_worked_for ,highesteducation : user.highesteducation ,totalexperience : user.totalexperience , category : user.category , stateId : user.stateid , countryId : user.countryid , cityId : user.cityid , firstName: user.firstname , lastName : user.lastname , language : user.language , noDetails : user.noDetails , linkedLink : user.linkedInLink ,gender : user.gender , token: token , userId: user.userId ,
    })).map((response: Response) => response.json());
  }

  updateCounsellorPaymentDetails(user: any, token: any){
    return this.http.post('/counsellor/Details/update_payment_details', JSON.stringify({
      userId: user.userId , ifscCode : user.ifsc_code , accountNumber : user.account_number ,
       branch : user.branch , accountHolder : user.account_holder, bankname : user.bankname,
        account_pan : user.account_pan,aadhar_card : user.aadhar_card, token: token
    })).map((response: Response) => response.json());
  }

  insertCounsellorDetails(user: any , token: any){
    return this.http.post('/counsellor/Details/counsellor_login', JSON.stringify({firstName: user.firstname , lastName: user.lastname, dobyear: user.year, password : user.password , dobmonth : user.month, dobday : user.day, gender : user.gender , mobile: user.mobile , email: user.emailid , userType: 3 , token: token
    })).map((response: Response) => response.json());
  }

  insertCounsellorPaymentDetails(user: any , token: any){
    return this.http.post('/counsellor/Details/insert_payment_details', JSON.stringify({
      ifscCode : user.ifsc_code , accountNumber : user.account_number , branch : user.branch ,
      accountHolder : user.account_holder , bankname : user.bankname,
      account_pan : user.account_pan,aadhar_card : user.aadhar_card, token: token
    })).map((response: Response) => response.json());
  }

  deleteCounsellorExperience(experienceId: number , token: any) {
    const url = `${this.baseUrl_counsellor_delete_experience}/${experienceId}/${token}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  deleteCounsellorEducation(educationId: number, token: any) {
    const url = `${this.baseUrl_counsellor_delete_education}/${educationId}/${token}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  deleteCounsellorCertificate(certificateId: number, token: any) {
    const url = `${this.baseUrl_counsellor_delete_certificate}/${certificateId}/${token}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  updateCounsellorExperience(user: any , token: any){
    return this.http.post('/counsellor/Details/update_counsellor_experience', JSON.stringify({ userId: user.userId , experienceId : user.experienceId , startTimeMonth: user.startTimeMonth , endTimeMonth : user.endTimeMonth ,  startTimeYear : user.startTimeYear ,  endTimeYear : user.endTimeYear , title : user.title , company : user.company , location : user.location , current : user.current , description : user.description , token: token
    })).map((response: Response) => response.json());
  }

  updateCounsellorEducation(user: any, token: any ){
    return this.http.post('/counsellor/Details/update_counsellor_education', JSON.stringify({ userId: user.userId , educationId : user.educationId  , school : user.school , field : user.field  , startTime : user.startTime , endTime : user.endTime , description : user.description , degree : user.degree, token: token
    })).map((response: Response) => response.json());
  }

  updateCounsellorCertificate(user: any , token: any){
    return this.http.post('/counsellor/Details/update_counsellor_certificate', JSON.stringify({userId: user.userId ,certificateId : user.certificateId ,
      certificateMonth : user.certificateMonth , certificateYear : user.certificateYear , certificate : user.certificate , institute : user.certificateInstitute, token: token
    })).map((response: Response) => response.json());
  }

  getAllServices() {
    return this.http.get(this.baseUrl_service_all).map((response: Response) => response.json());
  }

  getAllServiceslist() {
    return this.http.get(this.baseUrl_service_alllist).map((response: Response) => response.json());
  }

  getAllSubServices() {
    return this.http.get(this.baseUrl_subservice_all).map((response: Response) => response.json());
  }

  deleteProfileImage(userId: number, token: any) {
    const url = `${this.baseUrl_ProfileImage}/${userId}/${token}`;
    return this.http.get(url).map((response: Response) => response.json());
  }
}
