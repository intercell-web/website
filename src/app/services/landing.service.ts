import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

@Injectable()
export class LandingService {
  public baseUrl_services : string = '/core/LandingPage/send_services';
  public baseUrl_services_home : string = '/core/LandingPage/send_services_home';
  public baseUrl_sub_services : string = '/core/Services/all_patch';
  public baseUrl_student_speak : string = '/core/LandingPage/send_student_speak';
  public baseUrl_all_counsellor : string = '/core/LandingPage/get_all_counsellor';
  public baseUrl_track_counsellor : string = '/core/LandingPage/get_track_counsellor';
  public baseUrl_home_counsellor : string = '/core/LandingPage/get_home_counsellor';
  public baseUrl_service_detail: string = '/core/LandingPage/get_service_details';
  public baseUrl_field_detail: string = '/core/LandingPage/get_field_details';
  public baseUrl_track_service_detail: string = '/core/LandingPage/get_track_service_details';

  constructor(private http: Http){
  }

  getServices() {
    return this.http.get(this.baseUrl_services).map((response:Response) => response.json());
  }

  getServiceshome() {
    return this.http.get(this.baseUrl_services_home).map((response:Response) => response.json());
  }

  getSubServices() {
    return this.http.get(this.baseUrl_sub_services).map((response:Response) => response.json());
  }

  /*getMeetCounsellor() {
    return this.http.get(this.baseUrl_meet_counsellor).map((response: Response) => response.json());
  }
*/
  getStudentSpeak() {
    return this.http.get(this.baseUrl_student_speak).map((response: Response) => response.json());
  }

  getAllCounsellor(serviceId : number) {
    const url = `${this.baseUrl_all_counsellor}/${serviceId}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  getTrackCounsellor(trackId : number) {
    const url = `${this.baseUrl_track_counsellor}/${trackId}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  getMeetCounsellor() {
    const url = `${this.baseUrl_home_counsellor}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  getServiceDetails(serviceId : number) {
    const url = `${this.baseUrl_service_detail}/${serviceId}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  getTrackServiceDetails(trackId : number) {
    const url = `${this.baseUrl_track_service_detail}/${trackId}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  getFieldDetails(field : string) {
    const url = `${this.baseUrl_field_detail}/${field}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

}
