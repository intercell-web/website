import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { JwtHelper } from './jwt.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor( private router: Router) {}

  canActivate() {
    if (localStorage.getItem('userId')) {
      return true;
    } else {
      this.router.navigate(['/']);
      return false;
    }
  }
}
