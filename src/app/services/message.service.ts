import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

@Injectable()
export class MessageService {

  constructor(private http: Http) { }

  getMessages(index: any , token: any ) {
    return this.http.post('/core/Messages/get_inbox', JSON.stringify({index : `${index}` , token: token})).map((response: Response) => response.json());
  }
   getMessagesCount( token: any) {
    return this.http.post('/core/Messages/get_count', JSON.stringify({ token: token})).map((response: Response) => response.json());
  }

  getUnreadMessagesCount( token: any) {
    return this.http.post('/core/Messages/get_unread_message_count', JSON.stringify({ token: token})).map((response: Response) => response.json());
  }

  getupdateMessages( id: any,token: any) {
    return this.http.post('/core/Messages/get_message_update', JSON.stringify({ id: id, token: token})).map((response: Response) => response.json());
  }

}
