import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { RequestOptions } from '@angular/http';
import { Headers } from '@angular/http';

@Injectable()
export class BasicService {

  public baseUrl_countries : string = '/core/Basic/countries';
  public baseUrl_cities : string = '/core/Basic/cities';
  public baseUrl_states : string = '/core/Basic/states';
  public baseUrlProfile= '/core/Users/profile';

  constructor(private http: Http){
  }

  getUserProfile(userId: number,  token: any) {
    const url = `${this.baseUrlProfile}/${userId}/${token}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  getCountriesList() {
    const url = this.baseUrl_countries;
    return this.http.get(url).map((response: Response) => response.json());
  }

  updateImageUrl(image, sender){
    return this.http.post('/core/Users/update_image', JSON.stringify({
      dp_image: image , userId: sender
    })).map((response:Response) => response.json());
  }

  getStatesList(countryId : number) {
    const url = `${this.baseUrl_states}/${countryId}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  getCitiesList(stateId : number) {
    const url = `${this.baseUrl_cities}/${stateId}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  uploadImage(form){
    const headers = new Headers();
    /** No need to include Content-Type in Angular 4 */
    //headers.append('enctype', 'multipart/form-data');
    headers.append('Accept', 'application/json');

    const options = new RequestOptions({ headers: headers });

    return this.http.post('/core/Basic/upload', form, options).map((response:Response) => response.json());
  }


  refertofriend(formArray){

    return this.http.post('/core/Basic/sendtoFriend', JSON.stringify({name : formArray.name ,
      email : formArray.email , phone : formArray.phone ,
      message : formArray.message , userId : formArray.userId, usertype : formArray.usertype, careerfield : formArray.careerfield })).map((response:Response) => response.json());
  }


  ContactUs(formArray){
     return this.http.post('/core/Basic/Contactus', JSON.stringify({name : formArray.name ,
          message : formArray.message, email : formArray.email , phone : formArray.phone })).map((response:Response) => response.json());
           
      }

    NeedHelp(formArray){
      return this.http.post('/core/Basic/Contactus_api', JSON.stringify({user_id : formArray.user_id ,
            message : formArray.message})).map((response:Response) => response.json());
            
        }

      Feedback(formArray){
        return this.http.post('/core/Basic/Feedback', JSON.stringify({name : formArray.name ,
             message : formArray.message, email : formArray.email , phone : formArray.phone })).map((response:Response) => response.json());
              
         }

         getIpAddress(){
          
             return this.http.get('/core/common/ip_address').map((response: Response) => response.json());
           
         }   

}
