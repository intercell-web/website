import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

@Injectable()
export class StudentService {

  public baseUrl= '/core/Users/profile';
  public baseUrl_student = '/student/Details/details';
  public baseUrl_student_interest_details = '/student/Details/interest_details';
  public baseUrl_student_qualification_details = '/student/Details/qualification_details';
  public baseUrl_student_delete_interest = '/student/Details/delete_student_interest';
  public baseUrl_student_delete_qualification = '/student/Details/delete_student_qualification';
  public baseUrl_ProfileImage = '/core/Users/delete_ProfileImage';


  constructor(private http: Http){
  }

  updateStudentEducation(user,  token: any) {
    return this.http.post('/student/Details/add_others_qualification', JSON.stringify({ userId : user.userId, qualificationId : user.qualificationId , university : user.university, token: token , class : user.class , startDate : user.startDate , endDate : user.endDate , field : user.field , summary : user.profile_summary
    })).map((response: Response) => response.json());
  }


  getUserDetails(userId: number,  token: any) {
    const url = `${this.baseUrl}/${userId}/${token}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  getStudentDetails( userId: any , token: any ) {
    const url = `${this.baseUrl_student}/${userId}/${token}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  getStudentInterestDetails(userId: number, token: any) {
    const url = `${this.baseUrl_student_interest_details}/${userId}/${token}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  deleteStudentInterest(interestId: number, token: any) {
    const url = `${this.baseUrl_student_delete_interest}/${interestId}/${token}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  deleteStudentQualification(qualificationId: number, token: any) {
    const url = `${this.baseUrl_student_delete_qualification}/${qualificationId}/${token}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

  getStudentQualificationDetails(userId: number, token: any) {
    const url = `${this.baseUrl_student_qualification_details}/${userId}/${token}`;
    return this.http.get(url).map((response: Response) => response.json());
  }


  updateStudentField(user, token: any) {
    return this.http.post('/student/Details/add_others_interest', JSON.stringify({ userId : user.userId, field : user.field , fieldId : user.fieldId , token : token})).map((response: Response) => response.json());
  }

  updateStudentDetails(user, token: any) {
    return this.http.post('/student/Details/update_details', JSON.stringify({ category : user.category ,subcategory : user.subcategory ,userId : user.userId, stateId : user.stateid ,countryId : user.countryid , cityId : user.cityid , firstName: user.firstname , lastName : user.lastname , gender : user.gender , dob : user.dob , token: token })).map((response: Response) => response.json());
  }

  deleteProfileImage(userId: number, token: any) {
    const url = `${this.baseUrl_ProfileImage}/${userId}/${token}`;
    return this.http.get(url).map((response: Response) => response.json());
  }

}
