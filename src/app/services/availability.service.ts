import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AvailabilityService {
  constructor(private http: Http){}


  getTimeSlots(token: any){
    return this.http.get("/counsellor/calendars/get-availability/"+token).map((response:Response) => response.json());
  }

  setTimeSlots( userId: number, token: any, slots: string ) {
        return this.http.post("/counsellor/calendars/set-availability", JSON.stringify({ userId: userId,token : token,  slots: slots })).map((response:Response) => response.json());
  }
}
