import { Component, OnInit,Inject } from '@angular/core';
import { GlobalService } from '../services/global.service';
import { Title,Meta } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../services/login.service';
import { JwtHelper } from '../services/jwt.service';
import { AppointmentsService } from './../services/appointments.service';
import { environment } from '../../environments/environment';
import { DOCUMENT } from '@angular/platform-browser';
declare var swal: any;

@Component({
  selector: 'app-term',
  templateUrl: './term.component.html',
  styleUrls: ['./term.component.scss']
})
export class TermComponent implements OnInit {
  
  urlId: any;
  short_code: any;
  cms_data: any =[];
  data:any;
  paymentUrl: any = environment.paymentUrl + '/core/payment/pay_session';

    termUser: any = localStorage.getItem("termType");
    constructor(
      @Inject(DOCUMENT) private document,
      private globalService: GlobalService,
      private loginservice : LoginService,
      private title: Title,
      private meta: Meta,
      private route: ActivatedRoute,
      private router: Router,
      private jwtHelper: JwtHelper,
      private appointObj: AppointmentsService) {

      // Start to Set Component  Title, Description and Keywords
this.title.setTitle('Intercell Terms and Condition - Intercellworld.com');
this.meta.updateTag({ name: 'description', content: 'Intercell Terms and Condition' });
this.meta.updateTag({ name: 'keyword', content: 'Intercell' });
// End to Set Component Title, Description and Keywords

this.route.params.subscribe( params => {
  
  this.urlId = params['urlId'];
  if(this.urlId=='mentee-terms'){
    this.short_code = 'mentee-terms';
    this.getCmsPage(this.short_code);
  }else if(this.urlId=='mentor-terms'){
    this.short_code = 'mentor-terms';
    this.getCmsPage(this.short_code);
  }else{
    
    this.cms_data.cms_desc='Loading...';
    if(this.urlId!=undefined){
      this.data = this.jwtHelper.decodeToken(this.urlId);
        
      if(this.data.action == 'cancel' ){
        let userId = this.data.userId;
        let sessionId = this.data.sessionId;
        this.cancelsession(userId,sessionId,this.urlId);
      }else if(this.data.action == 'approve' ){
        let userId = this.data.userId;
        let sessionId = this.data.sessionId;
        this.approvesession(userId,sessionId,this.urlId);
      }else if(this.data.action == 'paynow' ){
    
        this.getSessionDetails(this.data.sessionId,this.data.userId);     
        
        
      }else{
        this.emailverify(this.urlId);
      }

    
      
    }else{
      swal("Invalid web page");
      this.router.navigate(['/']);
    }

  }
 

  });
   
     }

  ngOnInit() {
    $('html, body').animate({ scrollTop: 0 }, 'slow');     
   

  }

  payNow(sessionId,userId) {
    (<HTMLFormElement>document.getElementById('sessionId')).value = sessionId;
    (<HTMLFormElement>document.getElementById('paymentUserId')).value = userId;
    var myForm = <HTMLFormElement>document.getElementById('payment_form');
    myForm.submit();
  }


  getCmsPage = function(short_code){

    this.loginservice.getCmsPage(short_code)
    .finally(() => { })
    .subscribe((response: any) => {
      
      if((response.status ==1))
      {
          this.cms_data = response.response[0].data;
      } else {
        
      }
    });

  }


  cancelsession = function(userId,sessionId,token){

     this.appointObj.cancelAppointment(userId, sessionId, token)
    .finally(() => {
      this.isLoading = false;
    })
    .subscribe((response: any) => {
      if ((response.status == 1)) {
        swal(response.message);
       
      }else{
        swal(response.message); 
      }
      this.router.navigate(['/']);
    });

  }

  approvesession = function(userId,sessionId,token){
   
    this.appointObj.approveAppointment(userId, sessionId, token)
    .finally(() => {
      this.isLoading = false;
    }).subscribe((response: any) => {
      
    if ((response.status == 1) && (response.type == 'success')) {
      swal(response.message);
      
    }else{
      swal(response.message); 
    }
    this.router.navigate(['/']);
  });

  }

   emailverify = function(token){
     this.loginservice.verifyEmailUrl(token)
      .finally(() => { })
      .subscribe((response: any) => {
        
        if((response.status ==1))
        {
          swal('Thank you for verifying your email address');
          this.router.navigate(['/']);
        } else {
          swal("Invalid web page");
          this.router.navigate(['/']);
        }
      });
    }


    getSessionDetails = function(sessionId,userId){

      this.appointObj.getSessionDetails(sessionId)
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
       
        if((response.status == 1) && (response.type == 'success'))
        {
          this.SessionDetails = response.response[0];
          if(this.SessionDetails.appointment_status != 'Cancelled'){
            setTimeout(()=>{
              this.payNow(sessionId,userId);
            }, 100);
          }else{
            swal("Sorry! Your Session already Cancelled");
            this.router.navigate(['/overview']);
          }
          
        }else{
          this.router.navigate(['/overview']);
        }
    });
    
    }



    

}
