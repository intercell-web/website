import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LearnMentorComponent } from './learn-mentor.component';

describe('LearnMentorComponent', () => {
  let component: LearnMentorComponent;
  let fixture: ComponentFixture<LearnMentorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LearnMentorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LearnMentorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
