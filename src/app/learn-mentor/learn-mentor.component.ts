import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-learn-mentor',
  templateUrl: './learn-mentor.component.html',
  styleUrls: ['./learn-mentor.component.scss']
})
export class LearnMentorComponent implements OnInit {

  isLogin: any = localStorage.getItem('userId');
  constructor() { }

  ngOnInit() {
    $('html, body').animate({ scrollTop: 0 }, 'slow');
  }
}
