import { Component, OnInit, Inject } from '@angular/core';
import { PaymentService } from '../services/payment.service';
import { Router} from '@angular/router';
import { GlobalService } from '../services/global.service';
import { BasicService} from '../services/basic.service';
import { DOCUMENT } from '@angular/platform-browser';
import { environment } from '../../environments/environment';
declare var swal: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  maxAmount: number;
  Amount: number;
  money: number;
  moneyError: boolean = false;
  sessionPayment: any;
  returnedPayment: any;
  fullName: any;
  registerPayment: any;
  userType: any;
  UserProfile:any= {};
  imageUrl: any = '';
  cdnPath: any = environment.cdnProfileUrl;
  unreadmessagecount1:any;

constructor(@Inject(DOCUMENT) private document,private router: Router, private paymentService: PaymentService , private globalService: GlobalService, private basicservice: BasicService) { }

  ngOnInit() {

    
    $(window).scroll(function(){
      if ($(this).scrollTop() > 100) {
        $('.scrollToTop').fadeIn();
      } else {
        $('.scrollToTop').fadeOut();
      }
    });

    //Click event to scroll to top
    $('.scrollToTop').click(function(){
      $('html, body').animate({scrollTop : 0}, 800);
      return false;
    });

    this.maxAmount = 0;
    this.Amount = 0;
    this.fullName = this.globalService.userFullName;
    this.document.getElementById('menuDashboard').className = 'active';
    this.userType = this.globalService.userType;
    this.unreadmessagecount1 = this.globalService.unreadmessagecount;
  //fetching user Data
  this.basicservice.getUserProfile(this.globalService.userId, localStorage.getItem('id_token'))
  .finally(() => { })
  .subscribe((response: any) => {
    if((response.status ==1) && (response.type === 'success'))
    {
      this.UserProfile = response.response;
      //console.log(this.UserProfile);
       this.imageUrl = this.UserProfile.dp_image;
      if(this.UserProfile.dp_image){
        this.UserProfile.dp_image= this.cdnPath + this.UserProfile.dp_image;
      }
    } 
  });


    if(this.globalService.userType == 2){

      //fetching All Sub Services
      this.paymentService.studentPayments(this.globalService.userId , this.globalService.id_token)
        .finally(() => { })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type === 'success'))
          {
            this.sessionPayment = response.response;

            for (let session of this.sessionPayment) {
            }
          } else {

          }
        });

      //fetching All Sub Services
      this.paymentService.refundedPayment(this.globalService.userId , this.globalService.id_token)
        .finally(() => { })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type === 'success'))
          {
            this.returnedPayment = response.response;

            for (let returned of this.returnedPayment) {
              this.maxAmount -= returned.amount;
            }
          } else {
          }
        });
    } else {
      //fetching All Sub Services
      this.paymentService.sessionPayments(this.globalService.userId , this.globalService.id_token)
        .finally(() => { })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type === 'success'))
          {
            this.sessionPayment = response.response;
           // console.log(this.maxAmount);
            //console.log(this.sessionPayment);
            this.Amount = 0;
            for (let session of this.sessionPayment) {
        
              this.Amount = session.price_amount-(parseFloat(session.our_commision)+parseFloat(session.ccavenue_commision));
              
              this.maxAmount += this.Amount;
            }
           
          } else {

          }
        });

      //fetching All Sub Services
      this.paymentService.returnedPayment(this.globalService.userId, this.globalService.id_token)
        .finally(() => { })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type === 'success'))
          {
            this.returnedPayment = response.response;

            for (let returned of this.returnedPayment) {
              this.maxAmount -= returned.amount;
            }
          } else {
          }
        });

      //fetching All Sub Services
      this.paymentService.registerUser(this.globalService.userId, this.globalService.id_token)
        .finally(() => { })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type === 'success'))
          {
            this.registerPayment = response.response;
          } else {
          }
        });
    }
  }


  onSubmit = function() {

    console.log(this.money);
    console.log(this.maxAmount);
    if(this.money > this.maxAmount){
      this.moneyError = true;
    } else {
 
      //fetching All Sub Services
      this.paymentService.returnPayment(this.globalService.userId, this.money ,this.globalService.id_token)
        .finally(() => { })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type === 'success'))
          {
           swal('Withdraw request send successfully');
           this.ngOnInit();
            this.money = '';
          } else {
          }
        });
 
      this.moneyError = false;
    }
   }


   ngClassForColor(value){
    return this.globalService.ngClassForColorsection(value);
    }

    onProfileClick = function () {
      
          if (this.globalService.userType == 3) {
            this.router.navigate(['/counsellor']);
          } else {
            this.router.navigate(['/student']);
          }
        }
}
