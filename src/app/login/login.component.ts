import 'rxjs/add/operator/finally';
import { Component, OnInit , Inject } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import * as AppConst from '../app.const';
import { DOCUMENT, Title,Meta } from '@angular/platform-browser';
import { LandingService } from '../services/landing.service';
import { JwtHelper } from '../services/jwt.service';
import { GlobalService } from '../services/global.service';
import { environment } from '../../environments/environment';
declare var gapi: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit{

  isLoading: boolean;
  serviceDetails: any = [];
  meetCounsellorDetails: any = [];
  studentSpeakDetails:any = [];
  myConst: any;
  formSearch: FormGroup;
  formCounsellor: FormGroup;
  consellorId: number;
  popFullName: string;
  popUserPic: string;
  patchDetails: any = [];
  incorrectDetails: any = false;
  cdnPath: any = environment.cdnProfileUrl;
  userId: any = +localStorage.getItem('userId');
  userType: any = +localStorage.getItem('userType');
  activediv: boolean = true;
  

  constructor(@Inject(DOCUMENT) private document,private title: Title, private meta: Meta, private globalService: GlobalService, private loginservice : LoginService, private landingService: LandingService , private router: Router , private jwtHelper : JwtHelper,private fb: FormBuilder) {
    localStorage.setItem('serviceDetails', '');
    localStorage.setItem('showTrackId', '');
    localStorage.setItem('showServiceId', '');
    // Start to Set Component  Title, Description and Keywords
this.title.setTitle('Home - Intercellworld.com');
this.meta.updateTag({ name: 'description', content: 'Home' });
this.meta.updateTag({ name: 'keyword', content: 'Intercell' });
// End to Set Component Title, Description and Keywords


  }

  ngOnInit() {


    this.buildForm();

    $(window).scroll(function(){

      if ($(this).scrollTop() > 280) {
        $('#mentee-part').addClass('animated fadeInUp');
      }

      if ($(this).scrollTop() > 700) {
        $('#middle-part').addClass('animated fadeInDown');
      }

      if ($(this).scrollTop() > 1000) {
        $('#mentor-part').addClass('animated fadeInUp');
      }

      if ($(this).scrollTop() > 1700) {
        $('.bottom-part').addClass('animated fadeInUp');
      }

      if ($(this).scrollTop() > 280) {
        $('#stream-part').addClass('animated fadeInUp');
      }

    });

    $("#testimonial").css("background-attachment", "fixed");
    //$("#testimonial").parallax("50%", "0.4");

    this.myConst = AppConst;

      if(this.globalService.showMentor) {
        $('html, body').animate({ scrollTop: $('#mentor-part').offset().top+640 }, 'slow');    
      }

    this.globalService.showMentor = 0;
  }
  
//ngOnInit end 


  buildForm() {
    this.formSearch = this.fb.group({
      first_name: ['', ''],
      last_name: ['', '']
    });

  }

  //add menu/links functionality routering functionality
  aboutCounsellor(){
    this.router.navigate(['/about-mentor']);
  }

  //add menu/links functionality routering functionality
  aboutStudent(){
    this.router.navigate(['/about-mentee']);
  }



  setCounsellor = function(user, indCounsellor, imageUrl){
    this.consellorId = user.user_id;
    this.popFullName = user.first_name +' '+user.last_name;

    localStorage.setItem('directCounsellorName', user.first_name +' '+user.last_name);

    if(imageUrl){
      this.popUserPic = this.cdnPath + imageUrl;
    } else {
      this.popUserPic = "https://d3213zi13us7f1.cloudfront.net/website/assets/images/intercell-user-pic.png";
    }
    /*this.document.getElementById('username').value;*/
  }





  directAppointment = function(user){
    this.consellorId = user.user_id;
    localStorage.setItem('directCounsellorName', user.first_name +' '+user.last_name);

    if(this.userType == 3)
    {
      this.globalService.userTypeText = 'Mentor';
      this.router.navigate(['/overview']);
      //this.router.navigate(['/set-availability']);
    }
    else if(this.userType == 2)
    {
      this.globalService.userTypeText = 'Mentee';
      this.router.navigate(['/direct-appointment/'+this.consellorId]);
    } else{
      console.log("User type is" + this.globalService.userType);
    }

  }


  doLogin = function(){
    //this.isLoading = true;
    this.user={'emailid': this.document.getElementById('username').value,'password':this.document.getElementById('password').value,'usertype':'student'};

    this.loginservice.doSign(this.user)
      .finally(() => { //this.isLoading = false;
      })
      .subscribe((response: any) => {
        if ((response.status == 1) && (response.type === 'success')) {
          this.data = this.jwtHelper.decodeToken(response.response.token);
          localStorage.setItem('id_token', response.response.token);
          this.globalService.id_token = response.response.token;

          if(this.data.profilePic){
            this.globalService.userPic = this.cdnPath + this.data.profilePic;
          } else {
            this.globalService.userPic = 'https://d3213zi13us7f1.cloudfront.net/website/assets/images/intercell-user-pic.png';
          }

          this.globalService.userFullName = this.data.firstName + ' ' + this.data.lastName;
          this.globalService.newUser = 0;
          this.globalService.userId = this.data.id;
          this.globalService.userType = this.data.group_id;
          if(this.globalService.userType == 3)
          {
            this.globalService.userTypeText = 'Mentor';
            this.router.navigate(['/overview']);
            //this.router.navigate(['/set-availability']);
          }
          else if(this.globalService.userType == 2)
          {
            this.globalService.userTypeText = 'Mentee';
            this.router.navigate(['/direct-appointment/'+this.consellorId]);
          } else{
            console.log("User type is" + this.globalService.userType);
          }

          localStorage.setItem('userFullName', this.globalService.userFullName);
          localStorage.setItem('userPic', this.globalService.userPic);
          localStorage.setItem('userId', this.globalService.userId);
          localStorage.setItem('userType', this.globalService.userType);
          localStorage.setItem('newUser', this.globalService.newUser);

        }else{
          this.incorrectDetails = true;
          setTimeout(()=>{
            this.incorrectDetails = false;
          }, 8000);
        }
      });
  }



  onJoin = function () {

    if(this.userId){

      if(this.userType == 3)
      {
        this.globalService.userTypeText = 'Mentor';
        this.router.navigate(['/overview']);
        //this.router.navigate(['/set-availability']);
      } else if(this.userType == 2)
      {
        this.globalService.userTypeText = 'Mentee';
        this.router.navigate(['/make-appointment']);
      } else{
        console.log("User type is" + this.globalService.userType);
      }
    } else {
      this.router.navigate(['/basic-detail']);
    }
    //this.router.navigate(['/term']);
  }

  onLearnMentor = function () {
    this.router.navigate(['/learn-mentor']);
  }

  onLearnMentee = function () {
    this.router.navigate(['/learn-mentee']);
  }


  goTrack = function(id) {
    localStorage.setItem('serviceDetails', '1');
    localStorage.setItem('showTrackId', id);

    this.globalService.serviceDetails = 1;
    this.globalService.showTrackId = id;
    this.router.navigate(['/track']);
  }

  serviceDetail = function(id){
    localStorage.setItem('showServiceId', id);
    this.globalService.showServiceId = id;
    this.router.navigate(['/track']);
  }

  OverviewNav = function(){
    console.log('overview');
    this.router.navigate(['/overview']);
  }

  mentorSearch = function(user){
    localStorage.setItem('first_name', user.first_name);
    localStorage.setItem('last_name', user.last_name);
    this.router.navigate(['/search-mentor']);
  }

  streamView = function(user){
    this.router.navigate(['/career-counsellors']);
  }
}
