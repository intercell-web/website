import { Component, OnInit , Inject } from '@angular/core';
import { DOCUMENT, Title,Meta  } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ResetPasswordService } from '../services/reset-password.service';
import {FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';

declare var jQuery: any;

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})

export class ResetPasswordComponent implements OnInit {

  emailId: any;
  userId: any;
  validKey: any;
  insertId: any;
  emailError: boolean = false;
  keyError: boolean = false;
  passError: boolean = false;
  password: any;
  cpassword: any;
  formEmail:any;
  formPassword : any;
  isLogin: any = localStorage.getItem('userId');

  formEmailErrors = {
    emailId : 'Email address is mandatory'
  };

  formPasswordErrors = {
    password : '',
    cpassword : ''
  };


  validationMessages = {
    emailId : {
      required: 'Email address is mandatory',
      pattern : 'Not the right pattern'
    },
    password : {
      required : 'Password is mandatory',
      minlength : 'Your password must be minimum 6 characters',
      maxlength : 'Your password must not exceed 20 characters',
      pattern : 'Password must have a minimum of 1 digit and 1 special character'
    },
    cpassword : {
      required: 'Confirm Password is mandatory'
    }
  }

  constructor(@Inject(DOCUMENT) private document, private router: Router,private title: Title, private meta: Meta, private fb: FormBuilder,private resetPasswordService: ResetPasswordService) {
    window.scrollTo(0, 0);
    if(this.isLogin){
      this.router.navigate(['/privacy']);
    }
     // Start to Set Component  Title, Description and Keywords
this.title.setTitle('Forgot Password - Intercellworld.com');
this.meta.updateTag({ name: 'description', content: 'Forgot Password' });
this.meta.updateTag({ name: 'keyword', content: 'Intercell' });
// End to Set Component Title, Description and Keywords
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.formEmail = this.fb.group({
      emailId: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$')]],
    });

    this.formPassword = this.fb.group({
      password : ['', Validators.compose([Validators.required,  Validators.minLength(6) , Validators.maxLength(20) , Validators.pattern('^(?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*?[.~#?!@$%^&*-]).{3,}$')])],
      cpassword : ['', Validators.compose([Validators.required])]
    });


    this.formEmail.valueChanges.subscribe(data => this.validateForm());
    this.formPassword.valueChanges.subscribe(data => this.validatePasswordForm());
   }

  validateForm(){
    for(let field in this.formEmailErrors){
      this.formEmailErrors[field]= '';
      this.emailError = false;

      let input = this.formEmail.get(field);

      if(input.invalid && input.dirty){
        for(let error in input.errors){
          this.formEmailErrors[field] = this.validationMessages[field][error];
        }
      }
    }
  }

validatePasswordForm() {
  for(let field in this.formPasswordErrors){
    this.formPasswordErrors[field]= '';
    this.emailError = false;

    let input = this.formPassword.get(field);

    if(input.invalid && input.dirty){
      for(let error in input.errors){
        this.formPasswordErrors[field] = this.validationMessages[field][error];
      }
    }
  }
}


onSubmit = function() {

   if(this.formEmail.controls.emailId.value =='' || this.formEmail.controls.emailId.value ==undefined){
      this.formEmailErrors.emailId ='Email address is mandatory';
      return false;
    }
   

    this.resetPasswordService.doSubmit(this.emailId)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          this.userId = response.response.userId;
          this.insertId = response.response.insertId;
          this.formEmailErrors.emailId = '';
          jQuery('.forgot-blk').hide();
          jQuery('.forgot-blk1').slideDown();
        } else {
          this.emailError = true;
          this.formEmailErrors.emailId='This email ID does not exist';
        }
      });
   }

  onCheck = function() {
    this.resetPasswordService.onCheck(this.userId, this.validKey)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
            jQuery('.forgot-blk1').hide();
            jQuery('.forgot-blk2').slideDown();

        } else {
          this.keyError = true;
        }
      });
  }


  onReset = function() {


    if(this.formPassword.controls.password.value ==undefined || this.formPassword.controls.password.value =='' ){
      this.formPasswordErrors.password = 'Please Enter New Password Password';
      return false;
  
      }else if(this.formPassword.controls.cpassword.value ==undefined || this.formPassword.controls.cpassword.value =='' ){
        this.formPasswordErrors.password = 'Please Enter Confirm Password';
        return false;
    
        }else if(this.formPassword.controls.password.value != this.formPassword.controls.cpassword.value  ){
          this.formPasswordErrors.password = 'Password Mismatch';
          return false;
      
          }else{

            this.resetPasswordService.onReset(this.password, this.userId, this.insertId)
            .finally(() => {
            })
            .subscribe((response: any) => {
    
              if ((response.status == 1) && (response.type === 'success')) {
                jQuery('.forgot-blk2').hide();
                jQuery('.forgot-blk3').slideDown();
              } else {
              }
            });

          }

   
  }
}
