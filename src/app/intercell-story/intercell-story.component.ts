import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-intercell-story',
  templateUrl: './intercell-story.component.html',
  styleUrls: ['./intercell-story.component.scss']
})
export class IntercellStoryComponent implements OnInit {

  constructor() {
    window.scrollTo(0, 0);
   }

  ngOnInit() {
  }

}
