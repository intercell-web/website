import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntercellStoryComponent } from './intercell-story.component';

describe('IntercellStoryComponent', () => {
  let component: IntercellStoryComponent;
  let fixture: ComponentFixture<IntercellStoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntercellStoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntercellStoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
