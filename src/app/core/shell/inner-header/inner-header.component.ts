import { Component, OnInit, Inject } from '@angular/core';
import { GlobalService} from '../../../services/global.service';
import { Router} from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';

@Component({
  selector: 'app-inner-header',
  templateUrl: './inner-header.component.html',
  styleUrls: ['./inner-header.component.scss']
})
export class InnerHeaderComponent implements OnInit {

  menuHidden = true;
  userId: number;
  userType: number;

  userTypeText: string;
  userFullName: string;
  userPic: string;
  isMenuSetAvailability:boolean=false;
  isMenuGetPayment:boolean=false;

  constructor(@Inject(DOCUMENT) private document, private globalService: GlobalService , private router: Router) {
    this.userId = this.globalService.userId;
    this.userType = this.globalService.userType;
    this.userTypeText = this.globalService.userTypeText;
    this.userFullName = this.globalService.userFullName;
    this.userPic = this.globalService.userProfilePath + '/' + this.globalService.userPic;
  }
  ngOnInit() {


  }


  onProfileClick = function () {
    if (this.globalService.userType == 3) {
      this.router.navigate(['/counsellor']);
    } else {
      this.router.navigate(['/student']);
    }
  }

  logOutClick= function (){
    //Making all the Global Service NULL
    localStorage.setItem('userFullName', null);
    localStorage.setItem('userPic', null);
    localStorage.setItem('userId', '');
    localStorage.setItem('userType', '');
    localStorage.setItem('newUser', null);
    localStorage.setItem('unreadmessagecount', '');

    this.globalService.userFullName = null;
    this.globalService.userPic = null;
    this.globalService.userId = 0;
    this.globalService.userType = 0;
    this.globalService.newUser = null;
    this.globalService.unreadmessagecount = null;

    this.router.navigate(['/']);
  }

  overview(){
    this.router.navigate(['/overview']);
  }

  showAppointments(){
    this.router.navigate(['/appointments']);
  }

  setAvailability(){
    //this.setTabsOff();
    this.isMenuSetAvailability = true;
    this.router.navigate(['/set-availability']);
  }

  getPayment(){
    //this.setTabsOff();
    this.isMenuGetPayment = true;
    this.router.navigate(['/get-payment']);
  }


  messages(){
    this.router.navigate(['/message']);
  }

  account(){
    this.router.navigate(['/account']);
  }
  setting(){
    this.router.navigate(['/privacy']); //setting
  }

}
