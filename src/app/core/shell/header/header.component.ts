import { Component} from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from '../../../services/global.service';
import { MessageService } from '../../../services/message.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  menuHidden = true;
  userId: number;
  userType: number;
  userTypeText: string;
  userFullName: string;
  userPic: string;
  userName: string;
  subscription: Subscription;
  subsc: Subscription;
  profileSub: Subscription;
  token: any;
  userTypementee:any = false;
  unreadmessagecount:any ;

  constructor(private globalService: GlobalService , private router: Router , private messageService: MessageService) {
    
    if(!(this.globalService.userId)){
      this.globalService.userId = +localStorage.getItem('userId');
    }
    if(!(this.globalService.userType)){
      this.globalService.userType = +localStorage.getItem('userType');
    }
    if(!(this.globalService.userTypeText)){
      this.globalService.userTypeText = localStorage.getItem('userTypeText');
    }
    if(!(this.globalService.userFullName)){
      this.globalService.userFullName = localStorage.getItem('userFullName');
    }

    if(!(this.globalService.userPic)){
      this.globalService.userPic = localStorage.getItem('userPic');
    }

    if(!(this.globalService.id_token)){
      this.globalService.id_token = localStorage.getItem('id_token');
    }

   if(!(this.globalService.newUser)){
      this.globalService.newUser = +localStorage.getItem('newUser');
    }
    if(!(this.globalService.unreadmessagecount)){
      this.globalService.unreadmessagecount = +localStorage.getItem('unreadmessagecount');
    }

    this.userId = this.globalService.userId;
    this.token = this.globalService.id_token;
    this.userType = this.globalService.userType;
    this.userTypeText = this.globalService.userTypeText;
    this.userFullName = this.globalService.userFullName;
    this.unreadmessagecount = this.globalService.unreadmessagecount;
    /*this.userName = this.globalService.username;*/

    if(this.userType==2){
      this.userTypementee = true;
    }
    this.userPic = this.globalService.userPic;

    this.subscription = this.globalService.getMessage().subscribe(message => {this.globalService.userFullName = message.text; this.userFullName =   message.text; });

    this.subsc = this.globalService.getDisplayName().subscribe(message => {this.globalService.username = message.text; this.userName =  message.text;  });

    this.profileSub = this.globalService.getPic().subscribe(message => {this.globalService.userPic = message.text; this.userPic =  message.text; });
  
 

  }

  ngOnDestroy() {
      //unsubscribe to ensure no memory leaks
      this.subscription.unsubscribe();
      this.subsc.unsubscribe();
      this.profileSub.unsubscribe();
  }

  ngAfterViewInit() {
  }

  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("pageslide-left").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
    $(".sb-toggle-left").addClass('hide');
    $(".side-close-btn").removeClass('hide');
  }

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("pageslide-left").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
    $(".side-close-btn").addClass('hide');
    $(".sb-toggle-left").removeClass('hide');
  }



  //add menu/links functionality routering functionality
  aboutUs() {
    this.router.navigate(['/about-us']);
  }
  downloadApp(){
    this.router.navigate(['/download-app']);
  }
  makeAppointment(){
    this.router.navigate(['/make-appointment']);
  }
  showAppointments(){
    this.router.navigate(['/appointments']);
  }
  showProfile() {
    this.router.navigate(['/profile']);
  }

  onProfileClick = function () {

    if (this.globalService.userType == 3) {
      this.router.navigate(['/counsellor']);
    } else {
      this.router.navigate(['/student']);
    }
  }
  logOutClick= function (){
    //Making all the Global Service NULL
    localStorage.setItem('userFullName', null);
    localStorage.setItem('userPic', null);
    localStorage.setItem('userId', '');
    localStorage.setItem('userType', '');
    localStorage.setItem('newUser', null);
    localStorage.setItem('unreadmessagecount', '');

    this.globalService.userFullName = null;
    this.globalService.userPic = null;
    this.globalService.userId = 0;
    this.globalService.userType = 0;
    this.globalService.newUser = null;
    this.globalService.unreadmessagecount = null;
    localStorage.clear();
    this.router.navigate(['/']);
  }
  referFriend() {
    this.router.navigate(['/refer-friend']);
  }
  favorite(){
    this.router.navigate(['/favorite']);
  }

  messages(){
    this.router.navigate(['/message']);
  }

  privacy(){
    this.router.navigate(['/privacy']);
  }

  logOut(){
    //Making all the Global Service NULL
    localStorage.setItem('userFullName', null);
    localStorage.setItem('userPic', null);
    localStorage.setItem('userId', '');
    localStorage.setItem('userType', '');
    localStorage.setItem('newUser', null);
    localStorage.setItem('unreadmessagecount', '');

    this.globalService.userFullName = null;
    this.globalService.userPic = null;
    this.globalService.userId = 0;
    this.globalService.userType = 0;
    this.globalService.newUser = null;

    this.router.navigate(['/']);
  }

}
