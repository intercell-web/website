import { Component, OnInit } from '@angular/core';
import { GlobalService} from '../../../services/global.service';
import { Router} from '@angular/router';
import { LoginService } from '../../../services/login.service';
import { JwtHelper } from '../../../services/jwt.service';
import { environment } from '../../../../environments/environment';

declare var gapi: any;
declare var FB: any;
declare var IN: any;
declare var swal: any;

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public userType: number = 0;
   cdnPath: any = environment.cdnProfileUrl;

  isLogin: any = localStorage.getItem('userId');
  

  constructor(private globalService : GlobalService ,private router: Router , private loginservice: LoginService , private jwtHelper: JwtHelper) {
    this.userType = this.globalService.userType;
  }

  ngOnInit() {

    $(window).scroll(function(){
      if ($(this).scrollTop() > 100) {
        $('.scrollToTop').fadeIn();
      } else {
        $('.scrollToTop').fadeOut();
      }
    });


    //Click event to scroll to top
    $('.scrollToTop').click(function(){
      $('html, body').animate({scrollTop : 0}, 800);
      return false;
    });

    /*$('.slide-bx li').mouseenter(function () {
      $(this).css({'left': '0'})
    });

    $('.slide-bx li').mouseleave(function () {
      $(this).css({'left': '-117px', 'transition': '0.9s ease all'})
    });*/

    
  }

  fb_login = () =>  {
    this.router.navigate(['']);

/*    FB.login(function (response) {
      if (response.authResponse) {
        const access_token = response.authResponse.accessToken;
        const user_id = response.authResponse.userID;

        if (response.status === 'connected') {

          console.log('Welcome!  Fetching your information.....');

          FB.api('/me?fields=email,name', function (respon) {

            if (respon.email) {
              this.onSocial(respon.email);
            } else {
              this.emailNotPresent = false;

              setTimeout(() => {
                this.emailNotPresent = true;
              }, 10000);

            }

          }.bind(this));

          if (response.status === 'not_authorized') {
            // The person is logged into Facebook, but not your app.
            //document.getElementById('status').innerHTML = 'Please log ' + 'into this app.';
          } else {
            // The person is not logged into Facebook, so we're not sure if
            // they are logged into this app or not.
            //document.getElementById('status').innerHTML = 'Please log ' +'into Facebook.';
          }
        }

      } else {
        //user hit cancel button
        console.log('User cancelled login or did not fully authorize.');
      }
    }.bind(this), {
      scope: 'public_profile,email'
    });*/
  }

  googleLogin() {
    const googleAuth = gapi.auth2.getAuthInstance();
    googleAuth.then(() => {
      googleAuth.signIn({scope: 'profile email'}).then(googleUser => {
        this.onSocial(googleUser.getBasicProfile().getEmail());
      });
    });
  }


  //Invoke login window with button
  public liAuth() {
    const linkedIn = document.createElement('script');
    linkedIn.type = 'text/javascript';
    linkedIn.src = 'https://platform.linkedin.com/in.js';
    linkedIn.innerHTML = '\n' +
      ' api_key: 812lhg4xw3pp6x\n' +
      ' authorize: true\n' +
      ' onLoad: onLinkedInLoad\n' +
      ' scope: r_basicprofile r_emailaddress';
    document.head.appendChild(linkedIn);
    const script = document.createElement('script');
    script.type = 'in/Login';
    document.body.appendChild(script);

    setTimeout(() => {
      if(this.detectPopupBlocker()){
        IN.User.authorize(function () {
          console.log('authorize callback');
        });
        this.onLinkedInProfile();
      }
    }, 2000);
  }


  //add menu/links functionality routering functionality
  aboutCounsellor(){
    this.router.navigate(['/about-mentor']);
  }

  //add menu/links functionality routering functionality
  aboutStudent(){
    this.router.navigate(['/about-mentee']);
  }

  onTerm(){
    this.router.navigate(['/term']);
  }

  onNews(){
    this.router.navigate(['/news']);
  }

  aboutusView(){
    this.router.navigate(['/about-us']);
  }


  onFaq(){
    this.router.navigate(['/faq']);
  }

  onMentor(){
    this.router.navigate(['/search-mentor']);
  }

  detectPopupBlocker = function() {
    const myTest = window.open("about:blank","","height=100,width=100");

    if (!myTest)
    {
      swal('Please check browser Internet settings and disable pop-up blocker');
      return false;
    } else
    {
      myTest.close();
      return true;
    }
  }

  public onLinkedInProfile() {
    IN.API.Profile("me")
      .fields("id", "firstName", "lastName", "email-address")
      .result(this.displayProfiles)
      .error(this.displayProfilesErrors);
  }

  public displayProfiles = (profiles) => {
    this.onSocial(profiles.values[0]['emailAddress']);
  }

  public displayProfilesErrors(error) {
    console.log(error);
  }

  onSocial = function(email){
    this.loginservice.doSocialSign(email).finally(() => {
      //this.isLoading = false;
    }).subscribe((response: any) => {

      if ((response.status == 1) && (response.type === 'success')) {

        this.data = this.jwtHelper.decodeToken(response.response.response);
        localStorage.setItem('id_token', response.response.response);
        this.globalService.id_token = response.response.response;

        this.globalService.userFullName = this.data.firstName + ' ' + this.data.lastName;

        if(this.data.profilePic){
          this.globalService.userPic = this.cdnPath + this.data.profilePic;
        } else {
          this.globalService.userPic ="https://d3213zi13us7f1.cloudfront.net/website/assets/images/intercell-user-pic.png";
        }

        this.globalService.newUser = 0;
        this.globalService.userId = this.data.id;

        this.globalService.userType = this.data.group_id;

        localStorage.setItem('userFullName', this.globalService.userFullName);
        localStorage.setItem('userPic', this.globalService.userPic);
        localStorage.setItem('userId', this.globalService.userId + '');
        localStorage.setItem('userType', this.globalService.userType + '');
        /*localStorage.setItem('username', this.globalService.username);*/
        localStorage.setItem('newUser', this.globalService.newUser + '');

        if (this.globalService.userType == 3) {
          this.globalService.userTypeText = 'Mentor';
          localStorage.setItem('userTypeText', this.globalService.userTypeText);
          this.router.navigate(['/overview']);
          //this.router.navigate(['/set-availability']);
        }
        else if (this.globalService.userType == 2) {
          this.globalService.userTypeText = 'Mentee';
          localStorage.setItem('userTypeText', this.globalService.userTypeText);
          this.router.navigate(['/overview']);
          //this.router.navigate(['/student']);
        } else {
          console.log("User type is" + this.globalService.userType);
        }
      } else {
         swal("This credentials are not registered");
      }
    });
  }

}
