import { Component, OnInit, NgZone , AfterViewInit } from '@angular/core';
import { AuthService, AppGlobals } from 'angular2-google-login';

@Component({
  selector: 'app-google-test',
  templateUrl: './google-test.component.html',
  providers: [AuthService]
})

export class GoogleTestComponent implements OnInit {
  imageURL: string;
  email: string;
  name: string;
  token: string;

  constructor(private auth: AuthService, private zone: NgZone) { }

  /**
   * Ininitalizing Google Authentication API and getting data from localstorage if logged in
   */
  ngOnInit() {
    //Set your Google Client ID here
    AppGlobals.GOOGLE_CLIENT_ID = '473737229918-pfmo7bsgh22f6nietb987fg5phkdind7';
    this.getData();
    setTimeout(() => { this.googleAuthenticate()}, 5);
  }

  /**
   * Calling Google Authentication service
   */
  googleAuthenticate() {
       this.auth.authenticateUser((result) => {
      //Using Angular2 Zone dependency to manage the scope of variables
      this.zone.run(() => {
        this.getData();
      });
    });
  }

  /**
   * Getting data from browser's local storage
   */
  getData() {
    this.token = localStorage.getItem('token');
    this.imageURL = localStorage.getItem('image');
    this.name = localStorage.getItem('name');
    this.email = localStorage.getItem('email');
  }

  /**
   * Logout user and calls function to clear the localstorage
   */
  logout() {
    let scopeReference = this;
    this.auth.userLogout(function () {
      scopeReference.clearLocalStorage();
    });
  }

  /**
   * Clearing Localstorage of browser
   */
  clearLocalStorage() {
    localStorage.removeItem('token');
    localStorage.removeItem('image');
    localStorage.removeItem('name');
    localStorage.removeItem('email');
  }
}
