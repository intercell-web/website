import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleTestComponent } from './google-test.component';

describe('GoogleTestComponent', () => {
  let component: GoogleTestComponent;
  let fixture: ComponentFixture<GoogleTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoogleTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoogleTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
