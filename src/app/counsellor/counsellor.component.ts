import { Component, OnInit, Inject } from '@angular/core';
import { Router} from '@angular/router';
import {FormGroup, FormControl, Validators , FormBuilder} from '@angular/forms';
import { DOCUMENT } from '@angular/platform-browser';
import { CounsellorService } from '../services/counsellor.service';
import { GlobalService} from '../services/global.service';
import { BasicService} from '../services/basic.service';
import { environment } from '../../environments/environment';
declare var swal: any;
declare var jQuery: any;

@Component({
  selector: 'app-counsellor',
  templateUrl: './counsellor.component.html',
  styleUrls: ['./counsellor.component.scss']
})

export class CounsellorComponent implements OnInit {

  userDetails: any = {};
  form;
  formEducation;
  formExperience;
  formCertificate;
  formPayment;
  counsellorDetails: any = {};
  counsellorEducationDetails: any = [];
  counsellorExperienceDetails: any = [];
  formEducationCollection: any = [];
  formExperienceCollection: any = [];
  formEducationObject: any;
  formExperienceObject: any;
  services: any;
  subservices: any;
  subLoop: any = [];
  catLoop: any = [];
  counsellorCertificateDetails: any = [];
  formCertificateObject: any;
  formCertificateCollection: any = [];
  noDetails = true;
  paymentDetails: any = {};
  noPaymentDetails: boolean = true;
  accountError : boolean = false;
  userId : any;
  imageUrl: any = '';
  ErrorMessage:string ='';
  showCounsellingSub: boolean = false;
  eduYearValidation: boolean = false;
  expYearValidation: boolean = false;
  eduYearArrayValidation: any =[];
  expYearArrayValidation: any =[];
  cdnPath: any = environment.cdnProfileUrl;
  subLanguageLoop: any = [];

  selectedSubServices: any = [ ];
  selectedServices: any = [];
  countryList: any = [];
  stateList: any = [];
  cityList: any = [];
  subServiceList = [];
  serviceList:any = [];
  expyears:number[] = [];

  languageList : any = [{"id": 1, "itemName": "Bihari (भोजपुरी)"}, {"id": 2, "itemName": "English (English)"}, {"id": 3, "itemName": "Gujarati (ગુજરાતી)"}, {"id": 4, "itemName": "Hindi (हिन्दी)"}, {"id": 5, "itemName": "Marathi (मराठी)"}, {"id": 6,"itemName": "Punjabi (ਪੰਜਾਬੀ)"}, {"id": 7, "itemName": "Urdu (اردو)"},{"id": 8, "itemName": "Bengali (বাংলা)"}];

  formErrors = {
    category: '',
    subcategory : '',
    firstname : '',
    lastname : '',
    brand_worked_for : '',
    totalexperience : '',
    summary: '',
    gender : '',
    //language : '',
    linkedInLink : ''
  };

  formPaymentErrors = {
    aadhar_card : '',
    account_pan : '',
    account_holder : '',
    account_number : '',
    confirm_account_number : '',
    bankname : '',
    branch : '',
    ifsc_code : '',
  };

  validationMessages = {
    firstname: {
      required: 'You must enter your First Name',
    },
    lastname: {
      required: 'You must enter your Last Name',
    },
    category: {
      required: 'Category is Required',
    },
    subcategory: {
      required: 'Sub Category is Required',
    },
    brand_worked_for: {
      required: 'Brand worked for is Required',
    },
    totalexperience: {
      required: 'Total Experience is Required',
    },
    summary: {
      required: 'Summary is Required',
    },
    /*language: {
      required: 'Language is Required',
    },*/
    linkedInLink : {
        pattern: 'Invalid web page entered'
     },
     aadhar_card : {
      required: 'Aadhar Card Number is Required'
      },
     account_pan : {
      required: 'PAN Card Number is Required'
      },
      account_holder : {
       required: 'Account Holder Name is Required'
      },
      account_number : {
      required: 'Account Number is Required'
      },
      confirm_account_number : {
      required: 'Confirm Account Number is Required'
      },
      bankname : {
      required: 'Bank Name is Required'
      },
      branch : {
      required: 'Bank Branch Name is Required'
      },
      ifsc_code : {
      required: 'Bank IFSC Code is Required'
      },
      gender: {
        required: 'You must select one option from the menu',
      },
        
  }

  selectedLanguageList : any  = [];

  dropdownSettings= {
    singleSelection: false,
    text:"Select Specialisation",
    selectAllText:'Select All',
    unSelectAllText:'UnSelect All',
    enableSearchFilter: true,
    classes:"myclass custom-class"
  };

  dropdownServices= {
    singleSelection: false,
    text:"Select Field",
    selectAllText:'Select All',
    unSelectAllText:'UnSelect All',
    enableSearchFilter: true,
    classes:"myclass custom-class"
  };

  languageSettings= {
    singleSelection: false,
    text:"Select Language",
    selectAllText:'Select All',
    unSelectAllText:'UnSelect All',
    enableSearchFilter: true,
    classes:"myclass custom-class"
  };

  constructor(@Inject(DOCUMENT) private document, private globalService: GlobalService, private counsellorservice: CounsellorService, private router: Router, private fb: FormBuilder, private basicservice: BasicService ) {
  }

  ngOnInit() {
    //$('html, body').animate({ scrollTop: 0 }, 'slow');

    $(window).scroll(function(){
      if ($(this).scrollTop() > 100) {
        $('.scrollToTop').fadeIn();
      } else {
        $('.scrollToTop').fadeOut();
      }
    });

    //Click event to scroll to top
    $('.scrollToTop').click(function(){
      $('html, body').animate({scrollTop : 0}, 800);
      return false;
    });

    this.document.getElementById('menuProfile').className = 'active';

    //fetching All Services
      this.counsellorservice.getAllServiceslist()
        .finally(() => { })
        .subscribe((response: any) => {

          if((response.status == 1) && (response.type === 'success'))
          {
            this.services = response.response;
            this.serviceList = response.response;
           
          } else {
          }
        });


      //fetching All Sub Services
      this.counsellorservice.getAllSubServices()
        .finally(() => { })
        .subscribe((response: any) => {

          if((response.status == 1) && (response.type === 'success'))
          {
            this.subservices = response.response;

            

          } else {
          }
        });

      //fetching All Countries List
      this.basicservice.getCountriesList()
        .finally(() => { })
        .subscribe((response: any) => {

          if((response.status == 1) && (response.type === 'success'))
          {
            this.countryList = response.response;
          } else {
          }
        });

      this.userId = this.globalService.userId;
      //fetching All Sub Services
      this.counsellorservice.getCounsellorPaymentDetails(this.globalService.userId , this.globalService.id_token)
        .finally(() => { })
        .subscribe((response: any) => {

          if((response.status == 1) && (response.type === 'success'))
          {
            
            this.paymentDetails = response.response[0];
             this.noPaymentDetails = false;

            this.formPayment.patchValue({ifsc_code: this.paymentDetails.ifsc_code , branch : this.paymentDetails.branch,
              account_number : this.paymentDetails.account_number, confirm_account_number : this.paymentDetails.account_number,
              account_holder : this.paymentDetails.account_holder_name,aadhar_card : this.paymentDetails.aadhar_card,account_pan : this.paymentDetails.account_pan,bankname : this.paymentDetails.bankname
            });
           
          } else {
          }
        });


      //fetching user Data
      this.counsellorservice.getUserDetails(this.globalService.userId , this.globalService.id_token )
        .finally(() => { })
        .subscribe((response: any) => {

          if((response.status == 1) && (response.type === 'success'))
          {
            this.userDetails = response.response;

            
            this.imageUrl = this.userDetails.dp_image;

            if(this.userDetails.dp_image){
              this.userDetails.dp_image= this.cdnPath + this.userDetails.dp_image;
            }

            this.globalService.sendMessage(this.userDetails.first_name + ' ' + this.userDetails.last_name);

            this.form.patchValue({lastname: this.userDetails.last_name , countryid : this.userDetails.country_id,
              stateid : this.userDetails.state_id, cityid : this.userDetails.city_id ,
              firstname : this.userDetails.first_name , linkedInLink : this.userDetails.ln, gender : this.userDetails.gender, brand_worked_for : this.userDetails.brand_worked_for
            });


            if(this.userDetails.country_id ==1) {
              this.userDetails.country = "";
              this.form.patchValue({ countryid:"" });
            }

            if(this.userDetails.state_id==1) {
              this.userDetails.state = "";
              this.form.patchValue({stateid : ""});
            }

            if(this.userDetails.city_id ==1) {
              this.userDetails.city = "";
              this.form.patchValue({cityid: ""});
            }

            //fetching All State List
            this.basicservice.getStatesList(this.userDetails.country_id)
              .finally(() => { })
              .subscribe((response: any) => {

                if((response.status == 1) && (response.type === 'success'))
                {
                  this.stateList = response.response;
                } else {
                }
              });

            //fetching All City List
            this.basicservice.getCitiesList(this.userDetails.state_id)
              .finally(() => { })
              .subscribe((response: any) => {

                if((response.status == 1) && (response.type === 'success'))
                {
                  this.cityList = response.response;
                } else {
                }
              });

           // this.router.navigate(['/counsellor']);
          } else {
          }
        });


        this.CounsellorDetailsView();

      //fetching Counsellor Education Data
      this.counsellorservice.getCounsellorEducation(this.globalService.userId , this.globalService.id_token )
        .finally(() => { })
        .subscribe((response: any) => {

            if((response.status ==1) && (response.type === 'success'))
          {
            this.counsellorEducationDetails = response.response;

            for (let education of this.counsellorEducationDetails){

              this.formEducationObject = this.fb.group({
                school : new FormControl(education.school , Validators.compose([Validators.required])),
                degree : new FormControl(education.degree , Validators.compose([Validators.required])),
                //startTime : new FormControl(education.start_time , Validators.compose([Validators.required])),
               // endTime : new FormControl(education.end_time , Validators.compose([Validators.required])),
                field : new FormControl(education.field , Validators.compose([Validators.required])),
               // description : new FormControl(education.description , Validators.compose([])),
                educationId : new FormControl(education.id),
              });

              this.formEducationCollection.push(this.formEducationObject);
            }
          } else {
            this.counsellorEducationDetails = [];
          }
        });

      //fetching Counsellor Education Data0
      this.counsellorservice.getCounsellorCertificate(this.globalService.userId , this.globalService.id_token )
        .finally(() => { })
        .subscribe((response: any) => {

          if((response.status ==1) && (response.type === 'success'))
          {
            this.counsellorCertificateDetails = response.response;

            for (let certificate of this.counsellorCertificateDetails){

              this.formCertificateObject = this.fb.group({

                certificate : new FormControl(certificate.certificate, Validators.compose([Validators.required])),
                certificateMonth : new FormControl((certificate.time.split(" ",2)[0]), Validators.compose([Validators.required])),
                certificateYear : new FormControl((certificate.time.split(" ",2)[1]), Validators.compose([Validators.required])),
                certificateInstitute : new FormControl(certificate.institute, Validators.compose([Validators.required])),
                certificateId : new FormControl(certificate.id),
              });

              this.formCertificateCollection.push(this.formCertificateObject);
            }

          } else {
            this.counsellorCertificateDetails = [];
          }
        });


      //fetching user Data
      this.counsellorservice.getCounsellorExperience(this.globalService.userId , this.globalService.id_token )
        .finally(() => { })
        .subscribe((response: any) => {

          if((response.status ==1) && (response.type === 'success'))
          {
            this.counsellorExperienceDetails = response.response;

            for (let experience of this.counsellorExperienceDetails){

              this.formExperienceObject = this.fb.group({

                title : new FormControl(experience.title , Validators.compose([Validators.required])),
                company : new FormControl(experience.company , Validators.compose([Validators.required])),
                location : new FormControl(experience.location , Validators.compose([Validators.required])),
                description : new FormControl(experience.description , Validators.compose([])),
                startTimeMonth : new FormControl(experience.start_time_month , Validators.compose([Validators.required])),
                endTimeMonth : new FormControl(experience.end_time_month , Validators.compose([Validators.required])),
                startTimeYear : new FormControl(experience.start_time_year , Validators.compose([Validators.required])),
                endTimeYear : new FormControl(experience.end_time_year , Validators.compose([Validators.required])),
                current : new FormControl(experience.current),
                experienceId : new FormControl(experience.id)
              });

              this.formExperienceCollection.push(this.formExperienceObject);
            }
          } else {
            this.counsellorExperienceDetails = [];
          }
        });


    this.form = new FormGroup({
      category : new FormControl('' , Validators.compose([Validators.required])),
      subcategory : new FormControl({value:'' , disabled:true} , Validators.compose([Validators.required])),
      countryid : new FormControl('' , Validators.compose([])),
      stateid : new FormControl('' , Validators.compose([])),
      cityid : new FormControl('' , Validators.compose([])),
      firstname : new FormControl('' , Validators.compose([Validators.required])),
      lastname : new FormControl('', Validators.compose([Validators.required])),
      brand_worked_for : new FormControl('' , Validators.compose([Validators.required])),
      totalexperience : new FormControl('', Validators.compose([Validators.required])),
      summary : new FormControl('' , Validators.compose([])),
      //language : new FormControl('', Validators.compose([Validators.required])),
      gender : new FormControl('', Validators.compose([Validators.required])),
      linkedInLink : new FormControl('' , Validators.compose([Validators.pattern('^http(s)?:\/\/([w]{3}\.)?linkedin\.com\/in\/([a-zA-Z0-9-]{5,30})\/?')]))
    });


    this.formPayment = new FormGroup({
      branch : new FormControl('' , Validators.compose([Validators.required])),
      bankname : new FormControl('' , Validators.compose([Validators.required])),
      account_holder : new FormControl('' , Validators.compose([Validators.required])),
      ifsc_code : new FormControl('' , Validators.compose([Validators.required])),
      account_number : new FormControl('' , Validators.compose([Validators.required])),
      confirm_account_number : new FormControl('' , Validators.compose([Validators.required])),
      account_pan : new FormControl('' , Validators.compose([Validators.required])),
      aadhar_card : new FormControl('' , Validators.compose([Validators.required])),
    });

    this.formEducation = new FormGroup({
      school : new FormControl('' , Validators.compose([Validators.required])),
      degree : new FormControl('' , Validators.compose([Validators.required])),
      startTime : new FormControl('' , Validators.compose([Validators.required])),
      endTime : new FormControl('' , Validators.compose([Validators.required])),
      field : new FormControl('' , Validators.compose([Validators.required])),
      description : new FormControl('' , Validators.compose([])),
      educationId : new FormControl(''),
    });

    this.formExperience = new FormGroup({
      title : new FormControl('' , Validators.compose([Validators.required])),
      company : new FormControl('' , Validators.compose([Validators.required])),
      location : new FormControl('' , Validators.compose([Validators.required])),
      description : new FormControl('' , Validators.compose([])),
      startTimeYear : new FormControl('' , Validators.compose([Validators.required])),
      endTimeYear : new FormControl('' , Validators.compose([Validators.required])),
      startTimeMonth : new FormControl('' , Validators.compose([Validators.required])),
      endTimeMonth : new FormControl('' , Validators.compose([Validators.required])),
      current : new FormControl(''),
      experienceId : new FormControl('')
    });

    this.formCertificate = new FormGroup({
      certificate : new FormControl('', Validators.compose([Validators.required])),
      certificateMonth : new FormControl('', Validators.compose([Validators.required])),
      certificateYear : new FormControl('', Validators.compose([Validators.required])),
      certificateInstitute : new FormControl('', Validators.compose([Validators.required])),
      certificateId : new FormControl('')
    });


    this.form.valueChanges.subscribe(data => this.validateForm());
    this.formPayment.valueChanges.subscribe(data => this.validatePaymentForm());
    this.getexpYear();
  }

  validateForm(){
    for(let field in this.formErrors){
      this.formErrors[field]= '';

      let input = this.form.get(field);

      if(input.invalid && input.dirty){
        for(let error in input.errors){
          this.formErrors[field] = this.validationMessages[field][error];
        }
      }
    }
  }

  
  validatePaymentForm(){
    for(let field in this.formPaymentErrors){
      this.formPaymentErrors[field]= '';
      
      let input = this.formPayment.get(field);

      if(input.invalid && input.dirty){
        for(let error in input.errors){
          this.formPaymentErrors[field] = this.validationMessages[field][error];
        }
      }
    }
  }

  deleteExperience = function (id) {
    this.counsellorservice.deleteCounsellorExperience(id,this.globalService.id_token)
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
        this.ngOnInit();

        if((response.status ==1) && (response.type === 'success'))
        {
          this.ngOnInit();
        } else {
        }
      });
  };


  onServiceChange = function (event){
    setTimeout(()=> {      
      this.subServiceList = [];
      if (this.form.controls.category.value.length == 0) {
        this.showCounsellingSub = false;
        this.form.controls.subcategory.disable();
      } else {
        for(var p=0;p<this.form.controls.category.value.length;p++){
          if(this.subservices[this.form.controls.category.value[p].id] != undefined) {
            this.subServiceList = this.subServiceList.concat(this.subservices[this.form.controls.category.value[p].id]);
          }
          }
        this.showCounsellingSub = true;
        this.form.controls.subcategory.enable();
      }

      this.selectedSubServices = [];
    },1500);
  };


  onSubServiceChange = function (event){
  };

  onInput= function() {
    if(jQuery("#education_start_time").val() && jQuery("#education_end_time").val()){

      if(jQuery("#education_start_time").val() > jQuery("#education_end_time").val()){
        this.eduYearValidation = true;
      } else {
        this.eduYearValidation = false;
      }
    } else {
      this.eduYearValidation=false;
    }
  }


  onInput_= function(id) {

    if(jQuery("#education_start_time_"+id).val() && jQuery("#education_end_time_"+id).val()){

      if(jQuery("#education_start_time_"+id).val() > jQuery("#education_end_time_"+id).val()){
        this.eduYearArrayValidation[id] = true;
      } else {
        this.eduYearArrayValidation[id] = false;
      }
    } else {
      this.eduYearArrayValidation[id]=false;
    }
  }

  onExpInput= function() {

    if(jQuery('#startTimeYear').val() && jQuery('#endTimeYear').val() && jQuery('#startTimeMonth').val() && jQuery('#endTimeMonth').val()){
      if(jQuery('#startTimeYear').val() > jQuery('#endTimeYear').val()){
        this.expYearValidation=!this.expYearValidation;
      } else if(jQuery('#startTimeYear').val() == jQuery('#endTimeYear').val()) {

        if( jQuery('#startTimeMonth option:selected').attr('id') > jQuery('#endTimeMonth option:selected').attr('id')){
          this.expYearValidation = true;
        }else {
          this.expYearValidation=false;
        }
      }else {
        this.expYearValidation=false;
      }
    } else {
      this.expYearValidation=false;
    }
  }


  onExpInput_= function(id) {
    if(jQuery("#startTimeYear_"+id).val() && jQuery("#endTimeYear_"+id).val() && jQuery("#startTimeMonth_"+id).val() && jQuery("#endTimeMonth_"+id).val()){
      if(jQuery("#startTimeYear_"+id).val() > jQuery("#endTimeYear_"+id).val()) {
        this.expYearArrayValidation[id]=!this.expYearArrayValidation[id];
      } else if(jQuery("#startTimeYear_"+id).val() == jQuery("#endTimeYear_"+id).val()) {

        if ( +jQuery("#startTimeMonth_"+id+" option:selected").attr('id')  > +jQuery("#endTimeMonth_"+id+" option:selected").attr('id')){
          this.expYearArrayValidation[id] = true;
        } else {
          this.expYearArrayValidation[id] = false;
        }
      }else{
        this.expYearArrayValidation[id] = false;
      }
    } else {
      this.expYearArrayValidation[id] = false;
    }
  }

  onCountryChange = function (event){
    //fetching All State List
    this.basicservice.getStatesList(event)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          this.stateList = response.response;
          this.cityList = [];
        } else {
        }
      });
  };


  onStateChange = function (event){
    //fetching All State List
    this.basicservice.getCitiesList(event)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          this.cityList = response.response;
        } else {
        }
      });
  };

  deleteEducation = function (id) {
    this.counsellorservice.deleteCounsellorEducation(id , this.globalService.id_token )
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
        this.ngOnInit();
        if((response.status ==1) && (response.type === 'success'))
        {
          this.ngOnInit();
        } else {
        }
      });
  };

  deleteCertificate = function (id) {
    this.counsellorservice.deleteCounsellorCertificate(id , this.globalService.id_token )
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
        this.ngOnInit();
        if((response.status ==1) && (response.type === 'success'))
        {
          this.ngOnInit();
        } else {
        }
      });
  };

  onSubmit = function (user) {

    
    if(!this.form.valid){

      this.ErrorMessage = 'Please fill in required details';
      return false;
    }

    user.userId = this.globalService.userId;

    if(user.category){
      this.catLoop = user.category;
      for (var i = 0; i < this.catLoop.length; i++) {

        if(i==0){
          user.category = this.catLoop [i].id ;
        } else {
          user.category += ',' + this.catLoop [i].id ;
        }
      }
    } else {
      user.category = '';
    }


    if(user.subcategory){
      this.subLoop = user.subcategory;
      for (var i = 0; i < this.subLoop.length; i++) {

        if(i==0){
          user.subcategory = this.subLoop [i].id ;
        } else {
          user.subcategory += ',' + this.subLoop [i].id ;
        }
      }
    } else {
      user.subcategory = '';
    }

    /*this.subLanguageLoop = user.language  ;
    for (var i = 0; i < this.subLanguageLoop.length; i++) {

      if(i==0){
        user.language = this.subLanguageLoop [i].id ;
      } else {
        user.language += ',' + this.subLanguageLoop [i].id ;
      }
    } */
    user.language='';
    user.highesteducation='';
    user.noDetails= this.noDetails;
    this.counsellorservice.updateCounsellorDetails(user , this.globalService.id_token)
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
        if((response.status == 1) && (response.type === 'success'))
        {
          this.ErrorMessage = '';
          jQuery("#squarespaceModal").modal('hide');
          this.ngOnInit();
        } else {
        }
      });





      //fetching counsellor Data
      this.counsellorservice.getCounsellorDetails(this.globalService.userId , this.globalService.id_token )
        .finally(() => { })
        .subscribe((response: any) => {

          if((response.status == 1) && (response.type === 'success'))
          {
            
            this.counsellorDetails = response.response[0];
            this.noDetails = false;
            
            this.form.patchValue({summary : this.counsellorDetails.summary,
              brand_worked_for : this.counsellorDetails.brand_worked_for,
              totalexperience : this.counsellorDetails.totalexperience,
            });

            if(this.counsellorDetails.category){
              this.selectedServices = this.counsellorDetails.category;
              
            }else{
              this.selectedServices = [];
            }

            if(this.counsellorDetails.language){
              this.selectedLanguageList = this.counsellorDetails.language;
            }else{
              this.selectedLanguageList = [];
            }

            setTimeout(()=>{
              //this.subServiceList = this.subservices[this.counsellorDetails.category];
              
              //this.subServiceList = this.subservices[1];

              for(var p=0;p<this.counsellorDetails.category.length;p++){
                if(this.subservices[this.counsellorDetails.category[p].id] != undefined) {
                this.subServiceList = this.subServiceList.concat(this.subservices[this.counsellorDetails.category[p].id]);
                }
              }

              if (this.subServiceList == undefined){
                this.showCounsellingSub = false;
                this.selectedSubServices = [];
                this.form.controls.subcategory.disable();
              } else {
                this.showCounsellingSub= true;
                this.form.controls.subcategory.enable();
                this.selectedSubServices = this.counsellorDetails.sub_category;
              }

            },1500);
           } else {

          }
        });
        this.CounsellorDetailsView();
  };


  CounsellorDetailsView = function(){

  //fetching counsellor Data
  this.counsellorservice.getCounsellorDetails(this.globalService.userId , this.globalService.id_token )
  .finally(() => { })
  .subscribe((response: any) => {

    if((response.status == 1) && (response.type === 'success'))
    {

      this.counsellorDetails = response.response[0];
  
      this.noDetails = false;
      
      this.form.patchValue({summary : this.counsellorDetails.summary,
        brand_worked_for : this.counsellorDetails.brand_worked_for,
        totalexperience : this.counsellorDetails.totalexperience,
      });

      if(this.counsellorDetails.category){
        this.selectedServices = this.counsellorDetails.category;
       
      }else{
        this.selectedServices = [];
      }

      if(this.counsellorDetails.language){
        this.selectedLanguageList = this.counsellorDetails.language;
      }

      setTimeout(()=>{
        //this.subServiceList = this.subservices[this.counsellorDetails.category];
        for(var p=0;p<this.counsellorDetails.category.length;p++){
          if(this.subservices[this.counsellorDetails.category[p].id] != undefined) {
        this.subServiceList = this.subServiceList.concat(this.subservices[this.counsellorDetails.category[p].id]);
        }
      }
    
        if (this.subServiceList == undefined){
          this.showCounsellingSub = false;
          this.selectedSubServices = [];
          this.form.controls.subcategory.disable();
        } else {
          this.showCounsellingSub= true;
          this.form.controls.subcategory.enable();
          this.selectedSubServices = this.counsellorDetails.sub_category;
        }

      },1500);
     } else {

    }
  });
}

  updateImage = function(){

    swal({
      title: 'Updating profile picture!',
      text: '',
      timer: 5000,
      onOpen: function () {
        swal.showLoading()
      }
    }).then(
      function () {},
      // handling the promise rejection
      function (dismiss) {
        if (dismiss === 'timer') {
         // console.log('I was closed by the timer')
        }
      }
    )

    setTimeout(()=>{
      this.basicservice.updateImageUrl(jQuery('#awsfileNameCounsellor').val(), this.globalService.userId , this.globalService.id_token)
        .finally(() => { this.isLoading = false; })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type === 'success'))
          {
            this.globalService.sendPic(this.cdnPath + jQuery('#awsfileNameCounsellor').val());
            localStorage.setItem('userPic', this.cdnPath + jQuery('#awsfileNameStudent').val());
          } else {
          }
        });
    }, 4000);
  }

  onPaymentSubmit = function (user) {
    user.userId = this.globalService.userId;
   
    if(user.account_number !== user.confirm_account_number){
      this.formPaymentErrors.confirm_account_number = "Account No. Doesn't Matches";
    } else {
      
      if(this.formPayment.valid == false){

        for(let field in this.formPaymentErrors) {
          let input1 = this.formPayment.get(field);
           for (let error in input1.errors) {
              this.formPaymentErrors[field] = this.validationMessages[field][error];
            }
          
        }
        
        return false;
      }
     // this.document.getElementById('closePaymentModal').click(); [disabled] = "!formPayment.valid"

      if(!this.noPaymentDetails) {

        

        this.counsellorservice.updateCounsellorPaymentDetails(user , this.globalService.id_token )
          .finally(() => {
            this.isLoading = false;
          })
          .subscribe((response: any) => {
            
            if ((response.status == 1) && (response.type === 'success')) {
              this.ngOnInit();
              
            } else {
            }
            $("#bankDetails .close").click()
          });
      } else {

        
        this.counsellorservice.insertCounsellorPaymentDetails(user , this.globalService.id_token )
          .finally(() => {
            this.isLoading = false;
          })
          .subscribe((response: any) => {
            if ((response.status == 1) && (response.type === 'success')) {
              this.ngOnInit();
            } else {
            }
            $("#bankDetails .close").click()
          });
      }
    }
  };

  onEducationSubmit = function (user) {

    user.userId = this.globalService.userId;
    this.counsellorservice.updateCounsellorEducation(user , this.globalService.id_token )
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
        if((response.status == 1) && (response.type === 'success'))
        {
          this.ngOnInit();
        } else {
        }
      });
  };

  onExperienceSubmit = function (user) {
    user.userId = this.globalService.userId;

    this.counsellorservice.updateCounsellorExperience(user , this.globalService.id_token )
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
        if((response.status ==1) && (response.type === 'success'))
        {
          this.ngOnInit();
        } else {
        }
      });

  };

  onCertificateSubmit = function (user) {
    user.userId = this.globalService.userId;


    this.counsellorservice.updateCounsellorCertificate(user , this.globalService.id_token )
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
        if((response.status ==1) && (response.type === 'success'))
        {
          this.ngOnInit();
        } else {
        }
      });

  };

  getexpYear(){
    for(var i = 5; i <= 50; i++){
    this.expyears.push(i);}
}


deleteProfileImage = function(){
  
      swal({
        title: 'Profile Picture Delete!',
        text: '',
        timer: 5000,
        onOpen: function () {
          swal.showLoading()
        }
      }).then(
        function () {},
        // handling the promise rejection
        function (dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )
  
      this.counsellorservice.deleteProfileImage(this.globalService.userId, this.globalService.id_token)
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {      
        
        if((response.status ==1) && (response.type === 'success'))
        {
          
          this.globalService.sendPic(this.cdnPath + response.image);
          localStorage.setItem('userPic', this.cdnPath + response.image);
          jQuery("#header-right-pic").attr("src", this.cdnPath + response.image);
          
          this.ngOnInit();
        } else {
        }
      });
    }

}
