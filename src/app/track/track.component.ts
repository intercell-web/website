import { Component, OnInit, Inject } from '@angular/core';
import { LandingService } from '../services/landing.service';
import { GlobalService } from '../services/global.service';
import { Router ,ActivatedRoute} from '@angular/router';
import { DOCUMENT, Title,Meta  } from '@angular/platform-browser';
import { LoginService } from '../services/login.service';
import { JwtHelper } from '../services/jwt.service';
import { environment } from '../../environments/environment';
import * as AppConst from '../app.const';

/*const Entities = require('html-entities').AllHtmlEntities;*/


@Component({
  selector: 'app-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.scss']
})
export class TrackComponent implements OnInit {
  counsellorDetails:any = [];
  user: object={};
  consellorId: number;
  data: any;
  popFullName: string;
  popUserPic: string;
  profileUrl: any = environment.counsellorProfileUrl;
  myConst: any;
  serviceDetails: any;
  incorrectDetails: any = false;
  cdnPath: any = environment.cdnProfileUrl;
  userId: any = +localStorage.getItem('userId');
  userType: any = +localStorage.getItem('userType');
  ServiceIdslider:any ;
  field:any;

  constructor(
    @Inject(DOCUMENT) private document,
    private route: ActivatedRoute,
    private title: Title,
    private meta: Meta,
    private landingService: LandingService ,
    private globalService: GlobalService,
    private router: Router,
    private loginservice: LoginService,
    private jwtHelper: JwtHelper){
      this.route.params.subscribe( params => {this.field = params['field'];});
      this.fieldData(this.field);
   }

   ngOnInit() {
    this.myConst = AppConst;
    $("html, body").animate({ scrollTop: 0 }, 'slow');
  }

  fieldData = function(field){
     this.landingService.getFieldDetails(field)
    .finally(() => { })
    .subscribe((response: any) => {
      if((response.status ==1) && (response.type === 'success'))
      {
        this.serviceDetails = response.response[0];
        this.ServiceIdslider = this.serviceDetails.id;
      // Start to Set Component  Title, Description and Keywords
        this.title.setTitle('Track - Intercellworld.com');
        this.meta.updateTag({ name: 'description', content: 'Track' });
        this.meta.updateTag({ name: 'keyword', content: 'Intercell' });
        // End to Set Component Title, Description and Keywords
      } else {
        this.router.navigate(['/career-counsellors']);
      }
    });
  }


}
