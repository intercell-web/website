import { Component, OnInit } from '@angular/core';
import { Title,Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  isLogin: any = localStorage.getItem('userId');


  constructor(private title: Title, private meta: Meta) {

    // Start to Set Component  Title, Description and Keywords
this.title.setTitle('Intercell News - Intercellworld.com');
this.meta.updateTag({ name: 'description', content: 'Intercell News' });
this.meta.updateTag({ name: 'keyword', content: 'Intercell' });
// End to Set Component Title, Description and Keywords
   }

  ngOnInit() {
  }

}
