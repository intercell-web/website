import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import {CalendarModule} from "ap-angular2-fullcalendar";
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { AppointmentsComponent } from './appointments.component';
import { AppointmentsService } from './../services/appointments.service';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    Ng2Bs3ModalModule,
    CalendarModule
  ],
  declarations: [
    AppointmentsComponent
  ],
  providers: [
    AppointmentsService
  ]
})
export class AppointmentsModule { }
