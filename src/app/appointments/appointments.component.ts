import { Component , ViewChild, Input, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';
import { AppointmentsService } from './../services/appointments.service';
import {CalendarComponent} from 'ap-angular2-fullcalendar';
import * as $ from 'jquery';
import { GlobalService } from '../services/global.service';
import { environment } from '../../environments/environment';
declare var swal: any;

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.scss'] ,
})
export class AppointmentsComponent implements OnInit {
  @ViewChild(CalendarComponent) myCalendar: CalendarComponent;

  userId: number;
  userType: number;
  userTypeText: string;
  userFullName: string;
  videoScreenServer: string;
  calendarOptions: any;
  appointments: any;
  isLoading: boolean;
  showAppoint: any;
  sessionId: number;
  isAppointmentDetails: boolean = false;
  isError: boolean = false;
  errorMessage: string;
  paymentUrl: any = environment.paymentUrl + '/core/payment/pay_session';
  isDirect:boolean =false;
  link:any;
  todaytime: any = new Date(Date.now() +  2 * 1000 * 60 *60);  // 2 hour add in current time 
  todaytime1: any = this.todaytime.getTime();
  starttime: any = new Date(Date.now() +  1 * 1000 * 60 *60);  // 1 hour add in current time 
  starttime1: any = this.todaytime.getTime();
  counsellorProfileUrl: any = environment.cdnProfileUrl;
  constructor(@Inject(DOCUMENT) private document, private globalService: GlobalService, private router: Router, private appointObj: AppointmentsService) {
    this.setCalendarEvents();
    
  }

  ngOnInit() {
    this.document.getElementById('menuAppointment').className = 'active';
    this.isLoading = true;
    this.userId = this.globalService.userId;
    this.userType = this.globalService.userType;
    this.userTypeText = this.globalService.userTypeText;
    this.userFullName = this.globalService.userFullName;
    this.videoScreenServer = this.globalService.SESSION_SCREEN;

    //In case someone comes from Dashboard
    if(this.globalService.appointDirectDetail){
      this.isDirect = true;
      this.sessionId = this.globalService.appointDirectDetail.id;
      this.showAppoint = this.globalService.appointDirectDetail;
      $('#calend-view').addClass('hide');
      this.isAppointmentDetails = true;
      this.globalService.appointDirectDetail = null;
    } else {
      this.hideAppointmentDetails();
    }
  }


  @Input() callback: Function;

  changeCalendarView(view) {
    this.myCalendar.fullCalendar('changeView', view);
  }




  calEventOnClick(appObj, jsEvent, view) {
    this.sessionId = appObj.id;
    this.showAppoint = appObj;
    this.showAppointmentDetails(this.showAppoint);
    //$('#calend-view').addClass('hide');
    //this.isAppointmentDetails = true;
  }

  setCalendarEvents() {
    let today: any = new Date();
    let dd: any = today.getDate();
    let mm: any = today.getMonth() + 1;
    let yyyy: any = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    today = yyyy + '-' + mm + '-' + dd;
    this.calendarOptions = {
      height: 450,
      fixedWeekCount: false,
      defaultDate: today,
      editable: false,
      eventLimit: true, // allow "more" link when too many events
      header: {
        left: 'month agendaWeek agendaDay',
        center: 'title',
        right: 'today prev,next'
      },
      events: [],
      eventClick: this.calEventOnClick.bind(this),
      timeFormat: 'h(:mm)A' 
    };
  }

  showAppointmentDetails(appoint: any) {
    this.sessionId = appoint.id;
    this.showAppoint = appoint;
    this.router.navigate(['/appointments/'+this.sessionId]);
   // $('#calend-view').addClass('hide');

    //this.isAppointmentDetails = true;
  }

  hideAppointmentDetails() {
    setTimeout(() => {
      this.changeAppointmentView();
      this.getAppointments();
    }, 1500);
  }



  changeAppointmentView() {
    this.isAppointmentDetails = false;
    $('#calend-view').removeClass('hide');
  }

  getAppointments() {
    this.isLoading = true;
    this.userId = this.globalService.userId;
    this.userType = this.globalService.userType;
    this.userTypeText = this.globalService.userTypeText;
    this.userFullName = this.globalService.userFullName;
    this.videoScreenServer = this.globalService.SESSION_SCREEN;

    this.appointObj.getUpcoming(this.userTypeText , this.globalService.id_token)
      .finally(() => {
        this.isLoading = false;
      })
      .subscribe((response: any) => {
        if ((response.status == 1) && (response.type == 'success')) {
          this.isError = false;
          this.appointments = response.response;
          this.calendarOptions.events = [];
          $('#userCalendar').fullCalendar('removeEvents');

          $('#userCalendar').fullCalendar('addEventSource', response.response);
          $('#userCalendar').fullCalendar('refetchEvents');
          //console.log(response.response);
          //  this.calendarOptions.events = response.response;
          //  $('#userCalendar').fullCalendar( 'addEventSource', response.response );
          //  $('#userCalendar').fullCalendar('rerenderEvents', response.response, false);
        } else if ((response.status == 1) && (response.type == 'noRecord')) {
          this.isError = true;
          this.calendarOptions.events = [];
          $('#userCalendar').fullCalendar('removeEvents');
          $('#userCalendar').fullCalendar('addEventSource', '');
          $('#userCalendar').fullCalendar('refetchEvents');
          this.errorMessage = response.message;
        }
      });
  }


  /*
  
  directHideAppointmentDetails() {
    this.isDirect = false;
    this.changeAppointmentView();
    this.getAppointments();
  }
  
    payNow(appoint) {
    (<HTMLFormElement>document.getElementById('sessionId')).value = appoint.id;
    (<HTMLFormElement>document.getElementById('paymentUserId')).value = this.userId;
    var myForm = <HTMLFormElement>document.getElementById('payment_form');
    myForm.submit();
  }

  rejectAppointmentDetails(sessionId: number) {

    this.isLoading = true;
    swal({
      title: 'Auto close alert!',
      text: 'Please wait while we process your request',
      timer: 4000,
      onOpen: function () {
        swal.showLoading()
      }
    }).then(
      function () {},
      // handling the promise rejection
      function (dismiss) {
        if (dismiss === 'timer') {
          console.log('I was closed by the timer')
        }
      }
    )

    this.appointObj.rejectAppointment(this.userId, sessionId, this.globalService.id_token)
      .finally(() => {
        this.isLoading = false;
      }).subscribe((response: any) => {
      if ((response.status == 1) && (response.type == 'success')) {
        swal('successfully rejected');
        this.hideAppointmentDetails();
      }
    });
  }


  sessionstart(sessionD:any){
   this.sessionId = sessionD.id;
  
if(sessionD.session_start <= this.starttime1){
  
  this.appointObj.startAppointment(this.userId,this.userType, this.sessionId, this.globalService.id_token)
  .finally(() => {
    
  }).subscribe((response: any) => {
  if ((response.status == 1) && (response.type == 'success')) {     
  // console.log('start session'+sessionId+' user ID  '+this.userType);  
   
  }
});
this.link = this.videoScreenServer+'?uid='+this.userId+'&username=intercell&type='+this.userTypeText+'&fullname='+this.userFullName+'&sid='+this.sessionId;
//console.log(this.link);
window.open(this.link, '_blank');


}else{

  swal("Your session will start  "+sessionD.actual_start+". We request you to try again. ");
  //at 2 pm IST on 22nd November 2018
}
    
  }


  approveAppointmentDetails(sessionId: number) {

    this.isLoading = true;
    swal({
      title: 'Auto close alert!',
      text: 'Please wait while we process your request',
      timer: 5000,
      onOpen: function () {
        swal.showLoading()
      }
    }).then(
      function () {},
      // handling the promise rejection
      function (dismiss) {
        if (dismiss === 'timer') {
          console.log('I was closed by the timer')
        }
      }
    )

    this.appointObj.approveAppointment(this.userId, sessionId, this.globalService.id_token)
      .finally(() => {
        this.isLoading = false;
      }).subscribe((response: any) => {
      if ((response.status == 1) && (response.type == 'success')) {
        swal(response.message);
        this.hideAppointmentDetails();
      }
    });
  }

  resheduleAppointmentDetails(sessionId: number, consellorId: number) {
    this.router.navigate(['/reshedule-appointment/' + sessionId + '/' + consellorId]);
    this.isLoading = true;
  }


  cancelAppointmentDetails(sessionId: number) {

    swal({
      title: 'Are you sure?',
      text: 'Are you sure to cancel this session?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
      closeOnConfirm: true,
      closeOnCancel: true
    }).then(() => {
      this.isLoading = true;
      this.appointObj.cancelAppointment(this.userId, sessionId, this.globalService.id_token)
        .finally(() => {
          this.isLoading = false;
        })
        .subscribe((response: any) => {
          if ((response.status == 1)) {
            swal(response.message);
            this.hideAppointmentDetails();
          }
        });
    });

  }*/

 
  

}
