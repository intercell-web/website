import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalService } from '../services/global.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})

export class PaymentComponent implements OnInit {
  userType :any;

  constructor(private globalService: GlobalService , private router: Router) { }

  ngOnInit() {
    this.globalService.userId = +localStorage.getItem("userId");
    this.userType = +localStorage.getItem("userType");
    this.globalService.newUser = +localStorage.getItem("newUser");

    this.globalService.userType = this.userType;


    if(this.globalService.userType == 3)
    {
      this.globalService.userTypeText = 'Mentor';
      this.router.navigate(['/counsellor']);
    }
    else if(this.globalService.userType == 2)
    {
      this.globalService.userTypeText = 'Mentee';
      this.router.navigate(['/student']);
    }

  }

}
