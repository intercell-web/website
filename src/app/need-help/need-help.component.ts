import { Component, OnInit,Inject } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { BasicService } from '../services/basic.service';
import { DOCUMENT } from '@angular/platform-browser';
declare var swal: any;

@Component({
  selector: 'app-need-help',
  templateUrl: './need-help.component.html',
  styleUrls: ['./need-help.component.scss']
})
export class NeedHelpComponent implements OnInit {
  isLogin: any = localStorage.getItem('userId');
  formContact: FormGroup;
  submitted : false;
  formArray: any = {};

  AllRequiredError:string ='';
  formErrors = {
       message : '',
      };
    
    validationMessages = {   
    
    message : {
      required : 'Please enter details of your query'
    },    
       }

  constructor(@Inject(DOCUMENT) private document,private fb: FormBuilder , private router: Router,private globalService : GlobalService,private basicService : BasicService ) {
    window.scrollTo(0, 0);
   }

  ngOnInit() {
    this.buildForm();
	this.document.getElementById('menuNeedHelp').className = 'active';
  }
  buildForm() {
    this.formContact = this.fb.group({
            message: ['', [Validators.required]],
          });
    this.formContact.valueChanges.subscribe(data => this.validateForm());
  }

  validateForm(){
    for(let field in this.formErrors) {
      this.formErrors[field] = '';      
      let input = this.formContact.get(field);
      if (input.invalid && input.dirty) {
        for (let error in input.errors) {
          this.formErrors[field] = this.validationMessages[field][error];
        }
      }

   }
 }

 onDone = function (value) {

  if(!this.formContact.valid){      
    this.AllRequiredError = 'Please enter details of your query';
    return false;
  }else{
    for (var key in this.formContact.controls) {
      this.formArray[key] = this.formContact.controls[key].value;
    } 
    this.formArray["user_id"] = localStorage.getItem('userId');
    
    this.submitted = true;
    //call services for sending Code
    this.basicService.NeedHelp(this.formArray)
    .finally(() => { })
    .subscribe((response: any) => {
    if((response.status == 1) && (response.type === 'success'))
    {
    this.formContact.reset();
    swal(response.message); 
    } 
    });

  }
  

}



}
