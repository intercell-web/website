import { Component, OnInit, Inject } from '@angular/core';
import { PaymentService } from '../services/payment.service';
import { GlobalService } from '../services/global.service';
declare var swal: any;

import { DOCUMENT } from '@angular/platform-browser';
@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  maxAmount: number;
  Amount: number;
  money: number;
  moneyError: boolean = false;
  sessionPayment: any;
  returnedPayment: any;
  fullName: any;
  registerPayment: any;
  userType: any;

  constructor(@Inject(DOCUMENT) private document, private paymentService: PaymentService , private globalService: GlobalService) { }

  ngOnInit() {

    $(window).scroll(function(){
      if ($(this).scrollTop() > 100) {
        $('.scrollToTop').fadeIn();
      } else {
        $('.scrollToTop').fadeOut();
      }
    });

    //Click event to scroll to top
    $('.scrollToTop').click(function(){
      $('html, body').animate({scrollTop : 0}, 800);
      return false;
    });

    this.maxAmount = 0;
    this.Amount = 0;
    this.fullName = this.globalService.userFullName;
    this.document.getElementById('menuAccount').className = 'active';
    this.userType = this.globalService.userType;

    if(this.globalService.userType == 2){

      //fetching All Sub Services
      this.paymentService.studentPayments(this.globalService.userId , this.globalService.id_token)
        .finally(() => { })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type === 'success'))
          {
            this.sessionPayment = response.response;

            for (let session of this.sessionPayment) {
            }
          } else {

          }
        });

      //fetching All Sub Services
      this.paymentService.refundedPayment(this.globalService.userId , this.globalService.id_token)
        .finally(() => { })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type === 'success'))
          {
            this.returnedPayment = response.response;

            for (let returned of this.returnedPayment) {
              this.maxAmount -= returned.amount;
            }
          } else {
          }
        });
    } else {
      //fetching All Sub Services
      this.paymentService.sessionPayments(this.globalService.userId , this.globalService.id_token)
        .finally(() => { })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type === 'success'))
          {
            this.sessionPayment = response.response;
           // console.log(this.maxAmount);
            //console.log(this.sessionPayment);
            this.Amount = 0;
            for (let session of this.sessionPayment) {
        
              this.Amount = session.price_amount-(parseFloat(session.our_commision)+parseFloat(session.ccavenue_commision));
              
              this.maxAmount += this.Amount;
            }
           
          } else {

          }
        });

      //fetching All Sub Services
      this.paymentService.returnedPayment(this.globalService.userId, this.globalService.id_token)
        .finally(() => { })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type === 'success'))
          {
            this.returnedPayment = response.response;

            for (let returned of this.returnedPayment) {
              this.maxAmount -= returned.amount;
            }
          } else {
          }
        });

      //fetching All Sub Services
      this.paymentService.registerUser(this.globalService.userId, this.globalService.id_token)
        .finally(() => { })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type === 'success'))
          {
            this.registerPayment = response.response;
          } else {
          }
        });
    }

  }

  onSubmit = function() {

    this.money = this.maxAmount;
     if(this.money > this.maxAmount){
      
     this.moneyError = true;
   } else {

     //fetching All Sub Services
     this.paymentService.returnPayment(this.globalService.userId, this.money ,this.globalService.id_token)
       .finally(() => { })
       .subscribe((response: any) => {
         if((response.status == 1) && (response.type === 'success'))
         {
          swal('Withdraw request send successfully');
          this.ngOnInit();
           this.money = 0;
           this.maxAmount = 0;
         } else {
         }
       });

     this.moneyError = false;
   }
  }

  ngClassForColor(value){
    return this.globalService.ngClassForColorsection(value);
    }


}
