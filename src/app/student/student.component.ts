import { Component, OnInit, Inject } from '@angular/core';
import {FormGroup, FormControl, Validators, FormBuilder  } from '@angular/forms';
import { Router} from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';
import { BasicService} from '../services/basic.service';
import { StudentService } from '../services/student.service';
import { GlobalService} from '../services/global.service';
import { LoginService } from '../services/login.service';
import { CounsellorService } from '../services/counsellor.service';
import { environment } from '../../environments/environment';


declare var jQuery: any;
declare var swal: any;

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})

export class StudentComponent implements OnInit {
  userDetails: any= {};
  interest_data: any= [];
  qualification_data: any = [];
  form;
  formStudent;
  formStudentInterest;
  formCollection: any = [];
  formInterestCollection: any =[];
  formObject: any;
  formInterestObject: any;
  options: any;
  countryList: any = [];
  stateList: any = [];
  cityList: any = [];
  imageUrl: any = '';
  cdnPath: any = environment.cdnProfileUrl;
  eduYearArrayValidation: any = [];
  eduYearValidation: boolean = false;
  date: any;
  ErrorMessage:string='';
  ErrorMessageEducation:string='';
  ErrorMessageAddEducation:string = '';
  ErrorMessageInterest:string='';
  ErrorMessageAddInterest:string = '';
  services: any;
  studentDetails:any = [];
  subLoop: any = [];
  serviceList:any = [];
  subServiceList = [];
  selectedSubServices: any = [ ];
  subservices: any;
  yearDate = new Date();
  yearDateold = new Date(this.yearDate.getFullYear()-18, this.yearDate.getMonth(), this.yearDate.getDate(), 0, 0, 0);
  selectedServices: any = [];

  dropdownSettings= {
    singleSelection: false,
    text:"Select Sub Fields",
    selectAllText:'Select All',
    unSelectAllText:'UnSelect All',
    enableSearchFilter: true,
    classes:"myclass custom-class"
  };

  formErrors = {
    firstname : '',
    lastname : '',
    dob    : '',
    gender : '',
    cityid : '',
    countryid : '',
    stateid : '',
  };


  validationMessages = {
    firstname: {
      required: 'You must enter your First Name',
      minlength: '',
    },
    lastname: {
      required: 'You must enter your Last Name',
      minlength: 'Minimum length must be 5 characters'
    },
    mobile: {
      required: 'Mobile is Required',
      pattern : 'Your contact number should be of 10 digits'
    },
    gender: {
      required: 'You must select one option from the menu',
    },
    dob: {
      required: 'DOB is Required',
    },
    stateid: {
      required: 'State Id is Required',
    },
    countryid: {
      required: 'Country Id is Required',
    },
    cityid: {
      required: 'City Id is Required',
    },
    /*userName: {
      required: 'Display Name is Required',
    },*/
  }


  dropdownServices= {
    singleSelection: false,
    text:"Select Field",
    selectAllText:'Select All',
    unSelectAllText:'UnSelect All',
    enableSearchFilter: true,
    classes:"myclass custom-class"
  };


  constructor(@Inject(DOCUMENT) private document, private globalService: GlobalService,private counsellorservice: CounsellorService, private studentservice: StudentService, private router: Router,  private fb : FormBuilder, private basicservice: BasicService , private loginservice: LoginService) {
  }

  ngOnInit() {
    //$('html, body').animate({ scrollTop: 0 }, 'slow');
    
     
    $(window).scroll(function(){
      if ($(this).scrollTop() > 100) {
        $('.scrollToTop').fadeIn();
      } else {
        $('.scrollToTop').fadeOut();
      }
    });

    //Click event to scroll to top
    $('.scrollToTop').click(function(){
      $('html, body').animate({scrollTop : 0}, 800);
      return false;
    });

    this.document.getElementById('menuProfile').className = 'active';
    this.formStudent = this.fb.array([]);

    this.AllServices();

    this.getAllServiceslist();

     this.getAllSubServices();

      //fetching user Data
      this.studentservice.getUserDetails(this.globalService.userId, localStorage.getItem('id_token'))
        .finally(() => { })
        .subscribe((response: any) => {

          if((response.status ==1) && (response.type === 'success'))
          {
            this.userDetails = response.response;
           // console.log(this.userDetails);
            this.globalService.sendMessage(this.userDetails.first_name + ' ' + this.userDetails.last_name);

            this.imageUrl = this.userDetails.dp_image;

            if(this.userDetails.dp_image){
              this.userDetails.dp_image= this.cdnPath + this.userDetails.dp_image;
            }
            
           

            this.form.patchValue({lastname: this.userDetails.last_name , countryid : this.userDetails.country_id,
              stateid : this.userDetails.state_id, cityid : this.userDetails.city_id , gender : this.userDetails.gender ,
              firstname : this.userDetails.first_name , dob: this.userDetails.dob
            });

           //console.log(this.form.patchValue);
            jQuery("#dateValue").datepicker({ dateFormat: 'dd/mm/yyyy' }).val(this.userDetails.dob);
            //jQuery("#datePicker").datepicker({ dateFormat: 'dd/mm/yyyy' }).datepicker("setDate", this.userDetails.dob);


            if(!this.userDetails.gender) {
              this.form.patchValue({ gender:"" });
            }

            if(this.userDetails.country_id == 1) {
              this.userDetails.country = '';

              this.form.patchValue({ countryid: ''});
            }

            if(this.userDetails.state_id==1) {
              this.userDetails.state = '';
              this.form.patchValue({stateid : ''});
            }

            if(this.userDetails.city_id == 1) {
              this.userDetails.city = '';
              this.form.patchValue({cityid: ''});
            }

            this.onCountryChange(this.userDetails.country_id);
            this.onStateChange(this.userDetails.state_id);

          } else {

          }
        });

        this.studentfield();

    //fetching user Data
      this.studentservice.getStudentInterestDetails(this.globalService.userId , this.globalService.id_token)
        .finally(() => { })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type === 'success'))
          {
            this.interest_data = response.response;

            for (let interest of this.interest_data){

              this.formInterestObject = this.fb.group({ field : new FormControl('' , Validators.compose([Validators.required])),
                fieldId : new FormControl('')
              });

              this.formInterestCollection.push(this.formInterestObject);
            }
          } else {
            this.interest_data =[];

          }
        });

      //fetching user Data
      this.studentservice.getStudentQualificationDetails(this.globalService.userId , this.globalService.id_token)
        .finally(() => { })
        .subscribe((response: any) => {

          if((response.status ==1) && (response.type === 'success'))
          {
            this.qualification_data = response.response;

            for (let qualification of this.qualification_data){
              this.formObject = this.fb.group({
                university : new FormControl(qualification.school_name , Validators.compose([Validators.required])),
                field : new FormControl(qualification.field , Validators.compose([Validators.required])),
                class : new FormControl(qualification.qualification , Validators.compose([Validators.required])),
                startDate : new FormControl(qualification.enrollment_date , Validators.compose([Validators.required])),
                endDate : new FormControl(qualification.complation_date , Validators.compose([Validators.required])),
                profile_summary : new FormControl(qualification.profile_summary , Validators.compose([])),
                qualificationId : new FormControl(qualification.qua_id)
              });

              this.formCollection.push(this.formObject);
            }

          } else {
            this.qualification_data=[];
          }
        });

      //fetching All Countries List
      this.basicservice.getCountriesList()
        .finally(() => { })
        .subscribe((response: any) => {

          if((response.status == 1) && (response.type === 'success'))
          {
            this.countryList = response.response;
          } else {
          }
        });

       
    //build data model for our form
    this.buildForm();

    this.formStudent = new FormGroup({
      university : new FormControl('' , Validators.compose([Validators.required])),
      field : new FormControl('' , Validators.compose([Validators.required])),
      class : new FormControl('' , Validators.compose([Validators.required])),
      startDate : new FormControl('' , Validators.compose([Validators.required])),
      endDate : new FormControl('' , Validators.compose([Validators.required])),
      profile_summary: new FormControl('', Validators.compose([])),
      qualificationId : new FormControl('')
    });

    this.formStudentInterest = new FormGroup({
      field : new FormControl('' , Validators.compose([Validators.required])),
      fieldId : new FormControl('')
    });

      jQuery('#datePicker')
      .datepicker({format: 'dd/mm/yyyy',endDate: this.yearDateold,autoclose: true});

      //// The parameters are year, month, day, hours, minutes, seconds. 
  
    jQuery('#dateValue').change(() => {
      
      this.form.patchValue({dob: jQuery('#dateValue')
      .datepicker({dateFormat: 'dd/mm/yyyy',autoclose: true,endDate: this.yearDateold}).val()});
    });

  }

  buildForm()
  {
    this.form = this.fb.group({
      cityid :  ['' , []],
      stateid : ['' , []],
      countryid : ['' , []],
      firstname : ['' , [Validators.required ]],
      lastname : ['' , [Validators.required ]],
      dob : ['' , [Validators.required]],
      gender : ['' , [Validators.required]],
      category : ['' , [Validators.required]],
      subcategory : ['' , []],
    });

    this.form.valueChanges.subscribe(data => this.validateForm());
  }


  validateForm(){
    for(let field in this.formErrors){
      this.formErrors[field]= '';

      let input = this.form.get(field);

      if(input.invalid && input.dirty){
        for(let error in input.errors){
          this.formErrors[field] = this.validationMessages[field][error];
        }
      }
    }
  }


  updateImage = function(){

    swal({
      title: 'Updating profile picture!',
      text: '',
      timer: 5000,
      onOpen: function () {
        swal.showLoading()
      }
    }).then(
      function () {},
      // handling the promise rejection
      function (dismiss) {
        if (dismiss === 'timer') {
          console.log('I was closed by the timer')
        }
      }
    )

    setTimeout(() => {
      this.basicservice.updateImageUrl(jQuery('#awsfileNameStudent').val(), this.globalService.userId)
        .finally(() => { this.isLoading = false; })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type === 'success'))
          {
            //this.userDetails.dp_image = ;
            this.globalService.sendPic(this.cdnPath + jQuery('#awsfileNameStudent').val());
            localStorage.setItem('userPic', this.cdnPath + jQuery('#awsfileNameStudent').val());
          } else {
          }
        });
    }, 4000);
  }

  onlyNumberKey(event) {
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  }

  onSubmit = function (user) {

    if(!this.form.valid){
      this.ErrorMessage = 'Please fill in required details';
      return false;
    }

    
    user.userId = this.globalService.userId;
 
    if(user.category){
      this.catLoop = user.category;
      for (var i = 0; i < this.catLoop.length; i++) {

        if(i==0){
          user.category = this.catLoop [i].id ;
        } else {
          user.category += ',' + this.catLoop [i].id ;
        }
      }
    } else {
      user.category = '';
    }

   /* this.subLoop = user.subcategory;
    if(this.subLoop.length>0){
       for (var i = 0; i < this.subLoop.length; i++) {

        if(i==0){
          user.subcategory = this.subLoop [i].id ;
        } else {
          user.subcategory += ',' + this.subLoop [i].id ;
        }
      }
    } else {
      user.subcategory = '';
    }*/
    user.subcategory = '';

    this.studentservice.updateStudentDetails(user, this.globalService.id_token)
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
        console.log(response);
        if((response.status ==1) && (response.type === 'success'))
        {
          this.ErrorMessage = '';
          jQuery("#squarespaceModal").modal('hide');
          this.ngOnInit();
        } else {
        }
      });
  };

  onInput= function() {
    if(jQuery("#education_start_time").val() && jQuery("#education_end_time").val()){

      if(jQuery("#education_start_time").val() > jQuery("#education_end_time").val()){
        this.eduYearValidation = true;
      } else {
        this.eduYearValidation = false;
      }
    } else {
      this.eduYearValidation = false;
    }
  }

  onInput_= function(id) {

    if(jQuery("#education_start_time_"+id).val() && jQuery("#education_end_time_"+id).val()){

      if(jQuery("#education_start_time_"+id).val() > jQuery("#education_end_time_"+id).val()){
        this.eduYearArrayValidation[id] = true;
      } else {
        this.eduYearArrayValidation[id] = false;
      }
    } else {
      this.eduYearArrayValidation[id] = false;
    }
  }


  onCountryChange = function (event){
    //fetching All State List
    this.basicservice.getStatesList(event)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          this.stateList = response.response;
          this.cityList = [];
        } else {
        }
      });
  };


  onStateChange = function (event){
    //fetching All State List
    this.basicservice.getCitiesList(event)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          this.cityList = response.response;
        } else {
        }
      });
  };


  onStudentSubmit = function (user) {

    if(!this.formStudent.valid || this.eduYearValidation){
      this.ErrorMessageAddEducation = 'Please fill in required details';
      return false;
    }
     
    user.userId = this.globalService.userId;
    this.studentservice.updateStudentEducation(user, this.globalService.id_token)
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
        
        if((response.status ==1) && (response.type === 'success'))
        {
          jQuery("#squarespaceModal1").modal('hide');
          this.ngOnInit();
          this.ErrorMessageAddEducation = '';
          //this.router.navigate(['/home']);
        } else {
        }
      });
  };

  onStudentSubmit_edit = function (user,quaId,qua_id) {
    
        if(!this.formCollection[quaId].valid || this.eduYearArrayValidation[qua_id]){
        this.ErrorMessageEducation = 'Please fill in required details';
        return false;
        }
        user.userId = this.globalService.userId;
        this.studentservice.updateStudentEducation(user, this.globalService.id_token)
          .finally(() => { this.isLoading = false; })
          .subscribe((response: any) => {
           if((response.status ==1) && (response.type === 'success'))
            {
              jQuery("#qualification"+qua_id).modal('hide');
              this.ErrorMessageEducation = '';
              this.ngOnInit();
              //this.router.navigate(['/home']);
            } else {
            }
          });
      };


  deleteInterest = function (id) {
   
    this.studentservice.deleteStudentInterest(id, this.globalService.id_token)
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
        this.ngOnInit();

        if((response.status ==1) && (response.type === 'success'))
        {
          //this.router.navigate(['/home']);
        } else {
        }
      });

  };

  deleteQualification = function (id) {
    this.studentservice.deleteStudentQualification(id, this.globalService.id_token)
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
        this.ngOnInit();
        if((response.status ==1) && (response.type === 'success'))
        {
        } else {
        }
      });
  };

  onStudentFieldSubmit = function (user) {
    if(this.formStudentInterest.controls.field.value==''){
      this.ErrorMessageAddInterest = 'Please select your career field';
      return false;
      }
    user.userId = this.globalService.userId;
    this.studentservice.updateStudentField(user, this.globalService.id_token)
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
        
        if((response.status ==1) && (response.type === 'success'))
        {
          this.ErrorMessageAddInterest ='';
          jQuery("#interest").modal('hide');
          this.ngOnInit();
        } else {
        }
      });
  };


  onStudentFieldSubmit_edit = function (user,intoId,interest_id) {
    if(this.formInterestCollection[intoId].controls.field.value ==''){
      this.ErrorMessageInterest = 'Please select your career field';
      return false;
      }
    user.userId = this.globalService.userId;
    this.studentservice.updateStudentField(user, this.globalService.id_token)
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
        
        console.log(intoId);
        if((response.status ==1) && (response.type === 'success'))
        {
          this.ErrorMessageInterest ='';
          jQuery("#interestedit"+interest_id).modal('hide');
          this.ngOnInit();
        } else {
        }
      });
  };


  AllServices = function(){
    this.counsellorservice.getAllServices()
    .finally(() => { })
    .subscribe((response: any) => {
      if((response.status == 1))
      {
        this.services = response.response;
      } 
    });

    
  }


  getAllServiceslist = function(){
      //fetching All Services
      this.counsellorservice.getAllServiceslist()
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          //this.services = response.response;
          this.serviceList = response.response;
         
        } else {
        }
      });
  }


  studentfield = function(){
    this.studentservice.getStudentDetails(this.globalService.userId , this.globalService.id_token )
    .finally(() => { })
    .subscribe((response: any) => {
      if((response.status == 1) && (response.type === 'success'))
      {
        this.studentDetails = response.response[0];
        this.noDetails = false;
       
        
        if(this.studentDetails.category){
          this.selectedServices = this.studentDetails.category;
          
        }else{
          this.selectedServices = [];
        }

    setTimeout(()=>{
    
      for(var p=0;p<this.studentDetails.category.length;p++){
        if(this.subservices[this.studentDetails.category[p].id] != undefined) {
        this.subServiceList = this.subServiceList.concat(this.subservices[this.studentDetails.category[p].id]);
        }
      }
   

      if (this.subServiceList == undefined){
        this.showCounsellingSub = false;
        this.selectedSubServices = [];
        this.form.controls.subcategory.disable();
      } else {
        this.showCounsellingSub= true;
        this.form.controls.subcategory.enable();
        this.selectedSubServices = this.studentDetails.sub_category;
      }
        },1500);
       } else {
  
      }
    });
  }
  

  onSubServiceChange = function (event){
  };

 
  onServiceChange = function (event){
    setTimeout(()=> {      
      this.subServiceList = [];
      if (this.form.controls.category.value.length == 0) {
        this.showCounsellingSub = false;
        this.form.controls.subcategory.disable();
      } else {
        for(var p=0;p<this.form.controls.category.value.length;p++){
          if(this.subservices[this.form.controls.category.value[p].id] != undefined) {
            this.subServiceList = this.subServiceList.concat(this.subservices[this.form.controls.category.value[p].id]);
          }
          }
        this.showCounsellingSub = true;
        this.form.controls.subcategory.enable();
      }

      this.selectedSubServices = [];
    },1500);
  };

  onServiceChange1 = function (event){
    setTimeout(()=> {

      if (this.subservices[event] == undefined) {
        this.showCounsellingSub = false;
        this.form.controls.subcategory.disable();
      } else {
        this.subServiceList = this.subservices[event];
        this.showCounsellingSub = true;
        this.form.controls.subcategory.enable();
      }

      this.selectedSubServices = [];
    },1500);
  };


  deleteProfileImage = function(){

    swal({
      title: 'Profile Picture Delete!',
      text: '',
      timer: 5000,
      onOpen: function () {
        swal.showLoading()
      }
    }).then(
      function () {},
      // handling the promise rejection
      function (dismiss) {
        if (dismiss === 'timer') {
          console.log('I was closed by the timer')
        }
      }
    )

    this.studentservice.deleteProfileImage(this.globalService.userId, this.globalService.id_token)
    .finally(() => { this.isLoading = false; })
    .subscribe((response: any) => {      
      
      if((response.status ==1) && (response.type === 'success'))
      {
        
        this.globalService.sendPic(this.cdnPath + response.image);
        localStorage.setItem('userPic', this.cdnPath + response.image);
        jQuery("#header-right-pic").attr("src", this.cdnPath + response.image);
        
        this.ngOnInit();
      } else {
      }
    });
  }


  getAllSubServices =  function(){
      //fetching All Sub Services
      this.counsellorservice.getAllSubServices()
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          this.subservices = response.response;

          

        } else {
        }
      });
  }
  

}
