import { Component , Input, OnInit, Inject } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';
import { AppointmentsService } from './../services/appointments.service';
import * as $ from 'jquery';
import { GlobalService } from '../services/global.service';
import { environment } from '../../environments/environment';
declare var swal: any;


@Component({
  selector: 'app-appointments-details',
  templateUrl: './appointments-details.component.html',
  styleUrls: ['./appointments-details.component.scss']
})
export class AppointmentsDetailsComponent implements OnInit {

  userId: number;
  userType: number;
  userTypeText: string;
  userFullName: string;
  videoScreenServer: string;
  calendarOptions: any;
  appointments: any;
  isLoading: boolean;
  showAppoint: any;
  sessionId: any;
  isAppointmentDetails: boolean = false;
  isError: boolean = false;
  errorMessage: string;
  paymentUrl: any = environment.paymentUrl + '/core/payment/pay_session';
  isDirect:boolean =false;
  link:any;
  todaytime: any = new Date(Date.now() +  2 * 1000 * 60 *60);  // 2 hour add in current time 
  todaytime1: any = this.todaytime.getTime();
  starttime: any = new Date(Date.now() +  1 * 1000 * 60 *60);  // 1 hour add in current time 
  starttime1: any = this.todaytime.getTime();
  counsellorProfileUrl: any = environment.cdnProfileUrl;
  constructor(@Inject(DOCUMENT) private document,
  private route: ActivatedRoute,
  private globalService: GlobalService, private router: Router, private appointObj: AppointmentsService) {
   
    this.route.params.subscribe( params => {
      
      this.sessionId = params['SId'];
    });
  }

  ngOnInit() {

    this.userId = this.globalService.userId;
    this.userType = this.globalService.userType;
    this.videoScreenServer = this.globalService.SESSION_SCREEN;
    this.userTypeText = this.globalService.userTypeText;
    this.userFullName = this.globalService.userFullName;
    this.videoScreenServer = this.globalService.SESSION_SCREEN;

   
    this.appointObj.getSessionDetailsview(this.sessionId,this.userId,this.userType)
    .finally(() => { this.isLoading = false; })
    .subscribe((response: any) => {
     
      if((response.status == 1) && (response.type == 'success'))
      {
       
        this.showAppointmentDetails(response.response[0]);
       
      }else{
        this.router.navigate(['/overview']);
      }
  });


  
  }


  @Input() callback: Function;

   

  payNow(appoint) {
    (<HTMLFormElement>document.getElementById('sessionId')).value = appoint.id;
    (<HTMLFormElement>document.getElementById('paymentUserId')).value = this.userId;
    var myForm = <HTMLFormElement>document.getElementById('payment_form');
    myForm.submit();
  } 




  showAppointmentDetails(appoint: any) {
    this.sessionId = appoint.id;
    this.showAppoint = appoint;  
    this.isAppointmentDetails = true;
  }

  hideAppointmentDetails() {
    this.router.navigate(['/appointments']);
  }


   


  rejectAppointmentDetails(sessionId: number) {

    this.isLoading = true;
    swal({
      title: 'Processing your request!',
      text: 'Please wait while we process your request',
      timer: 4000,
      onOpen: function () {
        swal.showLoading()
      }
    }).then(
      function () {},
      // handling the promise rejection
      function (dismiss) {
        if (dismiss === 'timer') {
          console.log('I was closed by the timer')
        }
      }
    )

    this.appointObj.rejectAppointment(this.userId, sessionId, this.globalService.id_token)
      .finally(() => {
        this.isLoading = false;
      }).subscribe((response: any) => {
      if ((response.status == 1) && (response.type == 'success')) {
        swal('successfully rejected');
        this.hideAppointmentDetails();
      }
    });
  }


  sessionstart(sessionD:any){
   this.sessionId = sessionD.id;
  
if(sessionD.session_start <= this.starttime1){
  
  this.appointObj.startAppointment(this.userId,this.userType, this.sessionId, this.globalService.id_token)
  .finally(() => {
    
  }).subscribe((response: any) => {
  if ((response.status == 1) && (response.type == 'success')) {     
  // console.log('start session'+sessionId+' user ID  '+this.userType);  
   
  }
});
this.link = this.videoScreenServer+'?uid='+this.userId+'&username=intercell&type='+this.userTypeText+'&fullname='+this.userFullName+'&sid='+this.sessionId;
//console.log(this.link);
window.open(this.link, '_blank');


}else{

  var time  =  sessionD.start_time.split(':');
  var space  =  sessionD.start_time.split(' ');
  var time1 = time[0]+':'+time[1]+' '+space[1];
  swal("Your session will start  at "+time1+' IST on '+sessionD.start_date+". We request you to try again. ");
  //at 2 pm IST on 22nd November 2018
}
    
  }


  approveAppointmentDetails(sessionId: number) {

    this.isLoading = true;
    swal({
      title: 'Processing your request!',
      text: 'Please wait while we process your request',
      timer: 5000,
      onOpen: function () {
        swal.showLoading()
      }
    }).then(
      function () {},
      // handling the promise rejection
      function (dismiss) {
        if (dismiss === 'timer') {
          console.log('I was closed by the timer')
        }
      }
    )

    this.appointObj.approveAppointment(this.userId, sessionId, this.globalService.id_token)
      .finally(() => {
        this.isLoading = false;
      }).subscribe((response: any) => {
      if ((response.status == 1) && (response.type == 'success')) {
        swal(response.message);
        this.hideAppointmentDetails();
      }
    });
  }

  resheduleAppointmentDetails(sessionId: number, consellorId: number) {
    this.router.navigate(['/reshedule-appointment/' + sessionId + '/' + consellorId]);
    this.isLoading = true;
  }


  cancelAppointmentDetails(sessionId: number) {

    swal({
      title: 'Are you sure?',
      text: 'Are you sure to cancel this session?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
      closeOnConfirm: true,
      closeOnCancel: true
    }).then(() => {
      this.isLoading = true;
      this.appointObj.cancelAppointment(this.userId, sessionId, this.globalService.id_token)
        .finally(() => {
          this.isLoading = false;
        })
        .subscribe((response: any) => {
          if ((response.status == 1)) {
            swal(response.message);
            this.hideAppointmentDetails();
          }
        });
    });

  }




}
