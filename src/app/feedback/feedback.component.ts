import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { BasicService } from '../services/basic.service';
declare var swal: any;

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {

  

  formContact: FormGroup;
  submitted : false;
  formArray: any = {};
  emailTimer:any = null;
  phoneTimer:any =null;
  AllRequiredError:string ='';

  formErrors = {
    name : '',
    message : '',
    email : '',
    phone : '',
   
  };

  validationMessages = {   
    name : {
      required : 'Name is mandatory'
    },
    message : {
      required : 'Please enter details of your query'
    },    
    email : {
      required : 'Email address is mandatory'
    },
    phone : {
      minlength : 'Your contact number should be of 10 digits',
      maxlength : 'Your contact number should be of 10 digits'
    },
  
   }
  constructor(private fb: FormBuilder , private router: Router,private globalService : GlobalService,private basicService : BasicService ) {
  
   }

  ngOnInit() {
    this.buildForm();
  }


  buildForm() {
    this.formContact = this.fb.group({
      name: ['', [Validators.required]],
      message: ['', [Validators.required]],
      email : ['', Validators.compose([Validators.required , Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$')])],
      phone: ['', [Validators.pattern('[0-9]{10,11}$')]],
    });
    this.formContact.valueChanges.subscribe(data => this.validateForm());
  }
  
  validateForm(){
    for(let field in this.formErrors) {
      this.formErrors[field] = '';      
      let input = this.formContact.get(field);
      if (input.invalid && input.dirty) {
        for (let error in input.errors) {
          this.formErrors[field] = this.validationMessages[field][error];
        }
      }

   }
 }

 selectAllContent($event) {
  $event.target.select();
}

  onDone = function (value) {

    if(!this.formContact.valid){      
      this.AllRequiredError = 'Please fill in required details';
      return false;
    }else{
      this.AllRequiredError ='';
      for (var key in this.formContact.controls) {
        this.formArray[key] = this.formContact.controls[key].value;
      }
     
      this.submitted = true;
      //call services for sending Code
      this.basicService.Feedback(this.formArray)
      .finally(() => { })
      .subscribe((response: any) => {
      if((response.status == 1) && (response.type === 'success'))
      {
      this.formContact.reset();
      swal(response.response); 
      } 
      });

    }
    

  }



onEmailOut = function(){
  clearTimeout(this.emailTimer);
  if(this.formContact.controls.email.valid){
    this.formErrors.email ="";

  } else {
    this.emailTimer = setTimeout(()=>{
      this.formErrors.email ="Invalid email address";
    }, 1500);
  }
}


onPhoneOut = function(){
  clearTimeout(this.phoneTimer);

  if(this.formContact.controls.phone.valid){
    clearTimeout(this.timer);
    this.formErrors.phone ="";
 
  } else {
    this.phoneTimer = setTimeout(() => {
      this.formErrors.phone ="Your contact number should be of 10 digits";
    }, 1500);
  }
}

onlyNumberKey(event) {
  return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
}

}
