import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { MessageService } from '../services/message.service';
import { GlobalService} from '../services/global.service';

declare var jQuery: any;

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})

export class MessageComponent implements OnInit {
  messages : any;
  userFullName : any;
  index : any = 0;
  messagecount:any =0;
  unmessagecount:any =0;

  constructor(@Inject(DOCUMENT) private document , private messageService: MessageService , private globalService : GlobalService){ }


  onMessageClick = function (id){
    

    this.messageService.getupdateMessages(id, this.globalService.id_token)
    .finally(() => { })
    .subscribe((response: any) => {
     if((response.status == 1) && (response.type === 'success'))
      {
        this.unmessagecount = response.response[0].count;
        this.globalService.unreadmessagecount = this.unmessagecount;
        localStorage.setItem('unreadmessagecount',this.unmessagecount);
        if(this.unmessagecount>0){
           jQuery('.notification-top').html(this.unmessagecount);
        }else{
          jQuery('.notification-top').hide();
        }
      } else {
      }
    });
    
    jQuery("ul.messagetab-menu>li").click(function(e) {
      e.preventDefault();
      let index = jQuery(this).index();
      jQuery(this).siblings('li.active').removeClass("active");
      jQuery('#envelope_'+index).removeClass("fa-envelope-o");
      jQuery('#envelope_'+index).addClass("fa-envelope-open-o");
      jQuery('#msg_'+index).removeClass("unread");
      jQuery('#msg_'+index).addClass("read");
      jQuery(this).addClass("active");
     
      jQuery(".messagetab-content").removeClass("active");
      jQuery(".messagetab-content").eq(index).addClass("active");
      
     
    });
  };

  onScroll = function(){
    this.index += 6;
    //fetching All Sub Services
    this.messageService.getMessages(this.index , this.globalService.id_token)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          this.messages = this.messages.concat(response.response);
        } else {
        }
      });

  }


  ngOnInit() {

    $(window).scroll(function(){
      if ($(this).scrollTop() > 100) {
        $('.scrollToTop').fadeIn();
      } else {
        $('.scrollToTop').fadeOut();
      }
    });

    //Click event to scroll to top
    $('.scrollToTop').click(function(){
      $('html, body').animate({scrollTop : 0}, 800);
      return false;
    });

    this.document.getElementById('menuMessage').className = 'active';

    this.userFullName = this.globalService.userFullName;

    //fetching All Sub Services
    this.messageService.getMessages(this.index , this.globalService.id_token )
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          
          this.messages = response.response;
         
          this.messagecount = this.messages.length;
        } else {
         
        }
      });

  }





}
