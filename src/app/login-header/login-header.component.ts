import 'rxjs/add/operator/finally';
import * as AppConst from '../app.const';
import { Component, OnInit , NgZone, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { QuoteService } from '../services/quote.service';
import { BehaviorSubject} from 'rxjs/BehaviorSubject';
import { LoginService } from '../services/login.service';
import { GlobalService } from '../services/global.service';
import { JwtHelper } from '../services/jwt.service';
import { environment } from '../../environments/environment';
import { MessageService } from '../services/message.service';
declare var jQuery: any;
declare var gapi: any;
declare var FB: any;
declare var IN: any;
declare var swal: any;

@Component({
  selector: 'app-login-header',
  templateUrl: './login-header.component.html',
  styleUrls: ['./login-header.component.scss']
})

export class LoginHeaderComponent  implements OnInit {

  @Input() 
  ctMsg : boolean = false; 
  
  public ready = new BehaviorSubject<boolean>(false);
  quote: string;
  stat: any;
  isLoading: boolean;
  form: FormGroup;
  formLogin: FormGroup;
  formCounsellor: FormGroup;
  userId: any;
  valid: any;
  data: any;
  loginType: any;
  passwordError: any = false;
  emailError: boolean = false;
  confirmPasswordError: boolean = false;
  paymentUrl: any = environment.paymentUrl + '/core/payment/pay';
  termUrl: any = environment.termUrl;
  myConst: any;
  emailTimer: any = null;
  mypassword: any;
  emailPresent: any = true;
  emailTestTimer: any = null;
  emailRegistered: any = true;
  emailNotPresent: any = true;
  userType: any;
  cdnPath: any = environment.cdnProfileUrl;  
  isSignClicked: boolean = false;
  userPic :any;
  userFullName:any;
  AllRequiredError:any = false;
  urlId: any;
  activediv: boolean = false;
  unreadmessagecount:any = 0;
  formLoginErrors = {
    emailid: '',
    password: ''
  };

  formErrors = {
    emailid: '',
    password: '',
    cpassword: ''
  };

  formCounsellorErrors = {
    firstname: '',
    lastname: '',
    emailid: '',
    password: '',
    cpassword: '',
    gender: '',
    mobile: '',
    month: '',
    day: '',
    year: ''
  };

  validationMessages = {
    emailid: {
      required: 'Email ID is Required',
      minlength: 'Please enter a valid email address',
      maxlength: 'Max length cant be more then 30 characters',
    },
    password: {
      required: 'Password is mandatory',
      minlength: 'Minimum length must be 3 characters',
      maxlength: 'Your password must not exceed 20 characters',
      pattern: 'Contain atleast 1 character, 1 number and 1 special character'
    },
    firstname: {
      required: 'You must enter your First Name',
      minlength: 'Minimum length must be 3 characters',
    },
    lastname: {
      required: 'You must enter your Last Name',
      minlength: 'Minimum length must be 3 characters',
    },
    cpassword: {
      required: 'Password is mandatory'
    },
    mobile: {
      required: 'Contact number is mandatory',
      minlength: 'Your contact number should be of 10 digits',
    },
    gender: {
      required: 'You must select one option from the menu',
      minlength: 'Minimum length must be 3 characters',
    },
    month: {
      required: 'You must enter your month of birth',
    },
    year: {
      required: 'You must enter your year of birth',
    },
    day: {
      required: 'Your must enter your date of birth',
    },
    usertype: {
      required: 'Please select one option from the menu',
    }
  }

  constructor(private globalService: GlobalService, private messageService: MessageService, private route: ActivatedRoute, private router: Router, private quoteService: QuoteService, private loginservice: LoginService, private fb: FormBuilder, private jwtHelper: JwtHelper ,  private zone: NgZone) {



    //LinkedIn
    window['onLinkedInLoad'] = () => zone.run(() => this.onLinkedInLoad());
    window['displayProfiles'] = (profiles) => zone.run(() => this.displayProfiles(profiles));
    window['displayProfilesErrors'] = (error) => zone.run(() => this.displayProfilesErrors(error));


    

   
    /*this.loadSdkAsync(() => {
      setTimeout(() => {
        FB.init({
          appId: environment.fbAppId,
          status: false,
          xfbml: true,
          version: 'v2.8',
          cookie : true
        });
        this.ready.next(true);
      } , 600);
     });*/
   }

  loadSdkAsync(callback: () => void) {
    (<any>window).fbAsyncInit = () => this.zone.run(callback);
    // Load the Facebook SDK asynchronously
    const s = 'script';
    const id = 'facebook-jssdk';
    let js, fjs = document.getElementsByTagName(s)[0];
    if (document.getElementById(id)) return;
    js = document.createElement(s); js.id = id;
    js.src = '//connect.facebook.net/en_US/all.js';
    fjs.parentNode.insertBefore(js, fjs)
  }

  ngOnInit() {


     //LinkedIn Script ends
    this.myConst = AppConst;
    //build data model for our form
    this.buildForm();

    /*gapi.load('auth2', function () {
      gapi.auth2.init();
    }); */

  
    if (localStorage.getItem('userId')) {
      this.userId = localStorage.getItem('userId');
     
    } else {
     // this.userId = localStorage.getItem('userId');
     // console.log(this.userId+' local');
    }

   

    if (this.globalService.userType) {
      this.userType = this.globalService.userType;
    } else {
      this.userType = localStorage.getItem('userType');
    }
    this.userPic = localStorage.getItem('userPic');
    this.userFullName =  localStorage.getItem('userFullName');
  }


  googleLogin() {
    const googleAuth = gapi.auth2.getAuthInstance();
    googleAuth.then(() => {
      googleAuth.signIn({scope: 'profile email'}).then(googleUser => {
        this.onSocial(googleUser.getBasicProfile().getEmail());
      });
    });
  }

  buildForm() {
    this.formLogin = this.fb.group({
      emailid: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$')]],
      password: ['', [Validators.required]]
    });

    this.form = this.fb.group({
      emailid: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$')])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(20), Validators.pattern('^(?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*?[.~#?!@$%^&*-]).{4,}$')])],
      cpassword: ['', Validators.compose([Validators.required])],
      usertype: ['', Validators.compose([Validators.required])],
      term: ['', Validators.compose([Validators.required])]
    });

    this.formCounsellor = new FormGroup({
      firstname: new FormControl('', Validators.compose([Validators.required, Validators.minLength(3)])),
      lastname: new FormControl('', Validators.compose([Validators.required, Validators.minLength(3)])),
      emailid: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')])),
      password: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')])),
      cpassword: new FormControl('', Validators.compose([Validators.required])),
      gender: new FormControl('', Validators.compose([Validators.required])),
      mobile: new FormControl('', Validators.compose([Validators.required, Validators.minLength(10)])),
      month: new FormControl('', Validators.compose([Validators.required])),
      day: new FormControl('', Validators.compose([Validators.required])),
      year: new FormControl('', Validators.compose([Validators.required]))
    });

    this.formLogin.valueChanges.subscribe(data => this.validateLoginForm());
    this.formCounsellor.valueChanges.subscribe(data => this.validateCounsellorForm());
    this.form.valueChanges.subscribe(data => this.validateForm());
  }

  validateLoginForm() {
    for (let field in this.formLoginErrors) {
      this.formLoginErrors[field] = '';

      let input = this.formLogin.get(field);

      if (input.invalid && input.dirty) {
        for (let error in input.errors) {
          this.formLoginErrors[field] = this.validationMessages[field][error];
        }
      }
    }
  }

  validateCounsellorForm() {
    for (let field in this.formCounsellorErrors) {
      this.formCounsellorErrors[field] = '';

      let input = this.formCounsellor.get(field);
      if (input.invalid && input.dirty) {

        for (let error in input.errors) {
          this.formCounsellorErrors[field] = this.validationMessages[field][error];
        }
      }
    }
  }

  validateForm() {
    for (let field in this.formErrors) {
      this.formErrors[field] = '';
      this.confirmPasswordError = false;
      this.emailError = false;

      let input = this.form.get(field);

      if (input.invalid && input.dirty) {
        for (let error in input.errors) {
          this.formErrors[field] = this.validationMessages[field][error];
        }
      }
    }
  }


  onDashboard() {
    this.router.navigate(['/overview']);
  }

  onLogOut(){
    //Making all the Global Service NULL
    localStorage.setItem('userFullName', null);
    localStorage.setItem('userPic', null);
    localStorage.setItem('userId', '');
    localStorage.setItem('userType', null);
    localStorage.setItem('newUser', null);

    this.globalService.userFullName = null;
    this.globalService.userPic = null;
    this.globalService.userId = 0;
    this.globalService.userType = null;
    this.globalService.newUser = null;
    localStorage.clear();
    window.location.reload();
  }


  onSocial = function(email){
    this.loginservice.doSocialSign(email).finally(() => {
     //this.isLoading = false;
    }).subscribe((response: any) => {

  if ((response.status == 1) && (response.type === 'success')) {

    this.data = this.jwtHelper.decodeToken(response.response.response);
    localStorage.setItem('id_token', response.response.response);
    this.globalService.id_token = response.response.response;

    this.globalService.userFullName = this.data.firstName + ' ' + this.data.lastName;

    if(this.data.profilePic){
      this.globalService.userPic = this.cdnPath + this.data.profilePic;
    } else {
      this.globalService.userPic ="https://d3213zi13us7f1.cloudfront.net/website/assets/images/intercell-user-pic.png";
    }

    this.globalService.newUser = 0;
    this.globalService.userId = this.data.id;

    this.globalService.userType = this.data.group_id;

    localStorage.setItem('userFullName', this.globalService.userFullName);
    localStorage.setItem('userPic', this.globalService.userPic);
    localStorage.setItem('userId', this.globalService.userId + '');
    localStorage.setItem('userType', this.globalService.userType + '');
    /*localStorage.setItem('username', this.globalService.username);*/
    localStorage.setItem('newUser', this.globalService.newUser + '');

    if (this.globalService.userType == 3) {
      this.globalService.userTypeText = 'Mentor';
      localStorage.setItem('userTypeText', this.globalService.userTypeText);
      this.router.navigate(['/overview']);
      //this.router.navigate(['/set-availability']);
    }
    else if (this.globalService.userType == 2) {
      this.globalService.userTypeText = 'Mentee';
      localStorage.setItem('userTypeText', this.globalService.userTypeText);
      this.router.navigate(['/overview']);
      //this.router.navigate(['/student']);
    } else {
      console.log("User type is" + this.globalService.userType);
    }
    } else {
    this.emailRegistered = false;

    setTimeout(() => {
      this.emailRegistered = true;
    }, 12000);
    }
   });
  }


  public onLinkedInLoad() {
    IN.Event.on(IN, "auth", this.onLinkedInProfile);
  }

  public onLinkedInProfile() {
    IN.API.Profile("me")
      .fields("id", "firstName", "lastName", "email-address")
      .result(this.displayProfiles)
      .error(this.displayProfilesErrors);
  }

  public displayProfiles = (profiles) => {
    this.onSocial(profiles.values[0]['emailAddress']);
  }

  public displayProfilesErrors(error) {
    console.log(error);
  }

  //Invoke login window with button
  public liAuth() {
      const linkedIn = document.createElement('script');
      linkedIn.type = 'text/javascript';
      linkedIn.src = 'https://platform.linkedin.com/in.js';
      linkedIn.innerHTML = '\n' +
        ' api_key: 812lhg4xw3pp6x\n' +
        ' authorize: true\n' +
        ' onLoad: onLinkedInLoad\n' +
        ' scope: r_basicprofile r_emailaddress';
      document.head.appendChild(linkedIn);
      const script = document.createElement('script');
      script.type = 'in/Login';
      document.body.appendChild(script);

      setTimeout(() => {
        if(this.detectPopupBlocker()){
         IN.User.authorize(function () {
          console.log('authorize callback');
         });
         this.onLinkedInProfile();
        }
      }, 2000);
  }

  onDone = function(user) {
    this.isLoading = true;
    if (this.form.controls.password.value === this.form.controls.cpassword.value) {
      this.loginservice.checkEmail(user.emailid)
        .finally(() => {
          this.isLoading = false;
        })
        .subscribe((response: any) => {
          if (!response.response.response) {

            this.loginservice.doLogin(user)
              .finally(() => { this.isLoading = false; })
              .subscribe((response: any) => {

                this.globalService.userId = response.response.userId;
                this.userType = user.usertype;
                this.globalService.newUser = 0;
                this.globalService.userType = this.userType;

                if(this.globalService.userType == 3)
                {
                  this.globalService.userTypeText = 'Mentor';
                } else if(this.globalService.userType == 2)
                {
                  this.globalService.userTypeText = 'Mentee';
                }
                this.router.navigate(['/details']);
              });
          } else {
            this.emailError = true;
          }
        });
    } else {
      this.confirmPasswordError = true;
    }
  }



  onTerm = function () {
     localStorage.setItem('termType',  this.form.controls.usertype.value);
     window.open(this.termUrl, '_blank');
  }

  onJoin = function () {
    this.isSignClicked = false;
    this.router.navigate(['/basic-detail']);
  }

  onHome = function () {
    this.router.navigate(['']);
  }

  onForgot = function () {
    this.router.navigate(['/reset-password']);
  }

  onAbout = function () {
    this.isSignClicked = false;
    this.router.navigate(['/about-us']);
  }


  onSubmit = function (user) {
  };

   detectPopupBlocker = function() {
     const myTest = window.open("about:blank","","height=100,width=100");

     if (!myTest)
     {
       $('.signin-btn').click();
       swal('You need to disable your pop-up blocker before continuing');
       return false;
     } else
     {
       myTest.close();
       return true;
     }
   }


   alertmsg = function(){
    swal('Pls fill email and password');
    
   }
   fb_login = () =>  {

       FB.login(function (response) {

         if (response.authResponse) {
           const access_token = response.authResponse.accessToken;
           const user_id = response.authResponse.userID;

           if (response.status === 'connected') {

             console.log('Welcome!  Fetching your information.....');

             FB.api('/me?fields=email,name', function (respon) {

               if (respon.email) {
                 this.onSocial(respon.email);
               } else {
                 this.emailNotPresent = false;

                 setTimeout(() => {
                   this.emailNotPresent = true;
                 }, 10000);

               }

             }.bind(this));

             if (response.status === 'not_authorized') {
               // The person is logged into Facebook, but not your app.
               //document.getElementById('status').innerHTML = 'Please log ' + 'into this app.';
             } else {
               // The person is not logged into Facebook, so we're not sure if
               // they are logged into this app or not.
               //document.getElementById('status').innerHTML = 'Please log ' + 'into Facebook.';
             }
           }

         } else {
           //user hit cancel button
           console.log('User cancelled login or did not fully authorize.');
         }
       }.bind(this), {
         scope: 'public_profile,email'
       });
   }


  onStream = function(){
    this.isSignClicked = false;
    this.router.navigate(['/career-counsellors']);
  }

  onDownload = function(){
    this.isSignClicked = false;
    this.router.navigate(['/download-app']);
  }

  onPassword = function(){
    jQuery('#strength').show();
  }

  onPasswordOut = function(){
    jQuery('#strength').hide();
  }

  onSignIn = function(){
    this.isSignClicked = true ;
  }

  onEmailOut = function(){
    clearTimeout(this.emailTimer);
    clearTimeout(this.emailTestTimer);
    this.emailPresent = true;
    
    if(!this.formLogin.controls.emailid.valid){
      this.emailTimer = setTimeout(() => {
        this.formLoginErrors.emailid = 'Invalid email address';
      }, 2000);
    } else {

      if(this.isSignClicked){      
        this.emailTestTimer = setTimeout(() => {
          //check unique EmailId
          this.loginservice.checkLoginEmail(this.formLogin.controls.emailid.value)
            .finally(() => { })
            .subscribe((response: any) => {
              if((response.status == 1) && (response.type === 'success'))
              {
                this.emailPresent=false;
                
              } else {
                this.emailPresent=true;
                this.formLoginErrors.emailid = 'This email address has not been registered before';
              }
          
            });
        }, 2000);        
      }
    }
  }


  logOut(){
    //Making all the Global Service NULL
    localStorage.setItem('userFullName', null);
    localStorage.setItem('userPic', null);
    localStorage.setItem('userId', null);
    localStorage.setItem('userType', '');
    localStorage.setItem('newUser', null);
    localStorage.setItem('userCountryId', null);

    this.globalService.userCountryId = null;
    this.globalService.userFullName = null;
    this.globalService.userPic = null;
    this.globalService.userId = null;
    this.globalService.userType = 0;
    this.globalService.newUser = null;
    localStorage.clear();
    this.router.navigate(['/']);
  }



  onLogin = function (user) {

  if(!this.formLogin.valid || !this.emailPresent){
    this.AllRequiredError = true;
    this.formLoginErrors.emailid = 'Username and Password is required';
    return false;
  }    
    this.isLoading = true;
    this.AllRequiredError = false;
   
   /* this.quoteService.getRandomQuote({ category: 'dev' })
      .finally(() => { this.isLoading = false; })
      .subscribe((quote: string) => { this.quote = quote; });*/

    this.isLoading = true;

    this.loginservice.doSign(user)
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
        if ((response.status == 1) && (response.type === 'success')) {
          this.data = this.jwtHelper.decodeToken(response.response[0].token);
          if(this.data.is_actived>0){
      
            localStorage.setItem('id_token', response.response[0].token);
            this.globalService.id_token = response.response[0].token;
                
            this.globalService.unreadmessagecount = response.response[0].message_count;
            localStorage.setItem('unreadmessagecount',this.globalService.unreadmessagecount);
            this.globalService.userFullName = this.data.firstName + ' ' + this.data.lastName;
  
            this.globalService.username = this.data.username;
  
            if( this.data.profilePic){
              this.globalService.userPic = this.cdnPath + this.data.profilePic;
            } else {
              this.globalService.userPic ="https://d3213zi13us7f1.cloudfront.net/website/assets/images/intercell-user-pic.png";
            }
  
            this.globalService.newUser = 0;
            this.globalService.userId = this.data.id;
  
            this.globalService.userType = this.data.group_id;
            this.globalService.userCountryId = this.data.country_id;
            localStorage.setItem('userFullName', this.globalService.userFullName);
            localStorage.setItem('userPic', this.globalService.userPic);
            localStorage.setItem('userId', this.globalService.userId);
            localStorage.setItem('userType', this.globalService.userType);
            localStorage.setItem('newUser', this.globalService.newUser);
            localStorage.setItem('userCountryId', this.globalService.userCountryId);
  
            if(this.globalService.userType == 3)
            {
              this.globalService.userTypeText = 'Mentor';
              localStorage.setItem('userTypeText', this.globalService.userTypeText);
              this.router.navigate(['/overview']);
            }
            else if(this.globalService.userType == 2)
            {
              this.globalService.userTypeText = 'Mentee';
              localStorage.setItem('userTypeText', this.globalService.userTypeText);
              this.router.navigate(['/overview']);
            } else{
              console.log("User type is" + this.globalService.userType);
            }
          }else{
            this.passwordError = true;
            this.formLoginErrors.emailid = 'Your account will be active post your profile\'s verification.';
          }
          
        }else{
          this.passwordError = true;
          this.formLoginErrors.emailid = 'Either username/ password is invalid';
        }
      });
  }
  
  onProfileClick = function () {
    
        if (this.globalService.userType == 3) {
          this.router.navigate(['/counsellor']);
        } else {
          this.router.navigate(['/student']);
        }
      }

 
}
