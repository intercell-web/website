import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, FormBuilder, Validators, Form} from '@angular/forms';
import { GlobalService } from '../services/global.service';
import { BasicService } from '../services/basic.service';
import { environment } from '../../environments/environment';
import { CounsellorService } from '../services/counsellor.service';
import { Router } from '@angular/router';
declare var swal: any;
declare var jQuery: any;

@Component({
  selector: 'app-refer-friend',
  templateUrl: './refer-friend.component.html',
  styleUrls: ['./refer-friend.component.scss']
})
export class ReferFriendComponent implements OnInit {

  form: FormGroup;
  Post:any;
  name:string;
  email:string;
  phone:string;
  message:string;
  emailTimer:any = null;
  formArray: any = {};
  userId: any;
  formErrors = {
    name : '',
    email : '',
    phone : '',
    usertype :'',
    careerfield : ''
  };
  services: any;
  AllRequiredError:string ='';

  constructor(private globalService : GlobalService,private basicService : BasicService ,private fb:FormBuilder, private router: Router, private counsellorservice: CounsellorService) {

    if(!(this.globalService.userId)){
      this.globalService.userId = +localStorage.getItem('userId');
    }

    this.userId = this.globalService.userId;
   }


  ngOnInit() {

    this.buildForm();
    this.getAllServices();

  }

  buildForm() {

    this.form = this.fb.group({      
            'name':['',Validators.compose([Validators.required])],
            'email':['',Validators.compose([Validators.required , Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$')])],
            'phone':['',Validators.compose([Validators.required])],
            'usertype':['',Validators.compose([Validators.required])],
            'careerfield':['',Validators.compose([Validators.required])],
            'message':''
      
          });
         
          this.form.valueChanges.subscribe(data => this.validateForm());
  }

  validateForm(){
    for(let field in this.formErrors) {

      this.formErrors[field] = '';      
      let input = this.form.get(field);
      if (input.invalid && input.dirty) {
        for (let error in input.errors) {
          this.formErrors[field] = this.validationMessages[field][error];
          console.log(this.formErrors);
        }
      }

   }
 }


 validationMessages = {
  email : {
    required : false   
  }, 
  name : {
    required : 'Sorry! Name is mandatory'
  },
  phone : {
    required : 'Sorry! Phone number is a mandatory field'
  },
  usertype : {
    required : 'Sorry! Referral typer is mandatory'
  },
  careerfield : {
    required : 'Sorry! Career field is mandatory'
  }
}

   
  selectAllContent($event) {
    $event.target.select();
  }


  onlyNumberKey(event) {
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  }


  PostSend = function() {

    
    
    if(!this.form.valid){     
      
      this.formErrors.name =(this.form.controls.name.value=='')?'Sorry! Name is mandatory':'';

      if(this.form.controls.email.valid == false){
        if(this.form.controls.email.value == ''){
          //this.formErrors.email ='Email address is mandatory';
        }else{
          this.formErrors.email ='Invalid email address';
        }    
      }else{
           
        this.formErrors.email ='';
      }
      this.formErrors.phone =(this.form.controls.phone.value=='')?'Sorry! Phone number is a mandatory field':'';
      this.formErrors.usertype =(this.form.controls.usertype.value=='')?'Sorry! Referral typer is mandatory':'';
      this.formErrors.careerfield =(this.form.controls.careerfield.value=='')?'Sorry! Career field is mandatory':'';

      return false;
    }else{
      this.formErrors = {};
      this.AllRequiredError ='';
      for (var key in this.form.controls) {
        this.formArray[key] = this.form.controls[key].value;
      }
      this.formArray['userId'] = this.userId;

     // console.log(this.formArray);
     //call services for sending Code
      this.basicService.refertofriend(this.formArray)
        .finally(() => { })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type === 'success'))
          {
            this.form.reset();
            swal('Thank you for providing reference to us. We really appreciate your support to Intercell’s Virtual Mentor Network.');
            this.router.navigate(['/refer-friend']);
  
          } 

        });
    }
    
        
      }


      getAllServices = function(){

              //fetching All Services
            this.counsellorservice.getAllServices()
            .finally(() => { })
            .subscribe((response: any) => {

              if((response.status == 1) && (response.type === 'success'))
              {
                  this.services = response.response;
                
              } else {
              }
            });
      }
   


}
