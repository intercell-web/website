import { Component, OnInit,Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Title,Meta  } from '@angular/platform-browser';
import { GlobalService } from '../services/global.service';
import { AppointmentsService } from './../services/appointments.service';
import { FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { CounsellorService } from '../services/counsellor.service';
import { LoginService } from './../services/login.service';
import { environment } from '../../environments/environment';
import { DOCUMENT } from '@angular/platform-browser';
import { JwtHelper } from './../services/jwt.service';
declare var swal: any;

@Component({
  selector: 'app-search-mentor',
  templateUrl: './search-mentor.component.html',
  styleUrls: ['./search-mentor.component.scss']
})
export class SearchMentorComponent implements OnInit {

  counsellors: any;
  first_name :any;
  last_name :any;
  category :any;
  MentorcountryId :any;
  formSearch: FormGroup;
  services: any;
  counsellorProfileUrl: any = environment.cdnProfileUrl;
  cdnPath: any = environment.cdnProfileUrl;
  userId: any = +localStorage.getItem('userId');
  userType: any = +localStorage.getItem('userType');

  counsellorsErrormsg:string ='';
  
  constructor(@Inject(DOCUMENT) private document,private title: Title, private meta: Meta,private jwtHelper: JwtHelper,private loginservice: LoginService,private globalService : GlobalService,private router: Router, private appointObj: AppointmentsService,private fb: FormBuilder, private counsellorservice: CounsellorService) { 

    window.scrollTo(0, 0);

    if(!(this.globalService.id_token)){
      this.globalService.id_token = localStorage.getItem('id_token');
    }
    if(!(this.globalService.userCountryId)){
      this.globalService.userCountryId = localStorage.getItem('userCountryId');
    }
    this.category = 0;
    this.first_name = localStorage.getItem('first_name');
    this.last_name = localStorage.getItem('last_name');

// Start to Set Component  Title, Description and Keywords
this.title.setTitle('Search Mentor - Intercellworld.com');
this.meta.updateTag({ name: 'description', content: 'Search Mentor' });
this.meta.updateTag({ name: 'keyword', content: 'Intercell' });
// End to Set Component Title, Description and Keywords
  }

 

  ngOnInit() {

    this.buildForm(); 

    this.appointObj.getCounsellorslist(this.first_name, this.last_name,this.category, this.globalService.id_token)
    .finally(() => {  })
    .subscribe((response: any) => {
  
      
      if((response.status == 1))
      {
        this.counsellors = response.response;
        localStorage.setItem('first_name', '');
        localStorage.setItem('last_name', '');
        this.counsellorsErrormsg ='';
      }else{
        this.counsellors = false;
        this.counsellorsErrormsg ='We have not found the Mentor you are looking for. If you are looking for a specific Mentor, please write to us at info@intercellworld.com.';
      }
    });
 

     //fetching All Services
     this.counsellorservice.getAllServices()
     .finally(() => { })
     .subscribe((response: any) => {

       if((response.status == 1) && (response.type === 'success'))
       {
          this.services = response.response;
         
       } else {
       }
     });


  }

  buildForm() {
    this.formSearch = this.fb.group({
      first_name: ['', ''],
      last_name: ['', ''],
      category: ['', '']
    });
  }

  mentorSearch = function(user){
    localStorage.setItem('first_name', user.first_name);
    localStorage.setItem('last_name', user.last_name);
    this.mentorlist(user);
  }

mentorlist = function(user){

  this.appointObj.getCounsellorslist(user.first_name, user.last_name,user.category, this.globalService.id_token)
  .finally(() => {  })
  .subscribe((response: any) => {
   if((response.status == 1))
    {
      this.counsellors = response.response;
      localStorage.setItem('first_name', '');
      localStorage.setItem('last_name', '');
    }else{
      this.counsellors = false;
    }
  });
  
}


setCounsellor = function(user, indCounsellor , imageUrl){
  indCounsellor++;
  this.consellorId = user.id;
  this.summary = user.summary;
  this.MentorcountryId = user.country_id;
  this.popFullName = user.first_name +" "+user.last_name;

  localStorage.setItem('directCounsellorName', user.first_name +' '+user.last_name);

  if(imageUrl){
    this.popUserPic = this.cdnPath+imageUrl;
  }else{
    this.popUserPic = "https://d3213zi13us7f1.cloudfront.net/website/assets/images/intercell-user-pic.png";
  }
  //this.document.getElementById('username').value;
}


doLogin = function(){
  //console.log('click on do login');
  //this.isLoading = true;
  this.user={'emailid': this.document.getElementById('username').value,'password':this.document.getElementById('password').value,'usertype':'student'};
  //console.log(this.user);
  this.loginservice.doSign(this.user)
    .finally(() => { //this.isLoading = false;
    })
    .subscribe((response: any) => {
      //console.log(response);
      if ((response.status == 1) && (response.type === 'success')) {
        this.data = this.jwtHelper.decodeToken(response.response[0].token);
       // console.log(this.data);
        localStorage.setItem('id_token', response.response[0].token);
        this.globalService.id_token = response.response[0].token;

        if(this.data.profilePic){
          this.globalService.userPic = this.cdnPath + this.data.profilePic;
        } else {
          this.globalService.userPic = 'https://d3213zi13us7f1.cloudfront.net/website/assets/images/intercell-user-pic.png';
        }

        this.globalService.userFullName = this.data.firstName + ' ' + this.data.lastName;
        this.globalService.newUser = 0;
        this.globalService.userId = this.data.id;
        this.globalService.userType = this.data.group_id;
        //console.log(this.globalService.userFullName+'before loop');
        if(this.globalService.userType == 3)
        {
          this.globalService.userTypeText = 'Mentor';
          this.router.navigate(['/overview']);
        }
        else if(this.globalService.userType == 2)
        {
         
          this.globalService.userCountryId = this.data.country_id;
          localStorage.setItem('userCountryId', this.globalService.userCountryId);
          var metype = this.globalService.userCountryId;
          var mrtype = this.MentorcountryId;
          var msg = this.showMessageOfMentee(metype,mrtype);
          if(msg>0){
             this.globalService.userTypeText = 'Mentee';
             this.router.navigate(['/direct-appointment/' + this.consellorId]);
          }else{
            this.router.navigate(['/overview']);
          }
        } else{
          console.log('User type is' + this.globalService.userType);
        }

        localStorage.setItem('userFullName', this.globalService.userFullName);
        localStorage.setItem('userPic', this.globalService.userPic);
        localStorage.setItem('userId', this.globalService.userId);
        localStorage.setItem('userType', this.globalService.userType);
        localStorage.setItem('newUser', this.globalService.newUser);
        

      }else{
        console.log('User type is' + this.globalService.userType);
        this.incorrectDetails = true;
        setTimeout(() => {
          this.incorrectDetails = false;
        }, 8000);
      }
    });
}

directAppointment = function(user){
  
  this.consellorId = user.id;
  this.gendertype = (user.gender=='')?'Mr.':'Ms.';
  localStorage.setItem('directCounsellorName', this.gendertype +' '+user.first_name +' '+user.last_name);

  if(this.userType == 3)
  {
    this.globalService.userTypeText = 'Mentor';
    this.router.navigate(['/overview']);
    //this.router.navigate(['/set-availability']);
  }
  else if(this.userType == 2)
  {
    var metype = this.globalService.userCountryId;
    var mrtype = user.country_id;
    var msg = this.showMessageOfMentee(metype,mrtype);
    if(msg>0){
      this.globalService.userTypeText = 'Mentee';
      this.router.navigate(['/direct-appointment/'+this.consellorId]);
    }
    
  } else{
    console.log("User type is" + this.globalService.userType);
  }

}


showMessageOfMentee = function(metype,mrtype){
  
       if(metype!=101 && mrtype!=101){
        swal('Please be advised that users outside of India can book a session shortly. You will hear from us soon.');
        return 0;
       }else if(metype==101 && mrtype!=101){
        swal('Please be advised that sessions with International Mentors will be available soon. You will hear from us shortly.');
        return 0;
      }else if(metype!=101 && mrtype==101){
        swal('Please be advised that users outside of India can book a session shortly. You will hear from us soon.');
        return 0; 
       }else{
        return 1;
       }
      }

}
