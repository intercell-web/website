import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppointmentsComponent } from './appointments/appointments.component';
import { MakeAppointmentComponent } from './make-appointment/make-appointment.component';
import { DirectAppointmentComponent } from './direct-appointment/direct-appointment.component';
import { ResheduleAppointmentComponent } from './reshedule-appointment/reshedule-appointment.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { DownloadAppComponent } from './download-app/download-app.component';
import { CareersComponent } from './careers/careers.component';
import { ReferFriendComponent } from './refer-friend/refer-friend.component';
import { NewsComponent } from './news/news.component';
import { FaqComponent } from './faq/faq.component';
import { TermComponent } from './term/term.component';
import { CounsellorComponent } from './counsellor/counsellor.component';
import { StudentComponent } from './student/student.component';
import { AccountComponent } from './account/account.component';
import { OverviewComponent } from './overview/overview.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SetAvailabilityComponent } from './set-availability/set-availability.component';
import { SettingComponent } from './setting/setting.component';
import { MessageComponent } from './message/message.component';
import { ProfileComponent } from './profile/profile.component';
import { FavoriteComponent } from './favorite/favorite.component';
import { PaymentComponent } from './payment/payment.component';
import { PaymentCancelComponent } from './payment-cancel/payment-cancel.component';
import { GetPaymentComponent } from './get-payment/get-payment.component';
import { StreamComponent } from './stream/stream.component';
import { TrackComponent } from './track/track.component';
import { PaymentSuccessComponent } from './payment-success/payment-success.component';
import { CounsellorProfileComponent } from './counsellor-profile/counsellor-profile.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { DetailComponent } from './detail/detail.component';
import { BasicDetailsComponent } from './basic-details/basic-details.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { AboutStudentComponent } from './about-student/about-student.component';
import { AboutCounsellorComponent } from './about-counsellor/about-counsellor.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { LearnMenteeComponent } from './learn-mentee/learn-mentee.component';
import { LearnMentorComponent } from './learn-mentor/learn-mentor.component';
import { SearchMentorComponent } from './search-mentor/search-mentor.component';
import { LoginComponent } from './login/login.component';
import { NetworkComponent } from './network/network.component';
import { ContactusComponent } from './contactus/contactus.component';
import { AppointmentsDetailsComponent } from './appointments-details/appointments-details.component';
import { NeedHelpComponent } from './need-help/need-help.component';
import { IntercellStoryComponent } from './intercell-story/intercell-story.component';


import { AuthGuard } from './services/auth-guard.service';

const routes: Routes = [
    // Fallback when no prior route is matched
    
    //{ path: '', redirectTo: '', pathMatch: 'full'},
    { path: '', component: LoginComponent, pathMatch: 'full'},
    { path: 'appointments/:SId', component: AppointmentsDetailsComponent , canActivate: [AuthGuard] },
    { path: 'appointments', component: AppointmentsComponent , canActivate: [AuthGuard] },
    { path: 'contact-us', component: ContactusComponent },
    { path: 'about-us', component: AboutUsComponent },
    { path: 'direct-appointment/:counsellorId', component: DirectAppointmentComponent, canActivate: [AuthGuard] },
    { path: 'reshedule-appointment/:sessionId/:counsellorId', component: ResheduleAppointmentComponent, canActivate: [AuthGuard] },
    { path: 'make-appointment', component: MakeAppointmentComponent, canActivate: [AuthGuard] },
    { path: 'download-app' , component: DownloadAppComponent},
    { path: 'refer-friend' , component: ReferFriendComponent, canActivate: [AuthGuard]},
    { path: 'careers' , component: CareersComponent},
    { path: 'news' , component: NewsComponent},
    { path: 'term/:urlId' , component: TermComponent},
    { path: 'term' , component: TermComponent},
    { path: 'faq' , component: FaqComponent},
    { path: 'student' , component: StudentComponent, canActivate: [AuthGuard] },
    { path: 'counsellor' , component: CounsellorComponent , canActivate: [AuthGuard]},
    { path: 'account' , component: AccountComponent, canActivate: [AuthGuard]},
    { path: 'dashboard' , component: DashboardComponent , canActivate: [AuthGuard]},
    { path: 'overview' , component: DashboardComponent , canActivate: [AuthGuard]},
    { path: 'set-availability' , component: SetAvailabilityComponent, canActivate: [AuthGuard]},
    { path: 'setting' , component: SettingComponent, canActivate: [AuthGuard]},
    { path: 'message' , component: MessageComponent, canActivate: [AuthGuard]},
    { path: 'profile' , component: ProfileComponent, canActivate: [AuthGuard]},
    { path: 'favorite' , component: FavoriteComponent, canActivate: [AuthGuard]},
    { path: 'payment' , component: PaymentComponent},
    { path: 'payment_cancel' , component: PaymentCancelComponent, canActivate: [AuthGuard]},
    { path: 'get-payment' , component: GetPaymentComponent},
    { path: 'career-counsellors' , component: StreamComponent},
    { path: 'career-counsellor/:field' , component: TrackComponent},
    { path: 'track' , component: TrackComponent},
    { path: 'payment-success' , component: PaymentSuccessComponent},
    { path: 'counsellor-profile' , component: CounsellorProfileComponent},
    { path: 'reset-password' , component: ResetPasswordComponent},
    { path: 'change-password/:token' , component: ChangePasswordComponent},
    //{ path: 'change-password/:insertId/:userId' , component: ChangePasswordComponent},
    { path: 'details' , component: DetailComponent},
    { path: 'basic-detail' , component: BasicDetailsComponent},
    { path: 'privacy' , component: PrivacyComponent, canActivate: [AuthGuard]},
    { path: 'about-mentor' , component: AboutCounsellorComponent},
    { path: 'about-mentee' , component: AboutStudentComponent},
    { path: 'feedback' , component: FeedbackComponent},
    { path: 'learn-mentor' , component: LearnMentorComponent},
    { path: 'learn-mentee' , component: LearnMenteeComponent},
    { path: 'search-mentor' , component: SearchMentorComponent},
    { path: 'need-help' , component: NeedHelpComponent, canActivate: [AuthGuard] },
    { path: 'intercell-story' , component: IntercellStoryComponent},
    { path: 'twohmp' , component: NetworkComponent},
    { path: '**', component: LoginComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})


export class AppRoutingModule { }
