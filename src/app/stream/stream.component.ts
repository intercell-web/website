import { Component, OnInit } from '@angular/core';
import { LandingService } from '../services/landing.service';
import { GlobalService } from '../services/global.service';
import { Router} from '@angular/router';
import { DOCUMENT, Title,Meta  } from '@angular/platform-browser';

@Component({
  selector: 'app-stream',
  templateUrl: './stream.component.html',
  styleUrls: ['./stream.component.scss']
})
export class StreamComponent implements OnInit {
  serviceDetails:any = [];
  patchDetails :any = [];

  constructor(private title: Title, private meta: Meta,private landingService : LandingService , private globalService : GlobalService , private router: Router) { 
 // Start to Set Component  Title, Description and Keywords
 this.title.setTitle('Stream - Intercellworld.com');
 this.meta.updateTag({ name: 'description', content: 'Stream' });
 this.meta.updateTag({ name: 'keyword', content: 'Intercell' });
 // End to Set Component Title, Description and Keywords

  }

  ngOnInit() {

    //fetching Services Data
    this.landingService.getServices()
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status ==1) && (response.type === 'success'))
        {
          this.serviceDetails = response.response;

          //this.router.navigate(['/home']);
        } else {
          console.log("There is Some Error");
        }
      });

    //fetching Sub Services Data
    this.landingService.getSubServices()
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status ==1) && (response.type === 'success'))
        {
          this.patchDetails = response.response;
        } else {
          console.log("There is Some Error");
        }
      });

    $('html, body').animate({ scrollTop: 0 }, 'slow');
  }

  goTrack = function(id){
    localStorage.setItem('serviceDetails', '1');
    localStorage.setItem('showTrackId', id);

    this.globalService.serviceDetails = 1;
    this.globalService.showTrackId = id;
    this.router.navigate(['/track']);
  }

  feedback = function(){
    this.router.navigate(['/feedback']);
  }

  serviceDetail = function(url){
    //localStorage.setItem('showServiceId', id);
    //this.globalService.showServiceId = id;
    this.router.navigate(['/career-counsellor',url]);
  }

}
