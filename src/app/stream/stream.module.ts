import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { ModalModule } from "ng2-modal";
@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    ModalModule
  ],
  declarations: [
    //StreamComponent
  ],
  providers: [
    //AvailabilityService
  ]
})
export class StreamModule { }
