import { Component, OnInit,Inject } from '@angular/core';
import { LoginService } from '../services/login.service';
import { GlobalService} from '../services/global.service';
import {FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { ResetPasswordService } from '../services/reset-password.service';
declare var jQuery: any;
import { DOCUMENT } from '@angular/platform-browser';
import { BasicService} from '../services/basic.service';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.scss']
})
export class PrivacyComponent implements OnInit {

  phoneNumber: any;
  emailAddress: any;
  officialEmail: any;
  mailChange: boolean =false ;
  phoneChange: boolean =false;
  sameMail: boolean =false;
  invalidMail: boolean =false;
  samePhone: boolean =false;
  blankPhone: boolean =false;
  invalidPhone: boolean =false;
  mailResent: boolean =false;
  phoneResent: boolean =false;
  mailRegistered: boolean =false;
  invalidMailKey: boolean =false;
  invalidPhoneKey: boolean =false;
  blankmail: boolean =false;
  phoneRegistered: boolean =false;
  mailCode: any;
  phoneCode: any;
  emailVerified: boolean = false;
  phoneVerified: boolean = false;
  phoneerrormsg:string ='';

  password: any;
  cpassword: any;
  formPassword : any;
  passError: boolean = false;
  formPasswordErrorsAll:string =''; 
  countryList: any = [];
  countrycodeflags: any = false;
  countrycodes : number;

  formPasswordErrors = {
    password : '',
    cpassword : ''
  };

  validationMessages = {
    password : {
      required : 'Password is mandatory',
      minlength : 'Your password must be minimum 6 characters',
      maxlength : 'Your password must not exceed 20 characters',
      pattern : 'Your password must have a minimum of 1 digit and 1 special character'
    },
    cpassword : {
      required: 'Confirm Password is mandatory'
    }
  }



  constructor(@Inject(DOCUMENT) private document,
              private loginservice: LoginService, 
              private globalService: GlobalService,
              private basicservice: BasicService,
              private fb: FormBuilder,
              private resetPasswordService: ResetPasswordService
              ) { }

  ngOnInit() {
    this.buildForm();
    //fetching privacy Data
    this.loginservice.getPrivacyValues(this.globalService.userId, this.globalService.id_token)
      .finally(() => { })
      .subscribe((response: any) => {
        if((response.status ==1) && (response.type === 'success'))
        {
          if(!response.response.response[0].official_id){
            jQuery('#official_address').hide();
          }

          this.emailAddress = response.response.response[0].email;
          this.phoneNumber = response.response.response[0].mobile;
          this.officialEmail = response.response.response[0].official_id;

          if(!this.officialEmail) {
            jQuery('#off_mail').hide();
          }
        } else {
        }
      });

    jQuery('#flip_1').click(function(){
      jQuery('#panel_1').slideToggle('slow');
      
    });

    jQuery("#flip_2").click(function(){
      jQuery("#panel_2").slideToggle("slow");
     
    });

    jQuery('#flip_3').click(function(){
      jQuery("#panel_3").slideToggle("slow");
    });

    this.document.getElementById('menuSetting').className = 'active';

    this.CountriesList();
  }



  buildForm() {
    this.formPassword = this.fb.group({
      password : ['', Validators.compose([Validators.required,  Validators.minLength(6) , Validators.maxLength(20) , Validators.pattern('^(?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*?[.~#?!@$%^&*-]).{3,}$')])],
      cpassword : ['', Validators.compose([Validators.required])]
    });

    this.formPassword.valueChanges.subscribe(data => this.validatePasswordForm());
  }


  validatePasswordForm() {
    for(let field in this.formPasswordErrors){
      this.formPasswordErrors[field]= '';
      let input = this.formPassword.get(field);
      if(input.invalid && input.dirty){
        for(let error in input.errors){
          this.formPasswordErrors[field] = this.validationMessages[field][error];
        }
      }
    }
  }


  onReset = function() {

       
   if(this.formPassword.controls.password.value ==undefined || this.formPassword.controls.password.value =='' ){
    this.formPasswordErrors.password = 'Please Enter New Password Password';
    return false;

    }else if(this.formPassword.controls.cpassword.value ==undefined || this.formPassword.controls.cpassword.value =='' ){
      this.formPasswordErrors.password = 'Please Enter Confirm Password';
      return false;
  
      }else if(this.formPassword.controls.password.value != this.formPassword.controls.cpassword.value  ){
        this.formPasswordErrors.password = 'Password Mismatch';
        return false;
    
        }else{
          if(this.formPassword.valid == false){
            for(let field in this.formPasswordErrors) {
              let input1 = this.formPassword.get(field);
              for (let error in input1.errors) {
                this.formPasswordErrors[field] = this.validationMessages[field][error];
              }
            }
            return false;

          }
          this.resetPasswordService.onReset(this.password, this.globalService.userId, 1)
          .finally(() => {
          })
          .subscribe((response: any) => {
            
            if ((response.status == 1) && (response.type === 'success')) {
              this.formPassword.reset({password: '',cpassword: ''});
              jQuery("#panel_3").slideToggle("slow");
              jQuery('#forgot_success').show();
              setTimeout(()=>{
                jQuery('#forgot_success').hide();
              }, 3500);
             
            } else {
            }
          });

        }
     

  
      }


  onMailChange = function () {
    this.invalidMail = false;
    this.emailVerified =false;

    if(jQuery("#mailValue").val() == ''){
      this.blankmail = true;
      this.invalidMail = false;
      this.sameMail = false;
      jQuery('#box_1').hide();
    }else if(jQuery("#mailValue").val() == this.emailAddress){
     this.sameMail = true;
     this.blankmail = false;
     this.invalidMail = false;
     jQuery('#box_1').hide();
    } else {
     let re = /\S+@\S+\.\S+/;

     if(!(re.test(jQuery("#mailValue").val()))){
       this.invalidMail=true;
       this.sameMail = false;
       this.blankmail = false;
     } else{
       this.sameMail = false;

       //call services for sending Code
       this.loginservice.sendMailCode(jQuery("#mailValue").val(), this.globalService.id_token)
       .finally(() => { })
       .subscribe(( response: any) => {
        
         if((response.status == 1) && (response.type === 'success'))
         {
           this.mailCode = '';
           this.mailChange = false;
           jQuery('#box_1').show();
         } else if((response.status == 0) && (response.type === 'error'))
         {
           this.mailRegistered = true;
         } else {
           this.emailError=true;
         }
       });

     }
   }
  }


  onPhoneChange = function () {
    if(this.countrycodeflags == false){
      this.phoneerrormsg ='Please Select Country Code';
      jQuery('#box_2').hide();
    }else if(jQuery("#phoneValue").val() == ''){
      this.phoneerrormsg ='Phone number is mandatory';
      jQuery('#box_2').hide();
    }else if(jQuery("#phoneValue").val().length<8){
      this.phoneerrormsg ='Your Phone number should be of Minimum 8 digits';
      jQuery('#box_2').hide();
    }else if(jQuery("#phoneValue").val() == this.phoneNumber){
      this.phoneerrormsg ='This phone number has already been registered';
      jQuery('#box_2').hide();
    } else {
    
        //call services for sending Code
        this.loginservice.sendPhoneCode(jQuery("#phoneValue").val(),this.countrycodes, this.globalService.id_token)
        .finally(() => { })
        .subscribe((response: any) => {

          if((response.status == 1) && (response.type === 'success'))
          {
            this.phoneChange = false;
            this.phoneCode = '';
            jQuery('#box_2').show();
          } else if((response.status == 0) && (response.type === 'error'))
          {
            this.phoneerrormsg ='This phone number has already been registered.';
            jQuery('#box_2').hide();
            
          } else {
            this.phoneerrormsg ='This phone number has already been registered.';
            jQuery('#box_2').hide();
          }
        });

        this.phoneerrormsg ='';
    }
 

  }


  onPhoneChange1 = function () {
    this.invalidPhone=false;
    this.phoneVerified =false;

    if(this.countrycodeflags == false){
      this.countrycodes = 0;
      this.blankPhone = false;
      this.samePhone = false;
      this.invalidPhone=false;
      this.phoneRegistered=false;
      jQuery('#box_2').hide();
    }else if(jQuery("#phoneValue").val() == '' || jQuery("#phoneValue").val().length<8){
      this.blankPhone = true;
      this.samePhone = false;
      this.invalidPhone=false;
      this.phoneRegistered=false;
      jQuery('#box_2').hide();
    }else if(jQuery("#phoneValue").val() == this.phoneNumber){
      this.samePhone = true;
      this.blankPhone = false;
      this.invalidPhone=false;
      this.phoneRegistered=false;
      jQuery('#box_2').hide();
    } else {
      let re = /^\d{10}$/;

      if(!(re.test(jQuery("#phoneValue").val()))){
        this.invalidPhone=true;
        this.blankPhone = false;
        this.samePhone=false;
        this.phoneRegistered=false;
      } else {

        this.samePhone = false;
        this.blankPhone = false;
        this.invalidPhone=false;
        this.phoneRegistered=false;
          //call services for sending Code
          this.loginservice.sendPhoneCode(jQuery("#phoneValue").val(), this.globalService.id_token)
          .finally(() => { })
          .subscribe((response: any) => {

            if((response.status == 1) && (response.type === 'success'))
            {
              this.phoneChange = false;
              this.phoneCode = '';
              jQuery('#box_2').show();
            } else if((response.status == 0) && (response.type === 'error'))
            {
              this.phoneRegistered = true;
            } else {
            }
          });

      }
    }
  }


  verifyPhone = function (){
    //call services for sending Code
    this.loginservice.confirmPhoneCode(jQuery("#phoneValue").val(), this.phoneCode, this.globalService.id_token)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          this.invalidPhoneKey = false ;
          this.phoneVerified = true;

          //call services for updating Mail
          this.loginservice.updateNewPhone(jQuery("#phoneValue").val(), this.globalService.userId , this.globalService.id_token,this.countrycodes)
            .finally(() => { })
            .subscribe((response: any) => {
              if ((response.status == 1) && (response.type === 'success')) {
                this.phoneNumber = jQuery("#phoneValue").val();
                //Close All
                //jQuery('#flip_2').click();
                this.phoneCode = '';
                this.countrycodeflags = false;
                this.countrycodes = '';
                jQuery("#phoneValue").val('');
                jQuery("#panel_2").slideToggle("slow");                
                jQuery('#box_2').hide();
                jQuery('#mobile_success').slideDown();
                setTimeout(()=>{
                  jQuery('#mobile_success').hide();
                }, 3500);

              } else {

              }
            });
        } else {
          this.phoneCode = "";
          this.invalidPhoneKey = true ;

          setTimeout(()=>{
            this.invalidPhoneKey = false;
          }, 3000);
        }
      });
  };



  verifyMail = function (){

    //call services for sending Code
    this.loginservice.confirmMailCode(jQuery("#mailValue").val(), this.mailCode, this.globalService.id_token)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          this.emailInvalid = false ;
          this.emailVerified = true;

          //call services for updating Mail
          this.loginservice.updateNewMail(jQuery("#mailValue").val(), this.globalService.userId, this.globalService.id_token)
            .finally(() => { })
            .subscribe((response: any) => {

              if ((response.status == 1) && (response.type === 'success')) {
                this.emailAddress = jQuery("#mailValue").val();
                //Close All
                jQuery('#flip_1').click();
                this.mailCode = '';
                jQuery("#mailValue").val('');
                jQuery('#box_1').hide();
                jQuery('#email_success').slideDown();
                setTimeout(()=>{
                  jQuery('#email_success').hide();
                }, 3500);
              } else {

              }
           });
              } else {
          this.mailCode = "";
          this.invalidMailKey = true ;

          setTimeout(()=>{
            this.invalidMailKey = false;
          }, 3000);
        }
      });
  };

  resendMail = function (){

    //call services for sending Code
    this.loginservice.resendMailCode(jQuery("#mailValue").val(), this.globalService.id_token)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          this.emailVerified=false;
          this.invalidMailKey=false;
          this.mailCode = "";
          this.mailResent=true;

          setTimeout(()=>{
            this.mailResent=false;
          }, 3000);

        } else {
         // this.emailError=true;
        }
      });
  };


  resendPhone = function (){

    //call services for sending Code
    this.loginservice.resendPhoneCode(jQuery("#phoneValue").val(),this.countrycodes, this.globalService.id_token)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          this.phoneVerified=false;
          this.invalidPhoneKey=false;
          this.phoneCode = "";
          this.phoneResent=true;

          setTimeout(()=>{
            this.phoneResent=false;
          }, 3000);

        } else {
          //this.phoneError=true;
        }
      });
  };


  onPasswordChange = function(){

  }


  onlyNumberKey(event) {
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  }

  CountriesList = function(){
    
             //fetching All Countries List
           this.basicservice.getCountriesList()
           .finally(() => { })
           .subscribe((response: any) => {
    
             if((response.status == 1) && (response.type === 'success'))
             {
               this.countryList = response.response;
             } else {
             }
           });
    
          }


          countryCodechange(event){
            if(event!=''){
              this.countrycodes = event;
              this.countrycodeflags = true;
              this.phoneerrormsg = '';
            }else{
              this.countrycodeflags = false;
              this.phoneerrormsg = 'Please Select Country Code';
            }
             
          }

  }
