import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route } from '../core/route.service';
import { HomeComponent } from './home.component';
import { LoginComponent } from '../login/login.component';


const routes: Routes = Route.withShell([
  { path: '', component: LoginComponent, pathMatch: 'full'},
  /*{ path: '', redirectTo: '/login', pathMatch: 'full' },*/
  { path: 'home', component: HomeComponent, data: { title: 'Home - Intercell' } }
]);

@NgModule({
 // imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class HomeRoutingModule { }
