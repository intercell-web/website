import 'rxjs/add/operator/finally';

import { Component, OnInit } from '@angular/core';
import { LandingService } from '../services/landing.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit{
  isLoading: boolean;
  serviceDetails:any = [];
  meetCounsellorDetails:any = [];
  studentSpeakDetails:any = [];

  constructor(private landingService : LandingService) {

  }

  ngOnInit() {

    //fetching Services Data
    this.landingService.getServices()
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status ==1) && (response.type === 'success'))
        {
          this.serviceDetails = response.response;

          //this.router.navigate(['/home']);
        } else {
          console.log("There is Some Error");
        }
      });

    //fetching Services Data
    this.landingService.getStudentSpeak()
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status ==1) && (response.type === 'success'))
        {
          this.studentSpeakDetails = response.response;

          //this.router.navigate(['/home']);
        } else {
          console.log("There is Some Error");
        }
      });

    //fetching Services Data
    this.landingService.getMeetCounsellor()
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status ==1) && (response.type === 'success'))
        {
          this.meetCounsellorDetails = response.response;
          //this.router.navigate(['/home']);
        } else {
          console.log("There is Some Error");
        }
      });

  }

}
