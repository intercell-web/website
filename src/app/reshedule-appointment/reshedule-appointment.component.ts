import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { AppointmentsService } from './../services/appointments.service';
declare var swal: any;

@Component({
  selector: 'app-reshedule-appointment',
  templateUrl: './reshedule-appointment.component.html',
  styleUrls: ['./reshedule-appointment.component.scss']
})
export class ResheduleAppointmentComponent implements OnInit {
    request:any ={};
    params:any={};
    calendaring: any;
    isLoading: boolean;

    userId: number;
    userType: number;
    userTypeText:string;
    prevousArraow:boolean = false;
    nextArraow: boolean = false;
    appointmentTable:boolean = false;
    SessionDetails:any;
    sessionId: number;
    counsellorId: number;
    isCheckout = false;
    isCalendar = true;
    private sub: any;

    constructor(private globalService : GlobalService ,private route: ActivatedRoute,private router: Router,private appointObj: AppointmentsService) {
      
     

       }

    ngOnInit() {

      this.userId = this.globalService.userId;
      this.userType = this.globalService.userType;
      this.userTypeText = this.globalService.userTypeText;
      this.request.studentId = this.userId;

      if(this.userType==3){
        this.router.navigate(['/overview']);
      }
      
      this.sub = this.route.params.subscribe(params => {
          this.sessionId = +params['sessionId'];
          this.counsellorId = +params['counsellorId'];
          this.request.sessionId = this.sessionId;
          this.request.counsellorId = this.counsellorId;
          this.request.studentId = this.userId;
          this.request.duration = 30;
          this.showCounsellorCalendar('next');
          // (+) converts string 'id' to a number
      });


      this.appointObj.getSessionDetails(this.request.sessionId)
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
       
        if((response.status == 1) && (response.type == 'success'))
        {
          this.SessionDetails = response.response[0];
          if(this.SessionDetails.appointment_status == 'Completed'){
            this.router.navigate(['/overview']);
          }
          
          this.request.gendertype = (this.SessionDetails.gender_mr=='Male')?'Mr.':'Ms.';
          this.request.counsellorName = this.request.gendertype +' '+this.SessionDetails.first_name +' '+ this.SessionDetails.last_name;
          this.request.serviceName = this.SessionDetails.category_name;
        }else{
          this.router.navigate(['/overview']);
        }
    });

     }


    setCalendarTab(user:any) {
      if(user.id> 0){

          this.request.counsellorId = user.id;
          this.request.gendertype = (user.gender_mr=='Male')?'Mr.':'Ms.';
          this.request.counsellorName = this.request.gendertype+" "+user.first_name+" "+user.last_name;
          this.showCounsellorCalendar('next');
      }
      return true;
    }
    showCounsellorCalendar(action:string){
      this.appointmentTable = false;
      this.appointObj.getCalendar(this.request.counsellorId, action , this.globalService.id_token)
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
        if((response.status == 1) && (response.type == 'success'))
        {
          this.calendaring = response.response;
          var today:any ;
          var endday:any ;

          today = this.globalService.getdatefun(0);
          endday = this.globalService.getdatefun(30);

          if(response.response[0].date == today){
            this.prevousArraow = false;
          }else{
            this.prevousArraow = true;
          }
          if(response.response[6].date >= endday){
            this.nextArraow = false;
          }else{
            this.nextArraow = true;
          }

          this.appointmentTable=true;
          //this.showTab('isServicesTab');
        }
      });
    }
    setCheckoutTab(){
      //this.request.expectationMessages = this.expectationMessages;

    }
    checkOut() {
      //console.log('done');
      this.isLoading = true;
      this.request.token =  this.globalService.id_token;
      
      this.appointObj.rescheduleAppointment(this.request)
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
    
        if((response.status == 1) && (response.type == 'success'))
        {
          swal(response.message);
          this.router.navigate(['/appointments']);
        }
      });
      return true;

    }
    setStartDateTime(dateTime:any){
      this.request.dateTime = dateTime;
    }




      setDateTime(date,time){
        this.request.date = date;
        this.request.time = time;
        this.isCalendar = false;
        this.isCheckout = true;
      // alert('date'+date+'==time'+time);

      }

      generateArray(obj){
        return Object.keys(obj).map((key)=>{ return obj[key]});
      }
      getClassName(idx){
        if(idx==0){
          return 'one';
        }else if(idx==1){
          return 'two';
        }else if(idx==2){
          return 'three';
        }else if(idx==3){
          return 'four';
        }else if(idx==4){
          return 'five';
        }else if(idx==5){
          return 'six';
        }else if(idx==6){
          return 'seven';
        }
        return 'one';
      }



    getPreviousWeek(){
      this.showCounsellorCalendar('prev');
    }

    getNextWeek(){
      this.showCounsellorCalendar('next');
    }
}
