import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResheduleAppointmentComponent } from './reshedule-appointment.component';

describe('ResheduleAppointmentComponent', () => {
  let component: ResheduleAppointmentComponent;
  let fixture: ComponentFixture<ResheduleAppointmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResheduleAppointmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResheduleAppointmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
