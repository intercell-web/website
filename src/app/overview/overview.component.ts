import { Component, OnInit,Inject } from '@angular/core';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';
import { AppointmentsService } from './../services/appointments.service';
import { MessageService } from '../services/message.service';
import * as $ from 'jquery';
import { GlobalService } from '../services/global.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  userId: number;
  userType: number;
  userTypeText: string;
  //userFullName: string;
  videoScreenServer: string;
  appointments: any= [];
  isLoading: boolean;
  sessionId: number;
  isError: boolean = false;
  messages: any;
  totalMessages: number=0;
  totalSession: number=0;
  counsellorProfileUrl: any = environment.cdnProfileUrl;
  color:any ;
  constructor(
    @Inject(DOCUMENT) private document,
    private messageService: MessageService , 
    private globalService: GlobalService ,
    private router: Router, 
    private appointObj: AppointmentsService) { }

  ngOnInit() {
    $('html, body').animate({ scrollTop: 0 }, 'slow');

    this.color = ['grey','blue','red','grey'];

    $(window).scroll(function(){
      if ($(this).scrollTop() > 100) {
        $('.scrollToTop').fadeIn();
      } else {
        $('.scrollToTop').fadeOut();
      }
    });

    //Click event to scroll to top
    $('.scrollToTop').click(function(){
      $('html, body').animate({scrollTop : 0}, 800);
      return false;
    });

    this.document.getElementById('menuOverview').className = 'active';
    this.isLoading = true;
    this.userId = this.globalService.userId;
    this.userType = this.globalService.userType;
    this.userTypeText = this.globalService.userTypeText;
   // this.userFullName = this.globalService.userFullName;
    this.videoScreenServer = this.globalService.SESSION_SCREEN;

    this.appointObj.getUpcoming(this.userTypeText, this.globalService.id_token)
        .finally(() => { this.isLoading = false; })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type == 'success'))
          {
            this.isError = false;
            this.appointments = response.response;
            //console.log(this.appointments);
            if(this.appointments!=''){
              this.totalSession = this.appointments.length;
            }
            
          }

        });

       //fetching All Sub Services
       this.messageService.getMessages(0 , this.globalService.id_token)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
            this.messages = response.response;
        }
      });

      this.messageService.getMessagesCount(this.globalService.id_token)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
            this.totalMessages = response.response[0].count;
        }
      });
  }

  showAppointmentDetails(appoint: any) {
    this.globalService.appointDirectDetail= appoint;
    this.router.navigate(['/appointments']);
  }


  onMessageClick(){
    this.router.navigate(['/message']);
  }
}
