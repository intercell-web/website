import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { SetAvailabilityComponent } from './set-availability.component';
import { AvailabilityService } from './../services/availability.service';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule
  ],
  declarations: [
    SetAvailabilityComponent
  ],
  providers: [
    //AvailabilityService
  ]
})
export class SetAvailabilityModule { }
