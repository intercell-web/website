import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';
import { AvailabilityService } from './../services/availability.service';
import { GlobalService } from '../services/global.service';
import { AppointmentsService } from './../services/appointments.service';


declare var swal: any;

@Component({
  selector: 'app-set-availability',
  templateUrl: './set-availability.component.html',
  styleUrls: ['./set-availability.component.scss']
})
export class SetAvailabilityComponent implements OnInit {
  timePloter: any;
  userTimeSlots: any = [];
  userId: number;
  userType: number;
  userTypeText: string;
  userFullName: string;
  isLoading: boolean;

  constructor(@Inject(DOCUMENT) private document, private globalService: GlobalService , private router: Router, private availObj: AvailabilityService , private appointObj: AppointmentsService) { }

  ngOnInit() {

    $(window).scroll(function(){
      if ($(this).scrollTop() > 100) {
        $('.scrollToTop').fadeIn();
      } else {
        $('.scrollToTop').fadeOut();
      }
    });

    //Click event to scroll to top
    $('.scrollToTop').click(function(){
      $('html, body').animate({scrollTop : 0}, 800);
      return false;
    });


    this.userId = this.globalService.userId;
    this.userType = this.globalService.userType;

    if(this.userType==2){
      this.router.navigate(['/overview']);
    }


    this.userTypeText = this.globalService.userTypeText;
    this.userFullName = this.globalService.userFullName;
    this.timePloter = [
                        {'idx': 1,'st':"8:00", 'et':"8:30"},
                        {'idx': 2,'st':"8:30", 'et':"9:00"},
                        {'idx': 3,'st':"9:00", 'et':"9:30"},
                        {'idx': 4,'st':"9:30",'et':"10:00"},
                        {'idx': 5,'st':"10:00",'et':"10:30"},
                        {'idx': 6,'st':"10:30",'et':"11:00"},
                        {'idx': 7,'st':"11:00",'et':"11:30"},
                        {'idx': 8,'st':"11:30",'et':"12:00"},
                        {'idx': 9,'st':"12:00",'et':"12:30"},
                        {'idx': 10,'st':"12:30",'et':"13:00"},
                        {'idx': 11,'st':"13:00",'et':"13:30"},
                        {'idx': 12,'st':"13:30",'et':"14:00"},
                        {'idx': 13,'st':"14:00",'et':"14:30"},
                        {'idx': 14,'st':"14:30",'et':"15:00"},
                        {'idx': 15,'st':"15:00",'et':"15:30"},
                        {'idx': 16,'st':"15:30",'et':"16:00"},
                        {'idx': 17,'st':"16:00",'et':"16:30"},
                        {'idx': 18,'st':"16:30",'et':"17:00"},
                        {'idx': 19,'st':"17:00",'et':"17:30"},
                        {'idx': 20,'st':"17:30",'et':"18:00"},
                        {'idx': 21,'st':"18:00",'et':"18:30"},
                        {'idx': 22,'st':"18:30",'et':"19:00"},
                        {'idx': 23,'st':"19:00",'et':"19:30"},
                        {'idx': 24,'st':"19:30",'et':"20:00"},
                        {'idx': 25,'st':"20:00",'et':"20:30"},
                        {'idx': 26,'st':"20:30",'et':"21:00"},
                        {'idx': 27,'st':"21:00",'et':"21:30"},
                        {'idx': 28,'st':"21:30",'et':"22:00"},
                        {'idx': 29,'st':"22:00",'et':"22:30"},
                        {'idx': 30,'st':"22:30",'et':"23:00"},
                        {'idx': 31,'st':"23:00",'et':"23:30"},
                        {'idx': 32,'st':"23:30",'et':"00:00"},
                        {'idx': 33,'st':"00:00",'et':"00:30"},
                        {'idx': 34,'st':"00:30",'et':"01:00"},
                        {'idx': 35,'st':"01:00",'et':"01:30"},
                        {'idx': 36,'st':"01:30",'et':"02:00"},
                        {'idx': 37,'st':"02:00",'et':"02:30"},
                        {'idx': 38,'st':"02:30",'et':"03:00"},
                        {'idx': 39,'st':"03:00",'et':"03:30"},
                        {'idx': 40,'st':"03:30",'et':"04:00"},
                        {'idx': 41,'st':"04:00",'et':"04:30"},
                        {'idx': 42,'st':"04:30",'et':"05:00"},
                        {'idx': 43,'st':"05:00",'et':"05:30"},
                        {'idx': 44,'st':"05:30",'et':"06:00"},
                        {'idx': 45,'st':"06:00",'et':"06:30"},
                        {'idx': 46,'st':"06:30",'et':"07:00"},
                        {'idx': 47,'st':"07:00",'et':"07:30"},
                        {'idx': 48,'st':"07:30",'et':"08:00"}
                      ];
  }

  ngAfterViewInit(){
      this.document.getElementById('menuSetAvailability').className = 'active';
      //this.userId =16;
      this.isLoading = true;
      this.availObj.getTimeSlots( this.globalService.id_token )
        .finally(() => { this.isLoading = false;})
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type == 'success'))
          {
              for (var i = 0; i < response.response.length; i++) {
                  this.addintoTimeSlot(response.response[i]);
              }
              var stringMon , stringTue, stringWed , stringThu, stringFri, stringSat , stringSun ;
              for (var i=1;i<=48;i++)
              {
                stringMon = 'mon_'+i;
                stringTue = 'tue_'+i;
                stringWed = 'wed_'+i;
                stringThu = 'thu_'+i;
                stringFri = 'fri_'+i;
                stringSat = 'sat_'+i;
                stringSun = 'sun_'+i;

                if(
                  (response.response.includes(stringMon))
                                  &&
                  (response.response.includes(stringTue))
                                  &&
                  (response.response.includes(stringWed))
                                  &&
                  (response.response.includes(stringThu))
                                  &&
                  (response.response.includes(stringFri))
                                  &&
                  (response.response.includes(stringSat))
                                  &&
                  (response.response.includes(stringSun))
                ){
                  this.document.getElementById('time_slot_'+i).checked = true;

              }
            }
          }
      });
  }

  setTimeSlotToggle(idx){
    var timeSlot = this.document.getElementById('time_slot_'+idx) ;
    if(timeSlot.checked){
      this.addintoTimeSlot('mon_'+idx);
      this.addintoTimeSlot('tue_'+idx);
      this.addintoTimeSlot('wed_'+idx);
      this.addintoTimeSlot('thu_'+idx);
      this.addintoTimeSlot('fri_'+idx);
      this.addintoTimeSlot('sat_'+idx);
      this.addintoTimeSlot('sun_'+idx);
    }else{
      this.removeFromTimeSlot('mon_'+idx);
      this.removeFromTimeSlot('tue_'+idx);
      this.removeFromTimeSlot('wed_'+idx);
      this.removeFromTimeSlot('thu_'+idx);
      this.removeFromTimeSlot('fri_'+idx);
      this.removeFromTimeSlot('sat_'+idx);
      this.removeFromTimeSlot('sun_'+idx);
    }
  }

  addintoTimeSlot(timeslot){
    this.userTimeSlots.push(timeslot);
    this.document.getElementById(timeslot).className = 'active';
  }

  removeFromTimeSlot(timeslot){
    var index = this.userTimeSlots.indexOf(timeslot, 0);
    if (index > -1) {
      this.userTimeSlots.splice(index, 1);
    }
    this.document.getElementById(timeslot).className = 'center';
  }

  setCellToggle(day,idx){
      var myElement = this.document.getElementById(day+'_'+idx) ;
      if(myElement.className ==  'active'){
        this.removeFromTimeSlot(day+'_'+idx)
      }else{
        this.addintoTimeSlot(day+'_'+idx);
      }
      var monClassName = this.document.getElementById('mon_'+idx).className;
      var tueClassName = this.document.getElementById('tue_'+idx).className;
      var wedClassName = this.document.getElementById('wed_'+idx).className;
      var thuClassName = this.document.getElementById('thu_'+idx).className;
      var friClassName = this.document.getElementById('fri_'+idx).className;
      var satClassName = this.document.getElementById('sat_'+idx).className;
      var sunClassName = this.document.getElementById('sun_'+idx).className;
      if((monClassName == 'active') && (tueClassName == 'active') && (wedClassName == 'active') && (thuClassName == 'active') && (friClassName == 'active') && (satClassName == 'active') && (sunClassName == 'active')){
        this.document.getElementById('time_slot_'+idx).checked = true;
      }else{
        this.document.getElementById('time_slot_'+idx).checked = false;
      }
  }

  updateTimeSlots(){

    this.appointObj.getUpcomingCount(this.userTypeText , this.globalService.id_token)
      .finally(() => {
        this.isLoading = false;
      })
      .subscribe((response: any) => {
        if ((response.status == 1) && (response.type == 'success')) {

          if(response.response[0].count){

           /*swal({
             title: 'Are you sure?',
             text: 'If there is already scheduled appointments, you have to attends as it was booked. Are you sure to update your time slots?',
             type: 'warning',
             showCancelButton: true,
             confirmButtonColor: '#DD6B55',
             confirmButtonText: 'Yes',
             cancelButtonText: 'No',
             closeOnConfirm: true,
             closeOnCancel: true
           }).then(() => {*/
             this.availObj.setTimeSlots(this.userId, this.globalService.id_token , this.userTimeSlots)
               .finally(() => { })
               .subscribe((response: any) => {
                 // set availablity is updated but not getting status==1 need to cross check it. 
                // if((response.status == 1) && (response.type == 'success'))
                if((response.status == 0))
                 {
                   swal(
                     'Updated!',
                     'Your availability for sessions on Intercell has been successfully updated',
                     'success'
                   )
                 }
               });
           //});

         } else {
           this.availObj.setTimeSlots(this.userId, this.globalService.id_token , this.userTimeSlots)
             .finally(() => { })
             .subscribe((response: any) => {
               if((response.status == 1) && (response.type == 'success'))
               {
                 swal(
                   'Updated!',
                   'Your availability for sessions on Intercell has been successfully updated',
                   'success'
                 )
               }
             });

         }


        } else if ((response.status == 1) && (response.type == 'noRecord')) {

        }
      });
  }

  resetTimeSlots(){



      this.appointObj.getUpcomingCount(this.userTypeText , this.globalService.id_token)
        .finally(() => {
          this.isLoading = false;
        })
        .subscribe((response: any) => {
          if ((response.status == 1) && (response.type == 'success')) {

            if(response.response[0].count){

             /* swal({
                title: 'Are you sure?',
                text: 'If there is already scheduled appointments, you have to attends as it was booked. Are you sure to reset your time slots?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                closeOnConfirm: true,
                closeOnCancel: true
              }).then(() => {*/

                let dayArray = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat','sun'];

                for (let day of dayArray) {
                  for (let idx = 1; idx <= 48; idx++) {
                    let myElement = this.document.getElementById(day + '_' + idx);
                    this.removeFromTimeSlot(day + '_' + idx);
                    this.document.getElementById('time_slot_' + idx).checked = false;
                  }
                }


                this.availObj.setTimeSlots(this.userId, this.globalService.id_token , this.userTimeSlots)
                  .finally(() => { })
                  .subscribe((response: any) => {
                    if((response.status == 1) && (response.type == 'success'))
                    {
                      swal(
                        'Updated!',
                        'Your availability for sessions on Intercell has been successfully updated',
                        'success'
                      )
                    }
                  });
              //});

            } else {

              let dayArray = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat','sun'];

              for (let day of dayArray) {
                for (let idx = 1; idx <= 48; idx++) {
                  let myElement = this.document.getElementById(day + '_' + idx);
                  this.removeFromTimeSlot(day + '_' + idx);
                  this.document.getElementById('time_slot_' + idx).checked = false;
                }
              }

              this.availObj.setTimeSlots(this.userId, this.globalService.id_token , this.userTimeSlots)
                .finally(() => { })
                .subscribe((response: any) => {
                  if((response.status == 1) && (response.type == 'success'))
                  {
                    swal(
                      'Updated!',
                      'Your availability for sessions on Intercell has been successfully updated',
                      'success'
                    )
                  }
                });

            }


          } else if ((response.status == 1) && (response.type == 'noRecord')) {

          }
        });


  }

}
