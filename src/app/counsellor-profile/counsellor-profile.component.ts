import { Component, OnInit, Inject } from '@angular/core';
import { LandingService } from '../services/landing.service';
import { GlobalService } from '../services/global.service';
import { Router} from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';
import { LoginService } from '../services/login.service';
import { JwtHelper } from '../services/jwt.service';
import { CounsellorService } from '../services/counsellor.service';

@Component({
  selector: 'app-counsellor-profile',
  templateUrl: './counsellor-profile.component.html',
  styleUrls: ['./counsellor-profile.component.scss']
})
export class CounsellorProfileComponent implements OnInit {
  userDetails : any = {};
  counsellorDetails: any = {};
  counsellorEducationDetails: any = [];
  counsellorExperienceDetails: any = [];
  counsellorCertificateDetails: any = [];
  user:object={};
  consellorId:number;
  data:any;
  popFullName: string;
  popUserPic: string;
  userId: any = +localStorage.getItem('profileCounsId');
  isLogin: any = +localStorage.getItem('alreadyLogin');

  constructor(@Inject(DOCUMENT) private document,private landingService : LandingService , private globalService : GlobalService, private router: Router, private loginservice : LoginService, private jwtHelper : JwtHelper, private counsellorservice : CounsellorService) { }

  ngOnInit() {
    console.log("It is");
    console.log(this.isLogin);

    //fetching user Data
    this.counsellorservice.getUserDetails(+localStorage.getItem('profileCounsId') , localStorage.getItem('id_token'))
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success')) {
          this.userDetails = response.response;
        }
    });



    //fetching counsellor Data
    this.counsellorservice.getCounsellorDetails(+localStorage.getItem('profileCounsId'), this.globalService.id_token )
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          this.counsellorDetails = response.response[0];
        } else {

        }
      });


    //fetching Counsellor Education Data
    this.counsellorservice.getCounsellorEducation(+localStorage.getItem('profileCounsId') , this.globalService.id_token)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status ==1) && (response.type === 'success'))
        {
          this.counsellorEducationDetails = response.response;
        } else {
          this.counsellorEducationDetails = [];
        }
      });


    //fetching Counsellor Education Data0
    this.counsellorservice.getCounsellorCertificate(+localStorage.getItem('profileCounsId') , this.globalService.id_token)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status ==1) && (response.type === 'success'))
        {
          this.counsellorCertificateDetails = response.response;
        } else {
          this.counsellorCertificateDetails = [];
        }
      });


    //fetching user Data
    this.counsellorservice.getCounsellorExperience(+localStorage.getItem('profileCounsId') , this.globalService.id_token)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status ==1) && (response.type === 'success'))
        {
          this.counsellorExperienceDetails = response.response;
        }
        else {
          this.counsellorExperienceDetails = [];
        }
      });
  }

  setCounsellor = function(user){
    this.consellorId = +localStorage.getItem('profileCounsId');
    this.popFullName = user.first_name +" "+user.last_name;
    this.popUserPic = "assets/images/av-1.jpg";
    //this.document.getElementById('username').value;
  }

  doLogin = function(){
    //this.isLoading = true;
    this.user={'emailid':this.document.getElementById('username').value,'password':this.document.getElementById('password').value,'usertype':'student'};

    this.loginservice.doSign(this.user)
      .finally(() => {
      }).subscribe((response: any) => {
        if ((response.status == 1) && (response.type === 'success')) {
          this.data = this.jwtHelper.decodeToken(response.response.token);
          localStorage.setItem('id_token', response.response.token);
          this.globalService.userFullName = this.data.firstName + ' ' + this.data.lastName;
          this.globalService.userPic = this.data.profilePic;
          this.globalService.newUser = 0;
          this.globalService.userId = this.data.id;
          this.globalService.userType = this.data.group_id;
          if(this.globalService.userType == 3)
          {
            this.globalService.userTypeText = 'Mentor';
            this.router.navigate(['/overview']);
            //this.router.navigate(['/set-availability']);
          }
          else if(this.globalService.userType == 2)
          {
            this.globalService.userTypeText = 'Mentee';
          //  alert('/reshedule-appointment/'+consellorId);
            this.router.navigate(['/direct-appointment/'+this.consellorId]);

            //this.router.navigate(['/student']);
          } else{
            console.log("User type is" + this.globalService.userType);
          }

        }else{
          //this.passwordError = true;
        }
      });
  }
}
