import { Component, OnInit } from '@angular/core';
import { Title,Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss'],
})

export class FaqComponent implements OnInit {
  
  constructor( private title: Title, private meta: Meta) {

     // Start to Set Component  Title, Description and Keywords
this.title.setTitle('Intercell FAQ - Intercellworld.com');
this.meta.updateTag({ name: 'description', content: 'Intercell FAQ' });
this.meta.updateTag({ name: 'keyword', content: 'Intercell' });
// End to Set Component Title, Description and Keywords
  }

  ngOnInit() {
    
    $("html, body").animate({ scrollTop: 0 }, 'slow');
    $('#accordion').on('hide.bs.collapse', this.toggleChevron);
    $('#accordion').on('show.bs.collapse', this.toggleChevron);
  }

  toggleChevron = function(e) {
    $(e.target).prev('.panel-heading').find("i").toggleClass('glyphicon-plus glyphicon-minus');
    $('.panel-body.animated').toggleClass('zoomIn zoomOut');
   
  }
}
