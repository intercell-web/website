import { Component, OnInit } from '@angular/core';
import * as AppConst from '../app.const';
import {FormGroup, FormControl, FormBuilder, Validators, Form} from '@angular/forms';
import { LoginService } from '../services/login.service';
import { CounsellorService } from '../services/counsellor.service';
import { GlobalService } from '../services/global.service';
import { Router } from '@angular/router';
import { QuoteService } from '../services/quote.service';
declare var jQuery: any;
import { environment } from '../../environments/environment';
import { JwtHelper } from '../services/jwt.service';
import { BasicService} from '../services/basic.service';
declare var swal: any;

@Component({
  selector: 'app-signuphome',
  templateUrl: './signuphome.component.html',
  styleUrls: ['./signuphome.component.scss']
})
export class SignuphomeComponent implements OnInit {

  myConst: any;
  form: FormGroup;
  formEducation: FormGroup;
  emailError: boolean = false;
  phoneError: boolean = false;
  services: any;
  subservices: any;
  confirmPasswordError: boolean = false;
  barLabel: string = 'Strength';
  termUrl: any = environment.termUrl;
  formCounsellingStudent: FormGroup;
  formCounsellingCounsellor: FormGroup;
  formExperience: FormGroup;
  timer: any = null;
  selectedSubServices: any = [];
  subServiceList = [];
  subRequireCounselling = [];
  selectedSubRequire: any =[];
  emailInvalid: boolean = false;
  emailVerified: boolean = false;
  phoneVerified: boolean = false;
  phoneInvalid: boolean = false;
  phoneCode:any;
  mailCode:any;
  condtionsAgree: boolean =false;
  condtionsTerms: boolean =false;
  formArray: any = {};
  formExperienceArray : any = {};
  formEducationArray : any = {};
  formCounsellingStudentArray : any = {};
  formCounsellingCounsellorArray : any = {};
  subLoop : any;
  ssubLoop : any;
  catLoop: any = [];
  scatLoop: any = [];
  counsellorSubfield : any;
  studentfield : any;
  studentSubfield : any;
  resMail: boolean = false;
  resPhone: boolean = false;
  noneNeedSubField: boolean =false;
  emailTimer:any = null;
  sizeLimit = 2000000;
  imageUrl : any;
  mypassword : any ='';
  showDOB: boolean = false;
  showDiv: boolean = false
  hasBaseDropZoneOver: boolean = false;
  showCounsellingSub : boolean = false;
  showStudReq: boolean = false;
  currentScreen: any ="#step-1";
  eduYearValidation: boolean = true;
  expYearValidation: boolean = false;
  previousPhone: any;
  previousMail: any;
  cdnPath: any = environment.cdnProfileUrl;
  emailMessage: any = false;
  AllRequiredError:any = false;
  termsmsg:number = 0;
  bydefaultimg:string = 'intercell-user-pic.png';
  emailPattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$";//[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$
  profileimageerror:string  ='';
  selectedServices:any =[];
  serviceList:any = [];
  countryList: any = [];
  stateList: any = [];
  cityList: any = [];
  years: number[] =[];
  expyears:number[] = [];
  yy : number;
  
  yearDate = new Date();
  yearDateold = new Date(this.yearDate.getFullYear()-18, this.yearDate.getMonth(), this.yearDate.getDate(), 0, 0, 0);
 
 public ipaddress:any;
 public disabledusertype:boolean = false;
 countrycodeflags: any = false;
 countrycodes : number;

 
 dropdownServices= {
  singleSelection: false,
  text:"Select Field",
  selectAllText:'Select All',
  unSelectAllText:'UnSelect All',
  enableSearchFilter: true,
  classes:"myclass custom-class"
};

 dropdownSettings= {
    singleSelection: false,
    text:"Select Specialisation",
    selectAllText:'Select All',
    unSelectAllText:'UnSelect All',
    enableSearchFilter: true,
    classes:"myclass custom-class"
  };



  formErrors = {
    firstname : '',
    lastname : '',
    emailid : '',
    password : '',
    cpassword : '',
    mobile : '',
    gender : '',
    usertype:'',
    school:'',
    degree:'',
    field:'',
    timeperiod:'',
    stream:'',
    phonecodeerror:'',
    cstream:'',
    highestedu:'',
    totalexp:'',
    substream:'',
    shortbio:'',
    dob:'',
    country:'',
    state:'',
    city:'',
    brandworkedfor:'',
    condtionsTerms:''
  };


  formExperienceErrors = {
  };

  formCounsellingErrors = {
    linkedInLink : ''
  };

  validationMessages = {
    emailid : {
      required : 'Email address is mandatory',
      minlength : 'Please enter a valid email address',
      maxlength : 'Max length cant be more then 30 characters'
    },
    password : {
      required : 'Password is mandatory',
      minlength : 'Your password must be minimum 6 characters',
      maxlength : 'Your password must not exceed 20 characters',
      pattern : 'Your password must have a minimum of 1 digit and 1 special character'
    },
    firstname : {
      required : 'You must enter your First Name'
    },
    lastname : {
      required : 'You must enter your Last Name'
    },
    cpassword : {
      required : 'Confirm Password is mandatory'
    },
    mobile : {
      required : 'Contact number is mandatory',
      minlength : 'Your contact number should be of Minimum 8 digits',
      maxlength : 'Your contact number should be of Maximum 15 digits'
    },
    gender : {
      required : 'You must select one option from the menu'
    },
    month : {
      required : 'You must enter your month of birth',
    },
    year : {
      required : 'You must enter your year of birth',
    },
    day : {
      required : 'Your must enter your date of birth',
    },
    usertype: {
      required : 'Please select one option from the menu',
    },
    linkedInLink : {
    pattern: 'Invalid web page entered'
    },
    substream : {
      required : 'Please select one option from the menu',
    }
  }

  constructor(private globalService: GlobalService ,private basicservice: BasicService, private counsellorservice: CounsellorService, private router: Router, private quoteService: QuoteService, private loginservice: LoginService , private fb: FormBuilder , private jwtHelper: JwtHelper) {
  }


  ngOnInit() {

    $("html, body").animate({ scrollTop: 0 }, 'slow');
    /*jQuery('#step-7').fadeOut(0);
     jQuery('#step-6').fadeOut(0);
    jQuery('#step-5').fadeOut(0);
    jQuery('#step-4').fadeOut(0);
    jQuery('#step-3').fadeOut(0);
    jQuery('#step-2').fadeOut(0);
    jQuery('#step-8').fadeOut(0);*/    
    this.myConst = AppConst;
    this.buildForm();
    jQuery('#datePickers').datepicker({format: 'dd/mm/yyyy',endDate: this.yearDateold,autoclose: true});
    jQuery('#dateValue').change(() => {
      this.formEducation.patchValue({dob: jQuery('#dateValue').datepicker({format: 'dd/mm/yyyy',autoclose: true,endDate: this.yearDateold}).val()});
     
    });
    jQuery('.checkmarkPhone').hide();
    jQuery('.checkmarkMail').hide();
    jQuery('form :input').attr("autocomplete", "off");
    this.getYear();
    this.CountriesList();
    this.getexpYear();
    //this.getIpAddress();   
  }


  countryCodechange(event){
    if(event!=''){
      this.countrycodes = event;
      this.countrycodeflags = true;
      this.formErrors.mobile = '';
    }else{
      this.countrycodeflags = false;
      this.formErrors.mobile = 'Please Select Country Code';
    }
     
  }
  

  buildForm() {
    this.form = this.fb.group({
      firstname : ['', Validators.compose([Validators.required])],
      lastname : ['', Validators.compose([Validators.required])],
      emailid : ['', Validators.compose([Validators.required ,Validators.minLength(6), Validators.maxLength(50) ,Validators.pattern(this.emailPattern)])],
      password : ['', Validators.compose([Validators.required,  Validators.minLength(6) , Validators.maxLength(20) , Validators.pattern('^(?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*?[.~#?!@$%^_&*-]).{3,}$')])],
      cpassword : ['', Validators.compose([Validators.required])],
      usertype : ['' , Validators.compose([Validators.required])],
      mobile : ['' , Validators.compose([Validators.required])],
      gender : ['', Validators.compose([Validators.required])],
      termscond :['', Validators.compose([])]
     });


    this.formEducation = new FormGroup({
      school : new FormControl('' , Validators.compose([Validators.required])),
      degree : new FormControl('' , Validators.compose([Validators.required])),
      startTime : new FormControl('' , Validators.compose([])),
      endTime : new FormControl('' , Validators.compose([])),
      field : new FormControl('' , Validators.compose([Validators.required])),
      description : new FormControl('' , Validators.compose([])),
      educationId : new FormControl(''),
      countryid : new FormControl('', Validators.compose([Validators.required])),
      stateid : new FormControl('', Validators.compose([Validators.required])),
      cityid : new FormControl('', Validators.compose([])),
      dob   : new FormControl('', Validators.compose([Validators.required])),
    });

    this.formCounsellingStudent = new FormGroup({
      requireField : new FormControl('' , Validators.compose([Validators.required])),
      requireSubField : new FormControl('' , Validators.compose([]))
    });

    this.formCounsellingCounsellor = new FormGroup({
      brandworkedfor : new FormControl('' , Validators.compose([Validators.required])),
      totalexperience : new FormControl('' , Validators.compose([Validators.required])),
      currentField : new FormControl('' , Validators.compose([Validators.required])),
      currentSubField : new FormControl({value: '' , disabled:true} , Validators.compose([Validators.required])),
      linkedInLink : new FormControl('' , Validators.compose([Validators.pattern('^http(s)?:\/\/([w]{3}\.)?linkedin\.com\/in\/([a-zA-Z0-9-]{5,30})\/?')])),
      aboutusdesc : new FormControl('', Validators.compose([Validators.required,  Validators.minLength(40) , Validators.maxLength(150) ]))
    });

    this.formExperience = new FormGroup({
      title : new FormControl('' , Validators.compose([Validators.required])),
      company : new FormControl('' , Validators.compose([Validators.required])),
      location : new FormControl('' , Validators.compose([Validators.required])),
      description : new FormControl(''),
      startTimeYear : new FormControl('' , Validators.compose([Validators.required])),
      endTimeYear : new FormControl({value: '' , disabled: false} , Validators.compose([Validators.required])),
      startTimeMonth : new FormControl('' , Validators.compose([Validators.required])),
      endTimeMonth : new FormControl({value: '' , disabled: false} , Validators.compose([Validators.required])),
      //current : new FormControl(''),
      experienceId : new FormControl('')
    });

    this.form.valueChanges.subscribe(data => this.validateForm());
    this.formExperience.valueChanges.subscribe(data => this.validateExperienceForm());
    this.formCounsellingCounsellor.valueChanges.subscribe(data => this.validateCounsellingForm());
  }


  
  validateCounsellingForm(){
    for(let field in this.formCounsellingErrors){
      this.formCounsellingErrors[field]= '';

      let input = this.formCounsellingCounsellor.get(field);

      if(input.invalid && input.dirty){
        for(let error in input.errors){
          this.formCounsellingErrors[field] = this.validationMessages[field][error];
        }
      }
    }
  }



  showEmailMessage () {
    this.emailMessage = true;
    this.formErrors.emailid = 'This email address would be your login ID';
  }

  

  removeErrorMessage(val){   
    switch(val) {
      case 'firstname':
          if(this.form.controls.firstname.value==''){
            this.formErrors.firstname ="You must enter your First Name";
          }else{
            this.formErrors.firstname ='';
          }
          break;
      case 'lastname':
          if(this.form.controls.lastname.value==''){
            this.formErrors.lastname ="You must enter your Last Name";
          }else{
            this.formErrors.lastname ='';
          }
          break; 
      case 'emailid':
          this.onEmailOut();
          break;               
      case 'cpassword':
          if(this.form.controls.cpassword.value==''){
            this.formErrors.cpassword ="Confirm Password is mandatory";
          }else if(this.form.controls.password.value!=this.form.controls.cpassword.value){
            this.formErrors.cpassword ="Password Doesnt matches";
          }else{
            this.formErrors.cpassword ='';
          }
          break;    
      case 'school':
          this.formErrors.school ='';
          break;
      case 'field':
          this.formErrors.field ='';
          break;
      case 'degree':
          this.formErrors.degree ='';
          break;
      /*case 'highesteducation':
          this.formErrors.highestedu ='';
          break; */
      case 'totalexperience':
          this.formErrors.totalexp ='';
          break; 
      case 'usertype':
          this.formErrors.usertype ='';
          break;   
      case 'city':
          this.formErrors.city ='';
          break;   
      case 'dob':
          this.formErrors.dob ='';
          break;                    
      }   
  }

  validateForm(){
     for(let field in this.formErrors) {

       if(field == 'mobile1' || field == 'emailid1'|| field == 'school' || field == 'degree' || field == 'field' || field == 'timeperiod' || field == 'stream' || field == 'phonecodeerror' || field == 'cstream' || field == 'highestedu' || field == 'totalexp' || field == 'substream'|| field =='shortbio' || field =='dob' || field =='city' || field =='state' || field =='country' || field =='brandworkedfor' || field =='condtionsTerms'){

       } else {
         this.formErrors[field] = '';
         this.confirmPasswordError = false;
         //this.phoneError = false;
         //this.userNameError =false;
         //this.emailError = false;     
         let input = this.form.get(field);      
         if (input.invalid && input.dirty) {
           for (let error in input.errors) {
             this.formErrors[field] = this.validationMessages[field][error];
           
           }
         }
       }

    }
  }



  
  onlyNumberKey(event) {
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  }

  

 ValidateEmail(inputText)
  {
  var x = inputText;
  var atpos = x.indexOf("@");
  var dotpos = x.lastIndexOf(".");
  if(x==''){
    this.formErrors.emailid ="Email address is mandatory";
    this.emailError=true;

  }else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
    this.formErrors.emailid ="Invalid email address";
    this.emailError=true;
    
  } else{
    
     //check unique EmailId
     this.loginservice.checkLoginEmail(inputText,'')
     .finally(() => { })
     .subscribe((response: any) => {

       if((response.status == 1) )
       {
         this.emailError=false;
         this.formErrors.emailid ='';
       } else {
         this.emailError=true;
         this.formErrors.emailid ="This email address has been registered before";
       }
     });


  } 
  }


  ValidatePhone(inputText)
  {
   if(inputText==''){
    this.phoneError=true;
    this.formErrors.mobile ="Mobile Number is mandatory";
   }else if(inputText.length<8){
      this.phoneError=true;
      this.formErrors.mobile ="Your contact number should be of Minimum 8 digits";
      return false;
     }else{
      this.loginservice.checkPhone(inputText,'')
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) )
        {
          this.phoneError=false;
          this.formErrors.mobile ="";
        } else {
          this.phoneError=true;
          this.formErrors.mobile ="This contact number has been registered before ";
        }
      });
     }
  
  }


  validateExperienceForm(){
    for(let field in this.formExperienceErrors){
      this.formExperienceErrors[field]= '';

      let input = this.formExperience.get(field);

      if(input.invalid && input.dirty){
        for(let error in input.errors){
          this.formExperienceErrors[field] = this.validationMessages[field][error];
        }
      }
    }
  }


  fileOverBase(e: any) : void {
    this.hasBaseDropZoneOver = e;
  }


  onInput= function() {

    if(jQuery('#education_start_time').val() && jQuery('#education_end_time').val()){

    if(jQuery('#education_start_time').val() > jQuery('#education_end_time').val()){
      this.eduYearValidation = false;
      this.formErrors.timeperiod = 'Year submitted by you is invalid';
    } else {
      this.eduYearValidation = true;
      this.formErrors.timeperiod = '';
    }
    } else {
      this.eduYearValidation = false;
      this.formErrors.timeperiod = 'Please Select Time period';
    }
  }


  onExpInput = function() {
    $("html, body").animate({ scrollTop: 0 }, 'slow');

    if(jQuery("#expStartYear").val() && jQuery("#expEndYear").val() && jQuery("#expStartMonth").val() && jQuery("#expEndMonth").val()){
      if(jQuery("#expStartYear").val() > jQuery("#expEndYear").val()){
        this.expYearValidation=!this.expYearValidation;
      } else if(jQuery('#expStartYear').val() == jQuery('#expEndYear').val()) {

        if($('#expStartMonth option:selected').attr('id') >  $('#expEndMonth option:selected').attr('id')) {
          this.expYearValidation = true;
        } else {
          this.expYearValidation = false;
        }
      }else {
        this.expYearValidation = false;
      }
    } else {
      this.expYearValidation = false;
    }
  }


  onEducationPrevious = function() {
      this.ScreenShowHide("#step-1","#step-2");
  }


  onCounsPrevious = function() {
      this.ScreenShowHide("#step-2","#step-3");
  }

  onCancel = function(off){
     this.ScreenShowHide("#step-8",this.currentScreen);
  }

  onCancelled = function(){
    
    this.resetFrom();
    this.ScreenShowHide("#step-1","#step-8");
   
  }


  resetFrom = function(){
    this.form.reset({
      usertype: '',
      firstname: '',
      lastname: '',
      emailid: '',
      mobile: '',
      gender: '',
      password: '',
      cpassword: '',
      termscond:''
    });
    this.formEducation.reset({
      school: '',
      degree: '',
      field: '',
      startTime: '',
      endTime: '',
      description: '',
      dob:'',
      countryid:'',
      stateid:'',
      cityid:''
    });
    
    this.formCounsellingCounsellor.reset({
      //highesteducation: '',
      totalexperience: '',
      currentField: '',
      currentSubField: '',
      linkedInLink: '',
      aboutusdesc: ''
    });

    this.formCounsellingStudent.reset({
      requireField: '',
      requireSubField: ''
     
    });

    this.condtionsTerms = false;
    this.phoneCode = "";
    this.formErrors ={};

  }

  onReturn = function(off){
     this.ScreenShowHide(this.currentScreen,"#step-8");
  }

  onStudCounsPrevious = function() {
    this.ScreenShowHide("#step-2","#step-4");
  }

  onExpPrevious = function() {
    this.ScreenShowHide("#step-3","#step-5");
  }

  
  onPicPrevious = function() {
   if(this.globalService.userType == 3) {
      this.ScreenShowHide("#step-3","#step-6");
    } else {
      this.ScreenShowHide("#step-4","#step-6");
    }
  }

  onValidPrevious = function() {
    if(this.globalService.userType == 3) {
      this.ScreenShowHide("#step-6","#step-7");
     } else {
      this.ScreenShowHide("#step-6","#step-7");
    }
  }


  

  showBirth = function(dob){
    
    if(dob){
      this.form.controls.dob.enable();
    } else{
      this.form.controls.dob.disable();
    }
  }

showOffic= function(showOfficial){

if(showOfficial){
  this.formExperience.controls.endTimeMonth.disable();
  this.formExperience.controls.endTimeYear.disable();
  }else{
    this.formExperience.controls.endTimeMonth.enable();
    this.formExperience.controls.endTimeYear.enable();

}

}

  verifyMail = function (){
    //call services for sending Code
    this.loginservice.confirmMailCode(this.form.controls.emailid.value,this.mailCode)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1))
        {
          this.emailInvalid = false ;
          this.emailVerified = true;
          jQuery('.checkmarkMail').show();

        } else {
          this.mailCode = '';
          this.emailInvalid = true ;

          setTimeout(() => {
            this.emailInvalid = false;
          }, 3000);
        }
      });

  };

  resendMail = function (){

 if(this.mailCode){
    //call services for sending Code
    this.loginservice.resendMailCode(this.form.controls.emailid.value)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1))
        {
          this.emailVerified=false;
          this.emailInvalid=false;
          this.resMail=true;
          this.mailCode = "";

          setTimeout(()=>{
            this.resMail=false;
          }, 3000);

        } else {
          this.emailError=true;
        }
      });

      }
  };

  verifyPhone = function (){
    //call services for sending Code
    if(this.phoneCode== "" || this.phoneCode == undefined){
      this.formErrors.phonecodeerror = 'Please Enter OTP Code ';
    }else{
      this.loginservice.confirmPhoneCode(this.form.controls.mobile.value, this.phoneCode)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1))
        {
          this.phoneVerified = true;
          jQuery('.checkmarkPhone').show();
          this.phoneInvalid = false ;
          this.formErrors.phonecodeerror ='';
        } else {
          this.phoneInvalid = true ;
          this.phoneCode ="";
          this.formErrors.phonecodeerror ='Invalid OTP. Please check and enter valid OTP';
          setTimeout(()=>{
            this.phoneInvalid = false;
          }, 3000);

        }
      });
    }
         
  };



  resendPhone = function (){
    //call services for sending Code
    this.loginservice.sendMailPhoneCode(this.form.controls.mobile.value,this.form.controls.emailid.value,this.countrycodes)
      .finally(() => { })
      .subscribe((response: any) => {        
        if((response.status == 1))
        {
          this.phoneVerified=false;
          this.phoneInvalid=false;
          this.resPhone=true;
          this.phoneCode ="";
          this.formErrors.phonecodeerror ='OTP Resent on your Mobile';
          setTimeout(()=>{
            this.resPhone=false;
          }, 3000);

        } else {
          this.formErrors.phonecodeerror ='';
          this.emailError=false;
        }
      });
  };


  onServiceChange = function (event){
         
      this.subServiceList = [];
      if (this.formCounsellingCounsellor.controls.currentField.value.length == 0) {
        this.showCounsellingSub = false;
        this.formCounsellingCounsellor.controls.currentSubField.disable();
      } else {
        for(var p=0;p<this.formCounsellingCounsellor.controls.currentField.value.length;p++){
          if(this.subservices[this.formCounsellingCounsellor.controls.currentField.value[p].id] != undefined) {
            this.subServiceList = this.subServiceList.concat(this.subservices[this.formCounsellingCounsellor.controls.currentField.value[p].id]);
          }
          }
          this.showCounsellingSub= true;
          this.formCounsellingCounsellor.controls.currentSubField.enable();
      }

      this.selectedSubServices = [];
      this.formErrors.cstream ='';
   
  };


  onServiceChangementee = function (event){
    
    this.subRequireCounselling  = [];
 if (this.formCounsellingStudent.controls.requireField.value.length == 0) {
   this.showCounsellingSub = false;
   this.formCounsellingStudent.controls.requireSubField.disable();
 } else {
   for(var p=0;p<this.formCounsellingStudent.controls.requireField.value.length;p++){
     if(this.subservices[this.formCounsellingStudent.controls.requireField.value[p].id] != undefined) {
       this.subRequireCounselling = this.subRequireCounselling.concat(this.subservices[this.formCounsellingStudent.controls.requireField.value[p].id]);
     }
     }
     this.showCounsellingSub= true;
     this.formCounsellingStudent.controls.requireSubField.enable();
 }

 this.selectedSubServices = [];
 this.formErrors.cstream ='';

};

  

  onServiceChange1 = function (event){
    this.subServiceList = this.subservices[event];    
    if (this.subServiceList == undefined){
      this.showCounsellingSub = false;
      this.formCounsellingCounsellor.controls.currentSubField.disable();
    }else{
      this.showCounsellingSub= true;
      this.formCounsellingCounsellor.controls.currentSubField.enable();
    }

    this.selectedSubServices = [];
    this.formErrors.cstream ='';
  };


  onRequireServiceChange = function (event){
    if(event=="none"){
      this.noneNeedSubField = true;
    }else {
      this.noneNeedSubField = false;
      this.subRequireCounselling = this.subservices[event];

      if (this.subRequireCounselling == undefined){
        this.showStudReq = false;
        this.formCounsellingStudent.controls.requireSubField.disable();
      }else{
        this.showStudReq= true;
        this.formCounsellingStudent.controls.requireSubField.enable();
      }

      this.selectedSubRequire = [];
    }
    this.formErrors.stream = '';
  };

  onSubServiceChange = function (event){
    this.formErrors.substream = '';
  };

  onCounsellingStudent = function(counselling) {

    if(!this.formCounsellingStudent.valid){
      if(this.formCounsellingStudent.controls.requireField.value==''){
        this.formErrors.stream = 'Please select one option from the menu';
      }else{
        this.formErrors.stream = '';
      }
      return false;
    }
    this.formErrors.stream = '';
    this.ScreenShowHide("#step-6","#step-4");
  }


  ScreenShowHide(current,old){
        jQuery(old).hide();
        jQuery(current).removeClass('hide');
        jQuery(current).fadeIn(700);
        if(current =='#step-8'){
          this.currentScreen = old;
        }else{
          this.currentScreen = current;
        }
        
        $("html, body").animate({ scrollTop: 0 }, 'slow');
  }

  


  
  onFinish = function() {

    
    if(this.phoneCode== "" || this.phoneCode == undefined){
      this.formErrors.phonecodeerror = 'Please Enter OTP Code ';
    }else{
      this.loginservice.confirmPhoneCode(this.form.controls.mobile.value, this.phoneCode)
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1))
        {
         
          //################
          this.formErrors.phonecodeerror = '';     
                
              if(this.form.controls.usertype.value == 3){

                this.catLoop = this.formCounsellingCounsellor.controls['currentField'].value;
                
                    for (var i = 0; i < this.catLoop.length; i++) {    
                      if(i==0){
                        this.counsellorfield = this.catLoop [i].id ;
                      } else {
                        this.counsellorfield += ',' + this.catLoop [i].id ;
                      }
                    }
            
                this.subLoop = this.formCounsellingCounsellor.controls['currentSubField'].value;
                    for (var i = 0; i < this.subLoop.length; i++) {    
                      if(i==0){
                        this.counsellorSubfield = this.subLoop [i].id ;
                      } else {
                        this.counsellorSubfield += ',' + this.subLoop [i].id ;
                      }
                    }
                    this.formCounsellingCounsellorArray['subfield'] =  this.counsellorSubfield;
                    this.formCounsellingCounsellorArray['field'] =  this.counsellorfield;
                
              }else{

                this.scatLoop = this.formCounsellingStudent.controls['requireField'].value;
                
                    for (var i = 0; i < this.scatLoop.length; i++) {    
                      if(i==0){
                        this.studentfield = this.scatLoop [i].id ;
                      } else {
                        this.studentfield += ',' + this.scatLoop [i].id ;
                      }
                    }

                this.ssubLoop = this.formCounsellingStudent.controls['requireSubField'].value;
                for (var i = 0; i < this.ssubLoop.length; i++) {    
                  if(i==0){
                    this.studentSubfield = this.ssubLoop [i].id ;
                  } else {
                    this.studentSubfield += ',' + this.ssubLoop [i].id ;
                  }
                } 

                this.formCounsellingStudentArray['requireFieldstr'] =  this.studentfield;
                this.formCounsellingStudentArray['requireSubFieldstr'] =  this.studentSubfield;
              }
             
                
                    
                  for (var key in this.form.controls) {
                    this.formArray[key] = this.form.controls[key].value;
                    
                  }        
              
                  for (var key in this.formExperience.controls) {
                    this.formExperienceArray[key] = this.formExperience.controls[key].value;
                  }
              
                  for (var key in this.formEducation.controls) {
                    this.formEducationArray[key] = this.formEducation.controls[key].value;
                  }
              
                  for (var key in this.formCounsellingCounsellor.controls) {
                    this.formCounsellingCounsellorArray[key] = this.formCounsellingCounsellor.controls[key].value;
                  }
              
                  
              
                  for (var key in this.formCounsellingStudent.controls) {
                    this.formCounsellingStudentArray[key] = this.formCounsellingStudent.controls[key].value;
                  }
              
                  this.formArray['countrycode'] = this.countrycodes;
                  
                  //call services for sending Code
                  this.loginservice.createUser(this.formArray, this.formExperienceArray, this.formEducationArray, this.formCounsellingCounsellorArray, this.formCounsellingStudentArray,jQuery("#awsfileName").val())
                    .finally(() => { })
                    .subscribe((response: any) => {
                      if((response.status == 1))
                      {
                        
                        this.data = this.jwtHelper.decodeToken(response.response.response);             
                        this.globalService.userType = (this.form.controls.usertype.value == 4)? 2:this.form.controls.usertype.value;
                   
                        if(this.globalService.userType == 3)
                        {
                          this.resetFrom();
                          this.ScreenShowHide("#step-1","#step-7");
                          swal("Your profile has been submitted for verification. Please check your inbox to verify email.");
                         //You have registered successfully and your profile has been submitted for screening
                          this.router.navigate(['/home']);
                          //this.globalService.userTypeText = 'Mentor';
                          //localStorage.setItem('userTypeText','Mentor');
                         // this.router.navigate(['/counsellor']);
                        } else if(this.globalService.userType == 4 || this.globalService.userType == 2)
                        {
                          this.globalService.id_token = response.response.response;      
                          this.globalService.userId = this.data.id;
                          this.globalService.userFullName = this.data.firstName + ' ' + this.data.lastName;  
                          this.globalService.newUser = 0;
                          this.globalService.userTypeText = 'Mentee';
                          if(jQuery("#awsfileName").val()){
                            this.globalService.userPic = this.cdnPath + jQuery("#awsfileName").val();
                          } else {
                            this.globalService.userPic ="assets/images/anonymous.jpg";
                          }
                          localStorage.setItem('newUser', '0');
                          localStorage.setItem('userType', this.globalService.userType); 
                          localStorage.setItem('id_token', response.response.response);
                          localStorage.setItem('userFullName', this.globalService.userFullName);  
                          localStorage.setItem('userPic', this.globalService.userPic);
                          localStorage.setItem('userId', this.data.id);
                          localStorage.setItem('userTypeText','Mentee');
                          this.resetFrom();
                          this.router.navigate(['/dashboard']);
                        }
              
                      } else {
                        this.formErrors.phonecodeerror = 'Email ID/Mobile already exists ';
                      }
                    });
          //#################

        } else {
          this.formErrors.phonecodeerror ='Invalid OTP. Please check and enter valid OTP';
          return false;
        }
      });
    }

    
      }
    
      onCounsellingCounsellor = function(counselling) {

        if(!this.formCounsellingCounsellor.valid){
         this.formErrors.cstream = (this.formCounsellingCounsellor.controls.currentField.value=='')?'Please select one option from the menu':'';
         this.formErrors.brandworkedfor = (this.formCounsellingCounsellor.controls.brandworkedfor.value=='')?'Please enter brand portfolio':'';
         this.formErrors.totalexp = (this.formCounsellingCounsellor.controls.totalexperience.value=='')?'Please enter total experience':'';
         this.formErrors.substream = (this.formCounsellingCounsellor.controls.currentSubField.value=='')?'Please select one option from the menu':'';
         //this.formErrors.shortbio = (this.formCounsellingCounsellor.controls.aboutusdesc.value=='')?'Please enter short bio':'';
         this.formErrors.shortbio = this.lengthRange(this.formCounsellingCounsellor.controls.aboutusdesc.value,40,150);

         return false;
        }
        this.formErrors.cstream = '';this.formErrors.highestedu = '';this.formErrors.totalexp = '';this.formErrors.substream='';this.formErrors.shortbio='';
        this.formErrors.brandworkedfor ='';
        this.ScreenShowHide("#step-6","#step-3");
      }


      lengthRange =  function(inputtxt, minlength, maxlength)
      {  	
         var userInput = inputtxt;
         
         if(userInput.length <= 0){
             return "Please enter short bio";
         }else if(userInput.length <= minlength){
          return "Please enter  minimum 40 characters";
          
         }else if(userInput.length >= maxlength){
          return "Short Bio must not exceed 150 characters";
          
         }else{  	
         return "";  	
         }
           
      }

    

     onPicture = function() {
      if(jQuery("#awsfileName").val() == ''){
        this.profileimageerror = 'Please Upload Your Profile Image';
        return false;
      }
      
      this.profileimageerror ='';
      
        if(this.previousPhone!=this.form.controls.mobile.value && this.previousMail!=this.form.controls.emailid.value ) {
    
          //call services for sending Code
          this.loginservice.sendMailPhoneCode(this.form.controls.mobile.value,this.form.controls.emailid.value,this.countrycodes)
            .finally(() => {
            })
            .subscribe((response: any) => {
    
              if ((response.status == 1) && (response.type === 'noRecord')) {
                jQuery('.checkmarkPhone').hide();
                this.phoneVerified=false;
                this.phoneInvalid=false;
                this.resPhone=true;
                this.phoneCode ="";

                this.previousPhone = this.form.controls.mobile.value;
              } else if ((response.status == 1)) {
                this.emailError = false;
              } else {
                this.emailError = false;
              }
    
            });
        }
    
      this.ScreenShowHide("#step-7","#step-6");
      }
    
      selectAllContent($event) {
        
        //$event.target.select();
      }

      selectuserType($event) {
        
        this.termsmsg  = $event.target.value;
        $event.target.select();
      }

    
      onExperience = function() {
        this.ScreenShowHide("#step-6","#step-5");
        }
    
  
  
  onDone = function(user) {

    this.ValidateEmail(this.form.controls.emailid.value);
    this.ValidatePhone(this.form.controls.mobile.value);       

    if(!this.form.valid || this.phoneError || this.emailError || !this.condtionsTerms || !this.countrycodeflags){
          this.AllRequiredError = true;
          if(this.form.controls.usertype.value=='' || this.form.controls.usertype.value==undefined){
            this.formErrors.usertype ="Please select User Type";            
          }
          if(this.form.controls.firstname.value==''){
            this.formErrors.firstname ="You must enter your First Name";            
          }
          if(this.form.controls.lastname.value==''){
            this.formErrors.lastname ="You must enter your Last Name";            
          }
          if(this.countrycodeflags == false){
            this.formErrors.mobile = 'Please Select Country Code';
          }
          if(this.form.controls.gender.value ==''){
            this.formErrors.gender = "You must select one option from the menu";
          }
          if(this.form.controls.password.value ==''){
            this.formErrors.password = "Password is mandatory";
          }
          if(this.form.controls.cpassword.value ==''){
            this.formErrors.cpassword = "Confirm Password is mandatory";
          }
          if (this.form.controls.password.value != this.form.controls.cpassword.value) {
            this.formErrors.cpassword = 'Incorrect password entered';
          }
          if(this.condtionsTerms == false){
            this.formErrors.condtionsTerms = 'Please Select Terms and Condtions';
          }else{
            this.formErrors.condtionsTerms = '';
          }
          return false;
        }
        this.formErrors.password = '';
        this.formErrors.cpassword = '';
        this.formErrors.usertype='';
        this.formErrors.condtionsTerms = '';
        this.formErrors.mobile = '';
        this.isLoading = true;
        this.AllRequiredError = false;
        
        if (this.form.controls.password.value === this.form.controls.cpassword.value) {
    
          if(this.form.controls.usertype.value == 4){
            user.profile_status = 1;
            this.userType = 2;
          }else{
            this.userType = user.usertype;
          }

          if(this.form.controls.usertype.value == 3){
           this.showDiv = false;          
          }else{
            this.showDiv = true;
          }

          this.globalService.newUser = 0;
          this.globalService.userType = this.userType;
          this.bydefaultimg = this.defaultimage(this.userType,this.form.controls.gender.value);
          this.AllServices();
          this.AllServiceslist();     
          this.AllSubServices();
          this.ScreenShowHide("#step-2","#step-1");
          
        } else {
          this.confirmPasswordError = true;
          this.formErrors.cpassword = 'Incorrect password entered';
        }
      }
    
    

      onEducationSubmit = function(education) {
                      
            if(!this.formEducation.valid || !this.eduYearValidation){
        
              this.formErrors.dob =(this.formEducation.controls.dob.value=='')?'Please enter your date of birth':'';
              //this.formErrors.city =(this.formEducation.controls.cityid.value=='')?'Please select your city':'';
              this.formErrors.state =(this.formEducation.controls.stateid.value=='')?'Please select your state':'';
              this.formErrors.country =(this.formEducation.controls.countryid.value=='')?'Please select your country':'';
              this.formErrors.school =(this.formEducation.controls.school.value=='')?'School/College/University':'';
              this.formErrors.degree =(this.formEducation.controls.degree.value=='')?'Please select one option from the menu':'';
              this.formErrors.field =(this.formEducation.controls.field.value=='')?'Please enter field of study':'';
              
             if(this.form.controls.usertype.value != 3){

              if(this.formEducation.controls.startTime.value==''){
                this.formErrors.timeperiod = 'Please select time period';
                this.eduYearValidation = false;
              }else if(this.formEducation.controls.endTime.value==''){
                this.formErrors.timeperiod = 'Please select time period';
                this.eduYearValidation = false;
              }else if(this.formEducation.controls.startTime.value > this.formEducation.controls.endTime.value){
                this.eduYearValidation = false;
                this.formErrors.timeperiod = 'Year submitted by you is invalid';
              }else{
                this.formErrors.timeperiod = '';
                this.eduYearValidation = true;
              }

             }else{
               this.formErrors.timeperiod = '';
                this.eduYearValidation = true;
             }
              
              return false;
            }
            
            this.formErrors.school = ''; this.formErrors.degree = ''; this.formErrors.field = '';
            this.formErrors.timeperiod = '';this.formErrors.dob = '';this.formErrors.city = '';
            this.formErrors.state = '';this.formErrors.country = '';
           
            if(this.globalService.userType == 3)
            {
              this.ScreenShowHide("#step-3","#step-2");
            
            } else if(this.globalService.userType == 2)
            {
              this.ScreenShowHide("#step-4","#step-2");
            
            }
         }





      onPassword = function(){
        jQuery('#strength').show();
      }
    
      onPasswordOut = function(){
        jQuery('#strength').hide();
      }
    

      onGenderOut = function(){
        if(this.form.controls.gender.value ==''){
          this.formErrors.gender = "You must select one option from the menu";
        }else{
          this.formErrors.gender = "";
        }
                
      }
    
      

      onEmailOut = function(){
        
        if(this.form.controls.emailid.value==''){
          this.formErrors.emailid ="Email address is mandatory";
        }else if(this.form.controls.emailid.valid){
         
            //check unique EmailId
            this.loginservice.checkLoginEmail(this.form.controls.emailid.value)
              .finally(() => { })
              .subscribe((response: any) => {
    
                if((response.status == 1) )
                {
                  this.emailError=false;
                  this.formErrors.emailid ='';
                } else {
                  this.emailError=true;
                  this.formErrors.emailid ="This email address has been registered before";
                }
              });
    
        
        } else {
         
            this.formErrors.emailid ="Invalid email address";
            this.emailError=true;
        
        }
      }
    
    
      fileChange(event) {
        let fileList: FileList = event.target.files;
    
        if (fileList.length > 0) {
          let file: File = fileList[0];
          document.getElementById('image_user').setAttribute('src', window.URL.createObjectURL(file));
        }
      }
    

      onPhoneOut = function(){
        clearTimeout(this.phoneTimer);
    
        if(this.form.controls.mobile.valid){
          clearTimeout(this.timer);
          this.formErrors.mobile ="";
          this.timer = setTimeout(()=>{
            //check unique EmailId
            this.loginservice.checkPhone(this.form.controls.mobile.value)
              .finally(() => { })
              .subscribe((response: any) => {
    
                if((response.status == 1) )
                {
                  this.phoneError=false;
                } else {
                  this.phoneError=true;
                  this.formErrors.mobile ="This contact number has been registered before";
                }
              });
          }, 700);
        } else {
          this.phoneTimer = setTimeout(() => {
            this.formErrors.mobile ="Your contact number should be of Minimum 8 digits";
          }, 1500);
        }
      }
    
      onTerm = function () {
        localStorage.setItem('termType',  this.form.controls.usertype.value);
        window.open(this.termUrl, '_blank');
        //this.router.navigate(['/term']);
      }      
      
      
 CountriesList = function(){
  
           //fetching All Countries List
         this.basicservice.getCountriesList()
         .finally(() => { })
         .subscribe((response: any) => {
  
           if((response.status == 1) && (response.type === 'success'))
           {
             this.countryList = response.response;
           } else {
           }
         });
  
        }
  
        onCountryChange = function (event){
          //fetching All State List
          this.basicservice.getStatesList(event)
            .finally(() => { })
            .subscribe((response: any) => {
      
              if((response.status == 1) && (response.type === 'success'))
              {
                this.stateList = response.response;
                this.cityList = [];
              } else {
              }
            });
            this.formErrors.country = '';
        };
      
      
        onStateChange = function (event){
          //fetching All State List
          this.basicservice.getCitiesList(event)
            .finally(() => { })
            .subscribe((response: any) => {
      
              if((response.status == 1) && (response.type === 'success'))
              {
                this.cityList = response.response;
              } else {
              }
            });
            this.formErrors.state = '';
        };
   
   AllSubServices  = function(){

          //fetching All Sub Services
   this.counsellorservice.getAllSubServices()
   .finally(() => { })
   .subscribe((response: any) => {

     if((response.status == 1))
     {
       this.subservices = response.response;
     } else {
     }
   });

        }


      

        AllServiceslist = function(){
            //fetching All Services
          this.counsellorservice.getAllServiceslist()
          .finally(() => { })
          .subscribe((response: any) => {
            if((response.status == 1))
            {
              this.serviceList = response.response;
            }
          });
        } 
 
      AllServices = function(){
        this.counsellorservice.getAllServices()
        .finally(() => { })
        .subscribe((response: any) => {
          if((response.status == 1))
          {
            this.services = response.response;
          } 
        });
      }

      getYear(){
        let today = new Date();
        this.yy = today.getFullYear();        
        for(var i = (this.yy-48); i <= this.yy; i++){
        this.years.push(i);}
    }


    getexpYear(){
      for(var i = 5; i <= 50; i++){
      this.expyears.push(i);}
  }


  defaultimage(userType_d,gender_d){

    let image = 'intercell-user-pic.png';
    if(userType_d == 3 && gender_d == 'Male'){
      image = 'intercell-user-pic.png';
    }else if(userType_d == 3 && gender_d == 'Female'){
      image = 'mentor-female.jpg';
    }else if(userType_d != 3 && gender_d == 'Male'){
      image = 'mentee-profile.jpg';
    }else if(userType_d != 3 && gender_d == 'Female'){  
      image = 'mentee-female.jpg';
    }else{
      image = 'intercell-user-pic.png';
    }
    
    return image;
  }


  getIpAddress = function (){
    //fetching All State List
    this.basicservice.getIpAddress()
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status == 1) && (response.type === 'success'))
        {
          this.ipaddress = response.response;
          if(this.ipaddress == '180.151.238.93' || this.ipaddress =='122.180.144.51'){
            this.disabledusertype = true;
          }
         
          
        } else {
        }
      });
     
  };


}
