import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatedmentorComponent } from './relatedmentor.component';

describe('RelatedmentorComponent', () => {
  let component: RelatedmentorComponent;
  let fixture: ComponentFixture<RelatedmentorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelatedmentorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatedmentorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
