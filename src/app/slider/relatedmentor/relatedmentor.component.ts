import { Component, OnInit,Inject, Input } from '@angular/core';
import { LandingService } from '../../services/landing.service';
import { LoginService } from '../../services/login.service';
import { JwtHelper } from '../../services/jwt.service';
import { GlobalService } from '../../services/global.service';
import { environment } from '../../../environments/environment';
import { Router} from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';


@Component({
  selector: 'app-relatedmentor',
  templateUrl: './relatedmentor.component.html',
  styleUrls: ['./relatedmentor.component.scss']
})
export class RelatedmentorComponent implements OnInit {

  @Input() 
  sId : any = 0; 
  counsellorDetails: any = [];
  counsellorDetails1: any = [];
  user: object={};
  cdnPath: any = environment.cdnProfileUrl;
  popFullName: string;
  popUserPic: string;
  profileUrl: any = environment.counsellorProfileUrl;
  userId: any = +localStorage.getItem('userId');
  userType: any = +localStorage.getItem('userType');
  dataanimator:string = 'lazy';
  gendertype:any;

  constructor(@Inject(DOCUMENT) private document, private router: Router,private landingService: LandingService, private loginservice: LoginService, private jwtHelper: JwtHelper,private globalService: GlobalService) { }

  ngOnInit() {

        //fetching Services Data
      this.landingService.getAllCounsellor(this.sId)
      .finally(() => { })
      .subscribe((response: any) => {
        if((response.status ==1) && (response.type === 'success'))
        {
          this.counsellorDetails1 = response.response;
          if(this.counsellorDetails1.length>4){
          for (var i = 0; i < 10; i++) {
            this.counsellorDetails = this.counsellorDetails.concat(this.counsellorDetails1);
           }
          }else{
            this.counsellorDetails = this.counsellorDetails1;
            this.dataanimator = '';
          }
          //this.router.navigate(['/home']);
        } else {
          console.log("There is Some Error");
        }
      });

 

    ResCarouselOnInit();
    
          $(window).resize(function () {
            var r = new Date();
            setTimeout(function () {
              ResCarouselResize();
            }, 200);
          });
    
          function ResCarouselOnInit() {
            ResCarouselSize();
            $(document).on('click', '.leftRs, .rightRs', function () {
              ResCarousel(this);
            });
            $(document).on("mouseenter", ".ResHover", function () {
              $(this).addClass("ResHovered");
            });
    
            $(document).on("mouseleave", ".ResHover", function () {
              $(this).removeClass("ResHovered");
            });
          }



             // Rescarousel Auto Slide
    function ResCarouselSlide(e) {
      var thiss = $(e).find(".rightRs");
      var dataInterval : any= $(e).attr('data-interval');

      !isNaN(dataInterval) && $(e).addClass("ResHover") && setInterval(function () {
        !(thiss.parent().hasClass("ResHovered")) && ResCarousel(thiss);
      }, +(dataInterval));
    }


    function ResCarouselResize() {
      function myfunction() {

        $('.resCarousel').each(function () {
          var divValue : any = $(this).attr('data-value');
          var itemWidth : any = $(this).find('.item').width();
          $(this).find(".resCarousel-inner").scrollLeft(divValue * itemWidth);
        });
      }

      myfunction();
    }



    
    //this function define the size of the items
    function ResCarouselSize() {
      var t0 = performance.now();

      setTimeout(function () {

        $('.resCarousel').each(function (index) {
          var itemsSplit : any = $(this).attr("data-items").split('-');
          $(this).addClass("ResSlid" + index);

          for (var i = 0; i < 4; i++) {
            //console.log(i);

            if (i == 0) {
              var styleCollector0 : any = ".ResSlid" + index + " .item {width: " + 100 / itemsSplit[i] + "%}";
            } else if (i == 1) {
              var styleCollector1 : any = ".ResSlid" + index + " .item {width: " + 100 / itemsSplit[i] + "%}";
            } else if (i == 2) {
              var styleCollector2 : any = ".ResSlid" + index + " .item {width: " + 100 / itemsSplit[i] + "%}";
            } else if (i == 3) {
              var styleCollector3 : any = ".ResSlid" + index + " .item {width: " + 100 / itemsSplit[i] + "%}";
            }
          }

          $(this).attr("data-value", "0");

          var styleCollector = "@media (max-width:767px){" + styleCollector0 + "}" +
            "@media (min-width:768px){" + styleCollector1 + "}" +
            "@media (min-width:992px){" + styleCollector2 + "}" +
            "@media (min-width:1200px){" + styleCollector3 + "}";
          $(this).find("style").remove();
          $(this).append("<style>" + styleCollector + "</style>");
          ResCarouselSlide(this);

        });

      }, 500);

      var t1 = performance.now();
      //console.log('Took', (t1 - t0).toFixed(4), 'milliseconds to Size');
    }


      
    //this function used to move the items
    function ResCarousel(Btn) {
      var parent = $(Btn).parent(),
        slide = +parent.attr("data-slide"),
        itemsDiv = parent.find('.resCarousel-inner'),
        itemSpeed = +parent.attr("data-speed"),
        itemLoad = +parent.attr("data-load"),
        translateXval : any ,
        currentSlide : any ,
        itemLenght = itemsDiv.find(".item").length,
        itemWidth = itemsDiv.find('.item').outerWidth(),
        dataItm = +Math.round(itemsDiv.outerWidth() / itemWidth),
        cond = $(Btn).hasClass("leftRs"),
        divValue = Math.round(itemsDiv.scrollLeft() / itemWidth);
      itemSpeed = !isNaN(itemSpeed) ? itemSpeed : 400;
      slide = slide < dataItm ? slide : dataItm;

      if (cond) {
        currentSlide = divValue - slide;
        translateXval = currentSlide * itemWidth;
        var MoveSlide = currentSlide + slide;
        //console.log(itemloop);
        if (divValue == 0) {
          currentSlide = itemLenght - slide;
          translateXval = currentSlide * itemWidth;
          currentSlide = itemLenght - dataItm;
          itemSpeed = 400;
          //console.log(currentSlide + "," + translateXval);
        } else if (slide >= MoveSlide) {
          currentSlide = translateXval = 0;
        }
      } else {
        currentSlide = divValue + slide;
        translateXval = currentSlide * itemWidth;
        var MoveSlide = currentSlide + slide;

        if (divValue + dataItm == itemLenght) {
          currentSlide = translateXval = 0;
          itemSpeed = 400;
        } else if (itemLenght <= (MoveSlide - slide + dataItm)) {
          currentSlide = itemLenght - slide;
          translateXval = currentSlide * itemWidth;
          currentSlide = itemLenght - dataItm;
        }
        // resCarouselAnimator(itemsDiv, currentSlide + 1, currentSlide + slide);
      }
      //console.log(slide + "," + itemWidth);
      parent.attr("data-animator") == "lazy" && resCarouselAnimator(itemsDiv, cond ? 0 : 1, currentSlide + 1, currentSlide + dataItm, itemSpeed, (slide * itemWidth));
      if (!isNaN(itemLoad)) {
        itemLoad = itemLoad >= slide ? itemLoad : slide;
        (itemLenght - itemLoad) <= (currentSlide + dataItm) && ResCarouselLoad1(itemsDiv);
      }
      itemsDiv.animate({scrollLeft: translateXval}, itemSpeed);
      parent.attr("data-value", currentSlide);
    }


    function ResCarouselLoad1(e) {
      //console.log(e.attr("id"));
      $("#" + e.attr("id")).trigger("ResCarouselLoad");
    }

    function resCarouselAnimator(parent, direction, start, end, speed, length) {
      //console.log(direction + "," + start + "," + end);
      var val = 5;
      if (direction == 0) {
        for (var i = start - 1; i < end + 1; i++) {
          val = val * 2;
        }
        val = -val;
      }

      for (var i = start - 1; i < end; i++) {
        val = direction == 0 ? val / 2 : val * 2;
        //console.log(parent.find(".item").eq(i));
        parent.find(".item").eq(i).css("transform", "translateX(" + val + "px)");
      }
      setTimeout(function () {
        //console.log(speed);
        parent.find('.item').attr('style', '');
      }, speed - 70);
    }


}


doLogin = function(){
  //this.isLoading = true;
  this.user={'emailid': this.document.getElementById('username').value,'password':this.document.getElementById('password').value,'usertype':'student'};

  this.loginservice.doSign(this.user)
    .finally(() => { //this.isLoading = false;
    })
    .subscribe((response: any) => {
      if ((response.status == 1) && (response.type === 'success')) {
        this.data = this.jwtHelper.decodeToken(response.response.token);
        localStorage.setItem('id_token', response.response.token);
        this.globalService.id_token = response.response.token;

        if(this.data.profilePic){
          this.globalService.userPic = this.cdnPath + this.data.profilePic;
        } else {
          this.globalService.userPic = 'https://d3213zi13us7f1.cloudfront.net/website/assets/images/intercell-user-pic.png';
        }

        this.globalService.userFullName = this.data.firstName + ' ' + this.data.lastName;
        this.globalService.newUser = 0;
        this.globalService.userId = this.data.id;
        this.globalService.userType = this.data.group_id;
        if(this.globalService.userType == 3)
        {
          this.globalService.userTypeText = 'Mentor';
          this.router.navigate(['/overview']);
        }
        else if(this.globalService.userType == 2)
        {
          this.globalService.userTypeText = 'Mentee';
          this.router.navigate(['/direct-appointment/' + this.consellorId]);
        } else{
          console.log('User type is' + this.globalService.userType);
        }

        localStorage.setItem('userFullName', this.globalService.userFullName);
        localStorage.setItem('userPic', this.globalService.userPic);
        localStorage.setItem('userId', this.globalService.userId);
        localStorage.setItem('userType', this.globalService.userType);
        localStorage.setItem('newUser', this.globalService.newUser);

      }else{

        this.incorrectDetails = true;
        setTimeout(() => {
          this.incorrectDetails = false;
        }, 8000);
      }
    });
}


setCounsellor = function(user, indCounsellor , imageUrl){
  indCounsellor++;
  this.consellorId = user.user_id;
  this.popFullName = user.first_name +" "+user.last_name;

  localStorage.setItem('directCounsellorName', user.first_name +' '+user.last_name);

  if(imageUrl){
    this.popUserPic = this.cdnPath+imageUrl;
  }else{
    this.popUserPic = "https://d3213zi13us7f1.cloudfront.net/website/assets/images/intercell-user-pic.png";
  }
  //this.document.getElementById('username').value;
}



directAppointment = function(user){
  this.consellorId = user.user_id;
  this.gendertype = (user.gender=='')?'Mr.':'Ms.';
  localStorage.setItem('directCounsellorName', this.gendertype +' '+user.first_name +' '+user.last_name);

  if(this.userType == 3)
  {
    this.globalService.userTypeText = 'Mentor';
    this.router.navigate(['/overview']);
    //this.router.navigate(['/set-availability']);
  }
  else if(this.userType == 2)
  {
    this.globalService.userTypeText = 'Mentee';
    this.router.navigate(['/direct-appointment/'+this.consellorId]);
  } else{
    console.log("User type is" + this.globalService.userType);
  }

}

}
