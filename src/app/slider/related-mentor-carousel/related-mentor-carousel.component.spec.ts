import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatedMentorCarouselComponent } from './related-mentor-carousel.component';

describe('RelatedMentorCarouselComponent', () => {
  let component: RelatedMentorCarouselComponent;
  let fixture: ComponentFixture<RelatedMentorCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelatedMentorCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatedMentorCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
