import { Component, OnInit, Renderer2, Inject,Input } from "@angular/core";
import { DOCUMENT } from "@angular/platform-browser";
import { LandingService } from '../../services/landing.service';
import { GlobalService } from '../../services/global.service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { LoginService } from '../../services/login.service';
import { JwtHelper } from '../../services/jwt.service';
declare var swal: any;

@Component({
  selector: 'app-related-mentor-carousel',
  templateUrl: './related-mentor-carousel.component.html',
  styleUrls: ['./related-mentor-carousel.component.scss']
})
export class RelatedMentorCarouselComponent implements OnInit {


  @Input() 
  sId : any = 0; 
  counsellorDetails: any = [];
  counsellorDetails1: any = [];
  user: object={};
  cdnPath: any = environment.cdnProfileUrl;
  popFullName: string;
  popUserPic: string;
  profileUrl: any = environment.counsellorProfileUrl;
  userId: any = +localStorage.getItem('userId');
  userType: any = +localStorage.getItem('userType');
  dataanimator:string = 'lazy';
  gendertype:any;
  MentorcountryId :any;

  constructor(
      private _renderer2: Renderer2,
      @Inject(DOCUMENT) private _document,
      private landingService: LandingService,
      private globalService: GlobalService,
      private router: Router,
      private loginservice: LoginService,
      private jwtHelper: JwtHelper
  ) {
    if(!(this.globalService.userCountryId)){
      this.globalService.userCountryId = localStorage.getItem('userCountryId');
    }
  }

  ngOnInit(): void {
           //console.log(this._renderer2);
        this.getAllCounsellor();
        }


     
        
getAllCounsellor = function(){
                //fetching Services Data
              this.landingService.getAllCounsellor(this.sId)
              .finally(() => { })
              .subscribe((response: any) => {
                if((response.status ==1) && (response.type === 'success'))
                {
                  this.counsellorDetails1 = response.response;
                  this.counsellorDetails = this.counsellorDetails1;
                  this.dataanimator = '';
                  this.domElementChange();
                 
                  //this.router.navigate(['/home']);
                } else {
                  //console.log("There is Some Error");
                }
              });
}   


domElementChange = function(){

  let script = this._renderer2.createElement('script');
  script.type = `text/javascript`;
  script.text = `
      {
          $(document).ready(function () {
            setTimeout(function(){
              $('#related-slider').show();
              $('#content2-slider').lightSlider({
                loop: true,
                item:4,
                speed:500,
                auto:true,
                slideMargin: 20,
                controls: true,
                prevHtml: '&#8249;',
                nextHtml: '&#8250;',
                pager: false,
                pauseOnHover: true,
                responsive : [
                  {
                      breakpoint:920,
                      settings: {
                          item:3,
                          slideMove:1,
                        }
                  },
                  {
                    breakpoint:767,
                    settings: {
                        item:2 ,
                        slideMove:1
                      }
                },
                  {
                      breakpoint:480,
                      settings: {
                          item:1 ,
                          slideMove:1
                        }
                  }
              ]
                
              });
              
            },500);
                            
          });
      }
  `;
  
  this._renderer2.appendChild(this._document.body, script);
 

}



        doLogin = function(){
          //this.isLoading = true;
          this.user={'emailid': this._document.getElementById('username_pop').value,'password':this._document.getElementById('password_pop').value,'usertype':'student'};
        
          this.loginservice.doSign(this.user)
            .finally(() => { //this.isLoading = false;
            })
            .subscribe((response: any) => {
              
              if ((response.status == 1) && (response.type === 'success')) {
                this.data = this.jwtHelper.decodeToken(response.response[0].token);
                localStorage.setItem('id_token', response.response[0].token);
                this.globalService.id_token = response.response[0].token;
        
                if(this.data.profilePic){
                  this.globalService.userPic = this.cdnPath + this.data.profilePic;
                } else {
                  this.globalService.userPic = 'https://d3213zi13us7f1.cloudfront.net/website/assets/images/intercell-user-pic.png';
                }
        
                this.globalService.userFullName = this.data.firstName + ' ' + this.data.lastName;
                this.globalService.newUser = 0;
                this.globalService.userId = this.data.id;
                this.globalService.userType = this.data.group_id;
                if(this.globalService.userType == 3)
                {
                  this.globalService.userTypeText = 'Mentor';
                  this.router.navigate(['/overview']);
                }
                else if(this.globalService.userType == 2)
                {
                  
                  this.globalService.userCountryId = this.data.country_id;
                  localStorage.setItem('userCountryId', this.globalService.userCountryId);
                  var metype = this.globalService.userCountryId;
                  var mrtype = this.MentorcountryId;
                  var msg = this.showMessageOfMentee(metype,mrtype);
                  if(msg>0){
                     this.globalService.userTypeText = 'Mentee';
                     this.router.navigate(['/direct-appointment/' + this.consellorId]);
                  }else{
                    this.router.navigate(['/overview']);
                  }

                } else{
                  console.log('User type is' + this.globalService.userType);
                }
        
                localStorage.setItem('userFullName', this.globalService.userFullName);
                localStorage.setItem('userPic', this.globalService.userPic);
                localStorage.setItem('userId', this.globalService.userId);
                localStorage.setItem('userType', this.globalService.userType);
                localStorage.setItem('newUser', this.globalService.newUser);
        
              }else{
        
                this.incorrectDetails = true;
                setTimeout(() => {
                  this.incorrectDetails = false;
                }, 8000);
              }
            });
        }
        
        
        setCounsellor = function(user, indCounsellor , imageUrl){
          indCounsellor++;
          this.consellorId = user.user_id;
          this.summary = user.summary;
          this.popFullName = user.first_name +" "+user.last_name;
          this.MentorcountryId = user.country_id;
          localStorage.setItem('directCounsellorName', user.first_name +' '+user.last_name);
        
          if(imageUrl){
            this.popUserPic = this.cdnPath+imageUrl;
          }else{
            this.popUserPic = "https://d3213zi13us7f1.cloudfront.net/website/assets/images/intercell-user-pic.png";
          }
          //this.document.getElementById('username').value;
        }
        
        
        
        directAppointment = function(user){
          this.consellorId = user.user_id;
          this.gendertype = (user.gender=='')?'Mr.':'Ms.';
          localStorage.setItem('directCounsellorName', this.gendertype +' '+user.first_name +' '+user.last_name);
        
          if(this.userType == 3)
          {
            this.globalService.userTypeText = 'Mentor';
            this.router.navigate(['/overview']);
            //this.router.navigate(['/set-availability']);
          }
          else if(this.userType == 2)
          {
            var metype = this.globalService.userCountryId;
            var mrtype = user.country_id;
            var msg = this.showMessageOfMentee(metype,mrtype);
            if(msg>0){
            this.globalService.userTypeText = 'Mentee';
            this.router.navigate(['/direct-appointment/'+this.consellorId]);
            }
          } else{
            console.log("User type is" + this.globalService.userType);
          }
        
        }

        
showMessageOfMentee = function(metype,mrtype){
  
       if(metype!=101 && mrtype!=101){
        swal('Please be advised that users outside of India can book a session shortly. You will hear from us soon.');
        return 0;
       }else if(metype==101 && mrtype!=101){
        swal('Please be advised that sessions with International Mentors will be available soon. You will hear from us shortly.');
        return 0;
      }else if(metype!=101 && mrtype==101){
        swal('Please be advised that users outside of India can book a session shortly. You will hear from us soon.');
        return 0; 
       }else{
        return 1;
       }
      }

}
