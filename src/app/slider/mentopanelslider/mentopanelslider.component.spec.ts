import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentopanelsliderComponent } from './mentopanelslider.component';

describe('MentopanelsliderComponent', () => {
  let component: MentopanelsliderComponent;
  let fixture: ComponentFixture<MentopanelsliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentopanelsliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentopanelsliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
