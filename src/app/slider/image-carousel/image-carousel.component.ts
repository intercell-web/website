import { Component, OnInit, Renderer2, Inject } from "@angular/core";
import { DOCUMENT } from "@angular/platform-browser";
import { LandingService } from '../../services/landing.service';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-image-carousel',
  templateUrl: './image-carousel.component.html',
  styleUrls: ['./image-carousel.component.scss']
})
export class ImageCarouselComponent implements OnInit {
  
  meetCounsellorDetails: any = [];
  meetCounsellorDetails1: any = [];
  cdnPath: any = environment.cdnProfileUrl;
  popUserPic: string;
  popFullName: string;

constructor(
    private _renderer2: Renderer2,
    @Inject(DOCUMENT) private _document,
    private landingService: LandingService
) { }

ngOnInit(): void {

    this.getMeetCounsellordata();
    
    }


    getMeetCounsellordata = function(){

          //fetching Services Data
      this.landingService.getMeetCounsellor()
      .finally(() => { })
      .subscribe((response: any) => {
  
        if((response.status ==1))
        {
          this.meetCounsellorDetails1 = response.response;
          this.meetCounsellorDetails = this.meetCounsellorDetails1;
          this.domElementChange();
                   
        } else {
          console.log("There is Some Error");
        }
      });
    }


    domElementChange = function(){
      let script = this._renderer2.createElement('script');
      script.type = `text/javascript`;
      script.text = `
          {
              $(document).ready(function () {
                setTimeout(function(){
                  $('#mentor-slider').show();
                  $('#content-slider').lightSlider({
                    loop: true,
                    item:4,
                    speed:500,
                    auto:true,
                    slideMargin: 20,
                    controls: true,
                    prevHtml: '&#8249;',
                    nextHtml: '&#8250;',
                    pager: false,
                    pauseOnHover: true,
                    adaptiveHeight: true,
                    responsive : [
                      {
                          breakpoint:920,
                          settings: {
                              item:3,
                              slideMove:1,
                            }
                      },
                      {
                        breakpoint:767,
                        settings: {
                            item:2 ,
                            slideMove:1
                          }
                    },
                      {
                          breakpoint:480,
                          settings: {
                              item:1 ,
                              slideMove:1
                            }
                      }
                  ]
                    
                  });
                  
                },500);
                                
              });
          }
      `;
      
      this._renderer2.appendChild(this._document.body, script);
    }
    

    setCounsellor = function(user, indCounsellor , imageUrl){
      indCounsellor++;
      this.consellorId = user.id;
      this.summary = user.summary;
      this.popFullName = user.first_name +" "+user.last_name;
    
      localStorage.setItem('directCounsellorName', user.first_name +' '+user.last_name);
    
      if(imageUrl){
        this.popUserPic = this.cdnPath+imageUrl;
      }else{
        this.popUserPic = "https://d3213zi13us7f1.cloudfront.net/website/assets/images/intercell-user-pic.png";
      }
      
    }

}

