import { Component, OnInit, Renderer2, Inject } from "@angular/core";
import { DOCUMENT } from "@angular/platform-browser";
import { LandingService } from '../../services/landing.service';
import { GlobalService } from '../../services/global.service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-services-carousel',
  templateUrl: './services-carousel.component.html',
  styleUrls: ['./services-carousel.component.scss']
})
export class ServicesCarouselComponent implements OnInit {

  serviceDetails: any = [];
  serviceDetails1: any = [];
  cdnPath: any = environment.cdnProfileUrl;
  
  constructor(
      private _renderer2: Renderer2,
      @Inject(DOCUMENT) private _document,
      private landingService: LandingService,
      private globalService: GlobalService,
      private router: Router
  ) { }

  ngOnInit(): void {
    
        this.getMeetServicesdata();
        
       
        }


     getMeetServicesdata = function(){

            //fetching Services Data
      this.landingService.getServiceshome()
      .finally(() => { })
      .subscribe((response: any) => {

        if((response.status ==1) && (response.type === 'success'))
        {
          this.serviceDetails = response.response;
          this.domElementChange();
   
        } else {
          console.log("There is Some Error");
        }
      });

        }


        domElementChange = function(){

            let script = this._renderer2.createElement('script');
            script.type = `text/javascript`;
            script.text = `
                {
                    $(document).ready(function () {
                      setTimeout(function(){
                        $('#service-slider').show();
                        $('#content1-slider').lightSlider({
                          loop: true,
                          item:3,
                          speed:500,
                          auto:true,
                          slideMargin: 20,
                          controls: true,
                          prevHtml: '&#8249;',
                          nextHtml: '&#8250;',
                          pager: false,
                          pauseOnHover: true,
                          enableDrag:false,
                          responsive : [
                            {
                                breakpoint:920,
                                settings: {
                                    item:3,
                                    slideMove:1,
                                  }
                            },
                            {
                              breakpoint:767,
                              settings: {
                                  item:2 ,
                                  slideMove:1
                                }
                          },
                            {
                                breakpoint:480,
                                settings: {
                                    item:1 ,
                                    slideMove:1
                                  }
                            }
                        ]
                          
                        });
                        
                      },500);
                                      
                    });
                }
            `;
            
            this._renderer2.appendChild(this._document.body, script);
          }


        serviceDetail = function(url){
          this.router.navigate(['/career-counsellor',url]);
        }


}

