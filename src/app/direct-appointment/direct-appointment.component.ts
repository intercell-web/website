import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { AppointmentsService } from './../services/appointments.service';
import { environment } from '../../environments/environment';
declare var swal: any;

@Component({
  selector: 'app-direct-appointment',
  templateUrl: './direct-appointment.component.html',
  styleUrls: ['./direct-appointment.component.scss']
})
export class DirectAppointmentComponent implements OnInit {
    request: any ={};
    params: any={};
    calendaring: any;
    isLoading: boolean;

    userId: any;
    userType: any;
    userTypeText: string;
    prevousArraow: boolean = false;
    nextArraow: boolean = false;
    appointmentTable: boolean = false;
    expectationMessages: string;
    isExpectation = false;
    isCalendar = true;
    isCheckout = false;
    paymentUrl: any = environment.paymentUrl + '/core/payment/pay_session';
    item: any= [];
    discount: any= 0.00;
    promoCode: string = '';
    sessionId: number;
    counsellorId: number;
    counsellorName: any = localStorage.getItem('directCounsellorName');
    private sub: any;
   
      constructor(private globalService: GlobalService , private route: ActivatedRoute, private router: Router, private appointObj: AppointmentsService) {

      if(!(this.globalService.userId)){
        this.globalService.userId = +localStorage.getItem('userId');
      }
   
      this.userId = this.globalService.userId;
      this.userType = this.globalService.userType;
      this.userTypeText = this.globalService.userTypeText;
      this.request.studentId = this.userId;
      this.request.serviceId = 0;
      this.request.subServices = [];
      this.request.priceId = 3;
      this.request.paymentId = 3;
      this.request.duration = 30;
      this.request.genricSubServices = false;
      this.request.expectationMessages ='';
    }

    ngOnInit() {

      this.sub = this.route.params.subscribe(params => {
          this.counsellorId = +params['counsellorId'];
          this.request.counsellorId = this.counsellorId;
          this.showCounsellorCalendar('new');
          // (+) converts string 'id' to a number
      });

      this.appointObj.getPricing()
        .finally(() => { this.isLoading = false; })
        .subscribe((response: any) => {
          if((response.status == 1) && (response.type == 'success'))
          {
            this.item.unitPrice = response.response[0].amount;
            this.item.qty = 1;
            this.item.gst = response.response[0].gst;
          }
        });
    }

  getDiscount(){
    if(this.promoCode==''){
      swal('Please enter coupon/promo code');
      return false;
    }
    this.isLoading = true;
    this.appointObj.checkDiscount(this.promoCode, this.globalService.id_token)
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
        if((response.status == 1) && (response.type == 'success'))
        {
          this.discount = response.response.discount;
        }
      });
  }

   setCalendarTab(user:any) {
      if(user.id > 0){
          this.request.counsellorId = user.id;
          this.request.counsellorName = user.first_name+" "+user.last_name;
          this.showCounsellorCalendar('next');
      }
      return true;
    }

    showCounsellorCalendar(action:string){
      this.appointmentTable=false;
      this.appointObj.getCalendar(this.request.counsellorId, action, this.globalService.id_token)
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
        if((response.status == 1) && (response.type == 'success'))
        {
          this.calendaring = response.response;
          var today:any ;
          var endday:any ;
         
          today = this.globalService.getdatefun(0);
          endday = this.globalService.getdatefun(30);
          
          if(response.response[0].date == today){
            this.prevousArraow = false;
          }else{
            this.prevousArraow = true;
          }
          if(response.response[6].date >= endday){
            this.nextArraow = false;
          }else{
            this.nextArraow = true;
          }

          this.appointmentTable=true;
          //this.showTab('isServicesTab');
        }
      });
    }
    setCheckoutTab(){
      this.request.expectationMessages = this.expectationMessages;
      this.isExpectation = false;
      this.isCheckout = true;

    }


  


    checkOut() {
      this.isLoading = true;
      this.request.token =  this.globalService.id_token;

      this.appointObj.createAppointment(this.request)
      .finally(() => { this.isLoading = false; })
      .subscribe((response: any) => {
        if((response.status == 1) && (response.type == 'success'))
        {
          //defining Global Variable
          localStorage.setItem('userId', this.userId);
          localStorage.setItem('userType', this.userType);
          localStorage.setItem('newUser', '"' + this.globalService.newUser + '"');
          localStorage.setItem('userTypeText', this.userTypeText);
          localStorage.setItem('userFullName', this.globalService.userFullName);
          localStorage.setItem('userPic', this.globalService.userPic);

          //(<HTMLFormElement>document.getElementById('sessionId')).value =  response.response.sessionId;
          //(<HTMLFormElement>document.getElementById('paymentUserId')).value =  this.userId;

          //var myForm = <HTMLFormElement>document.getElementById('payment_form');
          //myForm.submit();
          swal('Your request has been sent to the Mentor');
          this.router.navigate(['/appointments']);
          
        }
      });
      return true;

    }
    setStartDateTime(dateTime:any){
      this.request.dateTime = dateTime;
    }




      setDateTime(date,time){
        this.request.date = date;
        this.request.time = time;
        this.isCalendar = false;
        this.isExpectation = true;
      }

      generateArray(obj){
        return Object.keys(obj).map((key)=>{ return obj[key]});
      }
      getClassName(idx){
        if(idx==0){
          return 'one';
        }else if(idx==1){
          return 'two';
        }else if(idx==2){
          return 'three';
        }else if(idx==3){
          return 'four';
        }else if(idx==4){
          return 'five';
        }else if(idx==5){
          return 'six';
        }else if(idx==6){
          return 'seven';
        }
        return 'one';
      }



    getPreviousWeek(){
      this.showCounsellorCalendar('prev');
    }

    getNextWeek(){
      this.showCounsellorCalendar('next');
    }
}
