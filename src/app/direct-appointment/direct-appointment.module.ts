import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TranslateModule } from '@ngx-translate/core';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
//import { DropdownMultiselectModule } from 'ng2-dropdown-multiselect';

import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';
import { DirectAppointmentComponent } from './direct-appointment.component';
import { AppointmentsService } from './../services/appointments.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    AngularMultiSelectModule
  ],
  declarations: [
    DirectAppointmentComponent
  ],
  providers: [
    AppointmentsService
  ]
})

export class DirectAppointmentModule { }
