import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {DirectAppointmentComponent } from './direct-appointment.component';

describe('DirectAppointmentComponent', () => {
  let component: DirectAppointmentComponent;
  let fixture: ComponentFixture<DirectAppointmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectAppointmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectAppointmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
